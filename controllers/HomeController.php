<?php

/**
 * Clase HomeController para carga el home del proyecto
 */
require 'models/Parqueadero.php';

class HomeController
{

    private $parqueadero;

    public function __construct()
    {
        $this->model = new Parqueadero;
    }

    public function index()
    {
        if (isset($_SESSION['user'])) {
            require 'views/layout.php';
            $parqueaderos = $this->model->getAll();
            $tarifas = $this->model->getAll2();
            $tipo_servicios = $this->model->getAll3();
            $Servicio_parqueadero = $this->model->getAll4();
            require 'views/home.php';
        } else {
            require 'views/InicioSesion/login.php';
        }
    }

    //muestra la vista de crear
    public function add()
    {
        require 'views/layout.php';
        $Parqueadero = $this->rol->getActiveRoles();
        require 'views/home.php';
    }

    // Realiza el proceso de guardar
    public function save()
    {
        $this->model->newParqueadero($_REQUEST);
        header('Location: ?controller=parqueadero');
    }

    //muestra la vista de editar
    public function edit()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_REQUEST['id_ServicioParqueadero'])) {
                $id_ServicioParqueadero = $_REQUEST['id_ServicioParqueadero'];
                $data = $this->model->getServicioById($id_ServicioParqueadero);
                $parqueaderos = $this->model->getAll();
                $tarifa = $this->model->getAll2();
                $Tipo_Servicio = $this->model->getAll3();
                require 'views/layout.php';
                require 'views/home.php';
            } else {
                echo "Error";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    // Realiza el proceso de actualizar
    public function update()
    {
        if (isset($_POST)) {
            $this->model->editUser($_POST);
            header('Location: ?controller=parqueadero');
        } else {
            echo "Error";
        }
    }

    // Realiza el proceso de borrar
    public function delete()
    {
        $this->model->deleteParqueadero($_REQUEST);
        header('Location: ?controller=parqueadero');
    }

    public function updateStatus()
    {
        $parqueadero = $this->model->getParqueaderoById($_REQUEST['id_Parqueadero']);
        $data = [];
        if ($parqueadero[0]->status_id == 1) {
            $data = [
                'id_Parqueadero' => $parqueadero[0]->id_Parqueadero,
                'status_id' => 2
            ];
        } elseif ($parqueadero[0]->status_id == 2) {
            $data = [
                'id_Parqueadero' => $parqueadero[0]->id_Parqueadero,
                'status_id' => 1
            ];
        }
        $this->model->editParqueadero($data);
        header('Location: ?controller=home');
    }
}
