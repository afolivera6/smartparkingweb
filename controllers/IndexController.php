<?php

require 'models/Index.php';

/**
 * Clase LoginController para carga el login del proyecto
 */
class IndexController
{

    private $model;

    public function __construct()
    {
        $this->model = new Index;
    }

    public function index()
    {
        require "views/Index/Index.php";
    }

    public function password()
    {
        require 'views/InicioSesion/password.php';
    }

    public function login()
    {
        $validateUser = $this->model->validateUser($_POST);
        if ($validateUser === true) {
            header('Location: ?controller=home');
        } else {
            $error = [
                'errorMessage' => $validateUser,
                'email' => $_POST['email']
            ];
            require "views/InicioSesion/login.php";
        }
    }

    public function logout()
    {
        if (isset($_SESSION['user'])) {
            session_destroy();

            header('Location: ?controller=login');
        } else {
            header('Location: ?controller=login');
        }
    }
}
