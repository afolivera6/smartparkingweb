<?php

require 'models/Empleado.php';
require 'models/Status.php';
require 'models/Rol.php';

class EmpleadoController
{

    private $model;
    private $rol;

    public function __construct()
    {
        $this->model = new Empleado;
        $this->status = new Status;
        $this->rol = new Rol;
    }

    public function index()
    {
        if (isset($_SESSION['user'])) {
            require 'views/layout.php';
            //Llamado al metodo que trae todos los usuarios
            $empleados = $this->model->getAll();
            require 'views/Empleado/list.php';
        } else {
            header('Location: ?controller=login');
        }
    }

    public function add()
    {
        if (isset($_SESSION['user'])) {
            $roles = $this->rol->getActiveRoles();
            require 'views/layout.php';
            require 'views/Empleado/new.php';
        } else {
            header('Location: ?controller=login');
        }
    }

    public function save()
    {
        $this->model->newEmpleado($_REQUEST);
        header('Location: ?controller=empleado');
    }

    public function edit()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $data = $this->model->getEmpleadoById($id);
                $statuses = $this->status->getAll();
                $roles = $this->rol->getActiveRoles();
                require 'views/layout.php';
                require 'views/Empleado/edit.php';
            } else {
                echo "Error actualizar";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    public function update()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_POST)) {
                $this->model->editEmpleado($_POST);
                header('Location: ?controller=empleado');
            } else {
                echo "Error actualizar";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    public function delete()
    {
        $this->model->deleteEmpleado($_REQUEST);
        header('Location: ?controller=empleado');
    }

    public function updateStatus()
    {
        $empleado = $this->model->getEmpleadoById($_REQUEST['id_Empleado']);
        $data = [];
        if ($empleado[0]->status_id == 1) {
            $data = [
                'id_Empleado' => $empleado[0]->id_Empleado,
                'status_id' => 2
            ];
        } elseif ($empleado[0]->status_id == 2) {
            $data = [
                'id_Empleado' => $empleado[0]->id_Empleado,
                'status_id' => 1
            ];
        }
        $this->model->editEmpleado($data);
        header('Location: ?controller=empleado');
    }
}
