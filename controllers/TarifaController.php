<?php

require 'models/Tarifa.php';

class TarifaController
{

    private $model;

    public function __construct()
    {
        $this->model = new Tarifa;
    }

    public function index()
    {
        if (isset($_SESSION['user'])) {
            require 'views/layout.php';
            //Llamado al metodo que trae todos los usuarios
            $tarifas = $this->model->getAll();
            require 'views/Tarifa/list.php';
        } else {
            header('Location: ?controller=login');
        }
    }

    public function add()
    {
        if (isset($_SESSION['user'])) {
            $tarifa = $this->model->getAll();
            require 'views/layout.php';
            require 'views/Tarifa/new.php';
        } else {
            header('Location: ?controller=login');
        }
    }

    public function save()
    {
        $this->model->newTarifa($_REQUEST);
        header('Location: ?controller=tarifa');
    }

    public function edit()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_REQUEST['id_tarifa'])) {
                $id_tarifa = $_REQUEST['id_tarifa'];
                $data = $this->model->getTarifaById($id_tarifa);
                $tarifa = $this->model->getAll();
                require 'views/layout.php';
                require 'views/Tarifa/edit.php';
            } else {
                echo "Error actualizar";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    public function update()
    {
        if (isset($_POST)) {
            $this->model->editTarifa($_POST);
            header('Location: ?controller=tarifa');
        } else {
            echo "Error actualizar";
        }
    }

    public function delete()
    {
        $this->model->deleteTarifa($_REQUEST);
        header('Location: ?controller=tarifa');
    }
}
