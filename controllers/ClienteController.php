<?php

/**
 * Clase ClienteController 
 */
require'models/Cliente.php';

class ClienteController {

    private $model;

    public function __construct() {
        $this->model = new Cliente;
    }

    public function index() {
        require 'views/layout.php';
        //Llamado al metodo que trae todos los usuarios
        $clientes = $this->model->getAll();
        require 'views/Cliente/list.php';
    }

    public function add() {
        require 'views/layout.php';
        require 'views/Cliente/new.php';
    }

    public function save() {
        $this->model->newCliente($_REQUEST);
        header('Location: ?controller=cliente');
    }

    public function edit() {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = $this->model->getClienteById($id);
            require 'views/layout.php';
            require 'views/Cliente/edit.php';
        } else {
            echo "Error actualizar";
        }
    }

    public function update() {
        if (isset($_POST)) {
            $this->model->editCliente($_POST);
            header('Location: ?controller=cliente');
        } else {
            echo "Error actualizar";
        }
    }

    public function delete() {
        $this->model->deleteCliente($_REQUEST);
        header('Location: ?controller=cliente');
    }

}

?>