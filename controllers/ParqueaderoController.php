    <?php

/**
 * Clase ParqueaderoController
 */
require 'models/Parqueadero.php';

class ParqueaderoController {

    private $model;
    private $parqueadero;

    public function __construct() {
        $this->model = new Parqueadero;
    }

    public function index() {
        require 'views/layout.php';
        $parqueaderos= $this->model->getAll();
        require 'views/home.php';

    }

    //muestra la vista de crear
    public function add() {
        require 'views/layout.php';
        $Parqueadero = $this->rol->getActiveRoles();
        require 'views/home.php';
    }

    // Realiza el proceso de guardar
    public function save() {
        $this->model->newParqueadero($_REQUEST);

        header('Location: ?controller=parqueadero');
    }

    //muestra la vista de editar
    public function edit() {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = $this->model->getUserById($id);
            $statuses = $this->status->getAll();
            $roles = $this->rol->getActiveRoles();
            require 'views/layout.php';
            require 'views/home.php';
        } else {
            echo "Error";
        }
    }

    // Realiza el proceso de actualizar
    public function update() {
        if (isset($_POST)) {
            $this->model->editUser($_POST);
            header('Location: ?controller=parqueadero');
        } else {
            echo "Error";
        }
    }

    // Realiza el proceso de borrar
    public function delete() {
        $this->model->deleteParqueadero($_REQUEST);
        header('Location: ?controller=parqueadero');
    }

    public function updateStatus() {
        $parqueadero = $this->model->getParqueaderoById($_REQUEST['id_Parqueadero']);
        $data = [];
        if ($parqueadero[0]->status_id == 1) {
            $data = [
                'id_Parqueadero' => $parqueadero[0]->id_Parqueadero,
                'status_id' => 2
            ];
        } elseif ($parqueadero[0]->status_id == 2) {
            $data = [
                'id_Parqueadero' => $parqueadero[0]->id_Parqueadero,
                'status_id' => 1
            ];
        }
        $this->model->editParqueadero($data);
        header('Location: ?controller=parqueadero');
    }

}
