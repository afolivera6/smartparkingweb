<?php

/**
 * Clase UserController 
 */
require 'models/User.php';
require 'models/Status.php';
require 'models/Rol.php';

class UserController
{

    private $model;
    private $rol;

    public function __construct()
    {
        $this->model = new User;
        $this->status = new Status;
        $this->rol = new Rol;
    }

    public function index()
    {
        if (isset($_SESSION['user'])) {
            require 'views/layout.php';
            //Llamado al metodo que trae todos los usuarios
            $users = $this->model->getAll();
            $roles = $this->rol->getAll();
            require 'views/user/list.php';
        } else {
            header('Location: ?controller=login');
        }
    }

    //muestra la vista de crear
    public function add()
    {
        if (isset($_SESSION['user'])) {
            require 'views/layout.php';
            $roles = $this->rol->getActiveRoles();
            require 'views/user/new.php';
        } else {
            header('Location: ?controller=login');
        }
    }

    // Realiza el proceso de guardar
    public function save()
    {
        if (isset($_SESSION['user'])) {
            $this->model->newUser($_REQUEST);
            header('Location: ?controller=user');
        } else {
            header('Location: ?controller=login');
        }
    }

    public function save2()
    {
        if (isset($_SESSION['user'])) {
            $this->model->newUser2($_REQUEST);
            header('Location: ?controller=home');
        } else {
            header('Location: ?controller=login');
        }
    }

    //muestra la vista de editar
    public function edit()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $data = $this->model->getUserById($id);
                $statuses = $this->status->getAll();
                $roles = $this->rol->getActiveRoles();
                require 'views/layout.php';
                require 'views/user/edit.php';
            } else {
                echo "Error";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    //muestra la vista de editar mi perfil
    public function editmyprofile()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $data = $this->model->getUserById($id);
                $statuses = $this->status->getAll();
                $roles = $this->rol->getActiveRoles();
                require 'views/layout.php';
                require 'views/user/EditarMiPerfil.php';
            } else {
                echo "Error";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    //muestra la vista de editar mi perfil
    public function editProfileOperador()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $data = $this->model->getUserById($id);
                $statuses = $this->status->getAll();
                $roles = $this->rol->getActiveRoles();
                require 'views/layout.php';
                require 'views/user/EditarPerfilOperador.php';
            } else {
                echo "Error";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    // Realiza el proceso de actualizar
    public function update()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_POST)) {
                $this->model->editUser($_POST);
                header('Location: ?controller=user');
            } else {
                echo "Error";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    // Realiza el proceso de actualizar
    public function update2()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_POST)) {
                $this->model->editUser($_POST);
                header('Location: ?controller=home');
            } else {
                echo "Error";
            }
        } else {
            header('Location: ?controller=login');
        }
    }

    // Realiza el proceso de borrar
    public function delete()
    {
        $this->model->deleteUser($_REQUEST);
        header('Location: ?controller=user');
    }

    public function updateStatus()
    {
        $user = $this->model->getUserById($_REQUEST['id']);
        $data = [];
        if ($user[0]->status_id == 1) {
            $data = [
                'id' => $user[0]->id,
                'status_id' => 2
            ];
        } elseif ($user[0]->status_id == 2) {
            $data = [
                'id' => $user[0]->id,
                'status_id' => 1
            ];
        }
        $this->model->editUser($data);
        header('Location: ?controller=user');
    }
}
