<?php

/**
 * Clase ServicioController 
 */
require 'models/Servicio.php';

class ServicioController
{

    private $model;

    public function __construct()
    {
        $this->model = new Servicio;
    }

    // Realiza el proceso de guardar
    public function save()
    {
        // dd($_POST);
        if (isset($_POST)) {
            $id=$_POST['id_Parqueadero'];
            $this->model->updateStatus($id);
            $saveService = $this->model->newServicioParqueadero($_POST);
            if($saveService) header('Location: ?controller=home');
        }
    }
    public function consultar()
    {
        // dd($_POST);
        if (isset($_REQUEST)) {
            $id=$_REQUEST['id'];
            $servicio=$this->model->getlastId($id);
            $todo=$this->model->getAllById($id,$servicio);
            require('views/layout.php');
            require('views/modalservicio.php');
        }else{
            echo("dasasd");
        }
    }

     public function end()
    {
     
      $Dataid=['id_Parqueadero'=>$_POST['ID_PARQUEADERO']];
      var_dump($Dataid);
        $this->model->editServiciostatus($Dataid);
         header('Location: ?controller=home');
       return true;
    }
    // Realiza el proceso de actualizar
    public function update()
    {
        if (isset($_POST)) {
            $this->model->editServicio($_POST);
            header('Location: ?controller=home');
        } else {
            echo "Error";
        }
    }

    // Realiza el proceso de borrar
    public function delete()
    {
        $this->model->deleteVehiculo($_REQUEST);
        header('Location: ?controller=home');
    }
}
