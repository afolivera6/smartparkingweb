<?php

/**
 * Clase ReporteController 
 */
require 'models/ReporteAlquiler.php';

class ReporteAlquilerController
{

    private $model;

    public function __construct()
    {
        $this->model = new ReporteAlquiler;
    }

    public function index()
    {
        if (isset($_SESSION['user'])) {
            require 'views/layout.php';
            //Llamado al metodo que trae todos los usuarios
            $ReporteAlquiler = $this->model->getAll();
            require 'views/ReporteAlquiler/list.php';
        } else {
            header('Location: ?controller=login');
        }
    }
    /**
    //muestra la vista de crear
    public function add() {
        $vehiculo = $this->model->getAll();
        require 'views/layout.php';
        require 'views/Vehiculo/new.php';
    }

    // Realiza el proceso de guardar
    public function save() {
        $this->model->newVehiculo($_REQUEST);
        header('Location: ?controller=vehiculo');
    }

    //muestra la vista de editar
    public function edit() {
        if (isset($_REQUEST['id_vehiculo'])) {
            $id_vehiculo = $_REQUEST['id_vehiculo'];
            $data = $this->model->getVehiculoById($id_vehiculo);
            $vehiculo = $this->model->getAll();
            require 'views/layout.php';
            require 'views/Vehiculo/edit.php';
        } else {
            echo "Error";
        }
    }

    // Realiza el proceso de actualizar
    public function update() {
        if (isset($_POST)) {
            $this->model->editVehiculo($_POST);
            header('Location: ?controller=vehiculo');
        } else {
            echo "Error";
        }
    }

    // Realiza el proceso de borrar
    public function delete() {
        $this->model->deleteVehiculo($_REQUEST);
        header('Location: ?controller=vehiculo');
    }
     */
}
