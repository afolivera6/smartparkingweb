<!DOCTYPE html>
<html ng-app='angularRoutingApp'>

<head>

    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Smart Parking</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="assets/css/now-ui-dashboard.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/demo/demo.css" rel="stylesheet">
    <!-- Javascript -->
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
</head>

<!-- Navbar -->
<div class="main-panel">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute bg-primary fixed-top">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <button class="dropbtn"><i class="fas fa-user-circle"></i></i>
                                <p class="text"><?php echo $_SESSION['user']->rol ?></b>
                            </button>
                        </a>
                        <?php
                        if ($_SESSION['user']->rol_id == 1) {
                        ?>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="?controller=user&method=editmyprofile&id=<?php echo $_SESSION['user']->id ?>">Editar Mi Perfil</a>
                                <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                            </div>
                        <?php
                        } elseif ($_SESSION['user']->rol_id == 2) {
                        ?>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="?controller=user&method=editProfileOperador&id=<?php echo $_SESSION['user']->id ?>">Cambiar contraseña</a>
                                <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                            </div>
                        <?php
                        }
                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="panel-header panel-header-lg"></div>
    <div class="row"></div>
    <?php
    if ($_SESSION['user']->rol_id == 1) {
    ?>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Inicio</li>
                <li class="breadcrumb-item"><a href="?controller=user">Lista de usuarios</a></li>
                <li class="breadcrumb-item"><a href="?controller=user" title="Volver al Lista de usuarios"><i class="fas fa-arrow-right"></i></a></li>
            </ol>
        </nav>
    <?php
    } elseif ($_SESSION['user']->rol_id == 2) {
    ?>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Inicio</li>
                <li class="breadcrumb-item"><a href="?controller=tarifa">Lista de Tarifas</a></li>
                <li class="breadcrumb-item"><a href="?controller=tarifa&method=add" title="Registrar tarifas"><i class="fas fa-arrow-right"></i></a></li>
            </ol>
        </nav>
    <?php
    }
    ?>
    <div class="content">
        <div class="row">
            <div class="container-fluid">
                <div class="card-subcategories">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-pills-primary justify-content-center" role="tablist">
                            <li class="nav-item">
                                <bottom class="nav-link active" style="cursor: pointer;" data-toggle="tab" href="#Pag-1" role="tablist">
                                    Parqueadero
                                </bottom>
                            </li>
                        </ul>

                        <div class="tab-content tab-space tab-subcategories">
                            <div class="tab-pane active container" id="Pag-1">

                                <div class="row">
                                    <?php foreach ($parqueaderos as $parqueadero) : ?>
                                        <div class="col-md-3 ml-auto mr-auto">
                                            <div class="card card-cascade">
                                                <!-- Card image -->
                                                <div class="view overlay">
                                                    <div align="center">
                                                        <h6>Espacio <?php echo isset($parqueadero->id_Parqueadero) ? $parqueadero->id_Parqueadero : 'No se encontro el id del parqueadero' ?></h6>
                                                        <img height="80" width="80" src="assets/icons/<?php echo $parqueadero->imagenParqueadero ?>.png" alt="" />
                                                        <p>Tipo <?php echo $parqueadero->nombre_tipoParqueadero ?></p>

                                                    </div>
                                                </div>

                                                <!-- Card content -->
                                                <div class="card-body card-body-cascade text-center">
                                                    <!-- Title -->
                                                    <?php
                                                    if ($parqueadero->status_id == 1) {
                                                    ?>
                                                        <img width="55" src="assets/icons/switch-On.png" alt="" id="<?php echo $parqueadero->id_Parqueadero ?>" />
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <img width="55" src="assets/icons/switch-Off.png" alt="" id="<?php echo $parqueadero->id_Parqueadero ?>" />
                                                    <?php
                                                    }
                                                    ?>
                                                    <!-- Button -->
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12 col-md-offset-2">
                                                                <div>
                                                                    <div>
                                                                        <?php
                                                                        if ($parqueadero->status_id == 2) {
                                                                            $fechai= $parqueadero->FechaIngreso_ServicioParqueadero;
                                                                            $id= $parqueadero->id_Parqueadero ;
                                                                        ?>  
                                                                            <?php echo $parqueadero->FechaIngreso_ServicioParqueadero ?>
                                                                            <button type="submit" id="btn-1" class="btn btn-round btn-danger btnsRegistrar" onclick="calcular('<?php echo $fechai ?>','<?php echo $id ?>');"></i> Finalizar Servicio</button>
                                                                            <a href="?controller=servicio&method=consultar&id=<?php echo $parqueadero->id_Parqueadero ?>" type="submit" id="btn-2" class="btn btn-round btn-info" data-target="#modalConsultarServicio<?php echo $parqueadero->id_Parqueadero ?>" data-whatever="" value="Consultar" title="Consultar"><i class="fas fa-search"></i> Consultar vehículo</a>
                                                                        <?php } else { ?>
                                                                            <button type="submit" id="btn-1" class="btn btn-round btn-success btnsRegistrar" data-toggle="modal" data-target="#RegistrarServicio<?php echo $parqueadero->id_Parqueadero ?>" data-whatever="" value="Registrar Servicio" title="Registrar Servicio"><i class="fas fa-plus"></i> Registrar Servicio</button>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    <?php endforeach ?>
                                </div>
                                <input type="hidden" value="<?php date_default_timezone_set("America/Bogota");
                                                            echo date("d/m/Y h:i:s A"); ?>" id="fechactual">
                                <?php foreach ($parqueaderos as $parqueadero) : ?>
                                    <div class="modal fade" id="RegistrarServicio<?php echo $parqueadero->id_Parqueadero ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="d-flex justify-content-between">
                                                                <div>
                                                                </div>
                                                                <div></div>
                                                                <div>
                                                                    <button class="btn btn-round btn-icon btn-danger" data-dismiss="modal" title="Cerrar" value="Cerrar"><i class="now-ui-icons ui-1_simple-remove"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="col-sm-11 container">
                                                        <h3 align="center">Registrar Servicio</h3>
                                                        <form action="?controller=Servicio&method=save" id="RegistrarServicio" method="post" class="was-validated">
                                                            <div class="row">

                                                                <div class="col-sm-12 col-lg-6">
                                                                    <label class="Text">Fecha inicio:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <input type="text" readonly class="form-control input-text" value="<?php date_default_timezone_set("America/Bogota");
                                                                                                                                        echo date("d/m/Y h:i:s A"); ?>" id="FechaIngreso_ServicioParqueadero" name="FechaIngreso_ServicioParqueadero" required="requiered" placeholder="Fecha ingreso" autocomplete="off" title="Registrar Solo Números">
                                                                    <br>
                                                                </div>
                                                                <input type="hidden" value="null" name="FechaFinal_ServicioParqueadero">
                                                                <div class="col-sm-12 col-lg-6">
                                                                    <label class="Text">Placa:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <input type="text" class="form-control input-text placa" id="placa_vehiculo" name="placa_vehiculo" required="requiered" placeholder="Placa" autocomplete="off">
                                                                    <br>
                                                                </div>

                                                                <div class="col-sm-12 col-lg-6">
                                                                    <label class="Text">Vehículo:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <input type="text" class="form-control input-text" id="nombre_vehiculo" name="nombre_vehiculo" required="requiered" placeholder="Marca o modelo del vehículo" autocomplete="off" onKeypress="sololetras()">
                                                                    <br>
                                                                </div>

                                                                <div class="col-sm-12 col-lg-6">
                                                                    <label class="Text">Novedad:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <input type="text" class="form-control input-text" id="novedad_vehiculo" name="novedad_vehiculo" required="requiered" placeholder="Novedad del vehículo" autocomplete="off" onKeypress="sololetras()">
                                                                    <br>
                                                                </div>

                                                                <div class="col-sm-12 col-lg-6">
                                                                    <label class="Text">Propietario:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <input type="text" class="form-control input-text" id="nombrePersonaVehiculo" name="nombrePersonaVehiculo" required="requiered" placeholder="Propietario" autocomplete="off" onKeypress="sololetras()">
                                                                    <br>
                                                                </div>

                                                                <div class="col-sm-12 col-lg-6">
                                                                    <label class="Text">Cedula:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <input type="number" class="form-control input-text vnum" id="cedulaPersonaVehiculo" name="cedulaPersonaVehiculo" required="requiered" placeholder="Cedula" autocomplete="off">
                                                                    <br>
                                                                </div>
                                                               
                                                                <div class="col-lg-6">
                                                                    <label class="Text">Tarifa:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <select id="id_Tarifa" name="id_Tarifa" class="form-control input-text" required="required">
                                                                        <option value="">Seleccione...</option>
                                                                        <?php
                                                                        foreach ($tarifas as $tarifa) {
                                                                        ?>
                                                                            <option value="<?php echo $tarifa->id_tarifa ?>"><?php echo $tarifa->nombre_tarifa ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label class="Text">Tipo de servicio:
                                                                        <span style="color: red;"><b>*</b></span>
                                                                    </label>
                                                                    <select id="id_tipo_Servicio" name="id_tipo_Servicio" class="form-control input-text" required="required">
                                                                        <option value="">Seleccione...</option>
                                                                        <?php foreach ($tipo_servicios as $tipo_servicio) { ?>
                                                                            <option value="<?php echo $tipo_servicio->id_tipo_Servicio  ?>"><?php echo $tipo_servicio->nombre_tipo_servicio ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <input type="hidden" name="id_Parqueadero" value="<?php echo $parqueadero->id_Parqueadero ?>">
                                                                <input type="hidden" name="id_Empleado" value="<?php echo $_SESSION['user']->id ?>" readonly>
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-md-offset-2">
                                                                            <div class="card-body d-flex justify-content-between align-items-center">
                                                                                <div></div>
                                                                                <div>
                                                                                    <button type="submit" class="btn btn-round btn-success btnsModalRegistrar" onclick="mostrarBoton2()" title="Registrar" value="Registrar">Registrar Servicio</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php foreach ($Servicio_parqueadero as $Servicio_parqueadero) : ?>
    <?php endforeach ?>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer" id="copyright">
                &copy; <script>
                    document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                </script> <a>SmartParking. Todos los derechos reservados.</a>
            </div>
        </div>
    </footer>


    <!--   Core JS Files   -->
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/now-ui-dashboard.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script>
        function vnum(maxle) {
            let input = document.querySelectorAll('.vnum');
            console.log(input);
            if (input) {
                input.forEach(e => {
                    e.addEventListener('input', function() {
                        if (this.value.length > maxle)
                            this.value = this.value.slice(0, maxle);
                    })
                });
            }
        }

        vnum(10); // longitud maxima a validar

        function placa(maxle) {
            let input = document.querySelectorAll('.placa');
            console.log(input);
            if (input) {
                input.forEach(e => {
                    e.addEventListener('input', function() {
                        if (this.value.length > maxle)
                            this.value = this.value.slice(0, maxle);
                    })
                });
            }
        }

        placa(6); // longitud maxima a validar
    </script>

    <script>
        function ele(element = '') { // esta funcion recibe el nombre del elemento y devuelve el elemento
            let ele = document.getElementById(element);
            return ele;
        }

        // let btnsRegistrar =  document.querySelectorAll('.btnsRegistrar');
        // let btnsModalRegistrar = document.querySelectorAll('.btnsModalRegistrar'),
        //     btnsConsultar = document.querySelectorAll('.btnOpenModalConsultar');

        //     if(btnsModalRegistrar){

        //         btnsModalRegistrar.forEach((elemento, i) => {

        //             elemento.addEventListener('click', (e) => {
        //                 console.log(e.target);
        //                 console.log(e);
        //                 console.log(elemento);
        //                 // console.log(e.target);
        //                 let FechaIngreso_ServicioParqueadero = ele("FechaIngreso_ServicioParqueadero"),
        //                     placa_vehiculo = ele("placa_vehiculo"),
        //                     nombre_vehiculo = ele("nombre_vehiculo"),
        //                     novedad_vehiculo = ele("novedad_vehiculo"),
        //                     nombrePersonaVehiculo = ele("nombrePersonaVehiculo"),
        //                     cedulaPersonaVehiculo = ele("cedulaPersonaVehiculo"),
        //                     id_Tarifa = ele("id_Tarifa"),
        //                     id_tipo_Servicio = ele("id_tipo_Servicio");

        //                     console.log(document.getElementById('nombre_vehiculo').value); // Unico 
        //                     console.log(id_Tarifa);
        //                 // CON let campo1 =  document.getElementById();
        //                 // VALIDAR QUE LOS CAMPOS NO VENGAN VACIOS PARA OCULTAR BTN Y MOSTRAR BTN CONSULTAR(REGISTRAR SERVICIO)
        //                 // if (1==1
        //                 //     // FechaIngreso_ServicioParqueadero.value != '' &&
        //                 //     // placa_vehiculo.value != '' &&
        //                 //     // nombre_vehiculo.value != '' &&
        //                 //     // novedad_vehiculo.value != '' &&
        //                 //     // nombrePersonaVehiculo.value != '' &&
        //                 //     // cedulaPersonaVehiculo.value != null &&
        //                 //     // id_Tarifa.value != undefined &&
        //                 //     // id_tipo_Servicio.value != undefined
        //                 //     // ESTO DE CUALQUIER MANERA NO SIRVE PORQUE LAS VALIDACIONES FUNCIONAN CON FETCH O AJAX ENTONCES VA A ENVIARLO INDEPENDIENTEMENTE DE QUE ESTN VACIOS
        //                 // ) { // aqui necesito que validen los campos para que no oculte el btn si no se pudo registrar
        //                     // ENVIAR DATOS AL BACK
        //                     // CONFIG DE FETCH
        //                     let data = [
        //                         FechaIngreso_ServicioParqueadero.value,
        //                         placa_vehiculo.value,
        //                         nombre_vehiculo.value,
        //                         novedad_vehiculo.value,
        //                         nombrePersonaVehiculo.value,
        //                         cedulaPersonaVehiculo.value,
        //                         id_Tarifa.value,
        //                         id_tipo_Servicio.value
        //                     ];

        //                     console.log(cedulaPersonaVehiculo.value);
        //                     console.log(data[cedulaPersonaVehiculo]);

        //                     alert(data);
        //                     console.log(data);

        //                     let options = {
        //                             method: 'POST',
        //                             body: "data=" + JSON.stringify(data),

        //                             headers: {
        //                                 "Content-Type": "application/x-www-form-urlencoded"
        //                             }
        //                         },
        //                         url = "?controller=servicio&method=save";

        //                     fetch(url, options)
        //                         .then(response => {
        //                             if (response.ok) return response.text()
        //                         })
        //                         .then(result => {
        //                             alert(result);
        //                             if (result == '"bien"') location.href = "?controller=home";
        //                             else if (result == "bien") location.href = "?controller=home";
        //                             else if(result.includes("SQLSTATE[23000]")) alertify.alert("Error 23000");
        //                             else alertify.alert("Error al guardar, intentelo más tarde");
        //                         })
        //                         .catch(e => reject(e));

        //                     // document.getElementsByClassName("btnsRegistrar")[i].classList.add("d-none");
        //                     // document.getElementsByClassName("btnOpenModalConsultar")[i].style.display = "block";
        //                 // } else {
        //                 //     // console.log(`LLENA TODOS LOS CAMPOS`);
        //                 //     alertify.alert("Llena todos los campos");
        //                 // }

        //             })
        //         })
        //     }

        btnsConsultar.forEach((ele, i) => {
            ele.addEventListener('click', (e) => {
                alert("click en el btn de consultar");
            })
        })

        // USTEDES VIERON YA SABEN COMO HACER LA PARTE DE AJAX? 
        // JMM BUENO PODEMOS HACERLO SIN AJAX PERO QUEDARÍA UN ERROR SI LLEGASE
        //  EL CASO NO SE PUDIERE REALIZAR EL SERVICIO.
        //  EN LUGAR DE AJAX ME PARECE MEJOR FETCH COMO LO EXPLICO EL PROFE DE JS O 
        // AXIOS SI ESTOS DOS SON LOS MISMO. AUNQUE AXIOS TOCA INCLUIRLO YA QUE ES LIBRERIA EXTERNA
    </script>


    <!-- Buscador Jquery Function Autocomplete -->
    <script>
        function autocomplete(inp, arr) {
            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function(e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) {
                    return false;
                }
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function(e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });

            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function(e) {
                closeAllLists(e.target);
            });
        }

        // Este monton de código lo pueden poner en un archivo aparte


        // autocomplete(AQUI VA EL ELEMENTO INPUT DE BUSCADOR, AQUI VA EL ARREGLO CON LOS DATOS QUE VAN A SER BUSCADOS);
    </script>
    <script src="assets/js/SweetAlert/sweetalert.min.js"></script>
    <script src="http://momentjs.com/downloads/moment.min.js"></script>
    <script src="assets/js/calcular.js"></script>