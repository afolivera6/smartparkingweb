<?php
@session_start();
if (!isset($_SESSION['user'])) {
    header('Location:http://localhost/Xamp/Smart-Parking-PHP/?controller=login');
    exit();
}
?>
<div class="modal-body">
    <div class="col-sm-11 container">
        <h3 align="center">Consultar Servicio</h3>
        <form action="?controller=Servicio&method=update" id="FormRegistrarUsuario" class="was-validated">
            <div class="row">
                <input type="hidden" name="id_ServicioParqueadero" value="<?php echo $data[0]->id_ServicioParqueadero ?>">
                <div class="col-sm-12 col-lg-6">
                    <label class="Text">Fecha inicio:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <input type="text" class="form-control input-text" value="<?php echo $data[0]->FechaIngreso_ServicioParqueadero ?>" name="FechaIngreso_ServicioParqueadero" required="requiered" placeholder="Fecha ingreso" autocomplete="off" title="Registrar Solo Números">
                    <br>
                </div>
                <input type="hidden" value="null" name="FechaFinal_ServicioParqueadero">
                <div class="col-sm-12 col-lg-6">
                    <label class="Text">Placa:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <input type="text" class="form-control input-text" value="<?php echo $data[0]->placa_vehiculo ?>" name="placa_vehiculo" required="requiered" placeholder="Placa" autocomplete="off">
                    <br>
                </div>

                <div class="col-sm-12 col-lg-6">
                    <label class="Text">Vehículo:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <input type="text" class="form-control input-text" value="<?php echo $data[0]->nombre_vehiculo ?>" name="nombre_vehiculo" required="requiered" placeholder="Marca o modelo del vehículo" autocomplete="off" onKeypress="sololetras()">
                    <br>
                </div>

                <div class="col-sm-12 col-lg-6">
                    <label class="Text">Novedad:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <input type="text" class="form-control input-text" value="<?php echo $data[0]->novedad_vehiculo ?>" name="novedad_vehiculo" required="requiered" placeholder="Novedad del vehículo" autocomplete="off" onKeypress="sololetras()">
                    <br>
                </div>

                <div class="col-sm-12 col-lg-6">
                    <label class="Text">Propietario:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <input type="text" class="form-control input-text" value="<?php echo $data[0]->nombrePersonaVehiculo ?>" name="nombrePersonaVehiculo" required="requiered" placeholder="Propietario" autocomplete="off" onKeypress="sololetras()">
                    <br>
                </div>

                <div class="col-sm-12 col-lg-6">
                    <label class="Text">Cedula:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <input type="number" class="form-control input-text" value="<?php echo $data[0]->cedulaPersonaVehiculo ?>" name="cedulaPersonaVehiculo" required="requiered" placeholder="Cedula" autocomplete="off">
                    <br>
                </div>

                <div class="col-lg-6">
                    <label class="Text">Tarifa:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <select name="id_Tarifa" class="form-control input-text" required="required">
                        <option value="">Seleccione...</option>
                        <?php
                        foreach ($tarifa as $tarifa) {
                            if ($tarifa->id_tarifa == $data[0]->id_Tarifa) {
                        ?>
                                <option selected value="<?php echo $tarifa->id_tarifa ?>"><?php echo $tarifa->nombre_tarifa ?></option>
                            <?php
                            } else {
                            ?>
                                <option value="<?php echo $tarifa->id_tarifa ?>"><?php echo $tarifa->nombre_tarifa ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label class="Text">Tipo de contrato:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <select name="id_tipo_Servicio" class="form-control input-text" required="required">
                        <option value="">Seleccione...</option>
                        <?php
                        foreach ($Tipo_Servicio as $Tipo_Servicio) {
                            if ($Tipo_Servicio->id_tipo_Servicio == $data[0]->id_tipo_Servicio) {
                        ?>
                                <option selected value="<?php echo $Tipo_Servicio->id_tipo_Servicio ?>"><?php echo $Tipo_Servicio->nombre_tipo_servicio ?></option>
                            <?php
                            } else {
                            ?>
                                <option value="<?php echo $Tipo_Servicio->id_tipo_Servicio ?>"><?php echo $Tipo_Servicio->nombre_tipo_servicio ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label class="Text">Numero de parqueadero:
                        <span style="color: red;"><b>*</b></span>
                    </label>
                    <select name="id_Parqueadero" class="form-control input-text" required="required">
                        <option value="">Seleccione...</option>
                        <?php foreach ($parqueaderos as $parqueadero) { ?>
                            <option value="<?php echo $parqueadero->id_Parqueadero ?>"><?php echo $parqueadero->id_Parqueadero ?></option>
                        <?php } ?>


                        <?php
                        foreach ($parqueaderos as $parqueadero) {
                            if ($parqueadero->id_Parqueadero == $data[0]->id_Parqueadero) {
                        ?>
                                <option selected value="<?php echo $parqueadero->id_Parqueadero ?>"><?php echo $parqueadero->id_Parqueadero ?></option>
                            <?php
                            } else {
                            ?>
                                <option value="<?php echo $parqueadero->id_Parqueadero ?>"><?php echo $parqueadero->id_Parqueadero ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <input type="hidden" name="id_Empleado" value="<?php echo $parqueadero->placa_vehiculo ?>" readonly>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-2">
                            <div class="card-body d-flex justify-content-between align-items-center">
                                <div></div>
                                <div>
                                    <button type="submit" class="btn btn-round btn-success" title="Registrar" value="Registrar">Servicio</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php exit(); ?>