<!DOCTYPE html>
<html ng-app='angularRoutingApp'>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Smart Parking</title>

        <!-- Icon Page -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/Icono.png">

        <link href="assets/css/autoptimize.css" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap-4.1.3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap-4.0.0-alpha.6/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/Style.css" rel="stylesheet" type="text/css"/>
        <!-- Fonts CSS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto%3A300%7CPoppins%3A700%2C600%2C500&#038;ver=1576167250" type=text/css media=all> 

    </head>

    <body id="page-top" class="kc-css-system">
        <a href="#jssor_1" class="scrolltotop homepage-1" style="bottom: 40px; display: block;">
            <span class="ti-angle-double-up"></span>
        </a>
        <header id=sticker class="headroom custom-header">

            <nav class="navbar navbar-toggleable-lg navbar-light bg-faded">

                <a class="navbar-brand" href="#jssor_1">
                    <img width="150" height="40" src="assets/img/Logo1.png" alt=""/>
                </a>
                <!-- Navbar -->
                <div id=navigation>
                    <div class=menu-main-menu-container>
                        <div class=menu-menu-principal-container>
                            <ul id=mainmenu class="nav navbar-nav navbar-right">

                                <li id=menu-item-3124 class="dropdown menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="nav-item dropdown nav-link active" href="#jssor_1">
                                        <span class="dropbtn card-flyder sub">Inicio</span>
                                    </a>
                                </li>

                                <li id=menu-item-1695 class="dropdown menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="nav-item dropdown nav-link active" href="#PorqueSmartparking">
                                        <span class="dropbtn card-flyder sub">Por qué SmartParking</span>
                                    </a>
                                </li>

                                <li id=menu-item-3124 class="dropdown menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="nav-item dropdown nav-link active" href="#TrabajaConNosotros">
                                        <span class="dropbtn card-flyder sub">Trabaja con nosotros</span>
                                    </a>
                                </li>

                                <li id=menu-item-3124 class="dropdown menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="nav-item dropdown nav-link active" href="#QuienesSomos">
                                        <span class="dropbtn card-flyder sub">Quiénes somos</span>
                                    </a>
                                </li>

                                <li id=menu-item-3124 class="dropdown menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="nav-item dropdown nav-link active" href="?controller=login">
                                        <span class="dropbtn card-flyder sub">Iniciar Sesión</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Navbar -->

            </nav>
        </header>

        <div id="jssor_1" class="Slider1">
            <div data-u="slides" class="Slider2">
                <div>
                    <div class="view">
                        <img data-u="image" src="assets/img/slider-01.jpg">
                        <div class="mask rgba-black-light"></div>
                        <div class="carousel-caption">
                            <h2><strong>Bienvenidos</strong></h2>
                            <h3>Somos Smart Parking lideres en control y monitoreo de parqueaderos</h3>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="view">
                        <img data-u="image" src="assets/img/slider-02.jpg">
                        <div class="mask flex-center rgba-blue-grey-light"></div>
                        <div class="carousel-caption">
                            <h2>Administre eficazmente el tiempo y la productividad de sus Parqueaderos</h2>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="view">
                        <img data-u="image" src="assets/img/slider-03.jpg">
                        <div class="mask rgba-black-light"></div>
                        <div class="carousel-caption">
                            <h2>Disfruta de la mejor seguridad y monitoreo de nuestros parqueaderos</h2>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:16px;height:16px;">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0px;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0px;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0px;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
        </div>
        
        <div class=page_builder_wrapper>
            <div>
                <div>
                    <section data-kc-fullheight=middle-content id="Home" class="kc-elm kc-css-250817 kc_row moving-background">
                        <div class="kc-row-container kc-container">
                            <div class=kc-wrap-columns>
                                <div class="kc-elm kc-css-61811 kc_col-sm-6 kc_column kc_col-sm-6">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-221588 kc-title-wrap " data-wow-delay=0s>
                                            <h2 class="kc_title color">Mejora y amplifica el monitoreo de tus parqueaderos.</h2>
                                        </div>
                                        <div class="kc-elm kc-css-998778 kc_text_block None"  data-wow-delay=0.2s>
                                            <p class="color"><strong>Con SmartParking podrás en una sola plataforma monitorear tus vehículos de forma eficiente y sencilla.</strong></p>
                                        </div>
                                        <div class="kc-elm kc-css-999851 kc-css-998778 wow fadeInUp"  data-wow-delay=0.2s>
                                            <a style="text-decoration: none;color: white!important" class=kc_button href="#SolicitarInformcion" title="SOLICITA INFORMACIÓN">
                                                SOLICITA INFORMACIÓN	
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="kc-elm kc-css-832738 kc_col-sm-6 kc_column kc_col-sm-6">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-425882 position kc-animated kc-animate-eff-fadeInLeft kc_shortcode kc_single_image wow fadeInUp" data-wow-delay=0.2s>
                                            <img width="600" height="350" src="assets/img/Parking-2.jpg" alt="" class="radius"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="PorqueSmartparking" data-kc-fullheight=middle-content  class="kc-elm kc-css-536005 kc_row" style="background-color: #e1e3ec;">
                        <div class="kc-row-container  kc-container">
                            <div class=kc-wrap-columns>
                                <div class="kc-elm kc-css-832738 kc_col-sm-6 kc_column kc_col-sm-6">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-425882 kc-animated kc-animate-eff-fadeInLeft kc_shortcode kc_single_image wow fadeInUp" data-wow-delay=0.2s>
                                            <img width="600" height="350" src="assets/img/Parking-1.jpg" alt="" class="radius"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="kc-elm kc-css-61811 kc_col-sm-6 kc_column kc_col-sm-6">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-221588 kc-title-wrap " data-wow-delay=0s>
                                            <h2 class="kc_title">Por qué SmartParking</h2>
                                        </div>
                                        <div class="kc-elm kc-css-998778 kc_text_block None"  data-wow-delay=0.2s>
                                            <p>En SmartParking no solo encontrarás canales de monitoreo y supervisión, si no también un aliado estratégico que aportará seguridad y confianza.</p>
                                        </div>
                                        <div class="kc-elm kc-css-318704">
                                            <div class=about-list>
                                                <ul>
                                                    <li>Monitorea tus parqueaderos con Smart Control.</li>
                                                    <li>Analiza el comportamiento de tus clientes con Smart Client.</li>
                                                    <li>Con nuestros canales puedes monitorear tus parqueaderos en tiempo real.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="TrabajaConNosotros" class="kc-elm kc-css-37174 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class=kc-wrap-columns>
                                <div class="kc-elm kc-css-802234 kc_col-sm-5 kc_column kc_col-sm-5">
                                    <div class="margin">
                                        <br><br><br><br><br><br><br><br><br>
                                        <div class="kc-elm kc-css-811655 kc-title-wrap " data-wow-delay=0s>
                                            <h1 class="kc_title">Trabaja con Nosotros</h1>
                                        </div>
                                        <div class="kc-elm kc-css-998778 kc_text_block None"  data-wow-delay=0.2s>
                                            <p><strong>Hoy en día es clave ser oportunos y relevantes cuando queremos comunicarnos con nuestros clientes.</strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="kc-elm kc-css-944508 kc_col-sm-7 kc_column kc_col-sm-7">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-611929 kc_row kc_row_inner">
                                            <div class="kc-elm kc-css-912030 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                                                <div class="kc_wrapper kc-col-inner-container">
                                                    <div class="kc-elm kc-css-948631">
                                                        <div class="single-advance hover_active">
                                                            <span><img src='assets/img/wallet.png' width="40" height="40"></span>
                                                            <h2>Eficiencia del presupuesto.</h2>
                                                            <p>Disminuye el desperdicio de presupuesto en comunicación.</p>
                                                        </div>
                                                    </div>
                                                    <div class="kc-elm kc-css-757157">
                                                        <div class="single-advance hover_active">
                                                            <span><img src='assets/img/bar-chart.png' width="40" height="40"></span>
                                                            <h2>Completamente medible.</h2>
                                                            <p>Podrás en tiempo real tomar acciones sobre los resultados que veas de tus flujos de comunicación.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kc-elm kc-css-201329 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                                                <div class="kc_wrapper kc-col-inner-container">
                                                    <div class="kc-elm kc-css-207774">
                                                        <div class="single-advance hover_active">
                                                            <span><img src='assets/img/bubble.png' width="42" height="42"></span>
                                                            <h2>Efectiva comunicación.</h2>
                                                            <p>Comunícate oportunamente con tus clientes de acuerdo a sus acciones.</p>
                                                        </div>
                                                    </div>
                                                    <div class="kc-elm kc-css-606041">
                                                        <div class="single-advance hover_active">
                                                            <span><img src='assets/img/globe.png' width="40" height="40"></span>
                                                            <h2>Flujos de comunicación multicanal.</h2>
                                                            <p>La automatización en SmartParking no es solo email, también podrás implementar canales como: SMS y mensajes de voz.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="QuienesSomos" class="kc-elm kc-css-37174 kc_row" style="background-color: #e1e3ec;">

                        <div class="kc-wrap-columns kc-container">
                            <div class="kc-elm kc-css-801684 kc-title-wrap" data-wow-delay=0s>
                                <h1 class="kc_title TextX">Quiénes Somos</h1>
                            </div>
                            <div class="kc-elm kc-css-801684 kc-title-wrap kc-elm kc-css-998778 kc_text_block None" data-wow-delay=0s>
                                <p class="kc_title">Somos un equipo de trabajo líder especializado en monitoreo y supervisión de parqueaderos. Nuestro foco está en generar valor a nuestros clientes, a través de productos innovadores con alta calidad y excelente servicio. Mientras el mundo está en constante cambio, siempre habrán nuevas desafíos y oportunidades para mejora. A través de la innovación queremos contribuir al adecuado desempeño de las compañías, transformando la información operativa en indicadores estratégicos.</p>
                            </div>
                        </div>
                        <div align="center">
                            <picture>
                                <source srcset="assets/img/porquesmart.png" media="(min-width:600px)">
                                <source srcset="assets/img/porquesmart.png" media="(min-width:200px)">
                                <img style="max-width : 100%" src="assets/img/porquesmart.png" >
                            </picture>
                        </div>

                    </section>

                    <section id="SolicitarInformcion" class="kc-elm kc-css-536005 kc_row" style="background-color:white;">
                        <div class="kc-row-container kc-container">
                            <div class=kc-wrap-columns>
                                <div class="kc-elm kc-css-686030 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-801684 kc-title-wrap" data-wow-delay=0s>
                                            <h1 class="kc_title TextX">Contáctanos</h1>
                                        </div>
                                        <div class="widget wpforms-widget kc-elm kc-css-393989" style="background-color: #f5f7fb; box-shadow: 2px 4px 8px 5px rgba(46,61,73,0.2);">
                                            <div class="wpforms-container wpforms-container-full" id=wpforms-2630>
                                                <form id="FormRegistrarUsuario" class="was-validated">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-lg-6">
                                                            <label class="Text">Nombre y Apellido: 
                                                                <span style="color: red;"><b>*</b></span>
                                                            </label>
                                                            <input type="text" class="form-control input-text" id="nombre" required="requiered">
                                                            <br>
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <label class="Text">Empresa: 
                                                                <span style="color: red;"><b>*</b></span>
                                                            </label>
                                                            <input type="text" class="form-control input-text" id="empresa" required="requiered">
                                                            <br>
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <label class="Text">Email: 
                                                                <span style="color: red;"><b>*</b></span>
                                                            </label>
                                                            <input type="text" class="form-control input-text" id="email" required="requiered">
                                                            <br>
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <label class="Text">Número de contacto: 
                                                                <span style="color: red;"><b>*</b></span>
                                                            </label>
                                                            <input type="text" class="form-control input-text" id="numero" required="requiered">
                                                            <br>
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <label class="Text">Mensaje:</label>
                                                            <textarea id="mensaje" class="form-control input-text" rows="5" required="required"></textarea>
                                                            <br>
                                                        </div>
                                                        <div class="kc-elm kc-css-999851 kc-css-998778 wow fadeInUp" style="text-align: center;" data-wow-delay=0.2s>
                                                            <a><button type=submit class="Submit-button" data-alt-text=Sending...>ENVIAR</button></a>                                 
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>

        <footer>

            <div class="container">

                <div class="footer2">

                    <section class="kc_row">
                        <br>
                        <div class="kc-row-container kc-container">
                            <div class=kc-wrap-columns>
                                <div class="kc-elm kc-css-798219 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class=kc-col-container>
                                        <div class=single-address>
                                            <div class=address-icon-bg>
                                                <span><img src='assets/img/location2.png' width="30" height="30"></span>
                                            </div>
                                            <p style="text-align: center;">Bogotá, Colombia.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class=kc-wrap-columns>
                                <div class="kc-elm kc-css-798219 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class=kc-col-container>
                                        <div class="kc-elm kc-css-998778 kc_text_block None"  data-wow-delay=0.2s>
                                            <p style="text-align: center; color: white">
                                                ©2020 SmartParking. Todos los derechos reservados.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <div class=kc-col-container>
                    </div>
                </div>

            </div>

        </footer>
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="assets/js/jssor.slider.min.js" type="text/javascript"></script>
        <script src="assets/js/slider.js" type="text/javascript"></script>
        <script src="assets/js/autoptimize.js" type="text/javascript"></script>
        <!-- Bootstrap core JavaScript-->
        <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <!-- Core plugin JavaScript-->
        <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
        
    </body>
</html>