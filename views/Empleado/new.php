<!DOCTYPE html>
<html ng-app='angularRoutingApp'>
    <head>

        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Smart Parking</title>

        <!-- Fonts and icons -->
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- CSS Files -->
        <link href="assets/css/now-ui-dashboard.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/demo/demo.css" rel="stylesheet">
        <!-- Javascript -->
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>

    </head>

    <!-- Navbar -->
    <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute bg-primary fixed-top">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        </li>
                        <li class="nav-item dropdown"> 
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <button class="dropbtn"><i class="fas fa-user-circle"></i></i> <p class="text"><?php echo $_SESSION['user']->rol ?></b></button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="?controller=user&method=editmyprofile&id=<?php echo $_SESSION['user']->id ?>">Editar Mi Perfil</a>
                            <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    <div class="panel-header panel-header-lg"></div>
    <div class="row"></div>
    <!-- Navbar -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="?controller=Empleado" title="Volver a Lista de empleados"><i class="fas fa-arrow-left"></i></a></li>
            <li class="breadcrumb-item"><a href="?controller=Empleado">Lista de empleados</a></li>
            <li class="breadcrumb-item active" aria-current="page">Registrar empleado</li>
        </ol>
    </nav>
    <div class="col-sm-11 container">
        <div class="card col-sm-12">
        <div align="center"><img height="100" src="assets/icons/Empleados.png" alt=""/></div>
        <h2 align="center">Registrar empleado</h2>
        <div class="w-100 m-100">
                <form action="?controller=empleado&method=save" method="post" class="was-validated">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Nombre: 
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="nombre" class="form-control input-text" placeholder="Nombre Completo" required="requiered" autocomplete="off" onKeypress="sololetras()">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Apellidos: 
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="apellido" class="form-control input-text" placeholder="Apellido Completo" required="requiered" autocomplete="off" onKeypress="sololetras()">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Cedula: 
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="number" name="cedula" class="form-control input-text" placeholder="Cedula" required="requiered" autocomplete="off">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Correo Electrónico: 
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="email" name="correo" class="form-control input-text" placeholder="Correo Electrónico" required="requiered" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" autocomplete="off">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Teléfono:
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="number" name="telefono" class="form-control input-text" placeholder="Teléfono" required="requiered" autocomplete="off">
                            <br>
                        </div>
                        <input type="hidden" name="status_id" value="1">
                        <div class="col-lg-6">
                            <label class="Text">Tipo de Estado: 
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <select name="rol_id" class="form-control input-text" required="required">
                                <option value="">Seleccione...</option>
                                <?php
                                foreach ($roles as $rol) {
                                    ?>
                                    <option value="<?php echo $rol->id ?>"><?php echo $rol->name ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div></div>
                                        <div class="col-xs-2">
                                            <button class="btn btn-round btn-info">Registrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><br><br>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer" id="copyright">
                &copy; <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script> <a>SmartParking. Todos los derechos reservados.</a>
            </div>
        </div>
    </footer>
</div>
<!--   Core JS Files   -->
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/now-ui-dashboard.min.js" type="text/javascript"></script>