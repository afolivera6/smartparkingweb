<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Iniciar Sesión</title>

    <!-- Icon Page -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Material Design Bootstrap -->
    <link href="assets/css/MDB.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/StyleSesion.css" rel="stylesheet" type="text/css" />
    <!-- Javascript -->
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
</head>


<body>
    <picture>
        <source srcset="assets/img/Parking-6.jpg" media="(min-width:600px)">
        <source srcset="assets/img/Parking-6.jpg" media="(min-width:300px)">
        <img style="max-width : 100%" src="assets/img/Parking-6.jpg">
        <img style="max-width : 100%" src="assets/img/Parking-6.jpg">
        <img style="max-width : 110%" src="assets/img/Parking-6.jpg">
        <img style="max-width : 100%" src="assets/img/Parking-6.jpg">
    </picture>
    <div class="container">
        <div class="side-1">
            <div class="header">
                <div class="col-sm-30 container-fluid" align="center">
                    <img width="137" height="137" src="assets/img/Logo3.png" alt="" />
                    <br><br>
                    <h2 class="h-main">¡Bienvenido!</h2>
                    <p class="h-sec">Inicie sesión con su nombre de usuario y contraseña</p>
                    <div class="form-group col-sm-9">
                        <div class="input-group mb-2">
                            <button type="submit" class="toggle-log" style="outline: none;" value="Únete">Únete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="side-2">
            <div class="header">
                <div class="col-sm-30 container-fluid" align="center">
                    <img width="137" height="137" src="assets/img/Logo3.png" alt="" />
                    <br><br>
                    <h2 class="h-main">¡Hola!</h2>
                    <p class="h-sec">¡Introduce tus datos personales y comienza hoy mismo!</p>
                    <div class="form-group col-sm-9">
                        <div class="input-group mb-2">
                            <button type="submit" class="toggle-log" style="outline: none;" value="Inicia Sesión">Inicia Sesión</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="forms">
            <div class="sign-up">
                <div class="form">
                    <fieldset class="block">
                        <h2 class="form-h">Únete</h2>
                        <div class="col-sm-30 container-fluid" align="center">
                            <form action="?controller=user&method=save3" method="post" class="was-validated">
                                <input type="hidden" name="rol_id" value="2">
                                <div class="form-group col-sm-9">
                                    <input type="text" name="first_name" id="nombres" class="form-control input-text" autocomplete="off" placeholder="Nombres" required="required">
                                </div>
                                <div class="form-group col-sm-9">
                                    <input type="text" name="last_name" id="apellidos" class="form-control input-text" autocomplete="off" placeholder="Apellidos" required="required">
                                </div>
                                <div class="form-group col-sm-9">
                                    <input type="text" name="email" id="correo" value="<?php echo isset($error['email']) ? $error['email'] : '' ?>" class="form-control input-text" autocomplete="off" placeholder="Correo electrónico" required="required">
                                </div>
                                <div class="form-group col-sm-9">
                                    <input type="number" name="phone" id="telefono" class="form-control input-text" autocomplete="off" placeholder="Número Telefónico" required="required">
                                </div>
                                <div class="form-group col-sm-9">
                                    <div class="input-group mb-2">
                                        <input type="password" name="password" id="password1" class="form-control input-text" autocomplete="off" placeholder="Contraseña" required="required">
                                        <div class="input-group-append">
                                            <span toggle="#password1" class="input-group-text far fa-eye-slash field-icon toggle-password"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-9">
                                    <div class="input-group mb-2">
                                        <button type="submit" class="input-submit" id="registroSistema" style="outline: none;" value="Únete">Únete</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="sign-in">
                <div class="form">
                    <fieldset>
                        <h2 class="form-h">Inicia sesión</h2>
                        <div class="col-sm-30 container-fluid" align="center">
                            <form action="?controller=login&method=login" method="post" class="was-validated">
                                <div class="form-group col-sm-9">
                                    <div class="input-group mb-2">
                                        <input type="email" name="email" id="email" value="<?php echo isset($error['email']) ? $error['email'] : '' ?>" class="form-control input-text" autocomplete="off" placeholder="Correo electrónico" required="required">
                                    </div>
                                </div>
                                <?php if (isset($error['errorMessage'])) { ?>
                             <?php echo $error['errorMessage']; ?>
                            <?php } ?>
                                <div class="form-group col-sm-9">
                                    <div class="input-group mb-2">
                                        <input type="password" name="password" id="password2" class="form-control input-text" autocomplete="off" placeholder="Contraseña" required="required">
                                        <div class="input-group-append">
                                            <span toggle="#password2" class="input-group-text far fa-eye-slash field-icon toggle-password"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-9">
                                    <div class="input-group mb-2">
                                        <button type="submit" class="input-submit" id="entrarSistema" style="outline: none;" value="Iniciar">Ingresar</button>
                                    </div>
                                </div>
                            </form>
                            <div class="text-center">
                                <a class="d-block small" href="?controller=login&method=password">¿Olvidaste tu contraseña?</a>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(".toggle-password").click(function() {
            $(this).toggleClass("far fa-eye far fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#entrarSistema', '').click(function() {
                if ($('#email').val() == "") {
                    alertify.alert("Debes ingresar el correo electronico");
                    return false;
                } else if ($('#password').val() == "") {
                    alertify.alert("Debes ingresar la contraseña");
                    return false;
                }

                cadena = "email=" + $('#email').val() +
                    "&password=" + $('#password').val();

                $.ajax({
                    type: "post",
                    url: "?controller=login&method=login",
                    data: cadena,
                    success: function(r) {
                        if (r === 1) {
                            window.location = "?controller=login&method=login";
                        } else {
                            <?php if (isset($error['errorMessage'])) { ?>
                                alertify.alert("<?php echo $error['errorMessage']; ?>");
                            <?php } ?>
                        }
                    }
                });
            });
        });
        $(document).ready(function() {
            $('#registroSistema', '').click(function() {
                if ($('#nombres').val() == "") {
                    alertify.alert("Debes ingresar los nombres");
                    return false;
                } else if ($('#apellidos').val() == "") {
                    alertify.alert("Debes ingresar los apellidos");
                    return false;
                } else if ($('#correo').val() == "") {
                    alertify.alert("Debes ingresar el correo electronico");
                    return false;
                } else if ($('#telefono').val() == "") {
                    alertify.alert("Debes ingresar el telefono");
                    return false;
                } else if ($('#password').val() == "") {
                    alertify.alert("Debes ingresar la contraseña");
                    return false;
                }

                cadena = "nombres=" + $('#nombres').val() +
                    "apellidos=" + $('#apellidos').val() +
                    "correo=" + $('#correo').val() +
                    "telefono=" + $('#telefono').val() +
                    "&password=" + $('#password').val();

                $.ajax({
                    type: "post",
                    url: "?controller=login&method=login",
                    data: cadena,
                    success: function(r) {
                        if (r === 1) {
                            window.location = "?controller=home";
                        } else {
                            <?php if (isset($error['errorMessage'])) { ?>
                                alertify.alert("<?php echo $error['errorMessage']; ?>");
                            <?php } ?>
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>