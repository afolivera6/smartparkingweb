<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Restablecer Mi Contraseña</title>
    <!-- Icon Page -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <!-- Material Design Bootstrap -->
    <link href="assets/css/MDB.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/StyleSesion.css" rel="stylesheet" type="text/css" />
</head>

<body class="ImgCars">
    <div class="col-sm-5 container-fluid">
        <div class="card card-login mx-auto mt-5">
            <!-- Card -->
            <div class="card testimonial-card" style="max-width: 90rem;">
                <!-- Background color -->
                <div class="card-up aqua-gradient"></div>
                <!-- Avatar -->
                <div class="avatar mx-auto white">
                    <img src="assets/img/Logo3.png" class="rounded-circle img-responsive" alt="woman avatar">
                </div>
                <div class="card-body">
                    <div class="text-center mb-4">
                        <h4 class="card-title">Restablecer Mi Contraseña</h4>
                        <hr>
                        <p>Ingrese su dirección de correo electrónico y le enviaremos instrucciones para restablecer su contraseña.</p>
                    </div>
                    <form class="was-validated">
                        <div class="form-group mx-5">
                            <div class="input-group mb-2">
                                <input type="email" name="email" class="form-control input-text" autocomplete="off" placeholder="Correo electrónico" required="required">
                            </div>
                        </div>
                        <div class="form-group mx-5">
                            <div class="input-group mb-2">
                                <button type="submit" class="input-submit" style="outline: none;" value="Iniciar">Restablecer Contraseña</button>
                            </div>
                        </div>
                    </form>
                    <div class="text-center">
                        <a class="d-block small" href="?controller=login">Volver a Inicio de Sesión</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<style>
    .ImgCars {
        position: absolute;
        width: 100%;
        height: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url(assets/img/Parking-6.jpg);
    }
</style>