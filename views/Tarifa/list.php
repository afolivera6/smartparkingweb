<!DOCTYPE html>
<html ng-app='angularRoutingApp'>

<head>

    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Smart Parking</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="assets/css/now-ui-dashboard.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/demo/demo.css" rel="stylesheet">
    <!-- Javascript -->
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>

    <script src="assets/js/jquery-3.5.1.js"></script>
    <script language="javascript">
        $(document).ready(function() {
            $(".Btn1").click(function(event) {
                $("#DatosExcel").val($("<div>").append($("#Exportar").eq(0).clone()).html());
                $("#ExportacionExcel").submit();
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".Btn2").click(function(event) {
                $("#DatosWord").val($("<div>").append($("#Exportar").eq(0).clone()).html());
                $("#ExportacionWord").submit();
            });
        });
    </script>
</head>

<!-- Navbar -->
<div class="main-panel">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute bg-primary fixed-top">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <button class="dropbtn"><i class="fas fa-user-circle"></i></i>
                                <p class="text"><?php echo $_SESSION['user']->rol ?></b>
                            </button>
                        </a>
                        <?php
                        if ($_SESSION['user']->rol_id == 1) {
                        ?>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="?controller=user&method=editmyprofile&id=<?php echo $_SESSION['user']->id ?>">Editar Mi Perfil</a>
                                <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                            </div>
                        <?php
                        } elseif ($_SESSION['user']->rol_id == 2) {
                        ?>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="?controller=user&method=editProfileOperador&id=<?php echo $_SESSION['user']->id ?>">Cambiar contraseña</a>
                                <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                            </div>
                        <?php
                        }
                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="panel-header panel-header-lg"></div>
    <div class="row"></div>
    <!-- Navbar -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="?controller=home" title="Volver al Inicio"><i class="fas fa-arrow-left"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Lista de tarifas</li>
            <li class="breadcrumb-item"><a href="?controller=tarifa&method=add">Registrar tarifa</a></li>
        </ol>
    </nav>
    <main class="container">
        <section>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="card">
                        <div>
                            <!--Card image-->
                            <div class="view Gradient py-0 mx-0 d-flex justify-content-between align-items-center">
                                <div></div>
                                <h3 class="card-title" style="color:white">Lista de Tarifas</h3>
                                <div>
                                    <a href="?controller=tarifa&method=add" class="btn btn-success btn-round text-icon btn-sm" title="Agregar">Tarifa <img height="19" width="19" src="assets/icons/pluss.png"></a>
                                    <button type="submit" title="Exportar Excel" class="Btn1 btn-rounded btn-md px-1 py-1" value="Exportar" style="outline:none;"><img width="30" height="30" src="assets/icons/excel.png" alt="" /></button>
                                    <button type="submit" title="Exportar Word" class="Btn2 btn-rounded btn-md px-1 py-1" value="Exportar" style="outline:none;"><img width="30" height="30" src="assets/icons/word.png" alt="" /></button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover" id="Exportar">
                                    <thead>
                                        <tr class="text-center white-text mx-0 Blue">
                                            <th><i class="fas fa-sort-amount-down"></i></th>
                                            <th><b>Tiempo</b></th>
                                            <th><b>Precio</b></th>
                                            <th><b>Acciones</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($tarifas as $tarifa) : ?>
                                            <tr>
                                                <th><?php echo $tarifa->id_tarifa ?></th>
                                                <td><?php echo $tarifa->nombre_tarifa ?><b></b></td>
                                                <td><i class="fas fa-dollar-sign"></i> <?php echo $tarifa->precio_tarifa ?> <b>COP</b></td>
                                                <td>
                                                    <a href="?controller=tarifa&method=edit&id_tarifa=<?php echo $tarifa->id_tarifa ?>" class="btn btn-round btn-icon btn-info" title="Editar"><i class="far fa-edit"></i></a>
                                                    <a href="?controller=tarifa&method=delete&id_tarifa=<?php echo $tarifa->id_tarifa ?>" class="btn btn-round btn-icon btn-danger" title="Eliminar"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                                <form action="views/HistorialVehiculo/FicheroExcel.php" method="post" target="_blank" id="ExportacionExcel">
                                    <input type="hidden" id="DatosExcel" name="DatosExcel" />
                                </form>
                                <form action="views/HistorialVehiculo/FicheroWord.php" method="post" target="_blank" id="ExportacionWord">
                                    <input type="hidden" id="DatosWord" name="DatosWord" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <br>
    <br>
    <br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer" id="copyright">
                &copy; <script>
                    document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                </script> <a>SmartParking. Todos los derechos reservados.</a>
            </div>
        </div>
    </footer>
</div>
<!--   Core JS Files   -->
<script src="assets/js/core/bootstrap.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/now-ui-dashboard.min.js" type="text/javascript"></script>