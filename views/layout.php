<!DOCTYPE html>
<html ng-app='angularRoutingApp'>

<head>

    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Smart Parking</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'>

    <!-- Fonts and icons -->

    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Files -->

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/now-ui-dashboard.min.css" rel="stylesheet" type="text/css" />


</head>

<body class="sidebar-mini">
    <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.0.2/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.0.2/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyAvL1npuDZJaEhCtoEITin-WePo2ZHiyHw",
    authDomain: "smartparkingweb.firebaseapp.com",
    databaseURL: "https://smartparkingweb.firebaseio.com",
    projectId: "smartparkingweb",
    storageBucket: "smartparkingweb.appspot.com",
    messagingSenderId: "649658631313",
    appId: "1:649658631313:web:8c3fdcab96e579535e6b30",
    measurementId: "G-R78VNY057G"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>

    <header>
        <?php
        if ($_SESSION['user']->rol_id == 1) {
        ?>
            <div class="sidebar" data-color="blue">
                <div class="logo">
                    <a href="?controller=home" class="simple-text logo-mini">
                        <img height="35" src="assets/img/favicon.png" alt="" />
                        <img height="35" src="assets/icons/man.png" alt="" />
                    </a>

                    <a href="?controller=home" class="simple-text logo-normal">
                        <img width="90" height="35" src="assets/img/Smart-Parking-White.png" alt="" />
                    </a>

                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-outline-white btn-icon btn-round">
                            <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
                            <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
                        </button>
                    </div>

                </div>

                <div class="sidebar-wrapper" id="sidebar-wrapper">

                    <div class="user">
                        <div class="photo">
                            <img src="assets/icons/usuario.png" alt="" />
                        </div>
                        <div class="info">
                            <a data-toggle="collapse" href="#collapse" class="collapsed">
                                <span>
                                    <?php echo $_SESSION['user']->name ?>
                                    <b class="caret"></b>
                                </span>
                            </a>
                            <div class="clearfix"></div>
                            <div class="collapse" id="collapse">
                                <ul class="nav">
                                    <li>
                                        <a href="?controller=user&method=editmyprofile&id=<?php echo $_SESSION['user']->id ?>">
                                            <span class="sidebar-mini-icon">EP</span>
                                            <span class="sidebar-normal">Editar mi perfil</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="?controller=login&method=logout">
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                            <span class="sidebar-normal">Cerrar sesión</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav">

                        <li class="active">

                            <a href="?controller=home">

                                <i><img width="25" height="25" src="assets/icons/location.png" alt="" /></i>

                                <p>Inicio</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=user">
                                <i><img width="25" height="25" src="assets/icons/group.png" alt="" /></i>
                                <p>Gestión de Usuarios</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=empleado">

                                <i><img width="28" height="28" src="assets/icons/user.png" alt="" /></i>

                                <p>Empleados</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=tarifa">

                                <i><img width="30" height="30" src="assets/icons/Tarifa.png" alt="" /></i>

                                <p>Tarifas</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=HistorialVehiculo">

                                <i><img width="25" height="25" src="assets/icons/Vehiculo.png" alt="" /></i>

                                <p>Historial Vehículo</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=ReporteServicio">

                                <i><img width="28" height="28" src="assets/icons/ReporteServicio.png" alt="" /></i>

                                <p>Reportes de Servicio</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=ReporteAlquiler">

                                <i><img width="25" height="25" src="assets/icons/ReporteAlquiler.png" alt="" /></i>

                                <p>Reportes de Alquiler</p>
                            </a>

                        </li>

                    </ul>
                </div>
            </div>

        <?php
        } elseif ($_SESSION['user']->rol_id == 2) {
        ?>
            <div class="sidebar" data-color="blue">
                <div class="logo">
                    <a href="?controller=home" class="simple-text logo-mini">
                        <img height="35" src="assets/img/favicon.png" alt="" />
                        <img height="35" src="assets/icons/man.png" alt="" />
                    </a>

                    <a href="?controller=home" class="simple-text logo-normal">
                        <img width="90" height="35" src="assets/img/Smart-Parking-White.png" alt="" />
                    </a>

                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-outline-white btn-icon btn-round">
                            <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
                            <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
                        </button>
                    </div>

                </div>

                <div class="sidebar-wrapper" id="sidebar-wrapper">

                    <div class="user">
                        <div class="photo">
                            <img src="assets/icons/usuario.png" alt="" />
                        </div>
                        <div class="info">
                            <a data-toggle="collapse" href="#collapse" class="collapsed">
                                <span>
                                    <?php echo $_SESSION['user']->name ?>
                                    <b class="caret"></b>
                                </span>
                            </a>
                            <div class="clearfix"></div>
                            <div class="collapse" id="collapse">
                                <ul class="nav">
                                    <li>
                                        <a href="?controller=user&method=editProfileOperador&id=<?php echo $_SESSION['user']->id ?>">
                                            <span class="sidebar-mini-icon">CC</span>
                                            <span class="sidebar-normal">Cambiar mi contraseña</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="?controller=login&method=logout">
                                            <i class="now-ui-icons ui-1_simple-remove"></i>
                                            <span class="sidebar-normal">Cerrar sesión</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav">

                        <li class="active">

                            <a href="?controller=home">

                                <i><img width="24" height="24" src="assets/icons/location.png" alt="" /></i>

                                <p>Inicio</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=tarifa">

                                <i><img width="30" height="30" src="assets/icons/Tarifa.png" alt="" /></i>

                                <p>Tarifas</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=HistorialVehiculo">

                                <i><img width="25" height="25" src="assets/icons/Vehiculo.png" alt="" /></i>

                                <p>Historial Vehículo</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=ReporteServicio">

                                <i><img width="28" height="28" src="assets/icons/ReporteServicio.png" alt="" /></i>

                                <p>Reportes de Servicio</p>
                            </a>

                        </li>

                        <li>

                            <a href="?controller=ReporteAlquiler">

                                <i><img width="25" height="25" src="assets/icons/ReporteAlquiler.png" alt="" /></i>

                                <p>Reportes de Alquiler</p>
                            </a>

                        </li>

                    </ul>
                </div>
            </div>
        <?php
        }
        ?>
    </header>
</body>
<style>
    .footer {
        background: #292f35;
        padding: 20px 0;
    }

    .footer {
        text-align: center;
        margin: 0px;
        color: #ffffff;
    }

    .footer {
        color: #00a9d8;
    }

    .footer:hover {
        color: #007cac;
    }
</style>
<!--   Core JS Files   -->
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/now-ui-dashboard.min.js" type="text/javascript"></script>
<script src="views/Codigo.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        demo.initDashboardPageCharts();
        demo.initVectorMap();

        $sidebar = $('.sidebar');
        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');
        sidebar_mini_active = true;

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        // if( window_width > 767 && fixed_plugin_open == 'Dashboard' ){
        //     if($('.fixed-plugin .dropdown').hasClass('show-dropdown')){
        //         $('.fixed-plugin .dropdown').addClass('show');
        //     }
        //
        // }

        $('.fixed-plugin a').click(function(event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });

        $('.fixed-plugin .img-holder').click(function() {
            $full_page_background = $('.full-page-background');

            $(this).parent('li').siblings().removeClass('active');
            $(this).parent('li').addClass('active');


            var new_image = $(this).find("img").attr('src');

            if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                $sidebar_img_container.fadeOut('fast', function() {
                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $sidebar_img_container.fadeIn('fast');
                });
            }

            if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                $full_page_background.fadeOut('fast', function() {
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                    $full_page_background.fadeIn('fast');
                });
            }

            if ($('.switch-sidebar-image input:checked').length == 0) {
                var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
            }

            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
            }
        });

        $('.switch-sidebar-image input').on("switchChange.bootstrapSwitch", function() {
            $full_page_background = $('.full-page-background');

            $input = $(this);

            if ($input.is(':checked')) {
                if ($sidebar_img_container.length != 0) {
                    $sidebar_img_container.fadeIn('fast');
                    $sidebar.attr('data-image', '#');
                }

                if ($full_page_background.length != 0) {
                    $full_page_background.fadeIn('fast');
                    $full_page.attr('data-image', '#');
                }

                background_image = true;
            } else {
                if ($sidebar_img_container.length != 0) {
                    $sidebar.removeAttr('data-image');
                    $sidebar_img_container.fadeOut('fast');
                }

                if ($full_page_background.length != 0) {
                    $full_page.removeAttr('data-image', '#');
                    $full_page_background.fadeOut('fast');
                }

                background_image = false;
            }
        });

        $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
            var $btn = $(this);

            if (sidebar_mini_active == true) {
                $('body').removeClass('sidebar-mini');
                sidebar_mini_active = false;
                nowuiDashboard.showSidebarMessage('Sidebar mini deactivated...');
            } else {
                $('body').addClass('sidebar-mini');
                sidebar_mini_active = true;
                nowuiDashboard.showSidebarMessage('Sidebar mini activated...');
            }

            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function() {
                window.dispatchEvent(new Event('resize'));
            }, 180);

            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function() {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
    });
    $(document).on('click', 'ul li', function() {
        $(this).addClass('active').siblings().removeClass('active')
    });
</script>

</html>