<!DOCTYPE html>
<html ng-app='angularRoutingApp'>

<head>

    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Smart Parking</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="assets/css/now-ui-dashboard.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/demo/demo.css" rel="stylesheet">
    <!-- Javascript -->
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>

</head>

<!-- Navbar -->
<div class="main-panel">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute bg-primary fixed-top">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <button class="dropbtn"><i class="fas fa-user-circle"></i></i>
                                <p class="text"><?php echo $_SESSION['user']->rol ?></b>
                            </button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="?controller=user&method=editmyprofile&id=<?php echo $_SESSION['user']->id ?>">Editar Mi Perfil</a>
                            <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="panel-header panel-header-lg"></div>
    <div class="row"></div>
    <!-- Navbar -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="?controller=home" title="Volver al Inicio"><i class="fas fa-arrow-left"></i></a></li>
        </ol>
    </nav>

    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex justify-content-between">
                            <div>
                            </div>
                            <div></div>
                            <div>
                                <a href="?controller=home" class="btn btn-round btn-icon btn-danger" data-dismiss="modal" title="Cerrar" value="Cerrar"><i class="now-ui-icons ui-1_simple-remove"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="col-sm-11 container">
                    <h3 align="center">Consultar Servicio</h3>
                    <form action="?controller=Servicio&method=update" method="post" id="FormRegistrarUsuario" class="was-validated">
                        <div class="row">
                            <input type="hidden" name="id_ServicioParqueadero" value="<?php echo $todo[0]->id_ServicioParqueadero ?>">
                            <div class="col-sm-12 col-lg-6">
                                <label class="Text">Fecha inicio:
                                    <span style="color: red;"><b>*</b></span>
                                </label>
                                <input type="text" class="form-control input-text" value="<?php echo $todo[0]->FechaIngreso_ServicioParqueadero ?>" name="FechaIngreso_ServicioParqueadero" required="requiered" placeholder="Fecha ingreso" autocomplete="off" title="Registrar Solo Números">
                                <br>
                            </div>
                            <input type="hidden" class="form-control input-text" value="<?php echo $todo[0]->FechaFinal_ServicioParqueadero ?>" name="FechaFinal_ServicioParqueadero" value="No Registrar">
                            <div class="col-sm-12 col-lg-6">
                                <label class="Text">Placa:
                                    <span style="color: red;"><b>*</b></span>
                                </label>
                                <input type="text" class="form-control input-text placa" name="placa_vehiculo" value="<?php echo $todo[0]->placa_vehiculo ?>" required="requiered" placeholder="Placa" autocomplete="off">
                                <br>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <label class="Text">Vehículo:
                                    <span style="color: red;"><b>*</b></span>
                                </label>
                                <input type="text" class="form-control input-text" name="nombre_vehiculo" value="<?php echo $todo[0]->nombre_vehiculo ?>" required="requiered" placeholder="Marca o modelo del vehículo" autocomplete="off" onKeypress="sololetras()">
                                <br>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <label class="Text">Novedad:
                                    <span style="color: red;"><b>*</b></span>
                                </label>
                                <input type="text" class="form-control input-text" name="novedad_vehiculo" value="<?php echo $todo[0]->novedad_vehiculo ?>" required="requiered" placeholder="Novedad del vehículo" autocomplete="off" onKeypress="sololetras()">
                                <br>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <label class="Text">Propietario:
                                    <span style="color: red;"><b>*</b></span>
                                </label>
                                <input type="text" class="form-control input-text" name="nombrePersonaVehiculo" value="<?php echo $todo[0]->nombrePersonaVehiculo ?>" required="requiered" placeholder="Propietario" autocomplete="off" onKeypress="sololetras()">
                                <br>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <label class="Text">Cedula:
                                    <span style="color: red;"><b>*</b></span>
                                </label>
                                <input type="number" class="form-control input-text vnum" name="cedulaPersonaVehiculo" value="<?php echo $todo[0]->cedulaPersonaVehiculo ?>" required="requiered" placeholder="Cedula" autocomplete="off">
                                <br>
                            </div>
                            <input type="hidden" name="id_Tarifa" value="<?php echo $todo[0]->id_Tarifa ?>">
                            <input type="hidden" name="id_tipo_Servicio" value="<?php echo $todo[0]->id_tipo_Servicio ?>">
                            <input type="hidden" name="id_Parqueadero" value="<?php echo $todo[0]->id_Parqueadero ?>">
                            <input type="hidden" name="id_Empleado" value="<?php echo $todo[0]->id_Empleado ?>">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 col-md-offset-2">
                                        <div class="card-body d-flex justify-content-between align-items-center">
                                            <div></div>
                                            <div>
                                                <button type="submit" id="btn-2" class="btn btn-round btn-info" value="Actualizar" title="Actualizar">Actualizar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</html>
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/now-ui-dashboard.min.js" type="text/javascript"></script>