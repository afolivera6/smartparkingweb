<!DOCTYPE html>
<html ng-app='angularRoutingApp'>

<head>

    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Smart Parking</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="assets/css/now-ui-dashboard.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/demo/demo.css" rel="stylesheet">
    <!-- Javascript -->
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>

</head>

<!-- Navbar -->
<div class="main-panel">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute bg-primary fixed-top">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">
                    <li class="nav-item">
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <button class="dropbtn"><i class="fas fa-user-circle"></i></i>
                                <p class="text"><?php echo $_SESSION['user']->rol ?></b>
                            </button>
                        </a>
                        <?php
                        if ($_SESSION['user']->rol_id == 1) {
                        ?>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="?controller=user&method=editmyprofile&id=<?php echo $_SESSION['user']->id ?>">Editar Mi Perfil</a>
                                <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                            </div>
                        <?php
                        } elseif ($_SESSION['user']->rol_id == 2) {
                        ?>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="?controller=user&method=editProfileOperador&id=<?php echo $_SESSION['user']->id ?>">Cambiar contraseña</a>
                                <a class="dropdown-item" href="?controller=login&method=logout">Cerrar Sesión</a>
                            </div>
                        <?php
                        }
                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="panel-header panel-header-lg"></div>
    <div class="row"></div>
    <!-- Navbar -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="?controller=user&method=add" title="Volver a Registrar usuario"><i class="fas fa-arrow-left"></i></a></li>
            <li class="breadcrumb-item"><a href="?controller=user">Lista de Vehículo</a></li>
            <li class="breadcrumb-item"><a href="?controller=user&method=add">Registrar Vehículo</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Vehículo</li>
        </ol>
    </nav>
    <div class="col-sm-11 container">
        <div class="card col-sm-12">
            <div align="center"><img height="100" src="assets/icons/CarBlack.png" alt="" /></div>
            <h3 align="center">Editar Vehículo</h3>
            <div class="w-100 m-100">
                <form action="?controller=vehiculo&method=update" method="post" class="was-validated">
                    <input type="hidden" name="id_vehiculo" value="<?php echo $data[0]->id_vehiculo ?>">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Placa:
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="placa_vehiculo" class="form-control input-text" value="<?php echo $data[0]->placa_vehiculo ?>" placeholder="Placa" required="requiered" autocomplete="off">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Vehículo:
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="nombre_vehiculo" class="form-control input-text" value="<?php echo $data[0]->nombre_vehiculo ?>" placeholder="Correo Electrónico" required="requiered" autocomplete="off">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Novedad:
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="novedad_vehiculo" class="form-control input-text" value="<?php echo $data[0]->novedad_vehiculo ?>" autocomplete="of">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Propietario:
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="nombrePersonaVehiculo" class="form-control input-text" value="<?php echo $data[0]->nombrePersonaVehiculo ?>" autocomplete="of">
                            <br>
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            <label class="Text">Cedula:
                                <span style="color: red;"><b>*</b></span>
                            </label>
                            <input type="text" name="cedulaPersonaVehiculo" class="form-control input-text" value="<?php echo $data[0]->cedulaPersonaVehiculo ?>" autocomplete="of">
                            <br>
                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div></div>
                                        <div class="col-xs-2">
                                            <button class="btn btn-round btn-info">Actualizar</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div><br><br><br><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer" id="copyright">
                &copy; <script>
                    document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                </script> <a>SmartParking. Todos los derechos reservados.</a>
            </div>
        </div>
    </footer>
</div>
<!--   Core JS Files   -->
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/now-ui-dashboard.min.js" type="text/javascript"></script>