// Declarar Array Global que contendra la lista de categorias
// var	arrCategories = []

showCategories()

//Llamar duncion de jquery para funcion del boto addElement
//cuando el formulario detecte que le dio click al boton de agregado "+" actuara en funcio el codigo javascript 
$("#addElement").click(function (e) {
	//Deshabilitar el envio por Http
	e.preventDefault()
	//se llama el valor del id con el select osea el valor que selecione o el tipo de categoria
	let idCategory = $("#category").val()
	//se llama la categoria seleccionada que se muestra en el formulario
	let nameCategory = $("#category option:selected").text()

	if(idCategory != '') {
		if(typeof existCategory(idCategory) === 'undefined') {
			//agregar categoria al array arrCategories
			arrCategories.push({
				'id': idCategory,
				'name': nameCategory  
			})
		} else {
			alert("La categoria ya esta asignada a la pelicula")
		}
		//metodo para mostrar el array de las categorias osea la lista general
		showCategories()
	} else {
		alert("Debe selecionar un categoria")
	}
});

//metodos que hacen parte del javascript de el agragado de categorias multiples a una pelicula

function existCategory(idCategory){
	let existCategory = arrCategories.find(function (category){
		return category.id == idCategory
	})
	return existCategory
}

function showCategories(){
	$("#list-categories").empty()

	arrCategories.forEach(function(category){
		$("#list-categories").append('<div class="form-group"><button onclick="removeElement('+category.id+')" class="btn btn-danger">X</button><span>'+category.name+'</span></div>')
	})
}

function removeElement(idCategory) {
	let index = arrCategories.indexOf(existCategory(idCategory))
	arrCategories.splice(index, 1)
	showCategories()
	console.log(arrCategories)
}	

//Ahora vamos a realizar el metodo de envio al controlador (back end)

$("#submit").click(function (e){
	//desabilitmos el envio por Http 
	e.preventDefault()

	let url = "?controller=movie&method=save"
	//se declara los llamados de los datos que se quieren reguistrar
	let params = {
		name: 				$("#name").val(),
		Description: 		$("#Description").val(),
		user_id: 			$("#user_id").val(),
		categories: 		arrCategories,
	}

	//metodo post usando ajax para enviar la información al back end (controlador)
	$.post(url, params, function(response){
		if (typeof response.error != 'undefined') {
			alert(response.message)
		} else {
			//redireccion al modulo registrar peliculas	
			alert("Insercion Satisfactoria")
			location.href = "?controller=movie"
		}
		//Respuesta del request 
	}, 'json').fail(function(error){
		alert("Insercion fallida ("+error.responseText+")")
	});
	//responseText sirve para mostrar el error php en la ventana modal

}); 

//Ahora vamos a realizar el metodo de envio al controlador (back end)

$("#update").click(function (e){
	//desabilitmos el envio por Http 
	e.preventDefault()

	let url = "?controller=movie&method=update"
	//se declara los llamados de los datos que se quieren reguistrar
	let params = {
		id: 				$("#id").val(),
		name: 				$("#name").val(),
		Description: 		$("#Description").val(),
		user_id: 			$("#user_id").val(),	
		categories: 		arrCategories,
	}

	//metodo post usando ajax para enviar la información al back end (controlador)
	$.post(url, params, function(response){
		if (typeof response.error != 'undefined') {
			alert(response.message)
		} else {
			//redireccion al modulo registrar peliculas	
			alert("Actualización Satisfactoria")
			location.href = "?controller=movie"
		}
		//Respuesta del request 
	}, 'json').fail(function(error){
		alert("Actualización fallida ("+error.responseText+")")
	});
	//responseText sirve para mostrar el error php en la ventana modal

}); 	