//declaramos la variable array global que contendra la lista de peluculas
// var arrMovies = []
	showMovies() 

//vamos a llamar la funcion para el id addElement para cuando le de click este despligegue susu funciones
$("#addElementMovie").click(function (e) {
	//desabilitamos el envio http apra que no se vuelva a recargar la pagina
	e.preventDefault()

	let idMovie = $("#movie_id").val()
	let nameMovie = $("#movie_id option:selected").text()

	if (idMovie != '') {
		if (typeof existMovie(idMovie) === 'undefined') {
			arrMovies.push({
				'id': 		idMovie,
				'name': 	nameMovie
			})
		}else{
			alert("La pelicula ya se encuentra seleccionada")
		}
		showMovies()		
	} else {
		alert("no se selecciono ninguna pelicula")
	}
});


function existMovie(idMovie){
	let existMovie = arrMovies.find(function(movie_id){
			return movie_id.id == idMovie
	})
	return existMovie 

}


function showMovies(){
	$("#list-movies").empty()

	arrMovies.forEach(function(movie_id){
		$("#list-movies").append('<div class="form-group"><button onclick="removeElement('+movie_id.id+')" class="btn btn-danger">X</button><span>'+movie_id.name+'</span></div>')
	})
}

function removeElement(idMovie) {
	let index = arrMovies.indexOf(existMovie(idMovie))
	arrMovies.splice(index, 1)
	showMovies()
	console.log(arrMovies)
}

//enviar datos a backend

$("#submit").click(function (e) {

	e.preventDefault()

	let url = "?controller=rental&method=save"
	let params = {
		start_date:         $("#start_date").val(),
		end_date:         	$("#end_date").val(),
		total:         		$("#total").val(),
		user_id: 			$("#user_id").val(),
		status_id: 			$("#status_id").val(),
		movies:         	arrMovies
	}

 //metodo post usando ajax para enviar la informacion al backend
 $.post(url,params, function(response) {
 	//respuesta del request
 	if (typeof response.error !== 'undefined'){
 		alert(response.error)
 	} else {
 		alert("insercion satisfactoria")
 		//redireccion al modulo de listar rentas
 		location.href = "?controller=rental"
 	}
 }, 'json').fail(function(error) {
 	alert ("insercion Fallida ("+error+")")
 });

});


$("#update").click(function (e) {

	e.preventDefault()

	let url = "?controller=rental&method=update"
	let params = {
		id:         		$("#id").val(),
		start_date:         $("#start_date").val(),
		end_date:         	$("#end_date").val(),
		total:         		$("#total").val(),
		user_id: 			$("#user_id").val(),
		status_id: 			$("#status_id").val(),
		movies:         	arrMovies
	}

 //metodo post usando ajax para enviar la informacion al backend
 $.post(url,params, function(response) {
 	//respuesta del request
 	if (typeof response.error !== 'undefined'){
 		alert(response.error)
 	} else {
 		alert("Actualizacion satisfactoria")
 		//redireccion al modulo de listar rentas
 		location.href = "?controller=rental"
 	}
 }, 'json').fail(function(error) {
 	alert ("Actualizacion Fallida ("+error+")")
 });

});

