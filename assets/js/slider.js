﻿jQuery(document).ready(function($) {
    $('.carousel').carousel({
        interval: 1700
    });
    $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function() {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        for (var i = 0; i < 3; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
        }
    });

    var jssor_1_options = {
        $AutoPlay: 1,
        $SlideDuration: 995,
        $SlideEasing: $Jease$.$OutQuint,
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$
        }
    };
    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
    /*#region responsive code begin*/

    var MAX_WIDTH = 3000;

    function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;
        if (containerWidth) {

            var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
            jssor_1_slider.$ScaleWidth(expectedWidth);
        } else {
            window.setTimeout(ScaleSlider, 30);
        }
    }

    ScaleSlider();
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    /*#endregion responsive code end*/
});

var buttonizer_ajax = { "ajaxurl": "https:\/\/smartparkingweb.000webhostapp.com\/wp-admin\/admin-ajax.php", "version": "2.1.4", "buttonizer_path": "https:\/\/smartparkingweb.000webhostapp.com\/wp-content\/plugins\/buttonizer-multifunctional-button", "buttonizer_assets": "https:\/\/smartparkingweb.000webhostapp.com\/wp-content\/plugins\/buttonizer-multifunctional-button\/assets\/", "base_url": "https:\/\/smartparkingweb.000webhostapp.com", "current": [], "in_preview": "", "is_admin": "", "cache": "95b6fbfc0c8f4a0f0c67a68117a810f6", "enable_ga_clicks": "1" };
var buffetjs = { "is_rtl": "" };
var wpforms_settings = { "val_required": "This field is required.", "val_url": "Please enter a valid URL.", "val_email": "Please enter a valid email address.", "val_email_suggestion": "Did you mean {suggestion}?", "val_email_suggestion_title": "Click to accept this suggestion.", "val_number": "Please enter a valid number.", "val_confirm": "Field values do not match.", "val_fileextension": "File type is not allowed.", "val_filesize": "File exceeds max size allowed.", "val_time12h": "Please enter time in 12-hour AM\/PM format (eg 8:45 AM).", "val_time24h": "Please enter time in 24-hour format (eg 22:45).", "val_requiredpayment": "Payment is required.", "val_creditcard": "Please enter a valid credit card number.", "val_smart_phone": "Please enter a valid phone number.", "val_post_max_size": "The total size of the selected files {totalSize} Mb exceeds the allowed limit {maxSize} Mb.", "val_checklimit": "You have exceeded the number of allowed selections: {#}.", "post_max_size": "272629760", "uuid_cookie": "1", "locale": "es", "wpforms_plugin_url": "https:\/\/smartparkingweb.000webhostapp.com\/wp-content\/plugins\/wpforms\/", "gdpr": "1", "currency_code": "USD", "currency_thousands": ",", "currency_decimal": ".", "currency_symbol": "$", "currency_symbol_pos": "left" }