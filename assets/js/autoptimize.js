/*! jQuery Migrate v1.4.1 | (c) jQuery Foundation and other contributors | jquery.org/license */
"undefined" == typeof jQuery.migrateMute && (jQuery.migrateMute = !0),
    function(a, b, c) {
        function d(c) {
            var d = b.console;
            f[c] || (f[c] = !0, a.migrateWarnings.push(c), d && d.warn && !a.migrateMute && (d.warn("JQMIGRATE: " + c), a.migrateTrace && d.trace && d.trace()))
        }

        function e(b, c, e, f) {
            if (Object.defineProperty) try { return void Object.defineProperty(b, c, { configurable: !0, enumerable: !0, get: function() { return d(f), e }, set: function(a) { d(f), e = a } }) } catch (g) {}
            a._definePropertyBroken = !0, b[c] = e
        }
        a.migrateVersion = "1.4.1";
        var f = {};
        a.migrateWarnings = [], b.console && b.console.log && b.console.log("JQMIGRATE: Migrate is installed" + (a.migrateMute ? "" : " with logging active") + ", version " + a.migrateVersion), a.migrateTrace === c && (a.migrateTrace = !0), a.migrateReset = function() { f = {}, a.migrateWarnings.length = 0 }, "BackCompat" === document.compatMode && d("jQuery is not compatible with Quirks Mode");
        var g = a("<input/>", { size: 1 }).attr("size") && a.attrFn,
            h = a.attr,
            i = a.attrHooks.value && a.attrHooks.value.get || function() { return null },
            j = a.attrHooks.value && a.attrHooks.value.set || function() { return c },
            k = /^(?:input|button)$/i,
            l = /^[238]$/,
            m = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
            n = /^(?:checked|selected)$/i;
        e(a, "attrFn", g || {}, "jQuery.attrFn is deprecated"), a.attr = function(b, e, f, i) {
            var j = e.toLowerCase(),
                o = b && b.nodeType;
            return i && (h.length < 4 && d("jQuery.fn.attr( props, pass ) is deprecated"), b && !l.test(o) && (g ? e in g : a.isFunction(a.fn[e]))) ? a(b)[e](f) : ("type" === e && f !== c && k.test(b.nodeName) && b.parentNode && d("Can't change the 'type' of an input or button in IE 6/7/8"), !a.attrHooks[j] && m.test(j) && (a.attrHooks[j] = { get: function(b, d) { var e, f = a.prop(b, d); return f === !0 || "boolean" != typeof f && (e = b.getAttributeNode(d)) && e.nodeValue !== !1 ? d.toLowerCase() : c }, set: function(b, c, d) { var e; return c === !1 ? a.removeAttr(b, d) : (e = a.propFix[d] || d, e in b && (b[e] = !0), b.setAttribute(d, d.toLowerCase())), d } }, n.test(j) && d("jQuery.fn.attr('" + j + "') might use property instead of attribute")), h.call(a, b, e, f))
        }, a.attrHooks.value = { get: function(a, b) { var c = (a.nodeName || "").toLowerCase(); return "button" === c ? i.apply(this, arguments) : ("input" !== c && "option" !== c && d("jQuery.fn.attr('value') no longer gets properties"), b in a ? a.value : null) }, set: function(a, b) { var c = (a.nodeName || "").toLowerCase(); return "button" === c ? j.apply(this, arguments) : ("input" !== c && "option" !== c && d("jQuery.fn.attr('value', val) no longer sets properties"), void(a.value = b)) } };
        var o, p, q = a.fn.init,
            r = a.find,
            s = a.parseJSON,
            t = /^\s*</,
            u = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,
            v = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g,
            w = /^([^<]*)(<[\w\W]+>)([^>]*)$/;
        a.fn.init = function(b, e, f) { var g, h; return b && "string" == typeof b && !a.isPlainObject(e) && (g = w.exec(a.trim(b))) && g[0] && (t.test(b) || d("$(html) HTML strings must start with '<' character"), g[3] && d("$(html) HTML text after last tag is ignored"), "#" === g[0].charAt(0) && (d("HTML string cannot start with a '#' character"), a.error("JQMIGRATE: Invalid selector string (XSS)")), e && e.context && e.context.nodeType && (e = e.context), a.parseHTML) ? q.call(this, a.parseHTML(g[2], e && e.ownerDocument || e || document, !0), e, f) : (h = q.apply(this, arguments), b && b.selector !== c ? (h.selector = b.selector, h.context = b.context) : (h.selector = "string" == typeof b ? b : "", b && (h.context = b.nodeType ? b : e || document)), h) }, a.fn.init.prototype = a.fn, a.find = function(a) {
            var b = Array.prototype.slice.call(arguments);
            if ("string" == typeof a && u.test(a)) try { document.querySelector(a) } catch (c) { a = a.replace(v, function(a, b, c, d) { return "[" + b + c + '"' + d + '"]' }); try { document.querySelector(a), d("Attribute selector with '#' must be quoted: " + b[0]), b[0] = a } catch (e) { d("Attribute selector with '#' was not fixed: " + b[0]) } }
            return r.apply(this, b)
        };
        var x;
        for (x in r) Object.prototype.hasOwnProperty.call(r, x) && (a.find[x] = r[x]);
        a.parseJSON = function(a) { return a ? s.apply(this, arguments) : (d("jQuery.parseJSON requires a valid JSON string"), null) }, a.uaMatch = function(a) { a = a.toLowerCase(); var b = /(chrome)[ \/]([\w.]+)/.exec(a) || /(webkit)[ \/]([\w.]+)/.exec(a) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a) || /(msie) ([\w.]+)/.exec(a) || a.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a) || []; return { browser: b[1] || "", version: b[2] || "0" } }, a.browser || (o = a.uaMatch(navigator.userAgent), p = {}, o.browser && (p[o.browser] = !0, p.version = o.version), p.chrome ? p.webkit = !0 : p.webkit && (p.safari = !0), a.browser = p), e(a, "browser", a.browser, "jQuery.browser is deprecated"), a.boxModel = a.support.boxModel = "CSS1Compat" === document.compatMode, e(a, "boxModel", a.boxModel, "jQuery.boxModel is deprecated"), e(a.support, "boxModel", a.support.boxModel, "jQuery.support.boxModel is deprecated"), a.sub = function() {
            function b(a, c) { return new b.fn.init(a, c) }
            a.extend(!0, b, this), b.superclass = this, b.fn = b.prototype = this(), b.fn.constructor = b, b.sub = this.sub, b.fn.init = function(d, e) { var f = a.fn.init.call(this, d, e, c); return f instanceof b ? f : b(f) }, b.fn.init.prototype = b.fn;
            var c = b(document);
            return d("jQuery.sub() is deprecated"), b
        }, a.fn.size = function() { return d("jQuery.fn.size() is deprecated; use the .length property"), this.length };
        var y = !1;
        a.swap && a.each(["height", "width", "reliableMarginRight"], function(b, c) {
            var d = a.cssHooks[c] && a.cssHooks[c].get;
            d && (a.cssHooks[c].get = function() { var a; return y = !0, a = d.apply(this, arguments), y = !1, a })
        }), a.swap = function(a, b, c, e) {
            var f, g, h = {};
            y || d("jQuery.swap() is undocumented and deprecated");
            for (g in b) h[g] = a.style[g], a.style[g] = b[g];
            f = c.apply(a, e || []);
            for (g in b) a.style[g] = h[g];
            return f
        }, a.ajaxSetup({ converters: { "text json": a.parseJSON } });
        var z = a.fn.data;
        a.fn.data = function(b) { var e, f, g = this[0]; return !g || "events" !== b || 1 !== arguments.length || (e = a.data(g, b), f = a._data(g, b), e !== c && e !== f || f === c) ? z.apply(this, arguments) : (d("Use of jQuery.fn.data('events') is deprecated"), f) };
        var A = /\/(java|ecma)script/i;
        a.clean || (a.clean = function(b, c, e, f) {
            c = c || document, c = !c.nodeType && c[0] || c, c = c.ownerDocument || c, d("jQuery.clean() is deprecated");
            var g, h, i, j, k = [];
            if (a.merge(k, a.buildFragment(b, c).childNodes), e)
                for (i = function(a) { return !a.type || A.test(a.type) ? f ? f.push(a.parentNode ? a.parentNode.removeChild(a) : a) : e.appendChild(a) : void 0 }, g = 0; null != (h = k[g]); g++) a.nodeName(h, "script") && i(h) || (e.appendChild(h), "undefined" != typeof h.getElementsByTagName && (j = a.grep(a.merge([], h.getElementsByTagName("script")), i), k.splice.apply(k, [g + 1, 0].concat(j)), g += j.length));
            return k
        });
        var B = a.event.add,
            C = a.event.remove,
            D = a.event.trigger,
            E = a.fn.toggle,
            F = a.fn.live,
            G = a.fn.die,
            H = a.fn.load,
            I = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
            J = new RegExp("\\b(?:" + I + ")\\b"),
            K = /(?:^|\s)hover(\.\S+|)\b/,
            L = function(b) { return "string" != typeof b || a.event.special.hover ? b : (K.test(b) && d("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"), b && b.replace(K, "mouseenter$1 mouseleave$1")) };
        a.event.props && "attrChange" !== a.event.props[0] && a.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement"), a.event.dispatch && e(a.event, "handle", a.event.dispatch, "jQuery.event.handle is undocumented and deprecated"), a.event.add = function(a, b, c, e, f) { a !== document && J.test(b) && d("AJAX events should be attached to document: " + b), B.call(this, a, L(b || ""), c, e, f) }, a.event.remove = function(a, b, c, d, e) { C.call(this, a, L(b) || "", c, d, e) }, a.each(["load", "unload", "error"], function(b, c) { a.fn[c] = function() { var a = Array.prototype.slice.call(arguments, 0); return "load" === c && "string" == typeof a[0] ? H.apply(this, a) : (d("jQuery.fn." + c + "() is deprecated"), a.splice(0, 0, c), arguments.length ? this.bind.apply(this, a) : (this.triggerHandler.apply(this, a), this)) } }), a.fn.toggle = function(b, c) {
            if (!a.isFunction(b) || !a.isFunction(c)) return E.apply(this, arguments);
            d("jQuery.fn.toggle(handler, handler...) is deprecated");
            var e = arguments,
                f = b.guid || a.guid++,
                g = 0,
                h = function(c) { var d = (a._data(this, "lastToggle" + b.guid) || 0) % g; return a._data(this, "lastToggle" + b.guid, d + 1), c.preventDefault(), e[d].apply(this, arguments) || !1 };
            for (h.guid = f; g < e.length;) e[g++].guid = f;
            return this.click(h)
        }, a.fn.live = function(b, c, e) { return d("jQuery.fn.live() is deprecated"), F ? F.apply(this, arguments) : (a(this.context).on(b, this.selector, c, e), this) }, a.fn.die = function(b, c) { return d("jQuery.fn.die() is deprecated"), G ? G.apply(this, arguments) : (a(this.context).off(b, this.selector || "**", c), this) }, a.event.trigger = function(a, b, c, e) { return c || J.test(a) || d("Global events are undocumented and deprecated"), D.call(this, a, b, c || document, e) }, a.each(I.split("|"), function(b, c) { a.event.special[c] = { setup: function() { var b = this; return b !== document && (a.event.add(document, c + "." + a.guid, function() { a.event.trigger(c, Array.prototype.slice.call(arguments, 1), b, !0) }), a._data(this, c, a.guid++)), !1 }, teardown: function() { return this !== document && a.event.remove(document, c + "." + a._data(this, c)), !1 } } }), a.event.special.ready = { setup: function() { this === document && d("'ready' event is deprecated") } };
        var M = a.fn.andSelf || a.fn.addBack,
            N = a.fn.find;
        if (a.fn.andSelf = function() { return d("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), M.apply(this, arguments) }, a.fn.find = function(a) { var b = N.apply(this, arguments); return b.context = this.context, b.selector = this.selector ? this.selector + " " + a : a, b }, a.Callbacks) {
            var O = a.Deferred,
                P = [
                    ["resolve", "done", a.Callbacks("once memory"), a.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", a.Callbacks("once memory"), a.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", a.Callbacks("memory"), a.Callbacks("memory")]
                ];
            a.Deferred = function(b) {
                var c = O(),
                    e = c.promise();
                return c.pipe = e.pipe = function() {
                    var b = arguments;
                    return d("deferred.pipe() is deprecated"), a.Deferred(function(d) {
                        a.each(P, function(f, g) {
                            var h = a.isFunction(b[f]) && b[f];
                            c[g[1]](function() {
                                var b = h && h.apply(this, arguments);
                                b && a.isFunction(b.promise) ? b.promise().done(d.resolve).fail(d.reject).progress(d.notify) : d[g[0] + "With"](this === e ? d.promise() : this, h ? [b] : arguments)
                            })
                        }), b = null
                    }).promise()
                }, c.isResolved = function() { return d("deferred.isResolved is deprecated"), "resolved" === c.state() }, c.isRejected = function() { return d("deferred.isRejected is deprecated"), "rejected" === c.state() }, b && b.call(c, c), c
            }
        }
    }(jQuery, window);
jQuery(function() {
    jQuery(":input").on("focus", function() {
        var input = jQuery(this);
        var inputID = input.attr("id") || "(no input ID)";
        var inputName = input.attr("name") || "(no input name)";
        var inputClass = input.attr("class") || "(no input class)";
        var form = jQuery(this.form);
        var formID = form.attr("id") || "(no form ID)";
        var formName = form.attr("name") || "(no form name)";
        var formClass = form.attr("class") || "(no form class)";
        window[gtm4wp_datalayer_name].push({ 'event': 'gtm4wp.formElementEnter', 'inputID': inputID, 'inputName': inputName, 'inputClass': inputClass, 'formID': formID, 'formName': formName, 'formClass': formClass });
    }).on("blur", function() {
        var input = jQuery(this);
        var inputID = input.attr("id") || "(no input ID)";
        var inputName = input.attr("name") || "(no input name)";
        var inputClass = input.attr("class") || "(no input class)";
        var form = jQuery(this.form);
        var formID = form.attr("id") || "(no form ID)";
        var formName = form.attr("name") || "(no form name)";
        var formClass = form.attr("class") || "(no form class)";
        window[gtm4wp_datalayer_name].push({ 'event': 'gtm4wp.formElementLeave', 'inputID': inputID, 'inputName': inputName, 'inputClass': inputClass, 'formID': formID, 'formName': formName, 'formClass': formClass });
    });
});
/*!
 * 
 * This file is part of the Buttonizer plugin that is downloadable through Wordpress.org,
 * please do not redistribute this plugin or the files without any written permission of the author.
 * 
 * If you need support, contact us at support@buttonizer.pro or visit our community website
 * https://community.buttonizer.pro/
 * 
 * Buttonizer is Freemium software. The free version (build) does not contain premium functionality.
 * 
 * (C) 2017-2020 Buttonizer
 * 
 */
! function(t) {
    var e = {};

    function n(o) { if (e[o]) return e[o].exports; var i = e[o] = { i: o, l: !1, exports: {} }; return t[o].call(i.exports, i, i.exports, n), i.l = !0, i.exports }
    n.m = t, n.c = e, n.d = function(t, e, o) { n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: o }) }, n.r = function(t) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 }) }, n.t = function(t, e) {
        if (1 & e && (t = n(t)), 8 & e) return t;
        if (4 & e && "object" == typeof t && t && t.__esModule) return t;
        var o = Object.create(null);
        if (n.r(o), Object.defineProperty(o, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t)
            for (var i in t) n.d(o, i, function(e) { return t[e] }.bind(null, i));
        return o
    }, n.n = function(t) { var e = t && t.__esModule ? function() { return t.default } : function() { return t }; return n.d(e, "a", e), e }, n.o = function(t, e) { return Object.prototype.hasOwnProperty.call(t, e) }, n.p = "", n(n.s = 877)
}({
    1250: function(t, e) {},
    875: function(t, e) {},
    876: function(t, e) {},
    877: function(t, e, n) {
        "use strict";

        function o(t) { return (o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) { return typeof t } : function(t) { return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t })(t) }

        function i(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)
            }
        }
        n.r(e);
        var a = function() {
            function t(e, n) {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.group = e, this.data = n, this.icon = n.icon.buttonIcon, this.show_mobile = n.device.show_mobile, this.show_desktop = n.device.show_desktop, this.style = "", this.button, this.init(), this.unique = "buttonizer-button-" + Array.apply(0, Array(15)).map((function() { return (t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").charAt(Math.floor(Math.random() * t.length)); var t })).join("") }
            var e, n, a;
            return e = t, (n = [{ key: "init", value: function() {} }, {
                key: "build",
                value: function() {
                    var t, e = this,
                        n = document.createElement("a");
                    if (n.className = "buttonizer-button", n.setAttribute("data-buttonizer", this.unique), "url" === this.data.action.type || "poptin" === this.data.action.type ? (n.href = this.data.action.action, "url" === this.data.action.type && !0 === this.data.action.action_new_tab && (n.target = "_blank")) : n.href = "javascript:void(0)", "elementor_popup" !== this.data.action.type && "popup_maker" !== this.data.action.type || (n.href = "#" + this.data.action.action), !0 === this.show_mobile && !1 === this.show_desktop ? (this.group.mobileButtonCount++, n.className += " button-mobile-" + this.group.mobileButtonCount + " button-hide-desktop") : !1 === this.show_mobile && !0 === this.show_desktop ? (this.group.desktopButtonCount++, n.className += " button-desktop-" + this.group.desktopButtonCount + " button-hide-mobile") : !1 === this.show_mobile && !1 === this.show_desktop ? n.className += " button-hide-desktop button-hide-mobile" : (this.group.mobileButtonCount++, this.group.desktopButtonCount++, n.className += " button-mobile-" + this.group.mobileButtonCount + " button-desktop-" + this.group.desktopButtonCount), "hover" === this.data.label.show_label_desktop ? n.className += " show-label-desktop-hover" : "hide" === this.data.label.show_label_desktop && (n.className += " label-desktop-hidden"), "hide" === this.data.label.show_label_mobile && (n.className += " label-mobile-hidden"), "messenger_chat" === this.data.action.type && this.addMessengerWindow(), this.data.label.label.length > 0) {
                        var i = document.createElement("div");
                        i.className = "buttonizer-label", i.innerText = this.data.label.label, n.appendChild(i)
                    } else if ("" === this.data.label.label && "rectangle" === this.group.data.styling.menu.style) {
                        var a = document.createElement("div");
                        a.className = "buttonizer-label", a.innerText = this.data.name + "'s label", n.appendChild(a)
                    }
                    return (t = document.createElement("i")).className = void 0 !== o(e.icon) ? e.icon : "fa fa-user", n.appendChild(t), n.addEventListener("click", (function(t) { return e.onButtonClick(t) })), this.generateStyle(), this.button = n, this.button
                }
            }, {
                key: "onButtonClick",
                value: function() {
                    if (window.Buttonizer.googleAnalyticsEvent({ type: "button-click", groupName: this.group.data.name, buttonName: this.data.name }), "url" !== this.data.action.type && "poptin" !== this.data.action.type)
                        if ("phone" !== this.data.action.type)
                            if ("mail" !== this.data.action.type)
                                if ("backtotop" !== this.data.action.type)
                                    if ("gotobottom" !== this.data.action.type)
                                        if ("gobackpage" !== this.data.action.type)
                                            if ("socialsharing" !== this.data.action.type)
                                                if ("whatsapp_pro" !== this.data.action.type && "whatsapp" !== this.data.action.type)
                                                    if ("skype" !== this.data.action.type)
                                                        if ("messenger" !== this.data.action.type)
                                                            if ("sms" !== this.data.action.type)
                                                                if ("telegram" !== this.data.action.type)
                                                                    if ("facebook" !== this.data.action.type)
                                                                        if ("instagram" !== this.data.action.type)
                                                                            if ("line" !== this.data.action.type)
                                                                                if ("twitter" !== this.data.action.type)
                                                                                    if ("twitter_dm" !== this.data.action.type)
                                                                                        if ("snapchat" !== this.data.action.type)
                                                                                            if ("linkedin" !== this.data.action.type)
                                                                                                if ("viber" !== this.data.action.type)
                                                                                                    if ("vk" !== this.data.action.type) { if ("popup_maker" !== this.data.action.type && "elementor_popup" !== this.data.action.type) { if ("popups" === this.data.action.type) { var t = this.data.action.action; return isNaN(t) && (t = t.replace(/\D/g, "")), void window.SPU.show(t) } "waze" !== this.data.action.type ? "wechat" !== this.data.action.type ? "clipboard" !== this.data.action.type ? "print" !== this.data.action.type ? "messenger_chat" !== this.data.action.type ? console.error("Buttonizer: Error! Unknown button action!") : void 0 !== window.Buttonizer.initializedFacebookChat && document.querySelectorAll(".fb-customerchat").length > 0 ? "0px" === document.querySelector(".fb-customerchat").querySelector("iframe").style.maxHeight ? FB.CustomerChat.showDialog() : "100%" === document.querySelector(".fb-customerchat").querySelector("iframe").style.maxHeight && FB.CustomerChat.hideDialog() : window.Buttonizer.previewInitialized ? window.Buttonizer.messageButtonizerAdminEditor("warning", "Facebook Messenger button is not found, it may be blocked or this domain is not allowed to load the Facebook widget.") : alert("Sorry, we were unable to open Facebook Messenger! Check the console for more information.") : window.print() : this.copyClipboard() : document.location.href = "weixin://dl/chat?".concat(this.data.action.action) : document.location.href = this.data.action.action } } else window.open("https://vk.me/".concat(this.data.action.action));
                    else document.location.href = "viber://chat?number=".concat(this.data.action.action);
                    else window.open("https://www.linkedin.com/".concat(this.data.action.action));
                    else window.open("https://www.snapchat.com/add/".concat(this.data.action.action));
                    else {
                        var e = "https://twitter.com/messages/compose?recipient_id=".concat(this.data.action.action).concat("" !== this.data.text.body ? "&text=" + encodeURIComponent(this.data.text.body) : "");
                        window.open(e)
                    } else window.open("https://twitter.com/".concat(this.data.action.action));
                    else window.open("https://line.me/R/ti/p/".concat(this.data.action.action));
                    else window.open("https://www.instagram.com/".concat(this.data.action.action));
                    else window.open("https://www.facebook.com/".concat(this.data.action.action));
                    else window.open("https://telegram.me/".concat(this.data.action.action));
                    else {
                        var n = "sms:".concat(this.data.action.action).concat("" !== this.data.text.body ? "?body=" + encodeURIComponent(this.data.text.body) : "");
                        document.location.href = n
                    } else window.open(this.data.action.action);
                    else document.location.href = "skype:".concat(this.data.action.action, "?chat");
                    else {
                        var o = "https://api.whatsapp.com/send?phone=".concat(this.data.action.action).concat("" !== this.data.text.body ? "&text=" + encodeURIComponent(this.data.text.body) : "");
                        window.open(o)
                    } else {
                        if ("facebook" === this.data.action.action) return window.open("http://www.facebook.com/sharer.php?u=" + document.location.href + "&t=" + document.title, "popupFacebook", "width=610, height=480, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0"), !1;
                        if ("twitter" === this.data.action.action) return window.open("https://twitter.com/intent/tweet?text=" + encodeURI(document.title) + " Hey! Check out this link:&url=" + document.location.href, "popupTwitter", "width=610, height=480, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0"), !1;
                        if ("whatsapp" === this.data.action.action) window.open("https://api.whatsapp.com/send?text=" + encodeURI(document.title + " Hey! Check out this link:" + document.location.href));
                        else {
                            if ("linkedin" === this.data.action.action) return window.open("http://www.linkedin.com/shareArticle?mini=true&url=" + document.location.href + "&title=" + encodeURI(document.title) + "&summary=" + encodeURI(document.title), "popupLinkedIn", "width=610, height=480, resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0"), !1;
                            if ("pinterest" === this.data.action.action) return window.open("http://pinterest.com/pin/create/button/?url=".concat(document.location.href)), !1;
                            if ("mail" === this.data.action.action) window.location.href = "mailto:?subject=" + encodeURI(document.title.replace(/&/g, "").trim()) + "&body= Hey! Check out this link: " + encodeURI(document.location.href.replace(/&/g, "").trim());
                            else { if ("reddit" === this.data.action.action) { var i = "https://www.reddit.com/submit?url=".concat(encodeURI("Hey! Check out this link: " + document.location.href), "&title=").concat(encodeURI(document.title)); return window.open(i), !1 } if ("tumblr" === this.data.action.action) return window.open("https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=".concat(encodeURI(document.location.href), "&posttype=link")), !1; if ("digg" === this.data.action.action) return window.open("http://digg.com/submit?url=".concat(encodeURI(document.location.href))), !1; if ("weibo" === this.data.action.action) return window.open("http://service.weibo.com/share/share.php?url=".concat(encodeURI(document.location.href), "&title=").concat(encodeURI(document.title), "&pic=https://plus.google.com/_/favicon?domain=").concat(document.location.origin)), !1; if ("vk" === this.data.action.action) return window.open("https://vk.com/share.php?url=".concat(encodeURI(document.location.href), "&title=").concat(encodeURI(document.title), "&comment=Hey%20Check%20this%20out!")), !1; if ("ok" === this.data.action.action) return window.open("https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=".concat(encodeURI(document.location.href))), !1; if ("xing" === this.data.action.action) return window.open("https://www.xing.com/spi/shares/new?url=".concat(encodeURI(document.location.href))), !1; if ("blogger" === this.data.action.action) return window.open("https://www.blogger.com/blog-this.g?u=".concat(encodeURI(document.location.href), "&n=").concat(encodeURI(document.title), "&t=Check%20this%20out!")), !1; if ("flipboard" === this.data.action.action) return window.open("https://share.flipboard.com/bookmarklet/popout?v=2&title=".concat(encodeURI(document.title), "&url=").concat(encodeURI(document.location.href))), !1; if ("sms" === this.data.action.action) return window.open("sms:?body=".concat(encodeURI(document.title + "Hey! Check out this link: " + document.location.href))), !1 }
                        }
                    } else window.history.back();
                    else jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, 750);
                    else jQuery("html, body").animate({ scrollTop: 0 }, 750);
                    else {
                        var a = "mailto:".concat(this.data.action.action, "?subject=").concat(encodeURIComponent(this.data.text.subject || "Subject"), "&body=").concat(encodeURIComponent(this.data.text.body));
                        document.location.href = a
                    } else document.location.href = "tel:".concat(this.data.action.action)
                }
            }, {
                key: "generateStyle",
                value: function() {
                    if (!this.data.styling.main_style) {
                        var t, e;
                        if ("12px" === this.data.styling.label.size && "3px" === this.data.styling.label.radius || (t = "px" === this.data.styling.label.size ? "px" === this.group.data.styling.label.size ? "12px" : this.group.data.styling.label.size : this.data.styling.label.size, e = "px" === this.data.styling.label.radius ? "px" === this.group.data.styling.label.radius ? "3px" : this.group.data.styling.label.radius : this.data.styling.label.radius, this.group.stylesheet += '\n                  [data-buttonizer="'.concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"] .buttonizer-label {\n                      font-size: ').concat(t, ";\n                      border-radius: ").concat(e, ";\n                      -moz-border-radius: ").concat(e, ";\n                      -webkit-border-radius: ").concat(e, ";\n                  }\n              ")), this.data.styling.button) { var n = ""; "%" !== this.data.styling.button.radius ? n += "\n                  border-radius: ".concat(this.data.styling.button.radius, ";\n                  ") : n += "\n                  border-radius: 50%;\n                  ", this.group.stylesheet += '\n              [data-buttonizer="'.concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"] {\n                  background-color: ').concat(this.data.styling.button.color, ";\n                  ").concat("00" === this.data.styling.button.color.substr(-2) ? "box-shadow: none;" : "", ";\n                  ").concat(n, '\n              }\n              \n              [data-buttonizer="').concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"]:hover {\n                  background-color: ').concat(this.data.styling.button.interaction, '\n              }\n              \n              [data-buttonizer="').concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"] .buttonizer-label {\n                  background-color: ').concat(this.data.styling.label.background, ";\n                  color: ").concat(this.data.styling.label.text, " !important;\n              }") }
                        this.data.styling.button && (this.group.stylesheet += '\n              [data-buttonizer="'.concat(this.group.unique, '"].attention-animation-true.buttonizer-animation-pulse.buttonizer-desktop-has-1.buttonizer-mobile-has-1 [data-buttonizer="').concat(this.unique, '"]:before, \n              [data-buttonizer="').concat(this.group.unique, '"].attention-animation-true.buttonizer-animation-pulse.buttonizer-desktop-has-1.buttonizer-mobile-has-1 [data-buttonizer="').concat(this.unique, '"]:after {\n                  background-color: ').concat(this.data.styling.button.color, ";\n              }\n          ")), this.group.stylesheet += '\n        [data-buttonizer="'.concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"] i {\n            color: ').concat(this.data.styling.icon.color, ';\n        }\n        \n        [data-buttonizer="').concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"]:hover i{\n          color: ').concat(this.data.styling.icon.interaction, ";\n\n          transition: all 0.2s ease-in-out;\n          -moz-transition: all 0.2s ease-in-out;\n          -webkit-transition: all 0.2s ease-in-out;\n          -o-transition: all 0.2s ease-in-out;\n        }\n      ")
                    }
                    this.group.stylesheet += '\n        [data-buttonizer="'.concat(this.group.unique, '"] [data-buttonizer="').concat(this.unique, '"] i {\n            font-size: ').concat(this.data.styling.icon.size, ";\n\n            transition: all 0.2s ease-in-out;\n            -moz-transition: all 0.2s ease-in-out;\n            -webkit-transition: all 0.2s ease-in-out;\n            -o-transition: all 0.2s ease-in-out;\n        }\n        ")
                }
            }, { key: "destroy", value: function() { this.button.remove(), this.group.removeButton(this), this.group = null, this.data = null, this.button = null } }, {
                key: "addMessengerWindow",
                value: function() {
                    if (void 0 === window.Buttonizer.initializedFacebookChat) {
                        window.Buttonizer.initializedFacebookChat = "" === this.data.action.action ? void 0 : this.data.action.action, window.fbAsyncInit = function() { FB.init({ xfbml: !0, version: "v3.3" }) };
                        var t = document.createElement("script");
                        t.innerHTML = "\n             (function(d, s, id) {\n              var js, fjs = d.getElementsByTagName(s)[0];\n              if (d.getElementById(id)) return;\n              js = d.createElement(s); js.id = id;\n              js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';\n              fjs.parentNode.insertBefore(js, fjs);\n            }(document, 'script', 'facebook-jssdk'));", document.head.appendChild(t)
                    }
                }
            }, {
                key: "copyClipboard",
                value: function() {
                    var t = document.createElement("input"),
                        e = window.location.href;
                    document.body.appendChild(t), t.value = e, t.select(), document.execCommand("copy"), document.body.removeChild(t);
                    var n = document.createElement("div");
                    n.className = "buttonizer-label buttonizer-label-popup", n.innerText = "Copied!", this.button.appendChild(n), setTimeout((function() { n.remove() }), 2e3)
                }
            }]) && i(e.prototype, n), a && i(e, a), t
        }();

        function s(t) {
            if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
                if (Array.isArray(t) || (t = function(t, e) { if (!t) return; if ("string" == typeof t) return r(t, e); var n = Object.prototype.toString.call(t).slice(8, -1); "Object" === n && t.constructor && (n = t.constructor.name); if ("Map" === n || "Set" === n) return Array.from(n); if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return r(t, e) }(t))) {
                    var e = 0,
                        n = function() {};
                    return { s: n, n: function() { return e >= t.length ? { done: !0 } : { done: !1, value: t[e++] } }, e: function(t) { throw t }, f: n }
                }
                throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
            }
            var o, i, a = !0,
                s = !1;
            return { s: function() { o = t[Symbol.iterator]() }, n: function() { var t = o.next(); return a = t.done, t }, e: function(t) { s = !0, i = t }, f: function() { try { a || null == o.return || o.return() } finally { if (s) throw i } } }
        }

        function r(t, e) {
            (null == e || e > t.length) && (e = t.length);
            for (var n = 0, o = new Array(e); n < e; n++) o[n] = t[n];
            return o
        }

        function c(t) { return (c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) { return typeof t } : function(t) { return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t })(t) }

        function u(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)
            }
        }
        var l = function() {
            function t(e, n) {! function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.data = e, this.groupIndex = n, this.buttons = [], this.isBuild = !1, this.opened = !1, this.usesExitIntent = !1, this.exitIntentExecuted = !1, this.usesOnSroll = !1, this.show_mobile = this.data.device.show_mobile, this.show_desktop = this.data.device.show_desktop, this.single_button_mode = this.data.single_button_mode, this.element = null, this.groupStyle = this.data.styling.menu.style, this.groupAnimation = this.data.styling.menu.animation, this.stylesheet = "", this.mobileButtonCount = 0, this.desktopButtonCount = 0, this.unique = "buttonizer-" + Array.apply(0, Array(15)).map((function() { return (t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").charAt(Math.floor(Math.random() * t.length)); var t })).join(""), this.init() }
            var e, n, o;
            return e = t, (n = [{
                key: "init",
                value: function() {
                    for (var t = 0; t < this.data.buttons.length; t++) {
                        var e = new a(this, this.data.buttons[t]);
                        this.buttons.push(e)
                    }
                    this.build(), this.animate()
                }
            }, {
                key: "build",
                value: function() {
                    var t = this;
                    if (!0 !== this.isBuild && 0 !== this.buttons.length) {
                        "fade-left-to-right" === this.groupStyle && (this.groupStyle = "faded");
                        var e = document.createElement("div");
                        if (e.className = "buttonizer buttonizer-group buttonizer-style-".concat(this.groupStyle, " buttonizer-animation-").concat(this.groupAnimation, " ").concat(this.data.position.bottom <= 50 ? "bottom" : "top", " ").concat(this.data.position.right <= 50 ? "right" : "left"), e.setAttribute("data-buttonizer", this.unique), !0 === this.show_mobile && !1 === this.show_desktop ? e.className += " button-mobile-only" : !1 === this.show_mobile && !0 === this.show_desktop ? e.className += " button-desktop-only" : !1 === this.show_mobile && !1 === this.show_desktop && (e.className += " button-hide"), this.buttons.length > 1) {
                            e.classList.add("buttonizer-is-menu"), !0 === this.data.openedByDefault && (document.cookie.match("buttonizer_open=") ? document.cookie.match("buttonizer_open=closed") || document.cookie.match("buttonizer_open=opened") && (e.classList.add("opened"), this.opened = !0) : (setTimeout((function() { e.classList.add("opened") }), 100), this.opened = !this.opened)), "1" === buttonizer_ajax.in_preview && document.cookie.match("buttonizer_preview_" + this.groupIndex + "=opened") && (this.opened = !0, e.classList.add("opened")), "square" !== this.data.styling.menu.style && "rectangle" !== this.data.styling.menu.style || e.classList.add("opened");
                            var n = document.createElement("div");
                            n.className = "buttonizer-button-list";
                            var o = document.createElement("a");
                            o.href = "javascript:void(0)", o.className = "buttonizer-button buttonizer-head", "hover" === this.data.label.show_label_desktop ? o.className += " show-label-desktop-hover" : "hide" === this.data.label.show_label_desktop && (o.className += " label-desktop-hidden"), "hide" === this.data.label.show_label_mobile && (o.className += " label-mobile-hidden");
                            var i, a = '<div class="buttonizer-label">'.concat(this.data.label.groupLabel, "</div>");
                            i = '<i class="'.concat(void 0 !== c(this.data.icon.groupIcon) ? this.data.icon.groupIcon : "fa fa-plus", '"></i>'), this.data.label.groupLabel.length <= 0 ? o.innerHTML = i : o.innerHTML = a + i, o.addEventListener("click", (function(e) { return t.toggleMenu(e) })), e.appendChild(n), e.appendChild(o);
                            for (var s = 0; s < this.buttons.length; s++) {
                                if ("messenger_chat" === this.buttons[s].data.action.type) {
                                    var r = document.createElement("div");
                                    r.className = "fb-customerchat buttonizer-facebook-messenger-overwrite-".concat(this.unique), r.setAttribute("page-id", "".concat(this.buttons[s].data.action.action)), r.setAttribute("greeting_dialog_display", "hide"), e.appendChild(r)
                                }
                                n.appendChild(this.buttons[s].build())
                            }
                        } else if (e.appendChild(this.buttons[0].build()), "messenger_chat" === this.buttons[0].data.action.type) {
                            var u = document.createElement("div");
                            u.className = "fb-customerchat buttonizer-facebook-messenger-overwrite-".concat(this.unique), u.setAttribute("page-id", "".concat(this.buttons[0].data.action.action)), u.setAttribute("greeting_dialog_display", "hide"), e.appendChild(u)
                        }
                        e.className += " buttonizer-desktop-has-".concat(this.desktopButtonCount, " buttonizer-mobile-has-").concat(this.mobileButtonCount), this.element = e, document.body.appendChild(this.element), this.isBuild = !0, this.writeCSS(), "rectangle" === this.data.styling.menu.style && this.maxLabelWidth(e, "rectangle")
                    }
                }
            }, { key: "toggleMenu", value: function(t) { t.preventDefault(), window.Buttonizer.googleAnalyticsEvent({ type: "group-open-close", name: this.data.name, interaction: this.opened ? "close" : "open" }), this.opened ? this.element.classList.remove("opened") : this.element.classList.add("opened"), this.opened = !this.opened, !0 === this.data.openedByDefault && (document.cookie.match("buttonizer_open=") ? document.cookie.match("buttonizer_open=closed") ? document.cookie = "buttonizer_open=opened" : document.cookie.match("buttonizer_open=opened") && (document.cookie = "buttonizer_open=closed") : document.cookie = "buttonizer_open=closed"), "1" === buttonizer_ajax.in_preview && this.togglePreviewOpened() } }, { key: "togglePreviewOpened", value: function() { this.opened ? document.cookie = "buttonizer_preview_" + this.groupIndex + "=opened" : document.cookie = "buttonizer_preview_" + this.groupIndex + "=closed" } }, {
                key: "writeCSS",
                value: function() {
                    var t = document.createElement("style");
                    t.id = this.unique;
                    var e = '            \n            [data-buttonizer="'.concat(this.unique, '"] {\n                ').concat(this.data.position.right <= 50 ? "right" : "left", ": ").concat(this.data.position.right <= 50 ? this.data.position.right : 100 - this.data.position.right, "%;\n                ").concat(this.data.position.bottom <= 50 ? "bottom" : "top", ": ").concat(this.data.position.bottom <= 50 ? this.data.position.bottom : 100 - this.data.position.bottom, '%;\n            }\n            \n            [data-buttonizer="').concat(this.unique, '"] .buttonizer-button {\n                background-color: ').concat(this.data.styling.button.color, ";\n                color: ").concat(this.data.styling.icon.color, ";\n                border-radius: ").concat(this.data.styling.button.radius, ";\n                ").concat("0" === this.data.styling.button.color.replace(/\D/g, "").substr(-1) ? "box-shadow: none;" : "", '\n            }\n            \n            [data-buttonizer="').concat(this.unique, '"] .buttonizer-button:hover {\n                background-color: ').concat(this.data.styling.button.interaction, ";\n                color: ").concat(this.data.styling.icon.color, '\n            }\n            \n            [data-buttonizer="').concat(this.unique, '"] .buttonizer-button .buttonizer-label {\n                background-color: ').concat(this.data.styling.label.background, ";\n                color: ").concat(this.data.styling.label.text, ";\n                font-size: ").concat(this.data.styling.label.size, ";\n                border-radius: ").concat(this.data.styling.label.radius, ';\n            }\n\n            [data-buttonizer="').concat(this.unique, '"] .buttonizer-button i {\n                color: ').concat(this.data.styling.icon.color, ";\n                font-size: ").concat(this.data.styling.icon.size, ';\n            }\n\n            [data-buttonizer="').concat(this.unique, '"] .buttonizer-button:hover i {\n                color: ').concat(this.data.styling.icon.interaction, ';\n            }\n            \n            [data-buttonizer="').concat(this.unique, '"].attention-animation-true.buttonizer-animation-pulse .buttonizer-head:before, \n            [data-buttonizer="').concat(this.unique, '"].attention-animation-true.buttonizer-animation-pulse .buttonizer-head:after {\n                background-color: ').concat(this.data.styling.button.color, ";\n            }\n        ");
                    1 === this.data.buttons.length && "pulse" === this.groupAnimation && (e += '\n                [data-buttonizer="'.concat(this.unique, '"].attention-animation-true.buttonizer-animation-pulse > .buttonizer-button.button-desktop-1:before, \n                [data-buttonizer="').concat(this.unique, '"].attention-animation-true.buttonizer-animation-pulse > .buttonizer-button.button-desktop-1:after {\n                    background-color: ').concat(this.data.styling.button.color, ';\n                }\n\n                [data-buttonizer="').concat(this.unique, '"].attention-animation-true.buttonizer-animation-pulse > .buttonizer-button.button-mobile-1:before, \n                [data-buttonizer="').concat(this.unique, '"].attention-animation-true.buttonizer-animation-pulse > .buttonizer-button.button-mobile-1:after {\n                    background-color: ').concat(this.data.styling.button.color, ";\n                }\n            ")), void 0 !== window.Buttonizer.initializedFacebookChat && (e += "\n                .fb_dialog {\n                    display: none !important;\n                }\n\n                .buttonizer-facebook-messenger-overwrite-".concat(this.unique, " span iframe {\n                    ").concat(this.data.position.right <= 50 ? "right" : "left", ": ").concat(this.data.position.right <= 50 ? +this.data.position.right - 1 : 100 - this.data.position.right - 1, "% !important;\n                    ").concat(this.data.position.bottom <= 50 ? "bottom" : "top", ": ").concat(this.data.position.bottom <= 50 ? +this.data.position.bottom + 4 : 100 - this.data.position.bottom + 6, "% !important;\n                }\n\n                @media screen and (max-width: 769px){\n                    .buttonizer-facebook-messenger-overwrite-").concat(this.unique, " span iframe {\n                        ").concat(this.data.position.right <= 50 ? "right" : "left", ": ").concat(this.data.position.right <= 50 ? +this.data.position.right - 5 : 100 - this.data.position.right - 1, "% !important;\n                        ").concat(this.data.position.bottom <= 50 ? "bottom" : "top", ": ").concat(this.data.position.bottom <= 50 ? +this.data.position.bottom + 4 : 100 - this.data.position.bottom + 7, "% !important;\n    \n                    }\n                }\n\n                .buttonizer-facebook-messenger-overwrite-").concat(this.unique, " span .fb_customer_chat_bounce_in_v2 {\n                    animation-duration: 300ms;\n                    animation-name: fb_bounce_in_v3 !important;\n                    transition-timing-function: ease-in-out;   \n                }\n\n                .buttonizer-facebook-messenger-overwrite-").concat(this.unique, " span .fb_customer_chat_bounce_out_v2 {\n                    max-height: 0px !important;\n                }\n\n                @keyframes fb_bounce_in_v3 {\n                    0% {\n                        opacity: 0;\n                        transform: scale(0, 0);\n                        transform-origin: bottom;\n                    }\n                    50% {\n                        transform: scale(1.03, 1.03);\n                        transform-origin: bottom;\n                    }\n                    100% {\n                        opacity: 1;\n                        transform: scale(1, 1);\n                        transform-origin: bottom;\n                    }\n                }\n            ")), e += this.stylesheet, t.innerHTML = e, document.head.appendChild(t)
                }
            }, {
                key: "animate",
                value: function() {
                    var t = this;
                    null !== this.element && "none" !== this.groupAnimation && (this.element.classList.contains("opened") || (this.element.classList.add("attention-animation-true"), setTimeout((function() { null !== t.element && t.element.classList.remove("attention-animation-true") }), 2e3)), setTimeout((function() { return t.animate() }), 1e4))
                }
            }, {
                key: "destroy",
                value: function() {
                    for (var t = 0; t < this.buttons.length; t++) this.buttons[t].destroy();
                    this.element.remove(), this.element = null, document.getElementById(this.unique).remove(), window.Buttonizer.destroyGroup(this)
                }
            }, {
                key: "removeButton",
                value: function(t) {
                    var e = this.buttons.indexOf(t);
                    e >= 0 && this.buttons.splice(e, 1)
                }
            }, {
                key: "maxLabelWidth",
                value: function(t, e) {
                    var n, o = [],
                        i = s(t.querySelectorAll(".buttonizer-label"));
                    try {
                        for (i.s(); !(n = i.n()).done;) {
                            var a = n.value;
                            o.push(a.offsetWidth)
                        }
                    } catch (t) { i.e(t) } finally { i.f() }
                    var r = Math.max.apply(Math, o),
                        c = '\n                [data-buttonizer="'.concat(this.unique, '"].buttonizer-style-').concat(e, " .buttonizer-button .buttonizer-label {\n                        min-width: ").concat(r, "px;\n                        text-align: ").concat(this.data.position.right <= 50 ? "right" : "left", ";\n                }\n            ");
                    document.getElementById(this.unique).innerHTML += c
                }
            }]) && u(e.prototype, n), o && u(e, o), t
        }();
        n(1250), n(875), n(876);

        function d(t) { return (d = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) { return typeof t } : function(t) { return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t })(t) }

        function h(t, e) {
            for (var n = 0; n < e.length; n++) {
                var o = e[n];
                o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)
            }
        }
        var p = function() {
            function t() {
                if (function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") }(this, t), this.firstTimeInitialize = !0, this.write = HTMLElement, this.previewInitialized = !1, this.settingsLoading = !1, this.isInPreviewContainer = !1, this.premium = !1, this.groups = [], this.exitIntent = null, this.onscroll = null, this.settings = { ga: null }, buttonizer_ajax.in_preview) {
                    var e = document.createElement("style");
                    e.innerHTML = "html { margin-top: 0 !important; }", window.document.head.appendChild(e)
                }
            }
            var e, n, o;
            return e = t, (n = [{ key: "getSettings", value: function() { var t = this; "undefined" == typeof buttonizer_data ? (buttonizer_ajax.current.url = document.location.href, this.settingsLoading = !0, jQuery.ajax({ url: buttonizer_ajax.ajaxurl + "?action=buttonizer", dataType: "json", data: { qpu: buttonizer_ajax.is_admin ? Date.now() : buttonizer_ajax.cache, preview: buttonizer_ajax.in_preview ? 1 : 0, data: buttonizer_ajax.current }, method: "get", success: function(e) { "success" === e.status ? t.init(e) : console.error("Buttonizer: Something went wrong! Buttonizer not loaded", e) }, error: function(e) { t.settingsLoading = !1, console.error("Buttonizer: OH NO! ERROR: '" + e.statusText + "'. That's all we know... Please check your PHP logs or contact Buttonizer support if you need help."), console.error("Buttonizer: Visit our community on https://community.buttonizer.pro/") } })) : this.init(buttonizer_data) } }, {
                key: "init",
                value: function(t) {
                    var e = this;
                    buttonizer_ajax.in_preview && !this.previewInitialized && (this.isInPreviewContainer = !0, this.listenToPreview(), window.onerror = function() {
                        var t = arguments.length <= 4 ? void 0 : arguments[4];
                        e.messageButtonizerAdminEditor("error", { name: t.name, message: t.message, column: t.column, line: t.line, sourceURL: t.sourceURL, stack: t.stack })
                    }), "" === buttonizer_ajax.in_preview && -1 !== document.cookie.indexOf("buttonizer_preview_") && (document.cookie = document.cookie.substring(document.cookie.indexOf("buttonizer_preview_")).substring(0, document.cookie.substring(document.cookie.indexOf("buttonizer_preview_")).indexOf("=")) + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"), t.result.length > 0 && (e.groups.push(new l(t.result[0], 0)), this.firstTimeInitialize && this.buttonizerInitialized()), buttonizer_ajax.in_preview && this.previewInitialized && (this.messageButtonizerAdminEditor("(re)loaded", !0), this.messageButtonizerAdminEditor("warning", t.warning)), this.settingsLoading = !1
                }
            }, { key: "messageButtonizerAdminEditor", value: function(t, e) { try { window.parent.postMessage({ eventType: "buttonizer", messageType: t, message: e }, document.location.origin) } catch (t) { console.error("Buttonizer tried to warn you in the front-end editor. But the message didn't came through. Well. Doesn't matter, it's just an extra function. It's nice to have."), console.error(t) } } }, {
                key: "listenToPreview",
                value: function() {
                    var t = this;
                    this.previewInitialized = !0, window.addEventListener("message", (function(e) { e.isTrusted && void 0 !== e.data.eventType && "buttonizer" === e.data.eventType && (console.log("[Buttonizer] Buttonizer preview - Data received:", e.data.messageType), buttonizer_ajax.in_preview && "preview-reload" === e.data.messageType && t.reload()) }), !1)
                }
            }, {
                key: "reload",
                value: function() {
                    var t = this;
                    if (this.settingsLoading) return console.log("[Buttonizer] Request too quick, first finish the previous one"), void setTimeout((function() { return t.reload() }), 100);
                    this.settingsLoading = !0;
                    for (var e = 0; e < this.groups.length; e++) this.groups[e].destroy();
                    for (var n = document.querySelectorAll(".buttonizer-group"), o = 0; o < n.length; o++) n[o].remove();
                    setTimeout((function() { t.groups = [], t.getSettings() }), 50)
                }
            }, {
                key: "googleAnalyticsEvent",
                value: function(t) {
                    if ("function" == typeof ga || "function" == typeof gtag || "object" === d(window.dataLayer) && "function" == typeof window.dataLayer.push) {
                        var e = {};
                        if ("group-open-close" === t.type ? (e.groupName = t.name, e.action = t.interaction) : "button-click" === t.type && (e.groupName = t.groupName, e.action = "Clicked button: " + t.buttonName), "gtag" in window && "function" == typeof gtag) gtag("event", "Buttonizer", { event_category: "Buttonizer group: " + e.groupName, event_action: e.action, event_label: document.title, page_url: document.location.href });
                        else if ("ga" in window) try {
                            var n = ga.getAll()[0];
                            if (!n) throw "No tracker found";
                            n.send("event", "Buttonizer group: " + e.groupName, e.action, document.title)
                        } catch (t) { console.error("Buttonizer Google Analytics: Last try to push to Google Analytics."), console.error("What does this mean?", "https://community.buttonizer.pro/knowledgebase/17"), ga("send", "event", { eventCategory: "Buttonizer group: " + e.groupName, eventAction: e.action, eventLabel: document.title }) } else console.error("Buttonizer Google Analytics: Unable to push data to Google Analytics"), console.error("What does this mean?", "https://community.buttonizer.pro/knowledgebase/17")
                    }
                }
            }, {
                key: "getCookie",
                value: function(t) {
                    for (var e = t + "=", n = decodeURIComponent(document.cookie).split(";"), o = 0; o < n.length; o++) {
                        for (var i = n[o];
                            " " == i.charAt(0);) i = i.substring(1);
                        if (0 == i.indexOf(e)) return i.substring(e.length, i.length)
                    }
                    return ""
                }
            }, {
                key: "destroyGroup",
                value: function(t) {
                    var e = this.groups.indexOf(t);
                    e >= 0 && this.groups.splice(e, 1)
                }
            }, { key: "hasPremium", value: function() { return this.premium } }, {
                key: "buttonizerInitialized",
                value: function() {
                    if (this.firstTimeInitialize) {
                        if ("function" == typeof window.buttonizerInitialized && window.buttonizerInitialized(), "undefined" != typeof FB || void 0 === this.initializedFacebookChat || this.isInPreviewContainer) { if (void 0 !== this.initializedFacebookChat && !this.isInPreviewContainer && document.querySelector(".fb-customerchat") && null === document.querySelector(".fb-customerchat").querySelector("iframe")) try { FB.XFBML.parse() } catch (t) { console.log("FB is defined but not rendering Messenger chat. \n              Is tracking blocked in your browser?\n              Do you have another Facebook SDK on your site?\n              \n              Error message: ".concat(t)) } } else { console.log("Facebook Messenger is still not initilized: RUN FB.XFBLM.PARSE"); try { FB.XFBML.parse() } catch (t) { console.log("FB is not defined. \n        Is your site whitelisted correctly?\n        Is your Facebook Messenger ID correct?") } }
                        this.firstTimeInitialize = !1
                    }
                }
            }, { key: "inPreview", value: function() { return this.isInPreviewContainer } }]) && h(e.prototype, n), o && h(e, o), t
        }();
        window.Buttonizer = new p, window.Buttonizer.getSettings()
    }
});
/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function(a) { "use strict"; var b = a.fn.jquery.split(" ")[0].split("."); if (b[0] < 2 && b[1] < 9 || 1 == b[0] && 9 == b[1] && b[2] < 1 || b[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4") }(jQuery), + function(a) {
    "use strict";

    function b() {
        var a = document.createElement("bootstrap"),
            b = { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd otransitionend", transition: "transitionend" };
        for (var c in b)
            if (void 0 !== a.style[c]) return { end: b[c] };
        return !1
    }
    a.fn.emulateTransitionEnd = function(b) {
        var c = !1,
            d = this;
        a(this).one("bsTransitionEnd", function() { c = !0 });
        var e = function() { c || a(d).trigger(a.support.transition.end) };
        return setTimeout(e, b), this
    }, a(function() { a.support.transition = b(), a.support.transition && (a.event.special.bsTransitionEnd = { bindType: a.support.transition.end, delegateType: a.support.transition.end, handle: function(b) { if (a(b.target).is(this)) return b.handleObj.handler.apply(this, arguments) } }) })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var c = a(this),
                e = c.data("bs.alert");
            e || c.data("bs.alert", e = new d(this)), "string" == typeof b && e[b].call(c)
        })
    }
    var c = '[data-dismiss="alert"]',
        d = function(b) { a(b).on("click", c, this.close) };
    d.VERSION = "3.3.7", d.TRANSITION_DURATION = 150, d.prototype.close = function(b) {
        function c() { g.detach().trigger("closed.bs.alert").remove() }
        var e = a(this),
            f = e.attr("data-target");
        f || (f = e.attr("href"), f = f && f.replace(/.*(?=#[^\s]*$)/, ""));
        var g = a("#" === f ? [] : f);
        b && b.preventDefault(), g.length || (g = e.closest(".alert")), g.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (g.removeClass("in"), a.support.transition && g.hasClass("fade") ? g.one("bsTransitionEnd", c).emulateTransitionEnd(d.TRANSITION_DURATION) : c())
    };
    var e = a.fn.alert;
    a.fn.alert = b, a.fn.alert.Constructor = d, a.fn.alert.noConflict = function() { return a.fn.alert = e, this }, a(document).on("click.bs.alert.data-api", c, d.prototype.close)
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.button"),
                f = "object" == typeof b && b;
            e || d.data("bs.button", e = new c(this, f)), "toggle" == b ? e.toggle() : b && e.setState(b)
        })
    }
    var c = function(b, d) { this.$element = a(b), this.options = a.extend({}, c.DEFAULTS, d), this.isLoading = !1 };
    c.VERSION = "3.3.7", c.DEFAULTS = { loadingText: "loading..." }, c.prototype.setState = function(b) {
        var c = "disabled",
            d = this.$element,
            e = d.is("input") ? "val" : "html",
            f = d.data();
        b += "Text", null == f.resetText && d.data("resetText", d[e]()), setTimeout(a.proxy(function() { d[e](null == f[b] ? this.options[b] : f[b]), "loadingText" == b ? (this.isLoading = !0, d.addClass(c).attr(c, c).prop(c, !0)) : this.isLoading && (this.isLoading = !1, d.removeClass(c).removeAttr(c).prop(c, !1)) }, this), 0)
    }, c.prototype.toggle = function() {
        var a = !0,
            b = this.$element.closest('[data-toggle="buttons"]');
        if (b.length) { var c = this.$element.find("input"); "radio" == c.prop("type") ? (c.prop("checked") && (a = !1), b.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == c.prop("type") && (c.prop("checked") !== this.$element.hasClass("active") && (a = !1), this.$element.toggleClass("active")), c.prop("checked", this.$element.hasClass("active")), a && c.trigger("change") } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var d = a.fn.button;
    a.fn.button = b, a.fn.button.Constructor = c, a.fn.button.noConflict = function() { return a.fn.button = d, this }, a(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(c) {
        var d = a(c.target).closest(".btn");
        b.call(d, "toggle"), a(c.target).is('input[type="radio"], input[type="checkbox"]') || (c.preventDefault(), d.is("input,button") ? d.trigger("focus") : d.find("input:visible,button:visible").first().trigger("focus"))
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(b) { a(b.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(b.type)) })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.carousel"),
                f = a.extend({}, c.DEFAULTS, d.data(), "object" == typeof b && b),
                g = "string" == typeof b ? b : f.slide;
            e || d.data("bs.carousel", e = new c(this, f)), "number" == typeof b ? e.to(b) : g ? e[g]() : f.interval && e.pause().cycle()
        })
    }
    var c = function(b, c) { this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), this.options = c, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", a.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", a.proxy(this.pause, this)).on("mouseleave.bs.carousel", a.proxy(this.cycle, this)) };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 600, c.DEFAULTS = { interval: 5e3, pause: "hover", wrap: !0, keyboard: !0 }, c.prototype.keydown = function(a) {
        if (!/input|textarea/i.test(a.target.tagName)) {
            switch (a.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            a.preventDefault()
        }
    }, c.prototype.cycle = function(b) { return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), this }, c.prototype.getItemIndex = function(a) { return this.$items = a.parent().children(".item"), this.$items.index(a || this.$active) }, c.prototype.getItemForDirection = function(a, b) {
        var c = this.getItemIndex(b),
            d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1;
        if (d && !this.options.wrap) return b;
        var e = "prev" == a ? -1 : 1,
            f = (c + e) % this.$items.length;
        return this.$items.eq(f)
    }, c.prototype.to = function(a) {
        var b = this,
            c = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        if (!(a > this.$items.length - 1 || a < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function() { b.to(a) }) : c == a ? this.pause().cycle() : this.slide(a > c ? "next" : "prev", this.$items.eq(a))
    }, c.prototype.pause = function(b) { return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this }, c.prototype.next = function() { if (!this.sliding) return this.slide("next") }, c.prototype.prev = function() { if (!this.sliding) return this.slide("prev") }, c.prototype.slide = function(b, d) {
        var e = this.$element.find(".item.active"),
            f = d || this.getItemForDirection(b, e),
            g = this.interval,
            h = "next" == b ? "left" : "right",
            i = this;
        if (f.hasClass("active")) return this.sliding = !1;
        var j = f[0],
            k = a.Event("slide.bs.carousel", { relatedTarget: j, direction: h });
        if (this.$element.trigger(k), !k.isDefaultPrevented()) {
            if (this.sliding = !0, g && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var l = a(this.$indicators.children()[this.getItemIndex(f)]);
                l && l.addClass("active")
            }
            var m = a.Event("slid.bs.carousel", { relatedTarget: j, direction: h });
            return a.support.transition && this.$element.hasClass("slide") ? (f.addClass(b), f[0].offsetWidth, e.addClass(h), f.addClass(h), e.one("bsTransitionEnd", function() { f.removeClass([b, h].join(" ")).addClass("active"), e.removeClass(["active", h].join(" ")), i.sliding = !1, setTimeout(function() { i.$element.trigger(m) }, 0) }).emulateTransitionEnd(c.TRANSITION_DURATION)) : (e.removeClass("active"), f.addClass("active"), this.sliding = !1, this.$element.trigger(m)), g && this.cycle(), this
        }
    };
    var d = a.fn.carousel;
    a.fn.carousel = b, a.fn.carousel.Constructor = c, a.fn.carousel.noConflict = function() { return a.fn.carousel = d, this };
    var e = function(c) {
        var d, e = a(this),
            f = a(e.attr("data-target") || (d = e.attr("href")) && d.replace(/.*(?=#[^\s]+$)/, ""));
        if (f.hasClass("carousel")) {
            var g = a.extend({}, f.data(), e.data()),
                h = e.attr("data-slide-to");
            h && (g.interval = !1), b.call(f, g), h && f.data("bs.carousel").to(h), c.preventDefault()
        }
    };
    a(document).on("click.bs.carousel.data-api", "[data-slide]", e).on("click.bs.carousel.data-api", "[data-slide-to]", e), a(window).on("load", function() {
        a('[data-ride="carousel"]').each(function() {
            var c = a(this);
            b.call(c, c.data())
        })
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) { var c, d = b.attr("data-target") || (c = b.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, ""); return a(d) }

    function c(b) {
        return this.each(function() {
            var c = a(this),
                e = c.data("bs.collapse"),
                f = a.extend({}, d.DEFAULTS, c.data(), "object" == typeof b && b);
            !e && f.toggle && /show|hide/.test(b) && (f.toggle = !1), e || c.data("bs.collapse", e = new d(this, f)), "string" == typeof b && e[b]()
        })
    }
    var d = function(b, c) { this.$element = a(b), this.options = a.extend({}, d.DEFAULTS, c), this.$trigger = a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle() };
    d.VERSION = "3.3.7", d.TRANSITION_DURATION = 350, d.DEFAULTS = { toggle: !0 }, d.prototype.dimension = function() { var a = this.$element.hasClass("width"); return a ? "width" : "height" }, d.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var b, e = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(e && e.length && (b = e.data("bs.collapse"), b && b.transitioning))) {
                var f = a.Event("show.bs.collapse");
                if (this.$element.trigger(f), !f.isDefaultPrevented()) {
                    e && e.length && (c.call(e, "hide"), b || e.data("bs.collapse", null));
                    var g = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var h = function() { this.$element.removeClass("collapsing").addClass("collapse in")[g](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse") };
                    if (!a.support.transition) return h.call(this);
                    var i = a.camelCase(["scroll", g].join("-"));
                    this.$element.one("bsTransitionEnd", a.proxy(h, this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])
                }
            }
        }
    }, d.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var b = a.Event("hide.bs.collapse");
            if (this.$element.trigger(b), !b.isDefaultPrevented()) {
                var c = this.dimension();
                this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var e = function() { this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse") };
                return a.support.transition ? void this.$element[c](0).one("bsTransitionEnd", a.proxy(e, this)).emulateTransitionEnd(d.TRANSITION_DURATION) : e.call(this)
            }
        }
    }, d.prototype.toggle = function() { this[this.$element.hasClass("in") ? "hide" : "show"]() }, d.prototype.getParent = function() {
        return a(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(a.proxy(function(c, d) {
            var e = a(d);
            this.addAriaAndCollapsedClass(b(e), e)
        }, this)).end()
    }, d.prototype.addAriaAndCollapsedClass = function(a, b) {
        var c = a.hasClass("in");
        a.attr("aria-expanded", c), b.toggleClass("collapsed", !c).attr("aria-expanded", c)
    };
    var e = a.fn.collapse;
    a.fn.collapse = c, a.fn.collapse.Constructor = d, a.fn.collapse.noConflict = function() { return a.fn.collapse = e, this }, a(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(d) {
        var e = a(this);
        e.attr("data-target") || d.preventDefault();
        var f = b(e),
            g = f.data("bs.collapse"),
            h = g ? "toggle" : e.data();
        c.call(f, h)
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        var c = b.attr("data-target");
        c || (c = b.attr("href"), c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));
        var d = c && a(c);
        return d && d.length ? d : b.parent()
    }

    function c(c) {
        c && 3 === c.which || (a(e).remove(), a(f).each(function() {
            var d = a(this),
                e = b(d),
                f = { relatedTarget: this };
            e.hasClass("open") && (c && "click" == c.type && /input|textarea/i.test(c.target.tagName) && a.contains(e[0], c.target) || (e.trigger(c = a.Event("hide.bs.dropdown", f)), c.isDefaultPrevented() || (d.attr("aria-expanded", "false"), e.removeClass("open").trigger(a.Event("hidden.bs.dropdown", f)))))
        }))
    }

    function d(b) {
        return this.each(function() {
            var c = a(this),
                d = c.data("bs.dropdown");
            d || c.data("bs.dropdown", d = new g(this)), "string" == typeof b && d[b].call(c)
        })
    }
    var e = ".dropdown-backdrop",
        f = '[data-toggle="dropdown"]',
        g = function(b) { a(b).on("click.bs.dropdown", this.toggle) };
    g.VERSION = "3.3.7", g.prototype.toggle = function(d) {
        var e = a(this);
        if (!e.is(".disabled, :disabled")) {
            var f = b(e),
                g = f.hasClass("open");
            if (c(), !g) {
                "ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click", c);
                var h = { relatedTarget: this };
                if (f.trigger(d = a.Event("show.bs.dropdown", h)), d.isDefaultPrevented()) return;
                e.trigger("focus").attr("aria-expanded", "true"), f.toggleClass("open").trigger(a.Event("shown.bs.dropdown", h))
            }
            return !1
        }
    }, g.prototype.keydown = function(c) {
        if (/(38|40|27|32)/.test(c.which) && !/input|textarea/i.test(c.target.tagName)) {
            var d = a(this);
            if (c.preventDefault(), c.stopPropagation(), !d.is(".disabled, :disabled")) {
                var e = b(d),
                    g = e.hasClass("open");
                if (!g && 27 != c.which || g && 27 == c.which) return 27 == c.which && e.find(f).trigger("focus"), d.trigger("click");
                var h = " li:not(.disabled):visible a",
                    i = e.find(".dropdown-menu" + h);
                if (i.length) {
                    var j = i.index(c.target);
                    38 == c.which && j > 0 && j--, 40 == c.which && j < i.length - 1 && j++, ~j || (j = 0), i.eq(j).trigger("focus")
                }
            }
        }
    };
    var h = a.fn.dropdown;
    a.fn.dropdown = d, a.fn.dropdown.Constructor = g, a.fn.dropdown.noConflict = function() { return a.fn.dropdown = h, this }, a(document).on("click.bs.dropdown.data-api", c).on("click.bs.dropdown.data-api", ".dropdown form", function(a) { a.stopPropagation() }).on("click.bs.dropdown.data-api", f, g.prototype.toggle).on("keydown.bs.dropdown.data-api", f, g.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown)
}(jQuery), + function(a) {
    "use strict";

    function b(b, d) {
        return this.each(function() {
            var e = a(this),
                f = e.data("bs.modal"),
                g = a.extend({}, c.DEFAULTS, e.data(), "object" == typeof b && b);
            f || e.data("bs.modal", f = new c(this, g)), "string" == typeof b ? f[b](d) : g.show && f.show(d)
        })
    }
    var c = function(b, c) { this.options = c, this.$body = a(document.body), this.$element = a(b), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function() { this.$element.trigger("loaded.bs.modal") }, this)) };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 300, c.BACKDROP_TRANSITION_DURATION = 150, c.DEFAULTS = { backdrop: !0, keyboard: !0, show: !0 }, c.prototype.toggle = function(a) { return this.isShown ? this.hide() : this.show(a) }, c.prototype.show = function(b) {
        var d = this,
            e = a.Event("show.bs.modal", { relatedTarget: b });
        this.$element.trigger(e), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function() { d.$element.one("mouseup.dismiss.bs.modal", function(b) { a(b.target).is(d.$element) && (d.ignoreBackdropClick = !0) }) }), this.backdrop(function() {
            var e = a.support.transition && d.$element.hasClass("fade");
            d.$element.parent().length || d.$element.appendTo(d.$body), d.$element.show().scrollTop(0), d.adjustDialog(), e && d.$element[0].offsetWidth, d.$element.addClass("in"), d.enforceFocus();
            var f = a.Event("shown.bs.modal", { relatedTarget: b });
            e ? d.$dialog.one("bsTransitionEnd", function() { d.$element.trigger("focus").trigger(f) }).emulateTransitionEnd(c.TRANSITION_DURATION) : d.$element.trigger("focus").trigger(f)
        }))
    }, c.prototype.hide = function(b) { b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), a(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), a.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", a.proxy(this.hideModal, this)).emulateTransitionEnd(c.TRANSITION_DURATION) : this.hideModal()) }, c.prototype.enforceFocus = function() { a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function(a) { document === a.target || this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.trigger("focus") }, this)) }, c.prototype.escape = function() { this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", a.proxy(function(a) { 27 == a.which && this.hide() }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal") }, c.prototype.resize = function() { this.isShown ? a(window).on("resize.bs.modal", a.proxy(this.handleUpdate, this)) : a(window).off("resize.bs.modal") }, c.prototype.hideModal = function() {
        var a = this;
        this.$element.hide(), this.backdrop(function() { a.$body.removeClass("modal-open"), a.resetAdjustments(), a.resetScrollbar(), a.$element.trigger("hidden.bs.modal") })
    }, c.prototype.removeBackdrop = function() { this.$backdrop && this.$backdrop.remove(), this.$backdrop = null }, c.prototype.backdrop = function(b) {
        var d = this,
            e = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var f = a.support.transition && e;
            if (this.$backdrop = a(document.createElement("div")).addClass("modal-backdrop " + e).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", a.proxy(function(a) { return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide())) }, this)), f && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b) return;
            f ? this.$backdrop.one("bsTransitionEnd", b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : b()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var g = function() { d.removeBackdrop(), b && b() };
            a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : g()
        } else b && b()
    }, c.prototype.handleUpdate = function() { this.adjustDialog() }, c.prototype.adjustDialog = function() {
        var a = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({ paddingLeft: !this.bodyIsOverflowing && a ? this.scrollbarWidth : "", paddingRight: this.bodyIsOverflowing && !a ? this.scrollbarWidth : "" })
    }, c.prototype.resetAdjustments = function() { this.$element.css({ paddingLeft: "", paddingRight: "" }) }, c.prototype.checkScrollbar = function() {
        var a = window.innerWidth;
        if (!a) {
            var b = document.documentElement.getBoundingClientRect();
            a = b.right - Math.abs(b.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < a, this.scrollbarWidth = this.measureScrollbar()
    }, c.prototype.setScrollbar = function() {
        var a = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", a + this.scrollbarWidth)
    }, c.prototype.resetScrollbar = function() { this.$body.css("padding-right", this.originalBodyPad) }, c.prototype.measureScrollbar = function() {
        var a = document.createElement("div");
        a.className = "modal-scrollbar-measure", this.$body.append(a);
        var b = a.offsetWidth - a.clientWidth;
        return this.$body[0].removeChild(a), b
    };
    var d = a.fn.modal;
    a.fn.modal = b, a.fn.modal.Constructor = c, a.fn.modal.noConflict = function() { return a.fn.modal = d, this }, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(c) {
        var d = a(this),
            e = d.attr("href"),
            f = a(d.attr("data-target") || e && e.replace(/.*(?=#[^\s]+$)/, "")),
            g = f.data("bs.modal") ? "toggle" : a.extend({ remote: !/#/.test(e) && e }, f.data(), d.data());
        d.is("a") && c.preventDefault(), f.one("show.bs.modal", function(a) { a.isDefaultPrevented() || f.one("hidden.bs.modal", function() { d.is(":visible") && d.trigger("focus") }) }), b.call(f, g, this)
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.tooltip"),
                f = "object" == typeof b && b;
            !e && /destroy|hide/.test(b) || (e || d.data("bs.tooltip", e = new c(this, f)), "string" == typeof b && e[b]())
        })
    }
    var c = function(a, b) { this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", a, b) };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.DEFAULTS = { animation: !0, placement: "top", selector: !1, template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover focus", title: "", delay: 0, html: !1, container: !1, viewport: { selector: "body", padding: 0 } }, c.prototype.init = function(b, c, d) {
        if (this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d), this.$viewport = this.options.viewport && a(a.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = { click: !1, hover: !1, focus: !1 }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var e = this.options.trigger.split(" "), f = e.length; f--;) {
            var g = e[f];
            if ("click" == g) this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this));
            else if ("manual" != g) {
                var h = "hover" == g ? "mouseenter" : "focusin",
                    i = "hover" == g ? "mouseleave" : "focusout";
                this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = a.extend({}, this.options, { trigger: "manual", selector: "" }) : this.fixTitle()
    }, c.prototype.getDefaults = function() { return c.DEFAULTS }, c.prototype.getOptions = function(b) { return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = { show: b.delay, hide: b.delay }), b }, c.prototype.getDelegateOptions = function() {
        var b = {},
            c = this.getDefaults();
        return this._options && a.each(this._options, function(a, d) { c[a] != d && (b[a] = d) }), b
    }, c.prototype.enter = function(b) { var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type); return c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusin" == b.type ? "focus" : "hover"] = !0), c.tip().hasClass("in") || "in" == c.hoverState ? void(c.hoverState = "in") : (clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void(c.timeout = setTimeout(function() { "in" == c.hoverState && c.show() }, c.options.delay.show)) : c.show()) }, c.prototype.isInStateTrue = function() {
        for (var a in this.inState)
            if (this.inState[a]) return !0;
        return !1
    }, c.prototype.leave = function(b) { var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type); if (c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusout" == b.type ? "focus" : "hover"] = !1), !c.isInStateTrue()) return clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void(c.timeout = setTimeout(function() { "out" == c.hoverState && c.hide() }, c.options.delay.hide)) : c.hide() }, c.prototype.show = function() {
        var b = a.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(b);
            var d = a.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (b.isDefaultPrevented() || !d) return;
            var e = this,
                f = this.tip(),
                g = this.getUID(this.type);
            this.setContent(), f.attr("id", g), this.$element.attr("aria-describedby", g), this.options.animation && f.addClass("fade");
            var h = "function" == typeof this.options.placement ? this.options.placement.call(this, f[0], this.$element[0]) : this.options.placement,
                i = /\s?auto?\s?/i,
                j = i.test(h);
            j && (h = h.replace(i, "") || "top"), f.detach().css({ top: 0, left: 0, display: "block" }).addClass(h).data("bs." + this.type, this), this.options.container ? f.appendTo(this.options.container) : f.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var k = this.getPosition(),
                l = f[0].offsetWidth,
                m = f[0].offsetHeight;
            if (j) {
                var n = h,
                    o = this.getPosition(this.$viewport);
                h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h, f.removeClass(n).addClass(h)
            }
            var p = this.getCalculatedOffset(h, k, l, m);
            this.applyPlacement(p, h);
            var q = function() {
                var a = e.hoverState;
                e.$element.trigger("shown.bs." + e.type), e.hoverState = null, "out" == a && e.leave(e)
            };
            a.support.transition && this.$tip.hasClass("fade") ? f.one("bsTransitionEnd", q).emulateTransitionEnd(c.TRANSITION_DURATION) : q()
        }
    }, c.prototype.applyPlacement = function(b, c) {
        var d = this.tip(),
            e = d[0].offsetWidth,
            f = d[0].offsetHeight,
            g = parseInt(d.css("margin-top"), 10),
            h = parseInt(d.css("margin-left"), 10);
        isNaN(g) && (g = 0), isNaN(h) && (h = 0), b.top += g, b.left += h, a.offset.setOffset(d[0], a.extend({ using: function(a) { d.css({ top: Math.round(a.top), left: Math.round(a.left) }) } }, b), 0), d.addClass("in");
        var i = d[0].offsetWidth,
            j = d[0].offsetHeight;
        "top" == c && j != f && (b.top = b.top + f - j);
        var k = this.getViewportAdjustedDelta(c, b, i, j);
        k.left ? b.left += k.left : b.top += k.top;
        var l = /top|bottom/.test(c),
            m = l ? 2 * k.left - e + i : 2 * k.top - f + j,
            n = l ? "offsetWidth" : "offsetHeight";
        d.offset(b), this.replaceArrow(m, d[0][n], l)
    }, c.prototype.replaceArrow = function(a, b, c) { this.arrow().css(c ? "left" : "top", 50 * (1 - a / b) + "%").css(c ? "top" : "left", "") }, c.prototype.setContent = function() {
        var a = this.tip(),
            b = this.getTitle();
        a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right")
    }, c.prototype.hide = function(b) {
        function d() { "in" != e.hoverState && f.detach(), e.$element && e.$element.removeAttr("aria-describedby").trigger("hidden.bs." + e.type), b && b() }
        var e = this,
            f = a(this.$tip),
            g = a.Event("hide.bs." + this.type);
        if (this.$element.trigger(g), !g.isDefaultPrevented()) return f.removeClass("in"), a.support.transition && f.hasClass("fade") ? f.one("bsTransitionEnd", d).emulateTransitionEnd(c.TRANSITION_DURATION) : d(), this.hoverState = null, this
    }, c.prototype.fixTitle = function() {
        var a = this.$element;
        (a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "")
    }, c.prototype.hasContent = function() { return this.getTitle() }, c.prototype.getPosition = function(b) {
        b = b || this.$element;
        var c = b[0],
            d = "BODY" == c.tagName,
            e = c.getBoundingClientRect();
        null == e.width && (e = a.extend({}, e, { width: e.right - e.left, height: e.bottom - e.top }));
        var f = window.SVGElement && c instanceof window.SVGElement,
            g = d ? { top: 0, left: 0 } : f ? null : b.offset(),
            h = { scroll: d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop() },
            i = d ? { width: a(window).width(), height: a(window).height() } : null;
        return a.extend({}, e, h, i, g)
    }, c.prototype.getCalculatedOffset = function(a, b, c, d) { return "bottom" == a ? { top: b.top + b.height, left: b.left + b.width / 2 - c / 2 } : "top" == a ? { top: b.top - d, left: b.left + b.width / 2 - c / 2 } : "left" == a ? { top: b.top + b.height / 2 - d / 2, left: b.left - c } : { top: b.top + b.height / 2 - d / 2, left: b.left + b.width } }, c.prototype.getViewportAdjustedDelta = function(a, b, c, d) {
        var e = { top: 0, left: 0 };
        if (!this.$viewport) return e;
        var f = this.options.viewport && this.options.viewport.padding || 0,
            g = this.getPosition(this.$viewport);
        if (/right|left/.test(a)) {
            var h = b.top - f - g.scroll,
                i = b.top + f - g.scroll + d;
            h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i)
        } else {
            var j = b.left - f,
                k = b.left + f + c;
            j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k)
        }
        return e
    }, c.prototype.getTitle = function() {
        var a, b = this.$element,
            c = this.options;
        return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title)
    }, c.prototype.getUID = function(a) { do a += ~~(1e6 * Math.random()); while (document.getElementById(a)); return a }, c.prototype.tip = function() { if (!this.$tip && (this.$tip = a(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!"); return this.$tip }, c.prototype.arrow = function() { return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow") }, c.prototype.enable = function() { this.enabled = !0 }, c.prototype.disable = function() { this.enabled = !1 }, c.prototype.toggleEnabled = function() { this.enabled = !this.enabled }, c.prototype.toggle = function(b) {
        var c = this;
        b && (c = a(b.currentTarget).data("bs." + this.type), c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c))), b ? (c.inState.click = !c.inState.click, c.isInStateTrue() ? c.enter(c) : c.leave(c)) : c.tip().hasClass("in") ? c.leave(c) : c.enter(c)
    }, c.prototype.destroy = function() {
        var a = this;
        clearTimeout(this.timeout), this.hide(function() { a.$element.off("." + a.type).removeData("bs." + a.type), a.$tip && a.$tip.detach(), a.$tip = null, a.$arrow = null, a.$viewport = null, a.$element = null })
    };
    var d = a.fn.tooltip;
    a.fn.tooltip = b, a.fn.tooltip.Constructor = c, a.fn.tooltip.noConflict = function() { return a.fn.tooltip = d, this }
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.popover"),
                f = "object" == typeof b && b;
            !e && /destroy|hide/.test(b) || (e || d.data("bs.popover", e = new c(this, f)), "string" == typeof b && e[b]())
        })
    }
    var c = function(a, b) { this.init("popover", a, b) };
    if (!a.fn.tooltip) throw new Error("Popover requires tooltip.js");
    c.VERSION = "3.3.7", c.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, { placement: "right", trigger: "click", content: "", template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' }), c.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), c.prototype.constructor = c, c.prototype.getDefaults = function() { return c.DEFAULTS }, c.prototype.setContent = function() {
        var a = this.tip(),
            b = this.getTitle(),
            c = this.getContent();
        a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c), a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide()
    }, c.prototype.hasContent = function() { return this.getTitle() || this.getContent() }, c.prototype.getContent = function() {
        var a = this.$element,
            b = this.options;
        return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content)
    }, c.prototype.arrow = function() { return this.$arrow = this.$arrow || this.tip().find(".arrow") };
    var d = a.fn.popover;
    a.fn.popover = b, a.fn.popover.Constructor = c, a.fn.popover.noConflict = function() { return a.fn.popover = d, this }
}(jQuery), + function(a) {
    "use strict";

    function b(c, d) { this.$body = a(document.body), this.$scrollElement = a(a(c).is(document.body) ? window : c), this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", a.proxy(this.process, this)), this.refresh(), this.process() }

    function c(c) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.scrollspy"),
                f = "object" == typeof c && c;
            e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]()
        })
    }
    b.VERSION = "3.3.7", b.DEFAULTS = { offset: 10 }, b.prototype.getScrollHeight = function() { return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight) }, b.prototype.refresh = function() {
        var b = this,
            c = "offset",
            d = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), a.isWindow(this.$scrollElement[0]) || (c = "position", d = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function() {
            var b = a(this),
                e = b.data("target") || b.attr("href"),
                f = /^#./.test(e) && a(e);
            return f && f.length && f.is(":visible") && [
                [f[c]().top + d, e]
            ] || null
        }).sort(function(a, b) { return a[0] - b[0] }).each(function() { b.offsets.push(this[0]), b.targets.push(this[1]) })
    }, b.prototype.process = function() {
        var a, b = this.$scrollElement.scrollTop() + this.options.offset,
            c = this.getScrollHeight(),
            d = this.options.offset + c - this.$scrollElement.height(),
            e = this.offsets,
            f = this.targets,
            g = this.activeTarget;
        if (this.scrollHeight != c && this.refresh(), b >= d) return g != (a = f[f.length - 1]) && this.activate(a);
        if (g && b < e[0]) return this.activeTarget = null, this.clear();
        for (a = e.length; a--;) g != f[a] && b >= e[a] && (void 0 === e[a + 1] || b < e[a + 1]) && this.activate(f[a])
    }, b.prototype.activate = function(b) {
        this.activeTarget = b, this.clear();
        var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]',
            d = a(c).parents("li").addClass("active");
        d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), d.trigger("activate.bs.scrollspy")
    }, b.prototype.clear = function() { a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active") };
    var d = a.fn.scrollspy;
    a.fn.scrollspy = c, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function() { return a.fn.scrollspy = d, this }, a(window).on("load.bs.scrollspy.data-api", function() {
        a('[data-spy="scroll"]').each(function() {
            var b = a(this);
            c.call(b, b.data())
        })
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.tab");
            e || d.data("bs.tab", e = new c(this)), "string" == typeof b && e[b]()
        })
    }
    var c = function(b) { this.element = a(b) };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.prototype.show = function() {
        var b = this.element,
            c = b.closest("ul:not(.dropdown-menu)"),
            d = b.data("target");
        if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) {
            var e = c.find(".active:last a"),
                f = a.Event("hide.bs.tab", { relatedTarget: b[0] }),
                g = a.Event("show.bs.tab", { relatedTarget: e[0] });
            if (e.trigger(f), b.trigger(g), !g.isDefaultPrevented() && !f.isDefaultPrevented()) {
                var h = a(d);
                this.activate(b.closest("li"), c), this.activate(h, h.parent(), function() { e.trigger({ type: "hidden.bs.tab", relatedTarget: b[0] }), b.trigger({ type: "shown.bs.tab", relatedTarget: e[0] }) })
            }
        }
    }, c.prototype.activate = function(b, d, e) {
        function f() { g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), h ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), b.parent(".dropdown-menu").length && b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), e && e() }
        var g = d.find("> .active"),
            h = e && a.support.transition && (g.length && g.hasClass("fade") || !!d.find("> .fade").length);
        g.length && h ? g.one("bsTransitionEnd", f).emulateTransitionEnd(c.TRANSITION_DURATION) : f(), g.removeClass("in")
    };
    var d = a.fn.tab;
    a.fn.tab = b, a.fn.tab.Constructor = c, a.fn.tab.noConflict = function() { return a.fn.tab = d, this };
    var e = function(c) { c.preventDefault(), b.call(a(this), "show") };
    a(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', e).on("click.bs.tab.data-api", '[data-toggle="pill"]', e)
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.affix"),
                f = "object" == typeof b && b;
            e || d.data("bs.affix", e = new c(this, f)), "string" == typeof b && e[b]()
        })
    }
    var c = function(b, d) { this.options = a.extend({}, c.DEFAULTS, d), this.$target = a(this.options.target).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), this.$element = a(b), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition() };
    c.VERSION = "3.3.7", c.RESET = "affix affix-top affix-bottom", c.DEFAULTS = { offset: 0, target: window }, c.prototype.getState = function(a, b, c, d) {
        var e = this.$target.scrollTop(),
            f = this.$element.offset(),
            g = this.$target.height();
        if (null != c && "top" == this.affixed) return e < c && "top";
        if ("bottom" == this.affixed) return null != c ? !(e + this.unpin <= f.top) && "bottom" : !(e + g <= a - d) && "bottom";
        var h = null == this.affixed,
            i = h ? e : f.top,
            j = h ? g : b;
        return null != c && e <= c ? "top" : null != d && i + j >= a - d && "bottom"
    }, c.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(c.RESET).addClass("affix");
        var a = this.$target.scrollTop(),
            b = this.$element.offset();
        return this.pinnedOffset = b.top - a
    }, c.prototype.checkPositionWithEventLoop = function() { setTimeout(a.proxy(this.checkPosition, this), 1) }, c.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var b = this.$element.height(),
                d = this.options.offset,
                e = d.top,
                f = d.bottom,
                g = Math.max(a(document).height(), a(document.body).height());
            "object" != typeof d && (f = e = d), "function" == typeof e && (e = d.top(this.$element)), "function" == typeof f && (f = d.bottom(this.$element));
            var h = this.getState(g, b, e, f);
            if (this.affixed != h) {
                null != this.unpin && this.$element.css("top", "");
                var i = "affix" + (h ? "-" + h : ""),
                    j = a.Event(i + ".bs.affix");
                if (this.$element.trigger(j), j.isDefaultPrevented()) return;
                this.affixed = h, this.unpin = "bottom" == h ? this.getPinnedOffset() : null, this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == h && this.$element.offset({ top: g - b - f })
        }
    };
    var d = a.fn.affix;
    a.fn.affix = b, a.fn.affix.Constructor = c, a.fn.affix.noConflict = function() { return a.fn.affix = d, this }, a(window).on("load", function() {
        a('[data-spy="affix"]').each(function() {
            var c = a(this),
                d = c.data();
            d.offset = d.offset || {}, null != d.offsetBottom && (d.offset.bottom = d.offsetBottom), null != d.offsetTop && (d.offset.top = d.offsetTop), b.call(c, d)
        })
    })
}(jQuery);
/*!
 * headroom.js v0.9.4 - Give your page some headroom. Hide your header until you need it
 * Copyright (c) 2017 Nick Williams - http://wicky.nillia.ms/headroom.js
 * License: MIT
 */
(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) { define([], factory); } else if (typeof exports === 'object') { module.exports = factory(); } else { root.Headroom = factory(); }
}(this, function() {
    'use strict';
    var features = { bind: !!(function() {}.bind), classList: 'classList' in document.documentElement, rAF: !!(window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame) };
    window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame;

    function Debouncer(callback) {
        this.callback = callback;
        this.ticking = false;
    }
    Debouncer.prototype = {
        constructor: Debouncer,
        update: function() {
            this.callback && this.callback();
            this.ticking = false;
        },
        requestTick: function() {
            if (!this.ticking) {
                requestAnimationFrame(this.rafCallback || (this.rafCallback = this.update.bind(this)));
                this.ticking = true;
            }
        },
        handleEvent: function() { this.requestTick(); }
    };

    function isDOMElement(obj) { return obj && typeof window !== 'undefined' && (obj === window || obj.nodeType); }

    function extend(object) {
        if (arguments.length <= 0) { throw new Error('Missing arguments in extend function'); }
        var result = object || {},
            key, i;
        for (i = 1; i < arguments.length; i++) {
            var replacement = arguments[i] || {};
            for (key in replacement) {
                if (typeof result[key] === 'object' && !isDOMElement(result[key])) { result[key] = extend(result[key], replacement[key]); } else { result[key] = result[key] || replacement[key]; }
            }
        }
        return result;
    }

    function normalizeTolerance(t) { return t === Object(t) ? t : { down: t, up: t }; }

    function Headroom(elem, options) {
        options = extend(options, Headroom.options);
        this.lastKnownScrollY = 0;
        this.elem = elem;
        this.tolerance = normalizeTolerance(options.tolerance);
        this.classes = options.classes;
        this.offset = options.offset;
        this.scroller = options.scroller;
        this.initialised = false;
        this.onPin = options.onPin;
        this.onUnpin = options.onUnpin;
        this.onTop = options.onTop;
        this.onNotTop = options.onNotTop;
        this.onBottom = options.onBottom;
        this.onNotBottom = options.onNotBottom;
    }
    Headroom.prototype = {
        constructor: Headroom,
        init: function() {
            if (!Headroom.cutsTheMustard) { return; }
            this.debouncer = new Debouncer(this.update.bind(this));
            this.elem.classList.add(this.classes.initial);
            setTimeout(this.attachEvent.bind(this), 100);
            return this;
        },
        destroy: function() {
            var classes = this.classes;
            this.initialised = false;
            for (var key in classes) { if (classes.hasOwnProperty(key)) { this.elem.classList.remove(classes[key]); } }
            this.scroller.removeEventListener('scroll', this.debouncer, false);
        },
        attachEvent: function() {
            if (!this.initialised) {
                this.lastKnownScrollY = this.getScrollY();
                this.initialised = true;
                this.scroller.addEventListener('scroll', this.debouncer, false);
                this.debouncer.handleEvent();
            }
        },
        unpin: function() {
            var classList = this.elem.classList,
                classes = this.classes;
            if (classList.contains(classes.pinned) || !classList.contains(classes.unpinned)) {
                classList.add(classes.unpinned);
                classList.remove(classes.pinned);
                this.onUnpin && this.onUnpin.call(this);
            }
        },
        pin: function() {
            var classList = this.elem.classList,
                classes = this.classes;
            if (classList.contains(classes.unpinned)) {
                classList.remove(classes.unpinned);
                classList.add(classes.pinned);
                this.onPin && this.onPin.call(this);
            }
        },
        top: function() {
            var classList = this.elem.classList,
                classes = this.classes;
            if (!classList.contains(classes.top)) {
                classList.add(classes.top);
                classList.remove(classes.notTop);
                this.onTop && this.onTop.call(this);
            }
        },
        notTop: function() {
            var classList = this.elem.classList,
                classes = this.classes;
            if (!classList.contains(classes.notTop)) {
                classList.add(classes.notTop);
                classList.remove(classes.top);
                this.onNotTop && this.onNotTop.call(this);
            }
        },
        bottom: function() {
            var classList = this.elem.classList,
                classes = this.classes;
            if (!classList.contains(classes.bottom)) {
                classList.add(classes.bottom);
                classList.remove(classes.notBottom);
                this.onBottom && this.onBottom.call(this);
            }
        },
        notBottom: function() {
            var classList = this.elem.classList,
                classes = this.classes;
            if (!classList.contains(classes.notBottom)) {
                classList.add(classes.notBottom);
                classList.remove(classes.bottom);
                this.onNotBottom && this.onNotBottom.call(this);
            }
        },
        getScrollY: function() { return (this.scroller.pageYOffset !== undefined) ? this.scroller.pageYOffset : (this.scroller.scrollTop !== undefined) ? this.scroller.scrollTop : (document.documentElement || document.body.parentNode || document.body).scrollTop; },
        getViewportHeight: function() { return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; },
        getElementPhysicalHeight: function(elm) { return Math.max(elm.offsetHeight, elm.clientHeight); },
        getScrollerPhysicalHeight: function() { return (this.scroller === window || this.scroller === document.body) ? this.getViewportHeight() : this.getElementPhysicalHeight(this.scroller); },
        getDocumentHeight: function() {
            var body = document.body,
                documentElement = document.documentElement;
            return Math.max(body.scrollHeight, documentElement.scrollHeight, body.offsetHeight, documentElement.offsetHeight, body.clientHeight, documentElement.clientHeight);
        },
        getElementHeight: function(elm) { return Math.max(elm.scrollHeight, elm.offsetHeight, elm.clientHeight); },
        getScrollerHeight: function() { return (this.scroller === window || this.scroller === document.body) ? this.getDocumentHeight() : this.getElementHeight(this.scroller); },
        isOutOfBounds: function(currentScrollY) {
            var pastTop = currentScrollY < 0,
                pastBottom = currentScrollY + this.getScrollerPhysicalHeight() > this.getScrollerHeight();
            return pastTop || pastBottom;
        },
        toleranceExceeded: function(currentScrollY, direction) { return Math.abs(currentScrollY - this.lastKnownScrollY) >= this.tolerance[direction]; },
        shouldUnpin: function(currentScrollY, toleranceExceeded) {
            var scrollingDown = currentScrollY > this.lastKnownScrollY,
                pastOffset = currentScrollY >= this.offset;
            return scrollingDown && pastOffset && toleranceExceeded;
        },
        shouldPin: function(currentScrollY, toleranceExceeded) {
            var scrollingUp = currentScrollY < this.lastKnownScrollY,
                pastOffset = currentScrollY <= this.offset;
            return (scrollingUp && toleranceExceeded) || pastOffset;
        },
        update: function() {
            var currentScrollY = this.getScrollY(),
                scrollDirection = currentScrollY > this.lastKnownScrollY ? 'down' : 'up',
                toleranceExceeded = this.toleranceExceeded(currentScrollY, scrollDirection);
            if (this.isOutOfBounds(currentScrollY)) { return; }
            if (currentScrollY <= this.offset) { this.top(); } else { this.notTop(); }
            if (currentScrollY + this.getViewportHeight() >= this.getScrollerHeight()) { this.bottom(); } else { this.notBottom(); }
            if (this.shouldUnpin(currentScrollY, toleranceExceeded)) { this.unpin(); } else if (this.shouldPin(currentScrollY, toleranceExceeded)) { this.pin(); }
            this.lastKnownScrollY = currentScrollY;
        }
    };
    Headroom.options = { tolerance: { up: 0, down: 0 }, offset: 0, scroller: window, classes: { pinned: 'headroom--pinned', unpinned: 'headroom--unpinned', top: 'headroom--top', notTop: 'headroom--not-top', bottom: 'headroom--bottom', notBottom: 'headroom--not-bottom', initial: 'headroom' } };
    Headroom.cutsTheMustard = typeof features !== 'undefined' && features.rAF && features.bind && features.classList;
    return Headroom;
}));
var myElement = document.querySelector(".headroom");
var headroom = new Headroom(myElement);
headroom.init();;
(function($) {
    'use strict';

    function initialize() { var mapElement = document.getElementById('map'); var mapData = $('#map').data('map-options'); var mapOptions = { zoom: 12, scrollwheel: false, styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }], center: new google.maps.LatLng(mapData.latitude, mapData.longitude) }; var map = new google.maps.Map(document.getElementById('map'), mapOptions); var marker = new google.maps.Marker({ position: map.getCenter(), icon: mapData.map_icon_mark, map: map }); }
    if ($('body').hasClass('has-google-map-key')) { google.maps.event.addDomListener(window, 'load', initialize); }
})(jQuery);
(function($) {
    function getTimeRemaining(endtime) { var t = Date.parse(endtime) - Date.parse(new Date()); var seconds = Math.floor((t / 1000) % 60); var minutes = Math.floor((t / 1000 / 60) % 60); var hours = Math.floor((t / (1000 * 60 * 60)) % 24); var days = Math.floor(t / (1000 * 60 * 60 * 24)); return { 'total': t, 'days': days, 'hours': hours, 'minutes': minutes, 'seconds': seconds }; }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);
            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
            if (t.total <= 0) { clearInterval(timeinterval); }
        }
        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
    var deadline = $('#clockdiv').data('coming-soon-date');
    if ($('#clockdiv').length > 0) { initializeClock('clockdiv', deadline); }
})(jQuery);
(function(a) {
    a.fn.prettySocial = function() {
        var b = { pinterest: { url: "http://pinterest.com/pin/create/button/?url={{url}}&media={{media}}&description={{description}}", popup: { width: 685, height: 500 } }, facebook: { url: "https://www.facebook.com/sharer/sharer.php?s=100&p[title]={{title}}&p[summary]={{description}}&p[url]={{url}}&p[images][0]={{media}}", popup: { width: 626, height: 436 } }, twitter: { url: "https://twitter.com/share?url={{url}}&via={{via}}&text={{description}}", popup: { width: 685, height: 500 } }, googleplus: { url: "https://plus.google.com/share?url={{url}}", popup: { width: 600, height: 600 } }, linkedin: { url: "https://www.linkedin.com/shareArticle?mini=true&url={{url}}&title={{title}}&summary={{description}}+&source={{via}}", popup: { width: 600, height: 600 } } },
            d = function(f, e) {
                var h = (window.innerWidth / 2) - (f.popup.width / 2),
                    g = (window.innerHeight / 2) - (f.popup.height / 2);
                return window.open(e, "", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + f.popup.width + ", height=" + f.popup.height + ", top=" + g + ", left=" + h)
            },
            c = function(f, g) { var e = f.url.replace(/{{url}}/g, encodeURIComponent(g.url)).replace(/{{title}}/g, encodeURIComponent(g.title)).replace(/{{description}}/g, encodeURIComponent(g.description)).replace(/{{media}}/g, encodeURIComponent(g.media)).replace(/{{via}}/g, encodeURIComponent(g.via)); return e };
        return this.each(function() {
            var i = a(this);
            var g = i.data("type"),
                f = b[g] || null;
            if (!f) { a.error("Social site is not set.") }
            var h = { url: i.data("url") || "", title: i.data("title") || "", description: i.data("description") || "", media: i.data("media") || "", via: i.data("via") || "" };
            var e = c(f, h);
            if (navigator.userAgent.match(/Android|IEMobile|BlackBerry|iPhone|iPad|iPod|Opera Mini/i)) {
                i.bind("touchstart", function(j) {
                    if (j.originalEvent.touches.length > 1) { return }
                    i.data("touchWithoutScroll", true)
                }).bind("touchmove", function() { i.data("touchWithoutScroll", false); return }).bind("touchend", function(k) {
                    k.preventDefault();
                    var j = i.data("touchWithoutScroll");
                    if (k.originalEvent.touches.length > 1 || !j) { return }
                    d(f, e)
                })
            } else {
                i.bind("click", function(j) {
                    j.preventDefault();
                    d(f, e)
                })
            }
        })
    }
})(jQuery);
(function($) {
    "use strict";
    var autoplay, bgcolor, blocknum, blocktitle, border, core, container, content, dest, extraCss, framewidth, frameheight, gallItems, infinigall, items, keyNavigationDisabled, margine, numeratio, overlayColor, overlay, title, thisgall, thenext, theprev, nextok, prevok, preloader, $preloader, navigation, obj, gallIndex, startouch, vbheader, images, startY, startX, endY, endX, diff, diffX, diffY, threshold;
    $.fn.extend({
        venobox: function(options) {
            var plugin = this;
            var defaults = { arrowsColor: '#B6B6B6', autoplay: false, bgcolor: '#fff', border: '0', closeBackground: '#161617', closeColor: "#d2d2d2", framewidth: '', frameheight: '', gallItems: false, infinigall: false, htmlClose: '&times;', htmlNext: '<span>Next</span>', htmlPrev: '<span>Prev</span>', numeratio: false, numerationBackground: '#161617', numerationColor: '#d2d2d2', numerationPosition: 'top', overlayClose: true, overlayColor: 'rgba(23,23,23,0.85)', spinner: 'double-bounce', spinColor: '#d2d2d2', titleattr: 'title', titleBackground: '#161617', titleColor: '#d2d2d2', titlePosition: 'top', cb_pre_open: function() { return true; }, cb_post_open: function() {}, cb_pre_close: function() { return true; }, cb_post_close: function() {}, cb_post_resize: function() {}, cb_after_nav: function() {}, cb_init: function() {} };
            var option = $.extend(defaults, options);
            option.cb_init(plugin);
            return this.each(function() {
                obj = $(this);
                if (obj.data('venobox')) { return true; }
                plugin.VBclose = function() { closeVbox(); };
                obj.addClass('vbox-item');
                obj.data('framewidth', option.framewidth);
                obj.data('frameheight', option.frameheight);
                obj.data('border', option.border);
                obj.data('bgcolor', option.bgcolor);
                obj.data('numeratio', option.numeratio);
                obj.data('gallItems', option.gallItems);
                obj.data('infinigall', option.infinigall);
                obj.data('overlaycolor', option.overlayColor);
                obj.data('titleattr', option.titleattr);
                obj.data('venobox', true);
                obj.on('click', function(e) {
                    e.preventDefault();
                    obj = $(this);
                    var cb_pre_open = option.cb_pre_open(obj);
                    if (cb_pre_open === false) { return false; }
                    plugin.VBnext = function() { navigateGall(thenext); };
                    plugin.VBprev = function() { navigateGall(theprev); };
                    overlayColor = obj.data('overlay') || obj.data('overlaycolor');
                    framewidth = obj.data('framewidth');
                    frameheight = obj.data('frameheight');
                    autoplay = obj.data('autoplay') || option.autoplay;
                    border = obj.data('border');
                    bgcolor = obj.data('bgcolor');
                    nextok = false;
                    prevok = false;
                    keyNavigationDisabled = false;
                    dest = obj.data('href') || obj.attr('href');
                    extraCss = obj.data('css') || '';
                    title = obj.attr(obj.data('titleattr')) || '';
                    preloader = '<div class="vbox-preloader">';
                    switch (option.spinner) {
                        case 'rotating-plane':
                            preloader += '<div class="sk-rotating-plane"></div>';
                            break;
                        case 'double-bounce':
                            preloader += '<div class="sk-double-bounce">' + '<div class="sk-child sk-double-bounce1"></div>' + '<div class="sk-child sk-double-bounce2"></div>' + '</div>';
                            break;
                        case 'wave':
                            preloader += '<div class="sk-wave">' + '<div class="sk-rect sk-rect1"></div>' + '<div class="sk-rect sk-rect2"></div>' + '<div class="sk-rect sk-rect3"></div>' + '<div class="sk-rect sk-rect4"></div>' + '<div class="sk-rect sk-rect5"></div>' + '</div>';
                            break;
                        case 'wandering-cubes':
                            preloader += '<div class="sk-wandering-cubes">' + '<div class="sk-cube sk-cube1"></div>' + '<div class="sk-cube sk-cube2"></div>' + '</div>';
                            break;
                        case 'spinner-pulse':
                            preloader += '<div class="sk-spinner sk-spinner-pulse"></div>';
                            break;
                        case 'chasing-dots':
                            preloader += '<div class="sk-chasing-dots">' + '<div class="sk-child sk-dot1"></div>' + '<div class="sk-child sk-dot2"></div>' + '</div>';
                            break;
                        case 'three-bounce':
                            preloader += '<div class="sk-three-bounce">' + '<div class="sk-child sk-bounce1"></div>' + '<div class="sk-child sk-bounce2"></div>' + '<div class="sk-child sk-bounce3"></div>' + '</div>';
                            break;
                        case 'circle':
                            preloader += '<div class="sk-circle">' + '<div class="sk-circle1 sk-child"></div>' + '<div class="sk-circle2 sk-child"></div>' + '<div class="sk-circle3 sk-child"></div>' + '<div class="sk-circle4 sk-child"></div>' + '<div class="sk-circle5 sk-child"></div>' + '<div class="sk-circle6 sk-child"></div>' + '<div class="sk-circle7 sk-child"></div>' + '<div class="sk-circle8 sk-child"></div>' + '<div class="sk-circle9 sk-child"></div>' + '<div class="sk-circle10 sk-child"></div>' + '<div class="sk-circle11 sk-child"></div>' + '<div class="sk-circle12 sk-child"></div>' + '</div>';
                            break;
                        case 'cube-grid':
                            preloader += '<div class="sk-cube-grid">' + '<div class="sk-cube sk-cube1"></div>' + '<div class="sk-cube sk-cube2"></div>' + '<div class="sk-cube sk-cube3"></div>' + '<div class="sk-cube sk-cube4"></div>' + '<div class="sk-cube sk-cube5"></div>' + '<div class="sk-cube sk-cube6"></div>' + '<div class="sk-cube sk-cube7"></div>' + '<div class="sk-cube sk-cube8"></div>' + '<div class="sk-cube sk-cube9"></div>' + '</div>';
                            break;
                        case 'fading-circle':
                            preloader += '<div class="sk-fading-circle">' + '<div class="sk-circle1 sk-circle"></div>' + '<div class="sk-circle2 sk-circle"></div>' + '<div class="sk-circle3 sk-circle"></div>' + '<div class="sk-circle4 sk-circle"></div>' + '<div class="sk-circle5 sk-circle"></div>' + '<div class="sk-circle6 sk-circle"></div>' + '<div class="sk-circle7 sk-circle"></div>' + '<div class="sk-circle8 sk-circle"></div>' + '<div class="sk-circle9 sk-circle"></div>' + '<div class="sk-circle10 sk-circle"></div>' + '<div class="sk-circle11 sk-circle"></div>' + '<div class="sk-circle12 sk-circle"></div>' + '</div>';
                            break;
                        case 'folding-cube':
                            preloader += '<div class="sk-folding-cube">' + '<div class="sk-cube1 sk-cube"></div>' + '<div class="sk-cube2 sk-cube"></div>' + '<div class="sk-cube4 sk-cube"></div>' + '<div class="sk-cube3 sk-cube"></div>' + '</div>';
                            break;
                    }
                    preloader += '</div>';
                    navigation = '<a class="vbox-next">' + option.htmlNext + '</a><a class="vbox-prev">' + option.htmlPrev + '</a>';
                    vbheader = '<div class="vbox-title"></div><div class="vbox-num">0/0</div><div class="vbox-close">' + option.htmlClose + '</div>';
                    core = '<div class="vbox-overlay ' + extraCss + '" style="background:' + overlayColor + '">' +
                        preloader + '<div class="vbox-container"><div class="vbox-content"></div></div>' + vbheader + navigation + '</div>';
                    $('body').append(core).addClass('vbox-open');
                    $('.vbox-preloader div:not(.sk-circle) .sk-child, .vbox-preloader .sk-rotating-plane, .vbox-preloader .sk-rect, .vbox-preloader div:not(.sk-folding-cube) .sk-cube, .vbox-preloader .sk-spinner-pulse').css('background-color', option.spinColor);
                    overlay = $('.vbox-overlay');
                    container = $('.vbox-container');
                    content = $('.vbox-content');
                    blocknum = $('.vbox-num');
                    blocktitle = $('.vbox-title');
                    $preloader = $('.vbox-preloader');
                    $preloader.show();
                    blocktitle.css(option.titlePosition, '-1px');
                    blocktitle.css({ 'color': option.titleColor, 'background-color': option.titleBackground });
                    $('.vbox-close').css({ 'color': option.closeColor, 'background-color': option.closeBackground });
                    $('.vbox-num').css(option.numerationPosition, '-1px');
                    $('.vbox-num').css({ 'color': option.numerationColor, 'background-color': option.numerationBackground });
                    $('.vbox-next span, .vbox-prev span').css({ 'border-top-color': option.arrowsColor, 'border-right-color': option.arrowsColor });
                    content.html('');
                    content.css('opacity', '0');
                    overlay.css('opacity', '0');
                    checknav();
                    overlay.animate({ opacity: 1 }, 250, function() {
                        if (obj.data('vbtype') == 'iframe') { loadIframe(); } else if (obj.data('vbtype') == 'inline') { loadInline(); } else if (obj.data('vbtype') == 'ajax') { loadAjax(); } else if (obj.data('vbtype') == 'video') { loadVid(autoplay); } else {
                            content.html('<img src="' + dest + '">');
                            preloadFirst();
                        }
                        option.cb_post_open(obj, gallIndex, thenext, theprev);
                    });
                    $('body').keydown(keyboardHandler);
                    $('.vbox-prev').on('click', function() { navigateGall(theprev); });
                    $('.vbox-next').on('click', function() { navigateGall(thenext); });
                    return false;
                });

                function checknav() {
                    thisgall = obj.data('gall');
                    numeratio = obj.data('numeratio');
                    gallItems = obj.data('gallItems');
                    infinigall = obj.data('infinigall');
                    if (gallItems) { items = gallItems; } else { items = $('.vbox-item[data-gall="' + thisgall + '"]'); }
                    thenext = items.eq(items.index(obj) + 1);
                    theprev = items.eq(items.index(obj) - 1);
                    if (!thenext.length && infinigall === true) { thenext = items.eq(0); }
                    if (items.length > 1) {
                        gallIndex = items.index(obj) + 1;
                        blocknum.html(gallIndex + ' / ' + items.length);
                    } else { gallIndex = 1; }
                    if (numeratio === true) { blocknum.show(); } else { blocknum.hide(); }
                    if (title !== '') { blocktitle.show(); } else { blocktitle.hide(); }
                    if (!thenext.length && infinigall !== true) {
                        $('.vbox-next').css('display', 'none');
                        nextok = false;
                    } else {
                        $('.vbox-next').css('display', 'block');
                        nextok = true;
                    }
                    if (items.index(obj) > 0 || infinigall === true) {
                        $('.vbox-prev').css('display', 'block');
                        prevok = true;
                    } else {
                        $('.vbox-prev').css('display', 'none');
                        prevok = false;
                    }
                    if (prevok === true || nextok === true) {
                        content.on(TouchMouseEvent.DOWN, onDownEvent);
                        content.on(TouchMouseEvent.MOVE, onMoveEvent);
                        content.on(TouchMouseEvent.UP, onUpEvent);
                    }
                }

                function navigateGall(destination) {
                    if (destination.length < 1) { return false; }
                    if (keyNavigationDisabled) { return false; }
                    keyNavigationDisabled = true;
                    overlayColor = destination.data('overlay') || destination.data('overlaycolor');
                    framewidth = destination.data('framewidth');
                    frameheight = destination.data('frameheight');
                    border = destination.data('border');
                    bgcolor = destination.data('bgcolor');
                    dest = destination.data('href') || destination.attr('href');
                    autoplay = destination.data('autoplay');
                    title = destination.attr(destination.data('titleattr')) || '';
                    if (destination === theprev) { content.addClass('animated').addClass('swipe-right'); }
                    if (destination === thenext) { content.addClass('animated').addClass('swipe-left'); }
                    $preloader.show();
                    content.animate({ opacity: 0, }, 500, function() {
                        overlay.css('background', overlayColor);
                        content.removeClass('animated').removeClass('swipe-left').removeClass('swipe-right').css({ 'margin-left': 0, 'margin-right': 0 });
                        if (destination.data('vbtype') == 'iframe') { loadIframe(); } else if (destination.data('vbtype') == 'inline') { loadInline(); } else if (destination.data('vbtype') == 'ajax') { loadAjax(); } else if (destination.data('vbtype') == 'video') { loadVid(autoplay); } else {
                            content.html('<img src="' + dest + '">');
                            preloadFirst();
                        }
                        obj = destination;
                        checknav();
                        keyNavigationDisabled = false;
                        option.cb_after_nav(obj, gallIndex, thenext, theprev);
                    });
                }

                function keyboardHandler(e) {
                    if (e.keyCode === 27) { closeVbox(); }
                    if (e.keyCode == 37 && prevok === true) { navigateGall(theprev); }
                    if (e.keyCode == 39 && nextok === true) { navigateGall(thenext); }
                }

                function closeVbox() {
                    var cb_pre_close = option.cb_pre_close(obj, gallIndex, thenext, theprev);
                    if (cb_pre_close === false) { return false; }
                    $('body').off('keydown', keyboardHandler).removeClass('vbox-open');
                    obj.focus();
                    overlay.animate({ opacity: 0 }, 500, function() {
                        overlay.remove();
                        keyNavigationDisabled = false;
                        option.cb_post_close();
                    });
                }
                var closeclickclass = '.vbox-overlay';
                if (!option.overlayClose) { closeclickclass = '.vbox-close'; }
                $('body').on('click', closeclickclass, function(e) { if ($(e.target).is('.vbox-overlay') || $(e.target).is('.vbox-content') || $(e.target).is('.vbox-close') || $(e.target).is('.vbox-preloader')) { closeVbox(); } });
                startX = 0;
                endX = 0;
                diff = 0;
                threshold = 50;
                startouch = false;

                function onDownEvent(e) {
                    content.addClass('animated');
                    startY = endY = e.pageY;
                    startX = endX = e.pageX;
                    startouch = true;
                }

                function onMoveEvent(e) {
                    if (startouch === true) {
                        endX = e.pageX;
                        endY = e.pageY;
                        diffX = endX - startX;
                        diffY = endY - startY;
                        var absdiffX = Math.abs(diffX);
                        var absdiffY = Math.abs(diffY);
                        if ((absdiffX > absdiffY) && (absdiffX <= 100)) {
                            e.preventDefault();
                            content.css('margin-left', diffX);
                        }
                    }
                }

                function onUpEvent(e) {
                    if (startouch === true) {
                        startouch = false;
                        var subject = obj;
                        var change = false;
                        diff = endX - startX;
                        if (diff < 0 && nextok === true) {
                            subject = thenext;
                            change = true;
                        }
                        if (diff > 0 && prevok === true) {
                            subject = theprev;
                            change = true;
                        }
                        if (Math.abs(diff) >= threshold && change === true) { navigateGall(subject); } else { content.css({ 'margin-left': 0, 'margin-right': 0 }); }
                    }
                }
                var TouchMouseEvent = { DOWN: "touchmousedown", UP: "touchmouseup", MOVE: "touchmousemove" };
                var onMouseEvent = function(event) {
                    var type;
                    switch (event.type) {
                        case "mousedown":
                            type = TouchMouseEvent.DOWN;
                            break;
                        case "mouseup":
                            type = TouchMouseEvent.UP;
                            break;
                        case "mouseout":
                            type = TouchMouseEvent.UP;
                            break;
                        case "mousemove":
                            type = TouchMouseEvent.MOVE;
                            break;
                        default:
                            return;
                    }
                    var touchMouseEvent = normalizeEvent(type, event, event.pageX, event.pageY);
                    $(event.target).trigger(touchMouseEvent);
                };
                var onTouchEvent = function(event) {
                    var type;
                    switch (event.type) {
                        case "touchstart":
                            type = TouchMouseEvent.DOWN;
                            break;
                        case "touchend":
                            type = TouchMouseEvent.UP;
                            break;
                        case "touchmove":
                            type = TouchMouseEvent.MOVE;
                            break;
                        default:
                            return;
                    }
                    var touch = event.originalEvent.touches[0];
                    var touchMouseEvent;
                    if (type == TouchMouseEvent.UP) { touchMouseEvent = normalizeEvent(type, event, null, null); } else { touchMouseEvent = normalizeEvent(type, event, touch.pageX, touch.pageY); }
                    $(event.target).trigger(touchMouseEvent);
                };
                var normalizeEvent = function(type, original, x, y) { return $.Event(type, { pageX: x, pageY: y, originalEvent: original }); };
                if ("ontouchstart" in window) {
                    $(document).on("touchstart", onTouchEvent);
                    $(document).on("touchmove", onTouchEvent);
                    $(document).on("touchend", onTouchEvent);
                } else {
                    $(document).on("mousedown", onMouseEvent);
                    $(document).on("mouseup", onMouseEvent);
                    $(document).on("mouseout", onMouseEvent);
                    $(document).on("mousemove", onMouseEvent);
                }

                function loadAjax() {
                    $.ajax({ url: dest, cache: false }).done(function(msg) {
                        content.html('<div class="vbox-inline">' + msg + '</div>');
                        preloadFirst();
                    }).fail(function() {
                        content.html('<div class="vbox-inline"><p>Error retrieving contents, please retry</div>');
                        updateoverlay();
                    });
                }

                function loadIframe() {
                    content.html('<iframe class="venoframe" src="' + dest + '"></iframe>');
                    updateoverlay();
                }

                function loadVid(autoplay) {
                    var player;
                    var videoObj = parseVideo(dest);
                    var stringAutoplay = autoplay ? "?rel=0&autoplay=1" : "?rel=0";
                    var queryvars = stringAutoplay + getUrlParameter(dest);
                    if (videoObj.type == 'vimeo') { player = 'https://player.vimeo.com/video/'; } else if (videoObj.type == 'youtube') { player = 'https://www.youtube.com/embed/'; }
                    content.html('<iframe class="venoframe vbvid" webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder="0" src="' + player + videoObj.id + queryvars + '"></iframe>');
                    updateoverlay();
                }

                function parseVideo(url) {
                    url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
                    var type;
                    if (RegExp.$3.indexOf('youtu') > -1) { type = 'youtube'; } else if (RegExp.$3.indexOf('vimeo') > -1) { type = 'vimeo'; }
                    return { type: type, id: RegExp.$6 };
                }

                function getUrlParameter(name) {
                    var result = '';
                    var sPageURL = decodeURIComponent(name);
                    var firstsplit = sPageURL.split('?');
                    if (firstsplit[1] !== undefined) {
                        var sURLVariables = firstsplit[1].split('&');
                        var sParameterName;
                        var i;
                        for (i = 0; i < sURLVariables.length; i++) {
                            sParameterName = sURLVariables[i].split('=');
                            result = result + '&' + sParameterName[0] + '=' + sParameterName[1];
                        }
                    }
                    return encodeURI(result);
                }

                function loadInline() {
                    content.html('<div class="vbox-inline">' + $(dest).html() + '</div>');
                    updateoverlay();
                }

                function preloadFirst() { images = content.find('img'); if (images.length) { images.each(function() { $(this).one('load', function() { updateoverlay(); }); }); } else { updateoverlay(); } }

                function updateoverlay() {
                    blocktitle.html(title);
                    content.find(">:first-child").addClass('figlio').css({ 'width': framewidth, 'height': frameheight, 'padding': border, 'background': bgcolor });
                    $('img.figlio').on('dragstart', function(event) { event.preventDefault(); });
                    updateOL();
                    content.animate({ 'opacity': '1' }, 'slow', function() { $preloader.hide(); });
                }

                function updateOL() {
                    var sonH = content.outerHeight();
                    var finH = $(window).height();
                    if (sonH + 60 < finH) { margine = (finH - sonH) / 2; } else { margine = '30px'; }
                    content.css('margin-top', margine);
                    content.css('margin-bottom', margine);
                    option.cb_post_resize();
                }
                $(window).resize(function() { if ($('.vbox-content').length) { setTimeout(updateOL(), 800); } });
            });
        }
    });
})(jQuery);
/*!
 * BeefUp v1.1.6 - A jQuery Accordion Plugin
 * Copyright 2016 Sascha KÃƒÂ¼nstler http://www.schaschaweb.de/
 */
! function(a) {
    "use strict";
    var b = {};
    b.defaults = { trigger: ".beefup__head", content: ".beefup__body", openClass: "is-open", animation: "slide", openSpeed: 200, closeSpeed: 200, scroll: !1, scrollSpeed: 400, scrollOffset: 0, openSingle: !1, stayOpen: null, selfClose: !1, hash: !0, breakpoints: null, onInit: function() {}, onOpen: function() {}, onClose: function() {}, onScroll: function() {} }, b.methods = {
        getVars: function(c) { var d = a.extend(!0, {}, c.data("beefup"), c.data("beefup-options")); return d.breakpoints && (d = b.methods.getResponsiveVars(d)), d },
        getResponsiveVars: function(b) { var c = window.innerWidth || a(window).width(); return b.breakpoints.sort(function(a, b) { return a.breakpoint < b.breakpoint ? -1 : a.breakpoint > b.breakpoint ? 1 : 0 }), a.each(b.breakpoints, function(d, e) { c > e.breakpoint && (b = a.extend({}, b, e.settings)) }), b },
        animation: function(a, b, c, d) {
            switch (a) {
                case "slideDown":
                    b.slideDown(c, d);
                    break;
                case "slideUp":
                    b.slideUp(c, d);
                    break;
                case "fadeIn":
                    b.fadeIn(c, d);
                    break;
                case "fadeOut":
                    b.fadeOut(c, d);
                    break;
                case "show":
                    b.show(c, d);
                    break;
                case "hide":
                    b.hide(c, d)
            }
        },
        getStayOpen: function(a, b) { var c; return "number" == typeof b ? c = a.eq(b) : "string" == typeof b && (c = a.filter(b)), c },
        selfClose: function(c, d) {
            a(document).on("click", function(e) {
                var f;
                a(e.target).closest(c).length || (f = c.filter("." + d.openClass), null !== d.stayOpen && (f = f.not(b.methods.getStayOpen(c, d.stayOpen))), f.length && c.close(f))
            })
        },
        hash: function(b, c) {
            var d = function() {
                var a = b.filter(window.location.hash);
                a.length && !a.hasClass(c.openClass) && b.click(a)
            };
            d(), a(window).on("hashchange", d)
        }
    }, a.fn.beefup = function(c) {
        var d = this;
        return this.open = function(c, e) {
            return c && c.length || (c = d), c.each(function() {
                var c = a(this),
                    d = b.methods.getVars(c),
                    f = c.find(d.content + ":first"),
                    g = "slide" === d.animation ? "slideDown" : "fade" === d.animation ? "fadeIn" : "show";
                b.methods.animation(g, f, d.openSpeed, function() { c.addClass(d.openClass), f.css("overflow", ""), e && "function" == typeof e && e(), d.onOpen && "function" == typeof d.onOpen && d.onOpen(c) })
            }), d
        }, this.close = function(c, e) {
            return c && c.length || (c = d), c.each(function() {
                var c = a(this),
                    d = b.methods.getVars(c),
                    f = c.find(d.content + ":first"),
                    g = "slide" === d.animation ? "slideUp" : "fade" === d.animation ? "fadeOut" : "hide";
                b.methods.animation(g, f, d.closeSpeed, function() { c.removeClass(d.openClass), f.css("overflow", ""), e && "function" == typeof e && e(), d.onClose && "function" == typeof d.onClose && d.onClose(c) })
            }), d
        }, this.scroll = function(c) { var e = b.methods.getVars(c); return a("html, body").animate({ scrollTop: c.offset().top + e.scrollOffset }, e.scrollSpeed).promise().done(function() { e.onScroll && "function" == typeof e.onScroll && e.onScroll(c) }), d }, this.click = function(a) { var c = b.methods.getVars(a); return c.openSingle && (null !== c.stayOpen ? d.close(d.not(a).not(b.methods.getStayOpen(d, c.stayOpen))) : d.close(d.not(a))), a.hasClass(c.openClass) ? d.close(a) : d.open(a, function() { c.scroll && d.scroll(a) }), d }, this.each(function(e, f) {
            var g = a(f),
                h = a.extend({}, b.defaults, c, g.data("beefup-options"));
            g.data("beefup") || (g.data("beefup", h), h.breakpoints && (h = b.methods.getResponsiveVars(h)), null !== h.stayOpen && g.is(b.methods.getStayOpen(d, h.stayOpen)) && g.addClass(h.openClass), g.not("." + h.openClass).find(h.content + ":first").hide(), h.onInit && "function" == typeof h.onInit && h.onInit(g), g.on("click", h.trigger + ":first", function(a) { a.preventDefault(), d.click(g) }), 0 === e && (h.hash && b.methods.hash(d, h), h.selfClose && b.methods.selfClose(d, h)))
        })
    }
}(jQuery);
! function(e, t) { "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.Swiper = t() }(this, function() {
    "use strict";
    var e = function(e) { for (var t = 0; t < e.length; t += 1) this[t] = e[t]; return this.length = e.length, this };

    function t(t, i) {
        var s = [],
            a = 0;
        if (t && !i && t instanceof e) return t;
        if (t)
            if ("string" == typeof t) {
                var r, n, o = t.trim();
                if (o.indexOf("<") >= 0 && o.indexOf(">") >= 0) { var l = "div"; for (0 === o.indexOf("<li") && (l = "ul"), 0 === o.indexOf("<tr") && (l = "tbody"), 0 !== o.indexOf("<td") && 0 !== o.indexOf("<th") || (l = "tr"), 0 === o.indexOf("<tbody") && (l = "table"), 0 === o.indexOf("<option") && (l = "select"), (n = document.createElement(l)).innerHTML = o, a = 0; a < n.childNodes.length; a += 1) s.push(n.childNodes[a]) } else
                    for (r = i || "#" !== t[0] || t.match(/[ .<>:~]/) ? (i || document).querySelectorAll(t.trim()) : [document.getElementById(t.trim().split("#")[1])], a = 0; a < r.length; a += 1) r[a] && s.push(r[a])
            } else if (t.nodeType || t === window || t === document) s.push(t);
        else if (t.length > 0 && t[0].nodeType)
            for (a = 0; a < t.length; a += 1) s.push(t[a]);
        return new e(s)
    }

    function i(e) { for (var t = [], i = 0; i < e.length; i += 1) - 1 === t.indexOf(e[i]) && t.push(e[i]); return t }
    t.fn = e.prototype, t.Class = e, t.Dom7 = e;
    "resize scroll".split(" ");
    var s = {
        addClass: function(e) {
            if (void 0 === e) return this;
            for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                for (var s = 0; s < this.length; s += 1) void 0 !== this[s].classList && this[s].classList.add(t[i]);
            return this
        },
        removeClass: function(e) {
            for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                for (var s = 0; s < this.length; s += 1) void 0 !== this[s].classList && this[s].classList.remove(t[i]);
            return this
        },
        hasClass: function(e) { return !!this[0] && this[0].classList.contains(e) },
        toggleClass: function(e) {
            for (var t = e.split(" "), i = 0; i < t.length; i += 1)
                for (var s = 0; s < this.length; s += 1) void 0 !== this[s].classList && this[s].classList.toggle(t[i]);
            return this
        },
        attr: function(e, t) {
            var i = arguments;
            if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;
            for (var s = 0; s < this.length; s += 1)
                if (2 === i.length) this[s].setAttribute(e, t);
                else
                    for (var a in e) this[s][a] = e[a], this[s].setAttribute(a, e[a]);
            return this
        },
        removeAttr: function(e) { for (var t = 0; t < this.length; t += 1) this[t].removeAttribute(e); return this },
        data: function(e, t) { var i; if (void 0 !== t) { for (var s = 0; s < this.length; s += 1)(i = this[s]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[e] = t; return this } if (i = this[0]) { if (i.dom7ElementDataStorage && e in i.dom7ElementDataStorage) return i.dom7ElementDataStorage[e]; var a = i.getAttribute("data-" + e); return a || void 0 } },
        transform: function(e) {
            for (var t = 0; t < this.length; t += 1) {
                var i = this[t].style;
                i.webkitTransform = e, i.transform = e
            }
            return this
        },
        transition: function(e) {
            "string" != typeof e && (e += "ms");
            for (var t = 0; t < this.length; t += 1) {
                var i = this[t].style;
                i.webkitTransitionDuration = e, i.transitionDuration = e
            }
            return this
        },
        on: function() {
            for (var e = [], i = arguments.length; i--;) e[i] = arguments[i];
            var s, a = e[0],
                r = e[1],
                n = e[2],
                o = e[3];

            function l(e) {
                var i = e.target;
                if (i) {
                    var s = e.target.dom7EventData || [];
                    if (s.unshift(e), t(i).is(r)) n.apply(i, s);
                    else
                        for (var a = t(i).parents(), o = 0; o < a.length; o += 1) t(a[o]).is(r) && n.apply(a[o], s)
                }
            }

            function d(e) {
                var t = e && e.target ? e.target.dom7EventData || [] : [];
                t.unshift(e), n.apply(this, t)
            }
            "function" == typeof e[1] && (a = (s = e)[0], n = s[1], o = s[2], r = void 0), o || (o = !1);
            for (var h, p = a.split(" "), c = 0; c < this.length; c += 1) {
                var u = this[c];
                if (r)
                    for (h = 0; h < p.length; h += 1) u.dom7LiveListeners || (u.dom7LiveListeners = []), u.dom7LiveListeners.push({ type: a, listener: n, proxyListener: l }), u.addEventListener(p[h], l, o);
                else
                    for (h = 0; h < p.length; h += 1) u.dom7Listeners || (u.dom7Listeners = []), u.dom7Listeners.push({ type: a, listener: n, proxyListener: d }), u.addEventListener(p[h], d, o)
            }
            return this
        },
        off: function() {
            for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
            var i, s = e[0],
                a = e[1],
                r = e[2],
                n = e[3];
            "function" == typeof e[1] && (s = (i = e)[0], r = i[1], n = i[2], a = void 0), n || (n = !1);
            for (var o = s.split(" "), l = 0; l < o.length; l += 1)
                for (var d = 0; d < this.length; d += 1) {
                    var h = this[d];
                    if (a) {
                        if (h.dom7LiveListeners)
                            for (var p = 0; p < h.dom7LiveListeners.length; p += 1) r ? h.dom7LiveListeners[p].listener === r && h.removeEventListener(o[l], h.dom7LiveListeners[p].proxyListener, n) : h.dom7LiveListeners[p].type === o[l] && h.removeEventListener(o[l], h.dom7LiveListeners[p].proxyListener, n)
                    } else if (h.dom7Listeners)
                        for (var c = 0; c < h.dom7Listeners.length; c += 1) r ? h.dom7Listeners[c].listener === r && h.removeEventListener(o[l], h.dom7Listeners[c].proxyListener, n) : h.dom7Listeners[c].type === o[l] && h.removeEventListener(o[l], h.dom7Listeners[c].proxyListener, n)
                }
            return this
        },
        trigger: function() {
            for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
            for (var i = e[0].split(" "), s = e[1], a = 0; a < i.length; a += 1)
                for (var r = 0; r < this.length; r += 1) {
                    var n = void 0;
                    try { n = new window.CustomEvent(i[a], { detail: s, bubbles: !0, cancelable: !0 }) } catch (e) {
                        (n = document.createEvent("Event")).initEvent(i[a], !0, !0), n.detail = s
                    }
                    this[r].dom7EventData = e.filter(function(e, t) { return t > 0 }), this[r].dispatchEvent(n), this[r].dom7EventData = [], delete this[r].dom7EventData
                }
            return this
        },
        transitionEnd: function(e) {
            var t, i = ["webkitTransitionEnd", "transitionend"],
                s = this;

            function a(r) {
                if (r.target === this)
                    for (e.call(this, r), t = 0; t < i.length; t += 1) s.off(i[t], a)
            }
            if (e)
                for (t = 0; t < i.length; t += 1) s.on(i[t], a);
            return this
        },
        outerWidth: function(e) { if (this.length > 0) { if (e) { var t = this.styles(); return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left")) } return this[0].offsetWidth } return null },
        outerHeight: function(e) { if (this.length > 0) { if (e) { var t = this.styles(); return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom")) } return this[0].offsetHeight } return null },
        offset: function() {
            if (this.length > 0) {
                var e = this[0],
                    t = e.getBoundingClientRect(),
                    i = document.body,
                    s = e.clientTop || i.clientTop || 0,
                    a = e.clientLeft || i.clientLeft || 0,
                    r = e === window ? window.scrollY : e.scrollTop,
                    n = e === window ? window.scrollX : e.scrollLeft;
                return { top: t.top + r - s, left: t.left + n - a }
            }
            return null
        },
        css: function(e, t) {
            var i;
            if (1 === arguments.length) {
                if ("string" != typeof e) {
                    for (i = 0; i < this.length; i += 1)
                        for (var s in e) this[i].style[s] = e[s];
                    return this
                }
                if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(e)
            }
            if (2 === arguments.length && "string" == typeof e) { for (i = 0; i < this.length; i += 1) this[i].style[e] = t; return this }
            return this
        },
        each: function(e) {
            if (!e) return this;
            for (var t = 0; t < this.length; t += 1)
                if (!1 === e.call(this[t], t, this[t])) return this;
            return this
        },
        html: function(e) { if (void 0 === e) return this[0] ? this[0].innerHTML : void 0; for (var t = 0; t < this.length; t += 1) this[t].innerHTML = e; return this },
        text: function(e) { if (void 0 === e) return this[0] ? this[0].textContent.trim() : null; for (var t = 0; t < this.length; t += 1) this[t].textContent = e; return this },
        is: function(i) {
            var s, a, r = this[0];
            if (!r || void 0 === i) return !1;
            if ("string" == typeof i) {
                if (r.matches) return r.matches(i);
                if (r.webkitMatchesSelector) return r.webkitMatchesSelector(i);
                if (r.msMatchesSelector) return r.msMatchesSelector(i);
                for (s = t(i), a = 0; a < s.length; a += 1)
                    if (s[a] === r) return !0;
                return !1
            }
            if (i === document) return r === document;
            if (i === window) return r === window;
            if (i.nodeType || i instanceof e) {
                for (s = i.nodeType ? [i] : i, a = 0; a < s.length; a += 1)
                    if (s[a] === r) return !0;
                return !1
            }
            return !1
        },
        index: function() { var e, t = this[0]; if (t) { for (e = 0; null !== (t = t.previousSibling);) 1 === t.nodeType && (e += 1); return e } },
        eq: function(t) { if (void 0 === t) return this; var i, s = this.length; return new e(t > s - 1 ? [] : t < 0 ? (i = s + t) < 0 ? [] : [this[i]] : [this[t]]) },
        append: function() {
            for (var t, i = [], s = arguments.length; s--;) i[s] = arguments[s];
            for (var a = 0; a < i.length; a += 1) {
                t = i[a];
                for (var r = 0; r < this.length; r += 1)
                    if ("string" == typeof t) { var n = document.createElement("div"); for (n.innerHTML = t; n.firstChild;) this[r].appendChild(n.firstChild) } else if (t instanceof e)
                    for (var o = 0; o < t.length; o += 1) this[r].appendChild(t[o]);
                else this[r].appendChild(t)
            }
            return this
        },
        prepend: function(t) {
            var i, s;
            for (i = 0; i < this.length; i += 1)
                if ("string" == typeof t) { var a = document.createElement("div"); for (a.innerHTML = t, s = a.childNodes.length - 1; s >= 0; s -= 1) this[i].insertBefore(a.childNodes[s], this[i].childNodes[0]) } else if (t instanceof e)
                for (s = 0; s < t.length; s += 1) this[i].insertBefore(t[s], this[i].childNodes[0]);
            else this[i].insertBefore(t, this[i].childNodes[0]);
            return this
        },
        next: function(i) { return this.length > 0 ? i ? this[0].nextElementSibling && t(this[0].nextElementSibling).is(i) ? new e([this[0].nextElementSibling]) : new e([]) : this[0].nextElementSibling ? new e([this[0].nextElementSibling]) : new e([]) : new e([]) },
        nextAll: function(i) {
            var s = [],
                a = this[0];
            if (!a) return new e([]);
            for (; a.nextElementSibling;) {
                var r = a.nextElementSibling;
                i ? t(r).is(i) && s.push(r) : s.push(r), a = r
            }
            return new e(s)
        },
        prev: function(i) { if (this.length > 0) { var s = this[0]; return i ? s.previousElementSibling && t(s.previousElementSibling).is(i) ? new e([s.previousElementSibling]) : new e([]) : s.previousElementSibling ? new e([s.previousElementSibling]) : new e([]) } return new e([]) },
        prevAll: function(i) {
            var s = [],
                a = this[0];
            if (!a) return new e([]);
            for (; a.previousElementSibling;) {
                var r = a.previousElementSibling;
                i ? t(r).is(i) && s.push(r) : s.push(r), a = r
            }
            return new e(s)
        },
        parent: function(e) { for (var s = [], a = 0; a < this.length; a += 1) null !== this[a].parentNode && (e ? t(this[a].parentNode).is(e) && s.push(this[a].parentNode) : s.push(this[a].parentNode)); return t(i(s)) },
        parents: function(e) {
            for (var s = [], a = 0; a < this.length; a += 1)
                for (var r = this[a].parentNode; r;) e ? t(r).is(e) && s.push(r) : s.push(r), r = r.parentNode;
            return t(i(s))
        },
        closest: function(t) { var i = this; return void 0 === t ? new e([]) : (i.is(t) || (i = i.parents(t).eq(0)), i) },
        find: function(t) {
            for (var i = [], s = 0; s < this.length; s += 1)
                for (var a = this[s].querySelectorAll(t), r = 0; r < a.length; r += 1) i.push(a[r]);
            return new e(i)
        },
        children: function(s) {
            for (var a = [], r = 0; r < this.length; r += 1)
                for (var n = this[r].childNodes, o = 0; o < n.length; o += 1) s ? 1 === n[o].nodeType && t(n[o]).is(s) && a.push(n[o]) : 1 === n[o].nodeType && a.push(n[o]);
            return new e(i(a))
        },
        remove: function() { for (var e = 0; e < this.length; e += 1) this[e].parentNode && this[e].parentNode.removeChild(this[e]); return this },
        add: function() { for (var e = [], i = arguments.length; i--;) e[i] = arguments[i]; var s, a; for (s = 0; s < e.length; s += 1) { var r = t(e[s]); for (a = 0; a < r.length; a += 1) this[this.length] = r[a], this.length += 1 } return this },
        styles: function() { return this[0] ? window.getComputedStyle(this[0], null) : {} }
    };
    Object.keys(s).forEach(function(e) { t.fn[e] = s[e] });
    var a, r, n, o = "undefined" == typeof window ? { navigator: { userAgent: "" }, location: {}, history: {}, addEventListener: function() {}, removeEventListener: function() {}, getComputedStyle: function() { return {} }, Image: function() {}, Date: function() {}, screen: {} } : window,
        l = {
            deleteProps: function(e) {
                var t = e;
                Object.keys(t).forEach(function(e) { try { t[e] = null } catch (e) {} try { delete t[e] } catch (e) {} })
            },
            nextTick: function(e, t) { return void 0 === t && (t = 0), setTimeout(e, t) },
            now: function() { return Date.now() },
            getTranslate: function(e, t) {
                var i, s, a;
                void 0 === t && (t = "x");
                var r = o.getComputedStyle(e, null);
                return o.WebKitCSSMatrix ? ((s = r.transform || r.webkitTransform).split(",").length > 6 && (s = s.split(", ").map(function(e) { return e.replace(",", ".") }).join(", ")), a = new o.WebKitCSSMatrix("none" === s ? "" : s)) : i = (a = r.MozTransform || r.OTransform || r.MsTransform || r.msTransform || r.transform || r.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === t && (s = o.WebKitCSSMatrix ? a.m41 : 16 === i.length ? parseFloat(i[12]) : parseFloat(i[4])), "y" === t && (s = o.WebKitCSSMatrix ? a.m42 : 16 === i.length ? parseFloat(i[13]) : parseFloat(i[5])), s || 0
            },
            parseUrlQuery: function(e) {
                var t, i, s, a, r = {},
                    n = e || o.location.href;
                if ("string" == typeof n && n.length)
                    for (a = (i = (n = n.indexOf("?") > -1 ? n.replace(/\S*\?/, "") : "").split("&").filter(function(e) { return "" !== e })).length, t = 0; t < a; t += 1) s = i[t].replace(/#\S+/g, "").split("="), r[decodeURIComponent(s[0])] = void 0 === s[1] ? void 0 : decodeURIComponent(s[1]) || "";
                return r
            },
            isObject: function(e) { return "object" == typeof e && null !== e && e.constructor && e.constructor === Object },
            extend: function() {
                for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
                for (var i = Object(e[0]), s = 1; s < e.length; s += 1) {
                    var a = e[s];
                    if (void 0 !== a && null !== a)
                        for (var r = Object.keys(Object(a)), n = 0, o = r.length; n < o; n += 1) {
                            var d = r[n],
                                h = Object.getOwnPropertyDescriptor(a, d);
                            void 0 !== h && h.enumerable && (l.isObject(i[d]) && l.isObject(a[d]) ? l.extend(i[d], a[d]) : !l.isObject(i[d]) && l.isObject(a[d]) ? (i[d] = {}, l.extend(i[d], a[d])) : i[d] = a[d])
                        }
                }
                return i
            }
        },
        d = "undefined" == typeof document ? { addEventListener: function() {}, removeEventListener: function() {}, activeElement: { blur: function() {}, nodeName: "" }, querySelector: function() { return {} }, querySelectorAll: function() { return [] }, createElement: function() { return { style: {}, setAttribute: function() {}, getElementsByTagName: function() { return [] } } }, location: { hash: "" } } : document,
        h = (n = d.createElement("div"), {
            touch: o.Modernizr && !0 === o.Modernizr.touch || !!("ontouchstart" in o || o.DocumentTouch && d instanceof o.DocumentTouch),
            pointerEvents: !(!o.navigator.pointerEnabled && !o.PointerEvent),
            prefixedPointerEvents: !!o.navigator.msPointerEnabled,
            transition: (r = n.style, "transition" in r || "webkitTransition" in r || "MozTransition" in r),
            transforms3d: o.Modernizr && !0 === o.Modernizr.csstransforms3d || (a = n.style, "webkitPerspective" in a || "MozPerspective" in a || "OPerspective" in a || "MsPerspective" in a || "perspective" in a),
            flexbox: function() {
                for (var e = n.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), i = 0; i < t.length; i += 1)
                    if (t[i] in e) return !0;
                return !1
            }(),
            observer: "MutationObserver" in o || "WebkitMutationObserver" in o,
            passiveListener: function() {
                var e = !1;
                try {
                    var t = Object.defineProperty({}, "passive", { get: function() { e = !0 } });
                    o.addEventListener("testPassiveListener", null, t)
                } catch (e) {}
                return e
            }(),
            gestures: "ongesturestart" in o
        }),
        p = function(e) {
            void 0 === e && (e = {});
            var t = this;
            t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach(function(e) { t.on(e, t.params.on[e]) })
        },
        c = { components: { configurable: !0 } };
    p.prototype.on = function(e, t) { var i = this; return "function" != typeof t ? i : (e.split(" ").forEach(function(e) { i.eventsListeners[e] || (i.eventsListeners[e] = []), i.eventsListeners[e].push(t) }), i) }, p.prototype.once = function(e, t) {
        var i = this;
        if ("function" != typeof t) return i;
        return i.on(e, function s() {
            for (var a = [], r = arguments.length; r--;) a[r] = arguments[r];
            t.apply(i, a), i.off(e, s)
        })
    }, p.prototype.off = function(e, t) { var i = this; return e.split(" ").forEach(function(e) { void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e].forEach(function(s, a) { s === t && i.eventsListeners[e].splice(a, 1) }) }), i }, p.prototype.emit = function() {
        for (var e = [], t = arguments.length; t--;) e[t] = arguments[t];
        var i, s, a, r = this;
        return r.eventsListeners ? ("string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0], s = e.slice(1, e.length), a = r) : (i = e[0].events, s = e[0].data, a = e[0].context || r), (Array.isArray(i) ? i : i.split(" ")).forEach(function(e) {
            if (r.eventsListeners[e]) {
                var t = [];
                r.eventsListeners[e].forEach(function(e) { t.push(e) }), t.forEach(function(e) { e.apply(a, s) })
            }
        }), r) : r
    }, p.prototype.useModulesParams = function(e) {
        var t = this;
        t.modules && Object.keys(t.modules).forEach(function(i) {
            var s = t.modules[i];
            s.params && l.extend(e, s.params)
        })
    }, p.prototype.useModules = function(e) {
        void 0 === e && (e = {});
        var t = this;
        t.modules && Object.keys(t.modules).forEach(function(i) {
            var s = t.modules[i],
                a = e[i] || {};
            s.instance && Object.keys(s.instance).forEach(function(e) {
                var i = s.instance[e];
                t[e] = "function" == typeof i ? i.bind(t) : i
            }), s.on && t.on && Object.keys(s.on).forEach(function(e) { t.on(e, s.on[e]) }), s.create && s.create.bind(t)(a)
        })
    }, c.components.set = function(e) { this.use && this.use(e) }, p.installModule = function(e) {
        for (var t = [], i = arguments.length - 1; i-- > 0;) t[i] = arguments[i + 1];
        var s = this;
        s.prototype.modules || (s.prototype.modules = {});
        var a = e.name || Object.keys(s.prototype.modules).length + "_" + l.now();
        return s.prototype.modules[a] = e, e.proto && Object.keys(e.proto).forEach(function(t) { s.prototype[t] = e.proto[t] }), e.static && Object.keys(e.static).forEach(function(t) { s[t] = e.static[t] }), e.install && e.install.apply(s, t), s
    }, p.use = function(e) { for (var t = [], i = arguments.length - 1; i-- > 0;) t[i] = arguments[i + 1]; var s = this; return Array.isArray(e) ? (e.forEach(function(e) { return s.installModule(e) }), s) : s.installModule.apply(s, [e].concat(t)) }, Object.defineProperties(p, c);
    var u = {
            updateSize: function() {
                var e, t, i = this.$el;
                e = void 0 !== this.params.width ? this.params.width : i[0].clientWidth, t = void 0 !== this.params.height ? this.params.height : i[0].clientHeight, 0 === e && this.isHorizontal() || 0 === t && this.isVertical() || (e = e - parseInt(i.css("padding-left"), 10) - parseInt(i.css("padding-right"), 10), t = t - parseInt(i.css("padding-top"), 10) - parseInt(i.css("padding-bottom"), 10), l.extend(this, { width: e, height: t, size: this.isHorizontal() ? e : t }))
            },
            updateSlides: function() {
                var e = this.params,
                    t = this.$wrapperEl,
                    i = this.size,
                    s = this.rtl,
                    a = this.wrongRTL,
                    r = t.children("." + this.params.slideClass),
                    n = this.virtual && e.virtual.enabled ? this.virtual.slides.length : r.length,
                    o = [],
                    d = [],
                    p = [],
                    c = e.slidesOffsetBefore;
                "function" == typeof c && (c = e.slidesOffsetBefore.call(this));
                var u = e.slidesOffsetAfter;
                "function" == typeof u && (u = e.slidesOffsetAfter.call(this));
                var f = n,
                    v = this.snapGrid.length,
                    m = this.snapGrid.length,
                    g = e.spaceBetween,
                    b = -c,
                    w = 0,
                    y = 0;
                if (void 0 !== i) {
                    var x, T;
                    "string" == typeof g && g.indexOf("%") >= 0 && (g = parseFloat(g.replace("%", "")) / 100 * i), this.virtualSize = -g, s ? r.css({ marginLeft: "", marginTop: "" }) : r.css({ marginRight: "", marginBottom: "" }), e.slidesPerColumn > 1 && (x = Math.floor(n / e.slidesPerColumn) === n / this.params.slidesPerColumn ? n : Math.ceil(n / e.slidesPerColumn) * e.slidesPerColumn, "auto" !== e.slidesPerView && "row" === e.slidesPerColumnFill && (x = Math.max(x, e.slidesPerView * e.slidesPerColumn)));
                    for (var E, S = e.slidesPerColumn, C = x / S, M = C - (e.slidesPerColumn * C - n), z = 0; z < n; z += 1) {
                        T = 0;
                        var P = r.eq(z);
                        if (e.slidesPerColumn > 1) {
                            var k = void 0,
                                $ = void 0,
                                L = void 0;
                            "column" === e.slidesPerColumnFill ? (L = z - ($ = Math.floor(z / S)) * S, ($ > M || $ === M && L === S - 1) && (L += 1) >= S && (L = 0, $ += 1), k = $ + L * x / S, P.css({ "-webkit-box-ordinal-group": k, "-moz-box-ordinal-group": k, "-ms-flex-order": k, "-webkit-order": k, order: k })) : $ = z - (L = Math.floor(z / C)) * C, P.css("margin-" + (this.isHorizontal() ? "top" : "left"), 0 !== L && e.spaceBetween && e.spaceBetween + "px").attr("data-swiper-column", $).attr("data-swiper-row", L)
                        }
                        "none" !== P.css("display") && ("auto" === e.slidesPerView ? (T = this.isHorizontal() ? P.outerWidth(!0) : P.outerHeight(!0), e.roundLengths && (T = Math.floor(T))) : (T = (i - (e.slidesPerView - 1) * g) / e.slidesPerView, e.roundLengths && (T = Math.floor(T)), r[z] && (this.isHorizontal() ? r[z].style.width = T + "px" : r[z].style.height = T + "px")), r[z] && (r[z].swiperSlideSize = T), p.push(T), e.centeredSlides ? (b = b + T / 2 + w / 2 + g, 0 === w && 0 !== z && (b = b - i / 2 - g), 0 === z && (b = b - i / 2 - g), Math.abs(b) < .001 && (b = 0), y % e.slidesPerGroup == 0 && o.push(b), d.push(b)) : (y % e.slidesPerGroup == 0 && o.push(b), d.push(b), b = b + T + g), this.virtualSize += T + g, w = T, y += 1)
                    }
                    if (this.virtualSize = Math.max(this.virtualSize, i) + u, s && a && ("slide" === e.effect || "coverflow" === e.effect) && t.css({ width: this.virtualSize + e.spaceBetween + "px" }), h.flexbox && !e.setWrapperSize || (this.isHorizontal() ? t.css({ width: this.virtualSize + e.spaceBetween + "px" }) : t.css({ height: this.virtualSize + e.spaceBetween + "px" })), e.slidesPerColumn > 1 && (this.virtualSize = (T + e.spaceBetween) * x, this.virtualSize = Math.ceil(this.virtualSize / e.slidesPerColumn) - e.spaceBetween, this.isHorizontal() ? t.css({ width: this.virtualSize + e.spaceBetween + "px" }) : t.css({ height: this.virtualSize + e.spaceBetween + "px" }), e.centeredSlides)) {
                        E = [];
                        for (var I = 0; I < o.length; I += 1) o[I] < this.virtualSize + o[0] && E.push(o[I]);
                        o = E
                    }
                    if (!e.centeredSlides) {
                        E = [];
                        for (var D = 0; D < o.length; D += 1) o[D] <= this.virtualSize - i && E.push(o[D]);
                        o = E, Math.floor(this.virtualSize - i) - Math.floor(o[o.length - 1]) > 1 && o.push(this.virtualSize - i)
                    }
                    0 === o.length && (o = [0]), 0 !== e.spaceBetween && (this.isHorizontal() ? s ? r.css({ marginLeft: g + "px" }) : r.css({ marginRight: g + "px" }) : r.css({ marginBottom: g + "px" })), l.extend(this, { slides: r, snapGrid: o, slidesGrid: d, slidesSizesGrid: p }), n !== f && this.emit("slidesLengthChange"), o.length !== v && (this.params.watchOverflow && this.checkOverflow(), this.emit("snapGridLengthChange")), d.length !== m && this.emit("slidesGridLengthChange"), (e.watchSlidesProgress || e.watchSlidesVisibility) && this.updateSlidesOffset()
                }
            },
            updateAutoHeight: function() {
                var e, t = [],
                    i = 0;
                if ("auto" !== this.params.slidesPerView && this.params.slidesPerView > 1)
                    for (e = 0; e < Math.ceil(this.params.slidesPerView); e += 1) {
                        var s = this.activeIndex + e;
                        if (s > this.slides.length) break;
                        t.push(this.slides.eq(s)[0])
                    } else t.push(this.slides.eq(this.activeIndex)[0]);
                for (e = 0; e < t.length; e += 1)
                    if (void 0 !== t[e]) {
                        var a = t[e].offsetHeight;
                        i = a > i ? a : i
                    }
                i && this.$wrapperEl.css("height", i + "px")
            },
            updateSlidesOffset: function() { for (var e = this.slides, t = 0; t < e.length; t += 1) e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop },
            updateSlidesProgress: function(e) {
                void 0 === e && (e = this.translate || 0);
                var t = this.params,
                    i = this.slides,
                    s = this.rtl;
                if (0 !== i.length) {
                    void 0 === i[0].swiperSlideOffset && this.updateSlidesOffset();
                    var a = -e;
                    s && (a = e), i.removeClass(t.slideVisibleClass);
                    for (var r = 0; r < i.length; r += 1) {
                        var n = i[r],
                            o = (a + (t.centeredSlides ? this.minTranslate() : 0) - n.swiperSlideOffset) / (n.swiperSlideSize + t.spaceBetween);
                        if (t.watchSlidesVisibility) {
                            var l = -(a - n.swiperSlideOffset),
                                d = l + this.slidesSizesGrid[r];
                            (l >= 0 && l < this.size || d > 0 && d <= this.size || l <= 0 && d >= this.size) && i.eq(r).addClass(t.slideVisibleClass)
                        }
                        n.progress = s ? -o : o
                    }
                }
            },
            updateProgress: function(e) {
                void 0 === e && (e = this.translate || 0);
                var t = this.params,
                    i = this.maxTranslate() - this.minTranslate(),
                    s = this.progress,
                    a = this.isBeginning,
                    r = this.isEnd,
                    n = a,
                    o = r;
                0 === i ? (s = 0, a = !0, r = !0) : (a = (s = (e - this.minTranslate()) / i) <= 0, r = s >= 1), l.extend(this, { progress: s, isBeginning: a, isEnd: r }), (t.watchSlidesProgress || t.watchSlidesVisibility) && this.updateSlidesProgress(e), a && !n && this.emit("reachBeginning toEdge"), r && !o && this.emit("reachEnd toEdge"), (n && !a || o && !r) && this.emit("fromEdge"), this.emit("progress", s)
            },
            updateSlidesClasses: function() {
                var e, t = this.slides,
                    i = this.params,
                    s = this.$wrapperEl,
                    a = this.activeIndex,
                    r = this.realIndex,
                    n = this.virtual && i.virtual.enabled;
                t.removeClass(i.slideActiveClass + " " + i.slideNextClass + " " + i.slidePrevClass + " " + i.slideDuplicateActiveClass + " " + i.slideDuplicateNextClass + " " + i.slideDuplicatePrevClass), (e = n ? this.$wrapperEl.find("." + i.slideClass + '[data-swiper-slide-index="' + a + '"]') : t.eq(a)).addClass(i.slideActiveClass), i.loop && (e.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + r + '"]').addClass(i.slideDuplicateActiveClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + r + '"]').addClass(i.slideDuplicateActiveClass));
                var o = e.nextAll("." + i.slideClass).eq(0).addClass(i.slideNextClass);
                i.loop && 0 === o.length && (o = t.eq(0)).addClass(i.slideNextClass);
                var l = e.prevAll("." + i.slideClass).eq(0).addClass(i.slidePrevClass);
                i.loop && 0 === l.length && (l = t.eq(-1)).addClass(i.slidePrevClass), i.loop && (o.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + o.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + o.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass), l.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass))
            },
            updateActiveIndex: function(e) {
                var t, i = this.rtl ? this.translate : -this.translate,
                    s = this.slidesGrid,
                    a = this.snapGrid,
                    r = this.params,
                    n = this.activeIndex,
                    o = this.realIndex,
                    d = this.snapIndex,
                    h = e;
                if (void 0 === h) {
                    for (var p = 0; p < s.length; p += 1) void 0 !== s[p + 1] ? i >= s[p] && i < s[p + 1] - (s[p + 1] - s[p]) / 2 ? h = p : i >= s[p] && i < s[p + 1] && (h = p + 1) : i >= s[p] && (h = p);
                    r.normalizeSlideIndex && (h < 0 || void 0 === h) && (h = 0)
                }
                if ((t = a.indexOf(i) >= 0 ? a.indexOf(i) : Math.floor(h / r.slidesPerGroup)) >= a.length && (t = a.length - 1), h !== n) {
                    var c = parseInt(this.slides.eq(h).attr("data-swiper-slide-index") || h, 10);
                    l.extend(this, { snapIndex: t, realIndex: c, previousIndex: n, activeIndex: h }), this.emit("activeIndexChange"), this.emit("snapIndexChange"), o !== c && this.emit("realIndexChange"), this.emit("slideChange")
                } else t !== d && (this.snapIndex = t, this.emit("snapIndexChange"))
            },
            updateClickedSlide: function(e) {
                var i = this.params,
                    s = t(e.target).closest("." + i.slideClass)[0],
                    a = !1;
                if (s)
                    for (var r = 0; r < this.slides.length; r += 1) this.slides[r] === s && (a = !0);
                if (!s || !a) return this.clickedSlide = void 0, void(this.clickedIndex = void 0);
                this.clickedSlide = s, this.virtual && this.params.virtual.enabled ? this.clickedIndex = parseInt(t(s).attr("data-swiper-slide-index"), 10) : this.clickedIndex = t(s).index(), i.slideToClickedSlide && void 0 !== this.clickedIndex && this.clickedIndex !== this.activeIndex && this.slideToClickedSlide()
            }
        },
        f = {
            getTranslate: function(e) {
                void 0 === e && (e = this.isHorizontal() ? "x" : "y");
                var t = this.params,
                    i = this.rtl,
                    s = this.translate,
                    a = this.$wrapperEl;
                if (t.virtualTranslate) return i ? -s : s;
                var r = l.getTranslate(a[0], e);
                return i && (r = -r), r || 0
            },
            setTranslate: function(e, t) {
                var i = this.rtl,
                    s = this.params,
                    a = this.$wrapperEl,
                    r = this.progress,
                    n = 0,
                    o = 0;
                this.isHorizontal() ? n = i ? -e : e : o = e, s.roundLengths && (n = Math.floor(n), o = Math.floor(o)), s.virtualTranslate || (h.transforms3d ? a.transform("translate3d(" + n + "px, " + o + "px, 0px)") : a.transform("translate(" + n + "px, " + o + "px)")), this.translate = this.isHorizontal() ? n : o;
                var l = this.maxTranslate() - this.minTranslate();
                (0 === l ? 0 : (e - this.minTranslate()) / l) !== r && this.updateProgress(e), this.emit("setTranslate", this.translate, t)
            },
            minTranslate: function() { return -this.snapGrid[0] },
            maxTranslate: function() { return -this.snapGrid[this.snapGrid.length - 1] }
        },
        v = {
            setTransition: function(e, t) { this.$wrapperEl.transition(e), this.emit("setTransition", e, t) },
            transitionStart: function(e) {
                void 0 === e && (e = !0);
                var t = this.activeIndex,
                    i = this.params,
                    s = this.previousIndex;
                i.autoHeight && this.updateAutoHeight(), this.emit("transitionStart"), e && t !== s && (this.emit("slideChangeTransitionStart"), t > s ? this.emit("slideNextTransitionStart") : this.emit("slidePrevTransitionStart"))
            },
            transitionEnd: function(e) {
                void 0 === e && (e = !0);
                var t = this.activeIndex,
                    i = this.previousIndex;
                this.animating = !1, this.setTransition(0), this.emit("transitionEnd"), e && t !== i && (this.emit("slideChangeTransitionEnd"), t > i ? this.emit("slideNextTransitionEnd") : this.emit("slidePrevTransitionEnd"))
            }
        },
        m = {
            slideTo: function(e, t, i, s) {
                void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
                var a = this,
                    r = e;
                r < 0 && (r = 0);
                var n = a.params,
                    o = a.snapGrid,
                    l = a.slidesGrid,
                    d = a.previousIndex,
                    p = a.activeIndex,
                    c = a.rtl,
                    u = a.$wrapperEl,
                    f = Math.floor(r / n.slidesPerGroup);
                f >= o.length && (f = o.length - 1), (p || n.initialSlide || 0) === (d || 0) && i && a.emit("beforeSlideChangeStart");
                var v = -o[f];
                if (a.updateProgress(v), n.normalizeSlideIndex)
                    for (var m = 0; m < l.length; m += 1) - Math.floor(100 * v) >= Math.floor(100 * l[m]) && (r = m);
                if (a.initialized) { if (!a.allowSlideNext && v < a.translate && v < a.minTranslate()) return !1; if (!a.allowSlidePrev && v > a.translate && v > a.maxTranslate() && (p || 0) !== r) return !1 }
                return c && -v === a.translate || !c && v === a.translate ? (a.updateActiveIndex(r), n.autoHeight && a.updateAutoHeight(), a.updateSlidesClasses(), "slide" !== n.effect && a.setTranslate(v), !1) : (0 !== t && h.transition ? (a.setTransition(t), a.setTranslate(v), a.updateActiveIndex(r), a.updateSlidesClasses(), a.emit("beforeTransitionStart", t, s), a.transitionStart(i), a.animating || (a.animating = !0, u.transitionEnd(function() { a && !a.destroyed && a.transitionEnd(i) }))) : (a.setTransition(0), a.setTranslate(v), a.updateActiveIndex(r), a.updateSlidesClasses(), a.emit("beforeTransitionStart", t, s), a.transitionStart(i), a.transitionEnd(i)), !0)
            },
            slideNext: function(e, t, i) {
                void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                var s = this.params,
                    a = this.animating;
                return s.loop ? !a && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex + s.slidesPerGroup, e, t, i)) : this.slideTo(this.activeIndex + s.slidesPerGroup, e, t, i)
            },
            slidePrev: function(e, t, i) {
                void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
                var s = this.params,
                    a = this.animating;
                return s.loop ? !a && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex - 1, e, t, i)) : this.slideTo(this.activeIndex - 1, e, t, i)
            },
            slideReset: function(e, t, i) { void 0 === e && (e = this.params.speed), void 0 === t && (t = !0); return this.slideTo(this.activeIndex, e, t, i) },
            slideToClickedSlide: function() {
                var e, i = this,
                    s = i.params,
                    a = i.$wrapperEl,
                    r = "auto" === s.slidesPerView ? i.slidesPerViewDynamic() : s.slidesPerView,
                    n = i.clickedIndex;
                if (s.loop) {
                    if (i.animating) return;
                    e = parseInt(t(i.clickedSlide).attr("data-swiper-slide-index"), 10), s.centeredSlides ? n < i.loopedSlides - r / 2 || n > i.slides.length - i.loopedSlides + r / 2 ? (i.loopFix(), n = a.children("." + s.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + s.slideDuplicateClass + ")").eq(0).index(), l.nextTick(function() { i.slideTo(n) })) : i.slideTo(n) : n > i.slides.length - r ? (i.loopFix(), n = a.children("." + s.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + s.slideDuplicateClass + ")").eq(0).index(), l.nextTick(function() { i.slideTo(n) })) : i.slideTo(n)
                } else i.slideTo(n)
            }
        },
        g = {
            loopCreate: function() {
                var e = this,
                    i = e.params,
                    s = e.$wrapperEl;
                s.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                var a = s.children("." + i.slideClass);
                if (i.loopFillGroupWithBlank) {
                    var r = i.slidesPerGroup - a.length % i.slidesPerGroup;
                    if (r !== i.slidesPerGroup) {
                        for (var n = 0; n < r; n += 1) {
                            var o = t(d.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                            s.append(o)
                        }
                        a = s.children("." + i.slideClass)
                    }
                }
                "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = a.length), e.loopedSlides = parseInt(i.loopedSlides || i.slidesPerView, 10), e.loopedSlides += i.loopAdditionalSlides, e.loopedSlides > a.length && (e.loopedSlides = a.length);
                var l = [],
                    h = [];
                a.each(function(i, s) {
                    var r = t(s);
                    i < e.loopedSlides && h.push(s), i < a.length && i >= a.length - e.loopedSlides && l.push(s), r.attr("data-swiper-slide-index", i)
                });
                for (var p = 0; p < h.length; p += 1) s.append(t(h[p].cloneNode(!0)).addClass(i.slideDuplicateClass));
                for (var c = l.length - 1; c >= 0; c -= 1) s.prepend(t(l[c].cloneNode(!0)).addClass(i.slideDuplicateClass))
            },
            loopFix: function() {
                var e, t = this.params,
                    i = this.activeIndex,
                    s = this.slides,
                    a = this.loopedSlides,
                    r = this.allowSlidePrev,
                    n = this.allowSlideNext;
                this.allowSlidePrev = !0, this.allowSlideNext = !0, i < a ? (e = s.length - 3 * a + i, e += a, this.slideTo(e, 0, !1, !0)) : ("auto" === t.slidesPerView && i >= 2 * a || i > s.length - 2 * t.slidesPerView) && (e = -s.length + i + a, e += a, this.slideTo(e, 0, !1, !0)), this.allowSlidePrev = r, this.allowSlideNext = n
            },
            loopDestroy: function() {
                var e = this.$wrapperEl,
                    t = this.params,
                    i = this.slides;
                e.children("." + t.slideClass + "." + t.slideDuplicateClass).remove(), i.removeAttr("data-swiper-slide-index")
            }
        },
        b = {
            setGrabCursor: function(e) {
                if (!h.touch && this.params.simulateTouch) {
                    var t = this.el;
                    t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab"
                }
            },
            unsetGrabCursor: function() { h.touch || (this.el.style.cursor = "") }
        },
        w = {
            appendSlide: function(e) {
                var t = this.$wrapperEl,
                    i = this.params;
                if (i.loop && this.loopDestroy(), "object" == typeof e && "length" in e)
                    for (var s = 0; s < e.length; s += 1) e[s] && t.append(e[s]);
                else t.append(e);
                i.loop && this.loopCreate(), i.observer && h.observer || this.update()
            },
            prependSlide: function(e) {
                var t = this.params,
                    i = this.$wrapperEl,
                    s = this.activeIndex;
                t.loop && this.loopDestroy();
                var a = s + 1;
                if ("object" == typeof e && "length" in e) {
                    for (var r = 0; r < e.length; r += 1) e[r] && i.prepend(e[r]);
                    a = s + e.length
                } else i.prepend(e);
                t.loop && this.loopCreate(), t.observer && h.observer || this.update(), this.slideTo(a, 0, !1)
            },
            removeSlide: function(e) {
                var t = this.params,
                    i = this.$wrapperEl,
                    s = this.activeIndex;
                t.loop && (this.loopDestroy(), this.slides = i.children("." + t.slideClass));
                var a, r = s;
                if ("object" == typeof e && "length" in e) {
                    for (var n = 0; n < e.length; n += 1) a = e[n], this.slides[a] && this.slides.eq(a).remove(), a < r && (r -= 1);
                    r = Math.max(r, 0)
                } else a = e, this.slides[a] && this.slides.eq(a).remove(), a < r && (r -= 1), r = Math.max(r, 0);
                t.loop && this.loopCreate(), t.observer && h.observer || this.update(), t.loop ? this.slideTo(r + this.loopedSlides, 0, !1) : this.slideTo(r, 0, !1)
            },
            removeAllSlides: function() {
                for (var e = [], t = 0; t < this.slides.length; t += 1) e.push(t);
                this.removeSlide(e)
            }
        },
        y = function() {
            var e = o.navigator.userAgent,
                t = { ios: !1, android: !1, androidChrome: !1, desktop: !1, windows: !1, iphone: !1, ipod: !1, ipad: !1, cordova: o.cordova || o.phonegap, phonegap: o.cordova || o.phonegap },
                i = e.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                s = e.match(/(Android);?[\s\/]+([\d.]+)?/),
                a = e.match(/(iPad).*OS\s([\d_]+)/),
                r = e.match(/(iPod)(.*OS\s([\d_]+))?/),
                n = !a && e.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
            if (i && (t.os = "windows", t.osVersion = i[2], t.windows = !0), s && !i && (t.os = "android", t.osVersion = s[2], t.android = !0, t.androidChrome = e.toLowerCase().indexOf("chrome") >= 0), (a || n || r) && (t.os = "ios", t.ios = !0), n && !r && (t.osVersion = n[2].replace(/_/g, "."), t.iphone = !0), a && (t.osVersion = a[2].replace(/_/g, "."), t.ipad = !0), r && (t.osVersion = r[3] ? r[3].replace(/_/g, ".") : null, t.iphone = !0), t.ios && t.osVersion && e.indexOf("Version/") >= 0 && "10" === t.osVersion.split(".")[0] && (t.osVersion = e.toLowerCase().split("version/")[1].split(" ")[0]), t.desktop = !(t.os || t.android || t.webView), t.webView = (n || a || r) && e.match(/.*AppleWebKit(?!.*Safari)/i), t.os && "ios" === t.os) {
                var l = t.osVersion.split("."),
                    h = d.querySelector('meta[name="viewport"]');
                t.minimalUi = !t.webView && (r || n) && (1 * l[0] == 7 ? 1 * l[1] >= 1 : 1 * l[0] > 7) && h && h.getAttribute("content").indexOf("minimal-ui") >= 0
            }
            return t.pixelRatio = o.devicePixelRatio || 1, t
        }(),
        x = function(e) {
            var i = this.touchEventsData,
                s = this.params,
                a = this.touches,
                r = e;
            if (r.originalEvent && (r = r.originalEvent), i.isTouchEvent = "touchstart" === r.type, (i.isTouchEvent || !("which" in r) || 3 !== r.which) && (!i.isTouched || !i.isMoved))
                if (s.noSwiping && t(r.target).closest("." + s.noSwipingClass)[0]) this.allowClick = !0;
                else if (!s.swipeHandler || t(r).closest(s.swipeHandler)[0]) {
                a.currentX = "touchstart" === r.type ? r.targetTouches[0].pageX : r.pageX, a.currentY = "touchstart" === r.type ? r.targetTouches[0].pageY : r.pageY;
                var n = a.currentX,
                    o = a.currentY;
                if (!(y.ios && !y.cordova && s.iOSEdgeSwipeDetection && n <= s.iOSEdgeSwipeThreshold && n >= window.screen.width - s.iOSEdgeSwipeThreshold)) {
                    if (l.extend(i, { isTouched: !0, isMoved: !1, allowTouchCallbacks: !0, isScrolling: void 0, startMoving: void 0 }), a.startX = n, a.startY = o, i.touchStartTime = l.now(), this.allowClick = !0, this.updateSize(), this.swipeDirection = void 0, s.threshold > 0 && (i.allowThresholdMove = !1), "touchstart" !== r.type) {
                        var h = !0;
                        t(r.target).is(i.formElements) && (h = !1), d.activeElement && t(d.activeElement).is(i.formElements) && d.activeElement.blur(), h && this.allowTouchMove && r.preventDefault()
                    }
                    this.emit("touchStart", r)
                }
            }
        },
        T = function(e) {
            var i = this.touchEventsData,
                s = this.params,
                a = this.touches,
                r = this.rtl,
                n = e;
            if (n.originalEvent && (n = n.originalEvent), !i.isTouchEvent || "mousemove" !== n.type) {
                var o = "touchmove" === n.type ? n.targetTouches[0].pageX : n.pageX,
                    h = "touchmove" === n.type ? n.targetTouches[0].pageY : n.pageY;
                if (n.preventedByNestedSwiper) return a.startX = o, void(a.startY = h);
                if (!this.allowTouchMove) return this.allowClick = !1, void(i.isTouched && (l.extend(a, { startX: o, startY: h, currentX: o, currentY: h }), i.touchStartTime = l.now()));
                if (i.isTouchEvent && s.touchReleaseOnEdges && !s.loop)
                    if (this.isVertical()) { if (h < a.startY && this.translate <= this.maxTranslate() || h > a.startY && this.translate >= this.minTranslate()) return i.isTouched = !1, void(i.isMoved = !1) } else if (o < a.startX && this.translate <= this.maxTranslate() || o > a.startX && this.translate >= this.minTranslate()) return;
                if (i.isTouchEvent && d.activeElement && n.target === d.activeElement && t(n.target).is(i.formElements)) return i.isMoved = !0, void(this.allowClick = !1);
                if (i.allowTouchCallbacks && this.emit("touchMove", n), !(n.targetTouches && n.targetTouches.length > 1)) {
                    a.currentX = o, a.currentY = h;
                    var p, c = a.currentX - a.startX,
                        u = a.currentY - a.startY;
                    if (void 0 === i.isScrolling) this.isHorizontal() && a.currentY === a.startY || this.isVertical() && a.currentX === a.startX ? i.isScrolling = !1 : c * c + u * u >= 25 && (p = 180 * Math.atan2(Math.abs(u), Math.abs(c)) / Math.PI, i.isScrolling = this.isHorizontal() ? p > s.touchAngle : 90 - p > s.touchAngle);
                    if (i.isScrolling && this.emit("touchMoveOpposite", n), "undefined" == typeof startMoving && (a.currentX === a.startX && a.currentY === a.startY || (i.startMoving = !0)), i.isTouched)
                        if (i.isScrolling) i.isTouched = !1;
                        else if (i.startMoving) {
                        this.allowClick = !1, n.preventDefault(), s.touchMoveStopPropagation && !s.nested && n.stopPropagation(), i.isMoved || (s.loop && this.loopFix(), i.startTranslate = this.getTranslate(), this.setTransition(0), this.animating && this.$wrapperEl.trigger("webkitTransitionEnd transitionend"), i.allowMomentumBounce = !1, !s.grabCursor || !0 !== this.allowSlideNext && !0 !== this.allowSlidePrev || this.setGrabCursor(!0), this.emit("sliderFirstMove", n)), this.emit("sliderMove", n), i.isMoved = !0;
                        var f = this.isHorizontal() ? c : u;
                        a.diff = f, f *= s.touchRatio, r && (f = -f), this.swipeDirection = f > 0 ? "prev" : "next", i.currentTranslate = f + i.startTranslate;
                        var v = !0,
                            m = s.resistanceRatio;
                        if (s.touchReleaseOnEdges && (m = 0), f > 0 && i.currentTranslate > this.minTranslate() ? (v = !1, s.resistance && (i.currentTranslate = this.minTranslate() - 1 + Math.pow(-this.minTranslate() + i.startTranslate + f, m))) : f < 0 && i.currentTranslate < this.maxTranslate() && (v = !1, s.resistance && (i.currentTranslate = this.maxTranslate() + 1 - Math.pow(this.maxTranslate() - i.startTranslate - f, m))), v && (n.preventedByNestedSwiper = !0), !this.allowSlideNext && "next" === this.swipeDirection && i.currentTranslate < i.startTranslate && (i.currentTranslate = i.startTranslate), !this.allowSlidePrev && "prev" === this.swipeDirection && i.currentTranslate > i.startTranslate && (i.currentTranslate = i.startTranslate), s.threshold > 0) { if (!(Math.abs(f) > s.threshold || i.allowThresholdMove)) return void(i.currentTranslate = i.startTranslate); if (!i.allowThresholdMove) return i.allowThresholdMove = !0, a.startX = a.currentX, a.startY = a.currentY, i.currentTranslate = i.startTranslate, void(a.diff = this.isHorizontal() ? a.currentX - a.startX : a.currentY - a.startY) }
                        s.followFinger && ((s.freeMode || s.watchSlidesProgress || s.watchSlidesVisibility) && (this.updateActiveIndex(), this.updateSlidesClasses()), s.freeMode && (0 === i.velocities.length && i.velocities.push({ position: a[this.isHorizontal() ? "startX" : "startY"], time: i.touchStartTime }), i.velocities.push({ position: a[this.isHorizontal() ? "currentX" : "currentY"], time: l.now() })), this.updateProgress(i.currentTranslate), this.setTranslate(i.currentTranslate))
                    }
                }
            }
        },
        E = function(e) {
            var t = this,
                i = t.touchEventsData,
                s = t.params,
                a = t.touches,
                r = t.rtl,
                n = t.$wrapperEl,
                o = t.slidesGrid,
                d = t.snapGrid,
                h = e;
            if (h.originalEvent && (h = h.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", h), i.allowTouchCallbacks = !1, i.isTouched) {
                s.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
                var p, c = l.now(),
                    u = c - i.touchStartTime;
                if (t.allowClick && (t.updateClickedSlide(h), t.emit("tap", h), u < 300 && c - i.lastClickTime > 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), i.clickTimeout = l.nextTick(function() { t && !t.destroyed && t.emit("click", h) }, 300)), u < 300 && c - i.lastClickTime < 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), t.emit("doubleTap", h))), i.lastClickTime = l.now(), l.nextTick(function() { t.destroyed || (t.allowClick = !0) }), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === a.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, void(i.isMoved = !1);
                if (i.isTouched = !1, i.isMoved = !1, p = s.followFinger ? r ? t.translate : -t.translate : -i.currentTranslate, s.freeMode) {
                    if (p < -t.minTranslate()) return void t.slideTo(t.activeIndex);
                    if (p > -t.maxTranslate()) return void(t.slides.length < d.length ? t.slideTo(d.length - 1) : t.slideTo(t.slides.length - 1));
                    if (s.freeModeMomentum) {
                        if (i.velocities.length > 1) {
                            var f = i.velocities.pop(),
                                v = i.velocities.pop(),
                                m = f.position - v.position,
                                g = f.time - v.time;
                            t.velocity = m / g, t.velocity /= 2, Math.abs(t.velocity) < s.freeModeMinimumVelocity && (t.velocity = 0), (g > 150 || l.now() - f.time > 300) && (t.velocity = 0)
                        } else t.velocity = 0;
                        t.velocity *= s.freeModeMomentumVelocityRatio, i.velocities.length = 0;
                        var b = 1e3 * s.freeModeMomentumRatio,
                            w = t.velocity * b,
                            y = t.translate + w;
                        r && (y = -y);
                        var x, T = !1,
                            E = 20 * Math.abs(t.velocity) * s.freeModeMomentumBounceRatio;
                        if (y < t.maxTranslate()) s.freeModeMomentumBounce ? (y + t.maxTranslate() < -E && (y = t.maxTranslate() - E), x = t.maxTranslate(), T = !0, i.allowMomentumBounce = !0) : y = t.maxTranslate();
                        else if (y > t.minTranslate()) s.freeModeMomentumBounce ? (y - t.minTranslate() > E && (y = t.minTranslate() + E), x = t.minTranslate(), T = !0, i.allowMomentumBounce = !0) : y = t.minTranslate();
                        else if (s.freeModeSticky) {
                            for (var S, C = 0; C < d.length; C += 1)
                                if (d[C] > -y) { S = C; break }
                            y = -(y = Math.abs(d[S] - y) < Math.abs(d[S - 1] - y) || "next" === t.swipeDirection ? d[S] : d[S - 1])
                        }
                        if (0 !== t.velocity) b = r ? Math.abs((-y - t.translate) / t.velocity) : Math.abs((y - t.translate) / t.velocity);
                        else if (s.freeModeSticky) return void t.slideReset();
                        s.freeModeMomentumBounce && T ? (t.updateProgress(x), t.setTransition(b), t.setTranslate(y), t.transitionStart(), t.animating = !0, n.transitionEnd(function() { t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(s.speed), t.setTranslate(x), n.transitionEnd(function() { t && !t.destroyed && t.transitionEnd() })) })) : t.velocity ? (t.updateProgress(y), t.setTransition(b), t.setTranslate(y), t.transitionStart(), t.animating || (t.animating = !0, n.transitionEnd(function() { t && !t.destroyed && t.transitionEnd() }))) : t.updateProgress(y), t.updateActiveIndex(), t.updateSlidesClasses()
                    }(!s.freeModeMomentum || u >= s.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses())
                } else { for (var M = 0, z = t.slidesSizesGrid[0], P = 0; P < o.length; P += s.slidesPerGroup) void 0 !== o[P + s.slidesPerGroup] ? p >= o[P] && p < o[P + s.slidesPerGroup] && (M = P, z = o[P + s.slidesPerGroup] - o[P]) : p >= o[P] && (M = P, z = o[o.length - 1] - o[o.length - 2]); var k = (p - o[M]) / z; if (u > s.longSwipesMs) { if (!s.longSwipes) return void t.slideTo(t.activeIndex); "next" === t.swipeDirection && (k >= s.longSwipesRatio ? t.slideTo(M + s.slidesPerGroup) : t.slideTo(M)), "prev" === t.swipeDirection && (k > 1 - s.longSwipesRatio ? t.slideTo(M + s.slidesPerGroup) : t.slideTo(M)) } else { if (!s.shortSwipes) return void t.slideTo(t.activeIndex); "next" === t.swipeDirection && t.slideTo(M + s.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo(M) } }
            }
        },
        S = function() {
            var e = this.params,
                t = this.el;
            if (!t || 0 !== t.offsetWidth) {
                e.breakpoints && this.setBreakpoint();
                var i = this.allowSlideNext,
                    s = this.allowSlidePrev;
                if (this.allowSlideNext = !0, this.allowSlidePrev = !0, this.updateSize(), this.updateSlides(), e.freeMode) {
                    var a = Math.min(Math.max(this.translate, this.maxTranslate()), this.minTranslate());
                    this.setTranslate(a), this.updateActiveIndex(), this.updateSlidesClasses(), e.autoHeight && this.updateAutoHeight()
                } else this.updateSlidesClasses(), ("auto" === e.slidesPerView || e.slidesPerView > 1) && this.isEnd && !this.params.centeredSlides ? this.slideTo(this.slides.length - 1, 0, !1, !0) : this.slideTo(this.activeIndex, 0, !1, !0);
                this.allowSlidePrev = s, this.allowSlideNext = i
            }
        },
        C = function(e) { this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation())) };
    var M = { init: !0, direction: "horizontal", touchEventsTarget: "container", initialSlide: 0, speed: 300, iOSEdgeSwipeDetection: !1, iOSEdgeSwipeThreshold: 20, freeMode: !1, freeModeMomentum: !0, freeModeMomentumRatio: 1, freeModeMomentumBounce: !0, freeModeMomentumBounceRatio: 1, freeModeMomentumVelocityRatio: 1, freeModeSticky: !1, freeModeMinimumVelocity: .02, autoHeight: !1, setWrapperSize: !1, virtualTranslate: !1, effect: "slide", breakpoints: void 0, spaceBetween: 0, slidesPerView: 1, slidesPerColumn: 1, slidesPerColumnFill: "column", slidesPerGroup: 1, centeredSlides: !1, slidesOffsetBefore: 0, slidesOffsetAfter: 0, normalizeSlideIndex: !0, watchOverflow: !1, roundLengths: !1, touchRatio: 1, touchAngle: 45, simulateTouch: !0, shortSwipes: !0, longSwipes: !0, longSwipesRatio: .5, longSwipesMs: 300, followFinger: !0, allowTouchMove: !0, threshold: 0, touchMoveStopPropagation: !0, touchReleaseOnEdges: !1, uniqueNavElements: !0, resistance: !0, resistanceRatio: .85, watchSlidesProgress: !1, watchSlidesVisibility: !1, grabCursor: !1, preventClicks: !0, preventClicksPropagation: !0, slideToClickedSlide: !1, preloadImages: !0, updateOnImagesReady: !0, loop: !1, loopAdditionalSlides: 0, loopedSlides: null, loopFillGroupWithBlank: !1, allowSlidePrev: !0, allowSlideNext: !0, swipeHandler: null, noSwiping: !0, noSwipingClass: "swiper-no-swiping", passiveListeners: !0, containerModifierClass: "swiper-container-", slideClass: "swiper-slide", slideBlankClass: "swiper-slide-invisible-blank", slideActiveClass: "swiper-slide-active", slideDuplicateActiveClass: "swiper-slide-duplicate-active", slideVisibleClass: "swiper-slide-visible", slideDuplicateClass: "swiper-slide-duplicate", slideNextClass: "swiper-slide-next", slideDuplicateNextClass: "swiper-slide-duplicate-next", slidePrevClass: "swiper-slide-prev", slideDuplicatePrevClass: "swiper-slide-duplicate-prev", wrapperClass: "swiper-wrapper", runCallbacksOnInit: !0 },
        z = {
            update: u,
            translate: f,
            transition: v,
            slide: m,
            loop: g,
            grabCursor: b,
            manipulation: w,
            events: {
                attachEvents: function() {
                    var e = this.params,
                        t = this.touchEvents,
                        i = this.el,
                        s = this.wrapperEl;
                    this.onTouchStart = x.bind(this), this.onTouchMove = T.bind(this), this.onTouchEnd = E.bind(this), this.onClick = C.bind(this);
                    var a = "container" === e.touchEventsTarget ? i : s,
                        r = !!e.nested;
                    if (h.pointerEvents || h.prefixedPointerEvents) a.addEventListener(t.start, this.onTouchStart, !1), (h.touch ? a : d).addEventListener(t.move, this.onTouchMove, r), (h.touch ? a : d).addEventListener(t.end, this.onTouchEnd, !1);
                    else {
                        if (h.touch) {
                            var n = !("touchstart" !== t.start || !h.passiveListener || !e.passiveListeners) && { passive: !0, capture: !1 };
                            a.addEventListener(t.start, this.onTouchStart, n), a.addEventListener(t.move, this.onTouchMove, h.passiveListener ? { passive: !1, capture: r } : r), a.addEventListener(t.end, this.onTouchEnd, n)
                        }(e.simulateTouch && !y.ios && !y.android || e.simulateTouch && !h.touch && y.ios) && (a.addEventListener("mousedown", this.onTouchStart, !1), d.addEventListener("mousemove", this.onTouchMove, r), d.addEventListener("mouseup", this.onTouchEnd, !1))
                    }(e.preventClicks || e.preventClicksPropagation) && a.addEventListener("click", this.onClick, !0), this.on("resize observerUpdate", S)
                },
                detachEvents: function() {
                    var e = this.params,
                        t = this.touchEvents,
                        i = this.el,
                        s = this.wrapperEl,
                        a = "container" === e.touchEventsTarget ? i : s,
                        r = !!e.nested;
                    if (h.pointerEvents || h.prefixedPointerEvents) a.removeEventListener(t.start, this.onTouchStart, !1), (h.touch ? a : d).removeEventListener(t.move, this.onTouchMove, r), (h.touch ? a : d).removeEventListener(t.end, this.onTouchEnd, !1);
                    else {
                        if (h.touch) {
                            var n = !("onTouchStart" !== t.start || !h.passiveListener || !e.passiveListeners) && { passive: !0, capture: !1 };
                            a.removeEventListener(t.start, this.onTouchStart, n), a.removeEventListener(t.move, this.onTouchMove, r), a.removeEventListener(t.end, this.onTouchEnd, n)
                        }(e.simulateTouch && !y.ios && !y.android || e.simulateTouch && !h.touch && y.ios) && (a.removeEventListener("mousedown", this.onTouchStart, !1), d.removeEventListener("mousemove", this.onTouchMove, r), d.removeEventListener("mouseup", this.onTouchEnd, !1))
                    }(e.preventClicks || e.preventClicksPropagation) && a.removeEventListener("click", this.onClick, !0), this.off("resize observerUpdate", S)
                }
            },
            breakpoints: {
                setBreakpoint: function() {
                    var e = this.activeIndex,
                        t = this.loopedSlides;
                    void 0 === t && (t = 0);
                    var i = this.params,
                        s = i.breakpoints;
                    if (s && (!s || 0 !== Object.keys(s).length)) {
                        var a = this.getBreakpoint(s);
                        if (a && this.currentBreakpoint !== a) {
                            var r = a in s ? s[a] : this.originalParams,
                                n = i.loop && r.slidesPerView !== i.slidesPerView;
                            l.extend(this.params, r), l.extend(this, { allowTouchMove: this.params.allowTouchMove, allowSlideNext: this.params.allowSlideNext, allowSlidePrev: this.params.allowSlidePrev }), this.currentBreakpoint = a, n && (this.loopDestroy(), this.loopCreate(), this.updateSlides(), this.slideTo(e - t + this.loopedSlides, 0, !1)), this.emit("breakpoint", r)
                        }
                    }
                },
                getBreakpoint: function(e) {
                    if (e) {
                        var t = !1,
                            i = [];
                        Object.keys(e).forEach(function(e) { i.push(e) }), i.sort(function(e, t) { return parseInt(e, 10) - parseInt(t, 10) });
                        for (var s = 0; s < i.length; s += 1) {
                            var a = i[s];
                            a >= o.innerWidth && !t && (t = a)
                        }
                        return t || "max"
                    }
                }
            },
            checkOverflow: {
                checkOverflow: function() {
                    var e = this.isLocked;
                    this.isLocked = 1 === this.snapGrid.length, this.allowTouchMove = !this.isLocked, e && e !== this.isLocked && (this.isEnd = !1, this.navigation.update())
                }
            },
            classes: {
                addClasses: function() {
                    var e = this.classNames,
                        t = this.params,
                        i = this.rtl,
                        s = this.$el,
                        a = [];
                    a.push(t.direction), t.freeMode && a.push("free-mode"), h.flexbox || a.push("no-flexbox"), t.autoHeight && a.push("autoheight"), i && a.push("rtl"), t.slidesPerColumn > 1 && a.push("multirow"), y.android && a.push("android"), y.ios && a.push("ios"), (h.pointerEvents || h.prefixedPointerEvents) && a.push("wp8-" + t.direction), a.forEach(function(i) { e.push(t.containerModifierClass + i) }), s.addClass(e.join(" "))
                },
                removeClasses: function() {
                    var e = this.$el,
                        t = this.classNames;
                    e.removeClass(t.join(" "))
                }
            },
            images: {
                loadImage: function(e, t, i, s, a, r) {
                    var n;

                    function l() { r && r() }
                    e.complete && a ? l() : t ? ((n = new o.Image).onload = l, n.onerror = l, s && (n.sizes = s), i && (n.srcset = i), t && (n.src = t)) : l()
                },
                preloadImages: function() {
                    var e = this;

                    function t() { void 0 !== e && null !== e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady"))) }
                    e.imagesToLoad = e.$el.find("img");
                    for (var i = 0; i < e.imagesToLoad.length; i += 1) {
                        var s = e.imagesToLoad[i];
                        e.loadImage(s, s.currentSrc || s.getAttribute("src"), s.srcset || s.getAttribute("srcset"), s.sizes || s.getAttribute("sizes"), !0, t)
                    }
                }
            }
        },
        P = {},
        k = function(e) {
            function i() {
                for (var s, a, r, n = [], o = arguments.length; o--;) n[o] = arguments[o];
                1 === n.length && n[0].constructor && n[0].constructor === Object ? a = n[0] : (s = (r = n)[0], a = r[1]);
                a || (a = {}), a = l.extend({}, a), s && !a.el && (a.el = s), e.call(this, a), Object.keys(z).forEach(function(e) { Object.keys(z[e]).forEach(function(t) { i.prototype[t] || (i.prototype[t] = z[e][t]) }) });
                var d = this;
                void 0 === d.modules && (d.modules = {}), Object.keys(d.modules).forEach(function(e) {
                    var t = d.modules[e];
                    if (t.params) {
                        var i = Object.keys(t.params)[0],
                            s = t.params[i];
                        if ("object" != typeof s) return;
                        if (!(i in a && "enabled" in s)) return;
                        !0 === a[i] && (a[i] = { enabled: !0 }), "object" != typeof a[i] || "enabled" in a[i] || (a[i].enabled = !0), a[i] || (a[i] = { enabled: !1 })
                    }
                });
                var p = l.extend({}, M);
                d.useModulesParams(p), d.params = l.extend({}, p, P, a), d.originalParams = l.extend({}, d.params), d.passedParams = l.extend({}, a);
                var c = t(d.params.el);
                if (s = c[0]) {
                    if (c.length > 1) {
                        var u = [];
                        return c.each(function(e, t) {
                            var s = l.extend({}, a, { el: t });
                            u.push(new i(s))
                        }), u
                    }
                    s.swiper = d, c.data("swiper", d);
                    var f, v, m = c.children("." + d.params.wrapperClass);
                    return l.extend(d, { $el: c, el: s, $wrapperEl: m, wrapperEl: m[0], classNames: [], slides: t(), slidesGrid: [], snapGrid: [], slidesSizesGrid: [], isHorizontal: function() { return "horizontal" === d.params.direction }, isVertical: function() { return "vertical" === d.params.direction }, rtl: "horizontal" === d.params.direction && ("rtl" === s.dir.toLowerCase() || "rtl" === c.css("direction")), wrongRTL: "-webkit-box" === m.css("display"), activeIndex: 0, realIndex: 0, isBeginning: !0, isEnd: !1, translate: 0, progress: 0, velocity: 0, animating: !1, allowSlideNext: d.params.allowSlideNext, allowSlidePrev: d.params.allowSlidePrev, touchEvents: (f = ["touchstart", "touchmove", "touchend"], v = ["mousedown", "mousemove", "mouseup"], h.pointerEvents ? v = ["pointerdown", "pointermove", "pointerup"] : h.prefixedPointerEvents && (v = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), { start: h.touch || !d.params.simulateTouch ? f[0] : v[0], move: h.touch || !d.params.simulateTouch ? f[1] : v[1], end: h.touch || !d.params.simulateTouch ? f[2] : v[2] }), touchEventsData: { isTouched: void 0, isMoved: void 0, allowTouchCallbacks: void 0, touchStartTime: void 0, isScrolling: void 0, currentTranslate: void 0, startTranslate: void 0, allowThresholdMove: void 0, formElements: "input, select, option, textarea, button, video", lastClickTime: l.now(), clickTimeout: void 0, velocities: [], allowMomentumBounce: void 0, isTouchEvent: void 0, startMoving: void 0 }, allowClick: !0, allowTouchMove: d.params.allowTouchMove, touches: { startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0 }, imagesToLoad: [], imagesLoaded: 0 }), d.useModules(), d.params.init && d.init(), d
                }
            }
            e && (i.__proto__ = e), i.prototype = Object.create(e && e.prototype), i.prototype.constructor = i;
            var s = { extendedDefaults: { configurable: !0 }, defaults: { configurable: !0 }, Class: { configurable: !0 }, $: { configurable: !0 } };
            return i.prototype.slidesPerViewDynamic = function() {
                var e = this.params,
                    t = this.slides,
                    i = this.slidesGrid,
                    s = this.size,
                    a = this.activeIndex,
                    r = 1;
                if (e.centeredSlides) { for (var n, o = t[a].swiperSlideSize, l = a + 1; l < t.length; l += 1) t[l] && !n && (r += 1, (o += t[l].swiperSlideSize) > s && (n = !0)); for (var d = a - 1; d >= 0; d -= 1) t[d] && !n && (r += 1, (o += t[d].swiperSlideSize) > s && (n = !0)) } else
                    for (var h = a + 1; h < t.length; h += 1) i[h] - i[a] < s && (r += 1);
                return r
            }, i.prototype.update = function() {
                var e = this;
                e && !e.destroyed && (e.updateSize(), e.updateSlides(), e.updateProgress(), e.updateSlidesClasses(), e.params.freeMode ? (t(), e.params.autoHeight && e.updateAutoHeight()) : (("auto" === e.params.slidesPerView || e.params.slidesPerView > 1) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0)) || t(), e.emit("update"));

                function t() {
                    var t = e.rtl ? -1 * e.translate : e.translate,
                        i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
                    e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses()
                }
            }, i.prototype.init = function() { this.initialized || (this.emit("beforeInit"), this.params.breakpoints && this.setBreakpoint(), this.addClasses(), this.params.loop && this.loopCreate(), this.updateSize(), this.updateSlides(), this.params.watchOverflow && this.checkOverflow(), this.params.grabCursor && this.setGrabCursor(), this.params.preloadImages && this.preloadImages(), this.params.loop ? this.slideTo(this.params.initialSlide + this.loopedSlides, 0, this.params.runCallbacksOnInit) : this.slideTo(this.params.initialSlide, 0, this.params.runCallbacksOnInit), this.attachEvents(), this.initialized = !0, this.emit("init")) }, i.prototype.destroy = function(e, t) {
                void 0 === e && (e = !0), void 0 === t && (t = !0);
                var i = this,
                    s = i.params,
                    a = i.$el,
                    r = i.$wrapperEl,
                    n = i.slides;
                i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), s.loop && i.loopDestroy(), t && (i.removeClasses(), a.removeAttr("style"), r.removeAttr("style"), n && n.length && n.removeClass([s.slideVisibleClass, s.slideActiveClass, s.slideNextClass, s.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), i.emit("destroy"), Object.keys(i.eventsListeners).forEach(function(e) { i.off(e) }), !1 !== e && (i.$el[0].swiper = null, i.$el.data("swiper", null), l.deleteProps(i)), i.destroyed = !0
            }, i.extendDefaults = function(e) { l.extend(P, e) }, s.extendedDefaults.get = function() { return P }, s.defaults.get = function() { return M }, s.Class.get = function() { return e }, s.$.get = function() { return t }, Object.defineProperties(i, s), i
        }(p),
        $ = { name: "device", proto: { device: y }, static: { device: y } },
        L = { name: "support", proto: { support: h }, static: { support: h } },
        I = function() { return { isSafari: (e = o.navigator.userAgent.toLowerCase(), e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0), isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(o.navigator.userAgent) }; var e }(),
        D = { name: "browser", proto: { browser: I }, static: { browser: I } },
        O = {
            name: "resize",
            create: function() {
                var e = this;
                l.extend(e, { resize: { resizeHandler: function() { e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize")) }, orientationChangeHandler: function() { e && !e.destroyed && e.initialized && e.emit("orientationchange") } } })
            },
            on: { init: function() { o.addEventListener("resize", this.resize.resizeHandler), o.addEventListener("orientationchange", this.resize.orientationChangeHandler) }, destroy: function() { o.removeEventListener("resize", this.resize.resizeHandler), o.removeEventListener("orientationchange", this.resize.orientationChangeHandler) } }
        },
        A = {
            func: o.MutationObserver || o.WebkitMutationObserver,
            attach: function(e, t) {
                void 0 === t && (t = {});
                var i = this,
                    s = new(0, A.func)(function(e) { e.forEach(function(e) { i.emit("observerUpdate", e) }) });
                s.observe(e, { attributes: void 0 === t.attributes || t.attributes, childList: void 0 === t.childList || t.childList, characterData: void 0 === t.characterData || t.characterData }), i.observer.observers.push(s)
            },
            init: function() {
                if (h.observer && this.params.observer) {
                    if (this.params.observeParents)
                        for (var e = this.$el.parents(), t = 0; t < e.length; t += 1) this.observer.attach(e[t]);
                    this.observer.attach(this.$el[0], { childList: !1 }), this.observer.attach(this.$wrapperEl[0], { attributes: !1 })
                }
            },
            destroy: function() { this.observer.observers.forEach(function(e) { e.disconnect() }), this.observer.observers = [] }
        },
        H = { name: "observer", params: { observer: !1, observeParents: !1 }, create: function() { l.extend(this, { observer: { init: A.init.bind(this), attach: A.attach.bind(this), destroy: A.destroy.bind(this), observers: [] } }) }, on: { init: function() { this.observer.init() }, destroy: function() { this.observer.destroy() } } },
        N = {
            update: function(e) {
                var t = this,
                    i = t.params,
                    s = i.slidesPerView,
                    a = i.slidesPerGroup,
                    r = i.centeredSlides,
                    n = t.virtual,
                    o = n.from,
                    d = n.to,
                    h = n.slides,
                    p = n.slidesGrid,
                    c = n.renderSlide,
                    u = n.offset;
                t.updateActiveIndex();
                var f, v, m, g = t.activeIndex || 0;
                f = t.rtl && t.isHorizontal() ? "right" : t.isHorizontal() ? "left" : "top", r ? (v = Math.floor(s / 2) + a, m = Math.floor(s / 2) + a) : (v = s + (a - 1), m = a);
                var b = Math.max((g || 0) - m, 0),
                    w = Math.min((g || 0) + v, h.length - 1),
                    y = (t.slidesGrid[b] || 0) - (t.slidesGrid[0] || 0);

                function x() { t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.lazy && t.params.lazy.enabled && t.lazy.load() }
                if (l.extend(t.virtual, { from: b, to: w, offset: y, slidesGrid: t.slidesGrid }), o === b && d === w && !e) return t.slidesGrid !== p && y !== u && t.slides.css(f, y + "px"), void t.updateProgress();
                if (t.params.virtual.renderExternal) return t.params.virtual.renderExternal.call(t, { offset: y, from: b, to: w, slides: function() { for (var e = [], t = b; t <= w; t += 1) e.push(h[t]); return e }() }), void x();
                var T = [],
                    E = [];
                if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();
                else
                    for (var S = o; S <= d; S += 1)(S < b || S > w) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + S + '"]').remove();
                for (var C = 0; C < h.length; C += 1) C >= b && C <= w && (void 0 === d || e ? E.push(C) : (C > d && E.push(C), C < o && T.push(C)));
                E.forEach(function(e) { t.$wrapperEl.append(c(h[e], e)) }), T.sort(function(e, t) { return e < t }).forEach(function(e) { t.$wrapperEl.prepend(c(h[e], e)) }), t.$wrapperEl.children(".swiper-slide").css(f, y + "px"), x()
            },
            renderSlide: function(e, i) { var s = this.params.virtual; if (s.cache && this.virtual.cache[i]) return this.virtual.cache[i]; var a = s.renderSlide ? t(s.renderSlide.call(this, e, i)) : t('<div class="' + this.params.slideClass + '" data-swiper-slide-index="' + i + '">' + e + "</div>"); return a.attr("data-swiper-slide-index") || a.attr("data-swiper-slide-index", i), s.cache && (this.virtual.cache[i] = a), a },
            appendSlide: function(e) { this.virtual.slides.push(e), this.virtual.update(!0) },
            prependSlide: function(e) {
                if (this.virtual.slides.unshift(e), this.params.virtual.cache) {
                    var t = this.virtual.cache,
                        i = {};
                    Object.keys(t).forEach(function(e) { i[e + 1] = t[e] }), this.virtual.cache = i
                }
                this.virtual.update(!0), this.slideNext(0)
            }
        },
        X = {
            name: "virtual",
            params: { virtual: { enabled: !1, slides: [], cache: !0, renderSlide: null, renderExternal: null } },
            create: function() { l.extend(this, { virtual: { update: N.update.bind(this), appendSlide: N.appendSlide.bind(this), prependSlide: N.prependSlide.bind(this), renderSlide: N.renderSlide.bind(this), slides: this.params.virtual.slides, cache: {} } }) },
            on: {
                beforeInit: function() {
                    if (this.params.virtual.enabled) {
                        this.classNames.push(this.params.containerModifierClass + "virtual");
                        var e = { watchSlidesProgress: !0 };
                        l.extend(this.params, e), l.extend(this.originalParams, e), this.virtual.update()
                    }
                },
                setTranslate: function() { this.params.virtual.enabled && this.virtual.update() }
            }
        },
        Y = {
            handle: function(e) {
                var t = e;
                t.originalEvent && (t = t.originalEvent);
                var i = t.keyCode || t.charCode;
                if (!this.allowSlideNext && (this.isHorizontal() && 39 === i || this.isVertical() && 40 === i)) return !1;
                if (!this.allowSlidePrev && (this.isHorizontal() && 37 === i || this.isVertical() && 38 === i)) return !1;
                if (!(t.shiftKey || t.altKey || t.ctrlKey || t.metaKey || d.activeElement && d.activeElement.nodeName && ("input" === d.activeElement.nodeName.toLowerCase() || "textarea" === d.activeElement.nodeName.toLowerCase()))) {
                    if (this.params.keyboard.onlyInViewport && (37 === i || 39 === i || 38 === i || 40 === i)) {
                        var s = !1;
                        if (this.$el.parents("." + this.params.slideClass).length > 0 && 0 === this.$el.parents("." + this.params.slideActiveClass).length) return;
                        var a = o.pageXOffset,
                            r = o.pageYOffset,
                            n = o.innerWidth,
                            l = o.innerHeight,
                            h = this.$el.offset();
                        this.rtl && (h.left -= this.$el[0].scrollLeft);
                        for (var p = [
                                [h.left, h.top],
                                [h.left + this.width, h.top],
                                [h.left, h.top + this.height],
                                [h.left + this.width, h.top + this.height]
                            ], c = 0; c < p.length; c += 1) {
                            var u = p[c];
                            u[0] >= a && u[0] <= a + n && u[1] >= r && u[1] <= r + l && (s = !0)
                        }
                        if (!s) return
                    }
                    this.isHorizontal() ? (37 !== i && 39 !== i || (t.preventDefault ? t.preventDefault() : t.returnValue = !1), (39 === i && !this.rtl || 37 === i && this.rtl) && this.slideNext(), (37 === i && !this.rtl || 39 === i && this.rtl) && this.slidePrev()) : (38 !== i && 40 !== i || (t.preventDefault ? t.preventDefault() : t.returnValue = !1), 40 === i && this.slideNext(), 38 === i && this.slidePrev()), this.emit("keyPress", i)
                }
            },
            enable: function() { this.keyboard.enabled || (t(d).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0) },
            disable: function() { this.keyboard.enabled && (t(d).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1) }
        },
        G = { name: "keyboard", params: { keyboard: { enabled: !1, onlyInViewport: !0 } }, create: function() { l.extend(this, { keyboard: { enabled: !1, enable: Y.enable.bind(this), disable: Y.disable.bind(this), handle: Y.handle.bind(this) } }) }, on: { init: function() { this.params.keyboard.enabled && this.keyboard.enable() }, destroy: function() { this.keyboard.enabled && this.keyboard.disable() } } };
    var B = {
            lastScrollTime: l.now(),
            event: o.navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : function() {
                var e = "onwheel" in d;
                if (!e) {
                    var t = d.createElement("div");
                    t.setAttribute("onwheel", "return;"), e = "function" == typeof t.onwheel
                }
                return !e && d.implementation && d.implementation.hasFeature && !0 !== d.implementation.hasFeature("", "") && (e = d.implementation.hasFeature("Events.wheel", "3.0")), e
            }() ? "wheel" : "mousewheel",
            normalize: function(e) {
                var t = 0,
                    i = 0,
                    s = 0,
                    a = 0;
                return "detail" in e && (i = e.detail), "wheelDelta" in e && (i = -e.wheelDelta / 120), "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = i, i = 0), s = 10 * t, a = 10 * i, "deltaY" in e && (a = e.deltaY), "deltaX" in e && (s = e.deltaX), (s || a) && e.deltaMode && (1 === e.deltaMode ? (s *= 40, a *= 40) : (s *= 800, a *= 800)), s && !t && (t = s < 1 ? -1 : 1), a && !i && (i = a < 1 ? -1 : 1), { spinX: t, spinY: i, pixelX: s, pixelY: a }
            },
            handle: function(e) {
                var t = e,
                    i = this,
                    s = i.params.mousewheel;
                t.originalEvent && (t = t.originalEvent);
                var a = 0,
                    r = i.rtl ? -1 : 1,
                    n = B.normalize(t);
                if (s.forceToAxis)
                    if (i.isHorizontal()) {
                        if (!(Math.abs(n.pixelX) > Math.abs(n.pixelY))) return !0;
                        a = n.pixelX * r
                    } else {
                        if (!(Math.abs(n.pixelY) > Math.abs(n.pixelX))) return !0;
                        a = n.pixelY
                    }
                else a = Math.abs(n.pixelX) > Math.abs(n.pixelY) ? -n.pixelX * r : -n.pixelY;
                if (0 === a) return !0;
                if (s.invert && (a = -a), i.params.freeMode) {
                    var d = i.getTranslate() + a * s.sensitivity,
                        h = i.isBeginning,
                        p = i.isEnd;
                    if (d >= i.minTranslate() && (d = i.minTranslate()), d <= i.maxTranslate() && (d = i.maxTranslate()), i.setTransition(0), i.setTranslate(d), i.updateProgress(), i.updateActiveIndex(), i.updateSlidesClasses(), (!h && i.isBeginning || !p && i.isEnd) && i.updateSlidesClasses(), i.params.freeModeSticky && (clearTimeout(i.mousewheel.timeout), i.mousewheel.timeout = l.nextTick(function() { i.slideReset() }, 300)), i.emit("scroll", t), i.params.autoplay && i.params.autoplayDisableOnInteraction && i.stopAutoplay(), 0 === d || d === i.maxTranslate()) return !0
                } else {
                    if (l.now() - i.mousewheel.lastScrollTime > 60)
                        if (a < 0)
                            if (i.isEnd && !i.params.loop || i.animating) { if (s.releaseOnEdges) return !0 } else i.slideNext(), i.emit("scroll", t);
                    else if (i.isBeginning && !i.params.loop || i.animating) { if (s.releaseOnEdges) return !0 } else i.slidePrev(), i.emit("scroll", t);
                    i.mousewheel.lastScrollTime = (new o.Date).getTime()
                }
                return t.preventDefault ? t.preventDefault() : t.returnValue = !1, !1
            },
            enable: function() { if (!B.event) return !1; if (this.mousewheel.enabled) return !1; var e = this.$el; return "container" !== this.params.mousewheel.eventsTarged && (e = t(this.params.mousewheel.eventsTarged)), e.on(B.event, this.mousewheel.handle), this.mousewheel.enabled = !0, !0 },
            disable: function() { if (!B.event) return !1; if (!this.mousewheel.enabled) return !1; var e = this.$el; return "container" !== this.params.mousewheel.eventsTarged && (e = t(this.params.mousewheel.eventsTarged)), e.off(B.event, this.mousewheel.handle), this.mousewheel.enabled = !1, !0 }
        },
        V = {
            update: function() {
                var e = this.params.navigation;
                if (!this.params.loop) {
                    var t = this.navigation,
                        i = t.$nextEl,
                        s = t.$prevEl;
                    s && s.length > 0 && (this.isBeginning ? s.addClass(e.disabledClass) : s.removeClass(e.disabledClass), s[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass)), i && i.length > 0 && (this.isEnd ? i.addClass(e.disabledClass) : i.removeClass(e.disabledClass), i[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass))
                }
            },
            init: function() {
                var e, i, s = this,
                    a = s.params.navigation;
                (a.nextEl || a.prevEl) && (a.nextEl && (e = t(a.nextEl), s.params.uniqueNavElements && "string" == typeof a.nextEl && e.length > 1 && 1 === s.$el.find(a.nextEl).length && (e = s.$el.find(a.nextEl))), a.prevEl && (i = t(a.prevEl), s.params.uniqueNavElements && "string" == typeof a.prevEl && i.length > 1 && 1 === s.$el.find(a.prevEl).length && (i = s.$el.find(a.prevEl))), e && e.length > 0 && e.on("click", function(e) { e.preventDefault(), s.isEnd && !s.params.loop || s.slideNext() }), i && i.length > 0 && i.on("click", function(e) { e.preventDefault(), s.isBeginning && !s.params.loop || s.slidePrev() }), l.extend(s.navigation, { $nextEl: e, nextEl: e && e[0], $prevEl: i, prevEl: i && i[0] }))
            },
            destroy: function() {
                var e = this.navigation,
                    t = e.$nextEl,
                    i = e.$prevEl;
                t && t.length && (t.off("click"), t.removeClass(this.params.navigation.disabledClass)), i && i.length && (i.off("click"), i.removeClass(this.params.navigation.disabledClass))
            }
        },
        R = {
            update: function() {
                var e = this.rtl,
                    i = this.params.pagination;
                if (i.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                    var s, a = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                        r = this.pagination.$el,
                        n = this.params.loop ? Math.ceil((a - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length;
                    if (this.params.loop ? ((s = Math.ceil((this.activeIndex - this.loopedSlides) / this.params.slidesPerGroup)) > a - 1 - 2 * this.loopedSlides && (s -= a - 2 * this.loopedSlides), s > n - 1 && (s -= n), s < 0 && "bullets" !== this.params.paginationType && (s = n + s)) : s = void 0 !== this.snapIndex ? this.snapIndex : this.activeIndex || 0, "bullets" === i.type && this.pagination.bullets && this.pagination.bullets.length > 0) {
                        var o = this.pagination.bullets;
                        if (i.dynamicBullets && (this.pagination.bulletSize = o.eq(0)[this.isHorizontal() ? "outerWidth" : "outerHeight"](!0), r.css(this.isHorizontal() ? "width" : "height", 5 * this.pagination.bulletSize + "px")), o.removeClass(i.bulletActiveClass + " " + i.bulletActiveClass + "-next " + i.bulletActiveClass + "-next-next " + i.bulletActiveClass + "-prev " + i.bulletActiveClass + "-prev-prev"), r.length > 1) o.each(function(e, a) {
                            var r = t(a);
                            r.index() === s && (r.addClass(i.bulletActiveClass), i.dynamicBullets && (r.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), r.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next")))
                        });
                        else {
                            var l = o.eq(s);
                            l.addClass(i.bulletActiveClass), i.dynamicBullets && (l.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), l.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next"))
                        }
                        if (i.dynamicBullets) {
                            var d = Math.min(o.length, 5),
                                h = (this.pagination.bulletSize * d - this.pagination.bulletSize) / 2 - s * this.pagination.bulletSize,
                                p = e ? "right" : "left";
                            o.css(this.isHorizontal() ? p : "top", h + "px")
                        }
                    }
                    if ("fraction" === i.type && (r.find("." + i.currentClass).text(s + 1), r.find("." + i.totalClass).text(n)), "progressbar" === i.type) {
                        var c = (s + 1) / n,
                            u = c,
                            f = 1;
                        this.isHorizontal() || (f = c, u = 1), r.find("." + i.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + u + ") scaleY(" + f + ")").transition(this.params.speed)
                    }
                    "custom" === i.type && i.renderCustom ? (r.html(i.renderCustom(this, s + 1, n)), this.emit("paginationRender", this, r[0])) : this.emit("paginationUpdate", this, r[0]), r[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](i.lockClass)
                }
            },
            render: function() {
                var e = this.params.pagination;
                if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                    var t = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                        i = this.pagination.$el,
                        s = "";
                    if ("bullets" === e.type) {
                        for (var a = this.params.loop ? Math.ceil((t - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length, r = 0; r < a; r += 1) e.renderBullet ? s += e.renderBullet.call(this, r, e.bulletClass) : s += "<" + e.bulletElement + ' class="' + e.bulletClass + '"></' + e.bulletElement + ">";
                        i.html(s), this.pagination.bullets = i.find("." + e.bulletClass)
                    }
                    "fraction" === e.type && (s = e.renderFraction ? e.renderFraction.call(this, e.currentClass, e.totalClass) : '<span class="' + e.currentClass + '"></span> / <span class="' + e.totalClass + '"></span>', i.html(s)), "progressbar" === e.type && (s = e.renderProgressbar ? e.renderProgressbar.call(this, e.progressbarFillClass) : '<span class="' + e.progressbarFillClass + '"></span>', i.html(s)), "custom" !== e.type && this.emit("paginationRender", this.pagination.$el[0])
                }
            },
            init: function() {
                var e = this,
                    i = e.params.pagination;
                if (i.el) {
                    var s = t(i.el);
                    0 !== s.length && (e.params.uniqueNavElements && "string" == typeof i.el && s.length > 1 && 1 === e.$el.find(i.el).length && (s = e.$el.find(i.el)), "bullets" === i.type && i.clickable && s.addClass(i.clickableClass), s.addClass(i.modifierClass + i.type), "bullets" === i.type && i.dynamicBullets && s.addClass("" + i.modifierClass + i.type + "-dynamic"), i.clickable && s.on("click", "." + i.bulletClass, function(i) {
                        i.preventDefault();
                        var s = t(this).index() * e.params.slidesPerGroup;
                        e.params.loop && (s += e.loopedSlides), e.slideTo(s)
                    }), l.extend(e.pagination, { $el: s, el: s[0] }))
                }
            },
            destroy: function() {
                var e = this.params.pagination;
                if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                    var t = this.pagination.$el;
                    t.removeClass(e.hiddenClass), t.removeClass(e.modifierClass + e.type), this.pagination.bullets && this.pagination.bullets.removeClass(e.bulletActiveClass), e.clickable && t.off("click", "." + e.bulletClass)
                }
            }
        },
        F = {
            setTranslate: function() {
                if (this.params.scrollbar.el && this.scrollbar.el) {
                    var e = this.scrollbar,
                        t = this.rtl,
                        i = this.progress,
                        s = e.dragSize,
                        a = e.trackSize,
                        r = e.$dragEl,
                        n = e.$el,
                        o = this.params.scrollbar,
                        l = s,
                        d = (a - s) * i;
                    t && this.isHorizontal() ? (d = -d) > 0 ? (l = s - d, d = 0) : -d + s > a && (l = a + d) : d < 0 ? (l = s + d, d = 0) : d + s > a && (l = a - d), this.isHorizontal() ? (h.transforms3d ? r.transform("translate3d(" + d + "px, 0, 0)") : r.transform("translateX(" + d + "px)"), r[0].style.width = l + "px") : (h.transforms3d ? r.transform("translate3d(0px, " + d + "px, 0)") : r.transform("translateY(" + d + "px)"), r[0].style.height = l + "px"), o.hide && (clearTimeout(this.scrollbar.timeout), n[0].style.opacity = 1, this.scrollbar.timeout = setTimeout(function() { n[0].style.opacity = 0, n.transition(400) }, 1e3))
                }
            },
            setTransition: function(e) { this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e) },
            updateSize: function() {
                if (this.params.scrollbar.el && this.scrollbar.el) {
                    var e = this.scrollbar,
                        t = e.$dragEl,
                        i = e.$el;
                    t[0].style.width = "", t[0].style.height = "";
                    var s, a = this.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
                        r = this.size / this.virtualSize,
                        n = r * (a / this.size);
                    s = "auto" === this.params.scrollbar.dragSize ? a * r : parseInt(this.params.scrollbar.dragSize, 10), this.isHorizontal() ? t[0].style.width = s + "px" : t[0].style.height = s + "px", i[0].style.display = r >= 1 ? "none" : "", this.params.scrollbarHide && (i[0].style.opacity = 0), l.extend(e, { trackSize: a, divider: r, moveDivider: n, dragSize: s }), e.$el[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](this.params.scrollbar.lockClass)
                }
            },
            setDragPosition: function(e) {
                var t, i = this.scrollbar,
                    s = i.$el,
                    a = i.dragSize,
                    r = i.trackSize;
                t = ((this.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY) - s.offset()[this.isHorizontal() ? "left" : "top"] - a / 2) / (r - a), t = Math.max(Math.min(t, 1), 0), this.rtl && (t = 1 - t);
                var n = this.minTranslate() + (this.maxTranslate() - this.minTranslate()) * t;
                this.updateProgress(n), this.setTranslate(n), this.updateActiveIndex(), this.updateSlidesClasses()
            },
            onDragStart: function(e) {
                var t = this.params.scrollbar,
                    i = this.scrollbar,
                    s = this.$wrapperEl,
                    a = i.$el,
                    r = i.$dragEl;
                this.scrollbar.isTouched = !0, e.preventDefault(), e.stopPropagation(), s.transition(100), r.transition(100), i.setDragPosition(e), clearTimeout(this.scrollbar.dragTimeout), a.transition(0), t.hide && a.css("opacity", 1), this.emit("scrollbarDragStart", e)
            },
            onDragMove: function(e) {
                var t = this.scrollbar,
                    i = this.$wrapperEl,
                    s = t.$el,
                    a = t.$dragEl;
                this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), i.transition(0), s.transition(0), a.transition(0), this.emit("scrollbarDragMove", e))
            },
            onDragEnd: function(e) {
                var t = this.params.scrollbar,
                    i = this.scrollbar.$el;
                this.scrollbar.isTouched && (this.scrollbar.isTouched = !1, t.hide && (clearTimeout(this.scrollbar.dragTimeout), this.scrollbar.dragTimeout = l.nextTick(function() { i.css("opacity", 0), i.transition(400) }, 1e3)), this.emit("scrollbarDragEnd", e), t.snapOnRelease && this.slideReset())
            },
            enableDraggable: function() {
                if (this.params.scrollbar.el) {
                    var e = this.scrollbar.$el,
                        i = h.touch ? e[0] : document;
                    e.on(this.scrollbar.dragEvents.start, this.scrollbar.onDragStart), t(i).on(this.scrollbar.dragEvents.move, this.scrollbar.onDragMove), t(i).on(this.scrollbar.dragEvents.end, this.scrollbar.onDragEnd)
                }
            },
            disableDraggable: function() {
                if (this.params.scrollbar.el) {
                    var e = this.scrollbar.$el,
                        i = h.touch ? e[0] : document;
                    e.off(this.scrollbar.dragEvents.start), t(i).off(this.scrollbar.dragEvents.move), t(i).off(this.scrollbar.dragEvents.end)
                }
            },
            init: function() {
                var e = this;
                if (e.params.scrollbar.el) {
                    var i = e.scrollbar,
                        s = e.$el,
                        a = e.touchEvents,
                        r = e.params.scrollbar,
                        n = t(r.el);
                    e.params.uniqueNavElements && "string" == typeof r.el && n.length > 1 && 1 === s.find(r.el).length && (n = s.find(r.el));
                    var o = n.find(".swiper-scrollbar-drag");
                    0 === o.length && (o = t('<div class="swiper-scrollbar-drag"></div>'), n.append(o)), e.scrollbar.dragEvents = !1 !== e.params.simulateTouch || h.touch ? a : { start: "mousedown", move: "mousemove", end: "mouseup" }, l.extend(i, { $el: n, el: n[0], $dragEl: o, dragEl: o[0] }), r.draggable && i.enableDraggable()
                }
            },
            destroy: function() { this.scrollbar.disableDraggable() }
        },
        W = {
            setTransform: function(e, i) {
                var s = this.rtl,
                    a = t(e),
                    r = s ? -1 : 1,
                    n = a.attr("data-swiper-parallax") || "0",
                    o = a.attr("data-swiper-parallax-x"),
                    l = a.attr("data-swiper-parallax-y"),
                    d = a.attr("data-swiper-parallax-scale"),
                    h = a.attr("data-swiper-parallax-opacity");
                if (o || l ? (o = o || "0", l = l || "0") : this.isHorizontal() ? (o = n, l = "0") : (l = n, o = "0"), o = o.indexOf("%") >= 0 ? parseInt(o, 10) * i * r + "%" : o * i * r + "px", l = l.indexOf("%") >= 0 ? parseInt(l, 10) * i + "%" : l * i + "px", void 0 !== h && null !== h) {
                    var p = h - (h - 1) * (1 - Math.abs(i));
                    a[0].style.opacity = p
                }
                if (void 0 === d || null === d) a.transform("translate3d(" + o + ", " + l + ", 0px)");
                else {
                    var c = d - (d - 1) * (1 - Math.abs(i));
                    a.transform("translate3d(" + o + ", " + l + ", 0px) scale(" + c + ")")
                }
            },
            setTranslate: function() {
                var e = this,
                    i = e.$el,
                    s = e.slides,
                    a = e.progress,
                    r = e.snapGrid;
                i.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) { e.parallax.setTransform(i, a) }), s.each(function(i, s) {
                    var n = s.progress;
                    e.params.slidesPerGroup > 1 && "auto" !== e.params.slidesPerView && (n += Math.ceil(i / 2) - a * (r.length - 1)), n = Math.min(Math.max(n, -1), 1), t(s).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(t, i) { e.parallax.setTransform(i, n) })
                })
            },
            setTransition: function(e) {
                void 0 === e && (e = this.params.speed);
                this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(i, s) {
                    var a = t(s),
                        r = parseInt(a.attr("data-swiper-parallax-duration"), 10) || e;
                    0 === e && (r = 0), a.transition(r)
                })
            }
        },
        j = {
            getDistanceBetweenTouches: function(e) {
                if (e.targetTouches.length < 2) return 1;
                var t = e.targetTouches[0].pageX,
                    i = e.targetTouches[0].pageY,
                    s = e.targetTouches[1].pageX,
                    a = e.targetTouches[1].pageY;
                return Math.sqrt(Math.pow(s - t, 2) + Math.pow(a - i, 2))
            },
            onGestureStart: function(e) {
                var i = this.params.zoom,
                    s = this.zoom,
                    a = s.gesture;
                if (s.fakeGestureTouched = !1, s.fakeGestureMoved = !1, !h.gestures) {
                    if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;
                    s.fakeGestureTouched = !0, a.scaleStart = j.getDistanceBetweenTouches(e)
                }
                a.$slideEl && a.$slideEl.length || (a.$slideEl = t(this), 0 === a.$slideEl.length && (a.$slideEl = this.slides.eq(this.activeIndex)), a.$imageEl = a.$slideEl.find("img, svg, canvas"), a.$imageWrapEl = a.$imageEl.parent("." + i.containerClass), a.maxRatio = a.$imageWrapEl.attr("data-swiper-zoom") || i.maxRatio, 0 !== a.$imageWrapEl.length) ? (a.$imageEl.transition(0), this.zoom.isScaling = !0) : a.$imageEl = void 0
            },
            onGestureChange: function(e) {
                var t = this.params.zoom,
                    i = this.zoom,
                    s = i.gesture;
                if (!h.gestures) {
                    if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;
                    i.fakeGestureMoved = !0, s.scaleMove = j.getDistanceBetweenTouches(e)
                }
                s.$imageEl && 0 !== s.$imageEl.length && (h.gestures ? this.zoom.scale = e.scale * i.currentScale : i.scale = s.scaleMove / s.scaleStart * i.currentScale, i.scale > s.maxRatio && (i.scale = s.maxRatio - 1 + Math.pow(i.scale - s.maxRatio + 1, .5)), i.scale < t.minRatio && (i.scale = t.minRatio + 1 - Math.pow(t.minRatio - i.scale + 1, .5)), s.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"))
            },
            onGestureEnd: function(e) {
                var t = this.params.zoom,
                    i = this.zoom,
                    s = i.gesture;
                if (!h.gestures) {
                    if (!i.fakeGestureTouched || !i.fakeGestureMoved) return;
                    if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !y.android) return;
                    i.fakeGestureTouched = !1, i.fakeGestureMoved = !1
                }
                s.$imageEl && 0 !== s.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, s.maxRatio), t.minRatio), s.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"), i.currentScale = i.scale, i.isScaling = !1, 1 === i.scale && (s.$slideEl = void 0))
            },
            onTouchStart: function(e) {
                var t = this.zoom,
                    i = t.gesture,
                    s = t.image;
                i.$imageEl && 0 !== i.$imageEl.length && (s.isTouched || (y.android && e.preventDefault(), s.isTouched = !0, s.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY))
            },
            onTouchMove: function(e) {
                var t = this.zoom,
                    i = t.gesture,
                    s = t.image,
                    a = t.velocity;
                if (i.$imageEl && 0 !== i.$imageEl.length && (this.allowClick = !1, s.isTouched && i.$slideEl)) {
                    s.isMoved || (s.width = i.$imageEl[0].offsetWidth, s.height = i.$imageEl[0].offsetHeight, s.startX = l.getTranslate(i.$imageWrapEl[0], "x") || 0, s.startY = l.getTranslate(i.$imageWrapEl[0], "y") || 0, i.slideWidth = i.$slideEl[0].offsetWidth, i.slideHeight = i.$slideEl[0].offsetHeight, i.$imageWrapEl.transition(0), this.rtl && (s.startX = -s.startX), this.rtl && (s.startY = -s.startY));
                    var r = s.width * t.scale,
                        n = s.height * t.scale;
                    if (!(r < i.slideWidth && n < i.slideHeight)) {
                        if (s.minX = Math.min(i.slideWidth / 2 - r / 2, 0), s.maxX = -s.minX, s.minY = Math.min(i.slideHeight / 2 - n / 2, 0), s.maxY = -s.minY, s.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !s.isMoved && !t.isScaling) { if (this.isHorizontal() && (Math.floor(s.minX) === Math.floor(s.startX) && s.touchesCurrent.x < s.touchesStart.x || Math.floor(s.maxX) === Math.floor(s.startX) && s.touchesCurrent.x > s.touchesStart.x)) return void(s.isTouched = !1); if (!this.isHorizontal() && (Math.floor(s.minY) === Math.floor(s.startY) && s.touchesCurrent.y < s.touchesStart.y || Math.floor(s.maxY) === Math.floor(s.startY) && s.touchesCurrent.y > s.touchesStart.y)) return void(s.isTouched = !1) }
                        e.preventDefault(), e.stopPropagation(), s.isMoved = !0, s.currentX = s.touchesCurrent.x - s.touchesStart.x + s.startX, s.currentY = s.touchesCurrent.y - s.touchesStart.y + s.startY, s.currentX < s.minX && (s.currentX = s.minX + 1 - Math.pow(s.minX - s.currentX + 1, .8)), s.currentX > s.maxX && (s.currentX = s.maxX - 1 + Math.pow(s.currentX - s.maxX + 1, .8)), s.currentY < s.minY && (s.currentY = s.minY + 1 - Math.pow(s.minY - s.currentY + 1, .8)), s.currentY > s.maxY && (s.currentY = s.maxY - 1 + Math.pow(s.currentY - s.maxY + 1, .8)), a.prevPositionX || (a.prevPositionX = s.touchesCurrent.x), a.prevPositionY || (a.prevPositionY = s.touchesCurrent.y), a.prevTime || (a.prevTime = Date.now()), a.x = (s.touchesCurrent.x - a.prevPositionX) / (Date.now() - a.prevTime) / 2, a.y = (s.touchesCurrent.y - a.prevPositionY) / (Date.now() - a.prevTime) / 2, Math.abs(s.touchesCurrent.x - a.prevPositionX) < 2 && (a.x = 0), Math.abs(s.touchesCurrent.y - a.prevPositionY) < 2 && (a.y = 0), a.prevPositionX = s.touchesCurrent.x, a.prevPositionY = s.touchesCurrent.y, a.prevTime = Date.now(), i.$imageWrapEl.transform("translate3d(" + s.currentX + "px, " + s.currentY + "px,0)")
                    }
                }
            },
            onTouchEnd: function() {
                var e = this.zoom,
                    t = e.gesture,
                    i = e.image,
                    s = e.velocity;
                if (t.$imageEl && 0 !== t.$imageEl.length) {
                    if (!i.isTouched || !i.isMoved) return i.isTouched = !1, void(i.isMoved = !1);
                    i.isTouched = !1, i.isMoved = !1;
                    var a = 300,
                        r = 300,
                        n = s.x * a,
                        o = i.currentX + n,
                        l = s.y * r,
                        d = i.currentY + l;
                    0 !== s.x && (a = Math.abs((o - i.currentX) / s.x)), 0 !== s.y && (r = Math.abs((d - i.currentY) / s.y));
                    var h = Math.max(a, r);
                    i.currentX = o, i.currentY = d;
                    var p = i.width * e.scale,
                        c = i.height * e.scale;
                    i.minX = Math.min(t.slideWidth / 2 - p / 2, 0), i.maxX = -i.minX, i.minY = Math.min(t.slideHeight / 2 - c / 2, 0), i.maxY = -i.minY, i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX), i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY), t.$imageWrapEl.transition(h).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                }
            },
            onTransitionEnd: function() {
                var e = this.zoom,
                    t = e.gesture;
                t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0, e.scale = 1, e.currentScale = 1)
            },
            toggle: function(e) {
                var t = this.zoom;
                t.scale && 1 !== t.scale ? t.out() : t.in(e)
            },
            in: function(e) {
                var i, s, a, r, n, o, l, d, h, p, c, u, f, v, m, g, b = this.zoom,
                    w = this.params.zoom,
                    y = b.gesture,
                    x = b.image;
                (y.$slideEl || (y.$slideEl = this.clickedSlide ? t(this.clickedSlide) : this.slides.eq(this.activeIndex), y.$imageEl = y.$slideEl.find("img, svg, canvas"), y.$imageWrapEl = y.$imageEl.parent("." + w.containerClass)), y.$imageEl && 0 !== y.$imageEl.length) && (y.$slideEl.addClass("" + w.zoomedSlideClass), void 0 === x.touchesStart.x && e ? (i = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, s = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (i = x.touchesStart.x, s = x.touchesStart.y), b.scale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, b.currentScale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, e ? (m = y.$slideEl[0].offsetWidth, g = y.$slideEl[0].offsetHeight, a = y.$slideEl.offset().left + m / 2 - i, r = y.$slideEl.offset().top + g / 2 - s, l = y.$imageEl[0].offsetWidth, d = y.$imageEl[0].offsetHeight, h = l * b.scale, p = d * b.scale, f = -(c = Math.min(m / 2 - h / 2, 0)), v = -(u = Math.min(g / 2 - p / 2, 0)), n = a * b.scale, o = r * b.scale, n < c && (n = c), n > f && (n = f), o < u && (o = u), o > v && (o = v)) : (n = 0, o = 0), y.$imageWrapEl.transition(300).transform("translate3d(" + n + "px, " + o + "px,0)"), y.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + b.scale + ")"))
            },
            out: function() {
                var e = this.zoom,
                    i = this.params.zoom,
                    s = e.gesture;
                s.$slideEl || (s.$slideEl = this.clickedSlide ? t(this.clickedSlide) : this.slides.eq(this.activeIndex), s.$imageEl = s.$slideEl.find("img, svg, canvas"), s.$imageWrapEl = s.$imageEl.parent("." + i.containerClass)), s.$imageEl && 0 !== s.$imageEl.length && (e.scale = 1, e.currentScale = 1, s.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), s.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), s.$slideEl.removeClass("" + i.zoomedSlideClass), s.$slideEl = void 0)
            },
            enable: function() {
                var e = this,
                    i = e.zoom;
                if (!i.enabled) {
                    i.enabled = !0;
                    var s = e.slides,
                        a = !("touchstart" !== e.touchEvents.start || !h.passiveListener || !e.params.passiveListeners) && { passive: !0, capture: !1 };
                    h.gestures ? (s.on("gesturestart", i.onGestureStart, a), s.on("gesturechange", i.onGestureChange, a), s.on("gestureend", i.onGestureEnd, a)) : "touchstart" === e.touchEvents.start && (s.on(e.touchEvents.start, i.onGestureStart, a), s.on(e.touchEvents.move, i.onGestureChange, a), s.on(e.touchEvents.end, i.onGestureEnd, a)), e.slides.each(function(s, a) {
                        var r = t(a);
                        r.find("." + e.params.zoom.containerClass).length > 0 && r.on(e.touchEvents.move, i.onTouchMove)
                    })
                }
            },
            disable: function() {
                var e = this,
                    i = e.zoom;
                if (i.enabled) {
                    e.zoom.enabled = !1;
                    var s = e.slides,
                        a = !("touchstart" !== e.touchEvents.start || !h.passiveListener || !e.params.passiveListeners) && { passive: !0, capture: !1 };
                    h.gestures ? (s.off("gesturestart", i.onGestureStart, a), s.off("gesturechange", i.onGestureChange, a), s.off("gestureend", i.onGestureEnd, a)) : "touchstart" === e.touchEvents.start && (s.off(e.touchEvents.start, i.onGestureStart, a), s.off(e.touchEvents.move, i.onGestureChange, a), s.off(e.touchEvents.end, i.onGestureEnd, a)), e.slides.each(function(s, a) {
                        var r = t(a);
                        r.find("." + e.params.zoom.containerClass).length > 0 && r.off(e.touchEvents.move, i.onTouchMove)
                    })
                }
            }
        },
        q = {
            loadInSlide: function(e, i) {
                void 0 === i && (i = !0);
                var s = this,
                    a = s.params.lazy;
                if (void 0 !== e && 0 !== s.slides.length) {
                    var r = s.virtual && s.params.virtual.enabled ? s.$wrapperEl.children("." + s.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : s.slides.eq(e),
                        n = r.find("." + a.elementClass + ":not(." + a.loadedClass + "):not(." + a.loadingClass + ")");
                    !r.hasClass(a.elementClass) || r.hasClass(a.loadedClass) || r.hasClass(a.loadingClass) || (n = n.add(r[0])), 0 !== n.length && n.each(function(e, n) {
                        var o = t(n);
                        o.addClass(a.loadingClass);
                        var l = o.attr("data-background"),
                            d = o.attr("data-src"),
                            h = o.attr("data-srcset"),
                            p = o.attr("data-sizes");
                        s.loadImage(o[0], d || l, h, p, !1, function() {
                            if (void 0 !== s && null !== s && s && (!s || s.params) && !s.destroyed) {
                                if (l ? (o.css("background-image", 'url("' + l + '")'), o.removeAttr("data-background")) : (h && (o.attr("srcset", h), o.removeAttr("data-srcset")), p && (o.attr("sizes", p), o.removeAttr("data-sizes")), d && (o.attr("src", d), o.removeAttr("data-src"))), o.addClass(a.loadedClass).removeClass(a.loadingClass), r.find("." + a.preloaderClass).remove(), s.params.loop && i) {
                                    var e = r.attr("data-swiper-slide-index");
                                    if (r.hasClass(s.params.slideDuplicateClass)) {
                                        var t = s.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + s.params.slideDuplicateClass + ")");
                                        s.lazy.loadInSlide(t.index(), !1)
                                    } else {
                                        var n = s.$wrapperEl.children("." + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                                        s.lazy.loadInSlide(n.index(), !1)
                                    }
                                }
                                s.emit("lazyImageReady", r[0], o[0])
                            }
                        }), s.emit("lazyImageLoad", r[0], o[0])
                    })
                }
            },
            load: function() {
                var e = this,
                    i = e.$wrapperEl,
                    s = e.params,
                    a = e.slides,
                    r = e.activeIndex,
                    n = e.virtual && s.virtual.enabled,
                    o = s.lazy,
                    l = s.slidesPerView;

                function d(e) { if (n) { if (i.children("." + s.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0 } else if (a[e]) return !0; return !1 }

                function h(e) { return n ? t(e).attr("data-swiper-slide-index") : t(e).index() }
                if ("auto" === l && (l = 0), e.lazy.initialImageLoaded || (e.lazy.initialImageLoaded = !0), e.params.watchSlidesVisibility) i.children("." + s.slideVisibleClass).each(function(i, s) {
                    var a = n ? t(s).attr("data-swiper-slide-index") : t(s).index();
                    e.lazy.loadInSlide(a)
                });
                else if (l > 1)
                    for (var p = r; p < r + l; p += 1) d(p) && e.lazy.loadInSlide(p);
                else e.lazy.loadInSlide(r);
                if (o.loadPrevNext)
                    if (l > 1 || o.loadPrevNextAmount && o.loadPrevNextAmount > 1) { for (var c = o.loadPrevNextAmount, u = l, f = Math.min(r + u + Math.max(c, u), a.length), v = Math.max(r - Math.max(u, c), 0), m = r + l; m < f; m += 1) d(m) && e.lazy.loadInSlide(m); for (var g = v; g < r; g += 1) d(g) && e.lazy.loadInSlide(g) } else {
                        var b = i.children("." + s.slideNextClass);
                        b.length > 0 && e.lazy.loadInSlide(h(b));
                        var w = i.children("." + s.slidePrevClass);
                        w.length > 0 && e.lazy.loadInSlide(h(w))
                    }
            }
        },
        K = {
            LinearSpline: function(e, t) { var i, s, a, r, n, o = function(e, t) { for (s = -1, i = e.length; i - s > 1;) e[a = i + s >> 1] <= t ? s = a : i = a; return i }; return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function(e) { return e ? (n = o(this.x, e), r = n - 1, (e - this.x[r]) * (this.y[n] - this.y[r]) / (this.x[n] - this.x[r]) + this.y[r]) : 0 }, this },
            getInterpolateFunction: function(e) { this.controller.spline || (this.controller.spline = this.params.loop ? new K.LinearSpline(this.slidesGrid, e.slidesGrid) : new K.LinearSpline(this.snapGrid, e.snapGrid)) },
            setTranslate: function(e, t) {
                var i, s, a = this,
                    r = a.controller.control;

                function n(e) { var t = e.rtl && "horizontal" === e.params.direction ? -a.translate : a.translate; "slide" === a.params.controller.by && (a.controller.getInterpolateFunction(e), s = -a.controller.spline.interpolate(-t)), s && "container" !== a.params.controller.by || (i = (e.maxTranslate() - e.minTranslate()) / (a.maxTranslate() - a.minTranslate()), s = (t - a.minTranslate()) * i + e.minTranslate()), a.params.controller.inverse && (s = e.maxTranslate() - s), e.updateProgress(s), e.setTranslate(s, a), e.updateActiveIndex(), e.updateSlidesClasses() }
                if (Array.isArray(r))
                    for (var o = 0; o < r.length; o += 1) r[o] !== t && r[o] instanceof k && n(r[o]);
                else r instanceof k && t !== r && n(r)
            },
            setTransition: function(e, t) {
                var i, s = this,
                    a = s.controller.control;

                function r(t) { t.setTransition(e, s), 0 !== e && (t.transitionStart(), t.$wrapperEl.transitionEnd(function() { a && (t.params.loop && "slide" === s.params.controller.by && t.loopFix(), t.transitionEnd()) })) }
                if (Array.isArray(a))
                    for (i = 0; i < a.length; i += 1) a[i] !== t && a[i] instanceof k && r(a[i]);
                else a instanceof k && t !== a && r(a)
            }
        },
        U = {
            makeElFocusable: function(e) { return e.attr("tabIndex", "0"), e },
            addElRole: function(e, t) { return e.attr("role", t), e },
            addElLabel: function(e, t) { return e.attr("aria-label", t), e },
            disableEl: function(e) { return e.attr("aria-disabled", !0), e },
            enableEl: function(e) { return e.attr("aria-disabled", !1), e },
            onEnterKey: function(e) {
                var i = this.params.a11y;
                if (13 === e.keyCode) {
                    var s = t(e.target);
                    this.navigation && this.navigation.$nextEl && s.is(this.navigation.$nextEl) && (this.isEnd && !this.params.loop || this.slideNext(), this.isEnd ? this.a11y.notify(i.lastSlideMessage) : this.a11y.notify(i.nextSlideMessage)), this.navigation && this.navigation.$prevEl && s.is(this.navigation.$prevEl) && (this.isBeginning && !this.params.loop || this.slidePrev(), this.isBeginning ? this.a11y.notify(i.firstSlideMessage) : this.a11y.notify(i.prevSlideMessage)), this.pagination && s.is("." + this.params.pagination.bulletClass) && s[0].click()
                }
            },
            notify: function(e) {
                var t = this.a11y.liveRegion;
                0 !== t.length && (t.html(""), t.html(e))
            },
            updateNavigation: function() {
                if (!this.params.loop) {
                    var e = this.navigation,
                        t = e.$nextEl,
                        i = e.$prevEl;
                    i && i.length > 0 && (this.isBeginning ? this.a11y.disableEl(i) : this.a11y.enableEl(i)), t && t.length > 0 && (this.isEnd ? this.a11y.disableEl(t) : this.a11y.enableEl(t))
                }
            },
            updatePagination: function() {
                var e = this,
                    i = e.params.a11y;
                e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.bullets.each(function(s, a) {
                    var r = t(a);
                    e.a11y.makeElFocusable(r), e.a11y.addElRole(r, "button"), e.a11y.addElLabel(r, i.paginationBulletMessage.replace(/{{index}}/, r.index() + 1))
                })
            },
            init: function() {
                this.$el.append(this.a11y.liveRegion);
                var e, t, i = this.params.a11y;
                this.navigation && this.navigation.$nextEl && (e = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (t = this.navigation.$prevEl), e && (this.a11y.makeElFocusable(e), this.a11y.addElRole(e, "button"), this.a11y.addElLabel(e, i.nextSlideMessage), e.on("keydown", this.a11y.onEnterKey)), t && (this.a11y.makeElFocusable(t), this.a11y.addElRole(t, "button"), this.a11y.addElLabel(t, i.prevSlideMessage), t.on("keydown", this.a11y.onEnterKey)), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.on("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey)
            },
            destroy: function() {
                var e, t;
                this.a11y.liveRegion && this.a11y.liveRegion.length > 0 && this.a11y.liveRegion.remove(), this.navigation && this.navigation.$nextEl && (e = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (t = this.navigation.$prevEl), e && e.off("keydown", this.a11y.onEnterKey), t && t.off("keydown", this.a11y.onEnterKey), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.off("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey)
            }
        },
        _ = {
            init: function() {
                if (this.params.history) {
                    if (!o.history || !o.history.pushState) return this.params.history.enabled = !1, void(this.params.hashNavigation.enabled = !0);
                    var e = this.history;
                    e.initialized = !0, e.paths = _.getPathValues(), (e.paths.key || e.paths.value) && (e.scrollToSlide(0, e.paths.value, this.params.runCallbacksOnInit), this.params.history.replaceState || o.addEventListener("popstate", this.history.setHistoryPopState))
                }
            },
            destroy: function() { this.params.history.replaceState || o.removeEventListener("popstate", this.history.setHistoryPopState) },
            setHistoryPopState: function() { this.history.paths = _.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1) },
            getPathValues: function() {
                var e = o.location.pathname.slice(1).split("/").filter(function(e) { return "" !== e }),
                    t = e.length;
                return { key: e[t - 2], value: e[t - 1] }
            },
            setHistory: function(e, t) {
                if (this.history.initialized && this.params.history.enabled) {
                    var i = this.slides.eq(t),
                        s = _.slugify(i.attr("data-history"));
                    o.location.pathname.includes(e) || (s = e + "/" + s);
                    var a = o.history.state;
                    a && a.value === s || (this.params.history.replaceState ? o.history.replaceState({ value: s }, null, s) : o.history.pushState({ value: s }, null, s))
                }
            },
            slugify: function(e) { return e.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "") },
            scrollToSlide: function(e, t, i) {
                if (t)
                    for (var s = 0, a = this.slides.length; s < a; s += 1) {
                        var r = this.slides.eq(s);
                        if (_.slugify(r.attr("data-history")) === t && !r.hasClass(this.params.slideDuplicateClass)) {
                            var n = r.index();
                            this.slideTo(n, e, i)
                        }
                    } else this.slideTo(0, e, i)
            }
        },
        Z = {
            onHashCange: function() {
                var e = d.location.hash.replace("#", "");
                e !== this.slides.eq(this.activeIndex).attr("data-hash") && this.slideTo(this.$wrapperEl.children("." + this.params.slideClass + '[data-hash="' + e + '"]').index())
            },
            setHash: function() {
                if (this.hashNavigation.initialized && this.params.hashNavigation.enabled)
                    if (this.params.hashNavigation.replaceState && o.history && o.history.replaceState) o.history.replaceState(null, null, "#" + this.slides.eq(this.activeIndex).attr("data-hash") || "");
                    else {
                        var e = this.slides.eq(this.activeIndex),
                            t = e.attr("data-hash") || e.attr("data-history");
                        d.location.hash = t || ""
                    }
            },
            init: function() {
                if (!(!this.params.hashNavigation.enabled || this.params.history && this.params.history.enabled)) {
                    this.hashNavigation.initialized = !0;
                    var e = d.location.hash.replace("#", "");
                    if (e)
                        for (var i = 0, s = this.slides.length; i < s; i += 1) {
                            var a = this.slides.eq(i);
                            if ((a.attr("data-hash") || a.attr("data-history")) === e && !a.hasClass(this.params.slideDuplicateClass)) {
                                var r = a.index();
                                this.slideTo(r, 0, this.params.runCallbacksOnInit, !0)
                            }
                        }
                    this.params.hashNavigation.watchState && t(o).on("hashchange", this.hashNavigation.onHashCange)
                }
            },
            destroy: function() { this.params.hashNavigation.watchState && t(o).off("hashchange", this.hashNavigation.onHashCange) }
        },
        Q = {
            run: function() {
                var e = this,
                    t = e.slides.eq(e.activeIndex),
                    i = e.params.autoplay.delay;
                t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), e.autoplay.timeout = l.nextTick(function() { e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) }, i)
            },
            start: function() { return void 0 === this.autoplay.timeout && (!this.autoplay.running && (this.autoplay.running = !0, this.emit("autoplayStart"), this.autoplay.run(), !0)) },
            stop: function() { return !!this.autoplay.running && (void 0 !== this.autoplay.timeout && (this.autoplay.timeout && (clearTimeout(this.autoplay.timeout), this.autoplay.timeout = void 0), this.autoplay.running = !1, this.emit("autoplayStop"), !0)) },
            pause: function(e) {
                var t = this;
                t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, 0 !== e && t.params.autoplay.waitForTransition ? t.$wrapperEl.transitionEnd(function() { t && !t.destroyed && (t.autoplay.paused = !1, t.autoplay.running ? t.autoplay.run() : t.autoplay.stop()) }) : (t.autoplay.paused = !1, t.autoplay.run())))
            }
        },
        J = {
            setTranslate: function() {
                for (var e = this.slides, t = 0; t < e.length; t += 1) {
                    var i = this.slides.eq(t),
                        s = -i[0].swiperSlideOffset;
                    this.params.virtualTranslate || (s -= this.translate);
                    var a = 0;
                    this.isHorizontal() || (a = s, s = 0);
                    var r = this.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(i[0].progress), 0) : 1 + Math.min(Math.max(i[0].progress, -1), 0);
                    i.css({ opacity: r }).transform("translate3d(" + s + "px, " + a + "px, 0px)")
                }
            },
            setTransition: function(e) {
                var t = this,
                    i = t.slides,
                    s = t.$wrapperEl;
                if (i.transition(e), t.params.virtualTranslate && 0 !== e) {
                    var a = !1;
                    i.transitionEnd(function() { if (!a && t && !t.destroyed) { a = !0, t.animating = !1; for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) s.trigger(e[i]) } })
                }
            }
        },
        ee = {
            setTranslate: function() {
                var e, i = this.$el,
                    s = this.$wrapperEl,
                    a = this.slides,
                    r = this.width,
                    n = this.height,
                    o = this.rtl,
                    l = this.size,
                    d = this.params.cubeEffect,
                    h = this.isHorizontal(),
                    p = this.virtual && this.params.virtual.enabled,
                    c = 0;
                d.shadow && (h ? (0 === (e = s.find(".swiper-cube-shadow")).length && (e = t('<div class="swiper-cube-shadow"></div>'), s.append(e)), e.css({ height: r + "px" })) : 0 === (e = i.find(".swiper-cube-shadow")).length && (e = t('<div class="swiper-cube-shadow"></div>'), i.append(e)));
                for (var u = 0; u < a.length; u += 1) {
                    var f = a.eq(u),
                        v = u;
                    p && (v = parseInt(f.attr("data-swiper-slide-index"), 10));
                    var m = 90 * v,
                        g = Math.floor(m / 360);
                    o && (m = -m, g = Math.floor(-m / 360));
                    var b = Math.max(Math.min(f[0].progress, 1), -1),
                        w = 0,
                        y = 0,
                        x = 0;
                    v % 4 == 0 ? (w = 4 * -g * l, x = 0) : (v - 1) % 4 == 0 ? (w = 0, x = 4 * -g * l) : (v - 2) % 4 == 0 ? (w = l + 4 * g * l, x = l) : (v - 3) % 4 == 0 && (w = -l, x = 3 * l + 4 * l * g), o && (w = -w), h || (y = w, w = 0);
                    var T = "rotateX(" + (h ? 0 : -m) + "deg) rotateY(" + (h ? m : 0) + "deg) translate3d(" + w + "px, " + y + "px, " + x + "px)";
                    if (b <= 1 && b > -1 && (c = 90 * v + 90 * b, o && (c = 90 * -v - 90 * b)), f.transform(T), d.slideShadows) {
                        var E = h ? f.find(".swiper-slide-shadow-left") : f.find(".swiper-slide-shadow-top"),
                            S = h ? f.find(".swiper-slide-shadow-right") : f.find(".swiper-slide-shadow-bottom");
                        0 === E.length && (E = t('<div class="swiper-slide-shadow-' + (h ? "left" : "top") + '"></div>'), f.append(E)), 0 === S.length && (S = t('<div class="swiper-slide-shadow-' + (h ? "right" : "bottom") + '"></div>'), f.append(S)), E.length && (E[0].style.opacity = Math.max(-b, 0)), S.length && (S[0].style.opacity = Math.max(b, 0))
                    }
                }
                if (s.css({ "-webkit-transform-origin": "50% 50% -" + l / 2 + "px", "-moz-transform-origin": "50% 50% -" + l / 2 + "px", "-ms-transform-origin": "50% 50% -" + l / 2 + "px", "transform-origin": "50% 50% -" + l / 2 + "px" }), d.shadow)
                    if (h) e.transform("translate3d(0px, " + (r / 2 + d.shadowOffset) + "px, " + -r / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + d.shadowScale + ")");
                    else {
                        var C = Math.abs(c) - 90 * Math.floor(Math.abs(c) / 90),
                            M = 1.5 - (Math.sin(2 * C * Math.PI / 360) / 2 + Math.cos(2 * C * Math.PI / 360) / 2),
                            z = d.shadowScale,
                            P = d.shadowScale / M,
                            k = d.shadowOffset;
                        e.transform("scale3d(" + z + ", 1, " + P + ") translate3d(0px, " + (n / 2 + k) + "px, " + -n / 2 / P + "px) rotateX(-90deg)")
                    }
                var $ = I.isSafari || I.isUiWebView ? -l / 2 : 0;
                s.transform("translate3d(0px,0," + $ + "px) rotateX(" + (this.isHorizontal() ? 0 : c) + "deg) rotateY(" + (this.isHorizontal() ? -c : 0) + "deg)")
            },
            setTransition: function(e) {
                var t = this.$el;
                this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e)
            }
        },
        te = {
            setTranslate: function() {
                for (var e = this.slides, i = 0; i < e.length; i += 1) {
                    var s = e.eq(i),
                        a = s[0].progress;
                    this.params.flipEffect.limitRotation && (a = Math.max(Math.min(s[0].progress, 1), -1));
                    var r = -180 * a,
                        n = 0,
                        o = -s[0].swiperSlideOffset,
                        l = 0;
                    if (this.isHorizontal() ? this.rtl && (r = -r) : (l = o, o = 0, n = -r, r = 0), s[0].style.zIndex = -Math.abs(Math.round(a)) + e.length, this.params.flipEffect.slideShadows) {
                        var d = this.isHorizontal() ? s.find(".swiper-slide-shadow-left") : s.find(".swiper-slide-shadow-top"),
                            h = this.isHorizontal() ? s.find(".swiper-slide-shadow-right") : s.find(".swiper-slide-shadow-bottom");
                        0 === d.length && (d = t('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "left" : "top") + '"></div>'), s.append(d)), 0 === h.length && (h = t('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "right" : "bottom") + '"></div>'), s.append(h)), d.length && (d[0].style.opacity = Math.max(-a, 0)), h.length && (h[0].style.opacity = Math.max(a, 0))
                    }
                    s.transform("translate3d(" + o + "px, " + l + "px, 0px) rotateX(" + n + "deg) rotateY(" + r + "deg)")
                }
            },
            setTransition: function(e) {
                var t = this,
                    i = t.slides,
                    s = t.activeIndex,
                    a = t.$wrapperEl;
                if (i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), t.params.virtualTranslate && 0 !== e) {
                    var r = !1;
                    i.eq(s).transitionEnd(function() { if (!r && t && !t.destroyed) { r = !0, t.animating = !1; for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) a.trigger(e[i]) } })
                }
            }
        },
        ie = {
            setTranslate: function() {
                for (var e = this.width, i = this.height, s = this.slides, a = this.$wrapperEl, r = this.slidesSizesGrid, n = this.params.coverflowEffect, o = this.isHorizontal(), l = this.translate, d = o ? e / 2 - l : i / 2 - l, p = o ? n.rotate : -n.rotate, c = n.depth, u = 0, f = s.length; u < f; u += 1) {
                    var v = s.eq(u),
                        m = r[u],
                        g = (d - v[0].swiperSlideOffset - m / 2) / m * n.modifier,
                        b = o ? p * g : 0,
                        w = o ? 0 : p * g,
                        y = -c * Math.abs(g),
                        x = o ? 0 : n.stretch * g,
                        T = o ? n.stretch * g : 0;
                    Math.abs(T) < .001 && (T = 0), Math.abs(x) < .001 && (x = 0), Math.abs(y) < .001 && (y = 0), Math.abs(b) < .001 && (b = 0), Math.abs(w) < .001 && (w = 0);
                    var E = "translate3d(" + T + "px," + x + "px," + y + "px)  rotateX(" + w + "deg) rotateY(" + b + "deg)";
                    if (v.transform(E), v[0].style.zIndex = 1 - Math.abs(Math.round(g)), n.slideShadows) {
                        var S = o ? v.find(".swiper-slide-shadow-left") : v.find(".swiper-slide-shadow-top"),
                            C = o ? v.find(".swiper-slide-shadow-right") : v.find(".swiper-slide-shadow-bottom");
                        0 === S.length && (S = t('<div class="swiper-slide-shadow-' + (o ? "left" : "top") + '"></div>'), v.append(S)), 0 === C.length && (C = t('<div class="swiper-slide-shadow-' + (o ? "right" : "bottom") + '"></div>'), v.append(C)), S.length && (S[0].style.opacity = g > 0 ? g : 0), C.length && (C[0].style.opacity = -g > 0 ? -g : 0)
                    }
                }(h.pointerEvents || h.prefixedPointerEvents) && (a[0].style.perspectiveOrigin = d + "px 50%")
            },
            setTransition: function(e) { this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e) }
        },
        se = [$, L, D, O, H, X, G, { name: "mousewheel", params: { mousewheel: { enabled: !1, releaseOnEdges: !1, invert: !1, forceToAxis: !1, sensitivity: 1, eventsTarged: "container" } }, create: function() { l.extend(this, { mousewheel: { enabled: !1, enable: B.enable.bind(this), disable: B.disable.bind(this), handle: B.handle.bind(this), lastScrollTime: l.now() } }) }, on: { init: function() { this.params.mousewheel.enabled && this.mousewheel.enable() }, destroy: function() { this.mousewheel.enabled && this.mousewheel.disable() } } }, {
            name: "navigation",
            params: { navigation: { nextEl: null, prevEl: null, hideOnClick: !1, disabledClass: "swiper-button-disabled", hiddenClass: "swiper-button-hidden", lockClass: "swiper-button-lock" } },
            create: function() { l.extend(this, { navigation: { init: V.init.bind(this), update: V.update.bind(this), destroy: V.destroy.bind(this) } }) },
            on: {
                init: function() { this.navigation.init(), this.navigation.update() },
                toEdge: function() { this.navigation.update() },
                fromEdge: function() { this.navigation.update() },
                destroy: function() { this.navigation.destroy() },
                click: function(e) {
                    var i = this.navigation,
                        s = i.$nextEl,
                        a = i.$prevEl;
                    !this.params.navigation.hideOnClick || t(e.target).is(a) || t(e.target).is(s) || (s && s.toggleClass(this.params.navigation.hiddenClass), a && a.toggleClass(this.params.navigation.hiddenClass))
                }
            }
        }, { name: "pagination", params: { pagination: { el: null, bulletElement: "span", clickable: !1, hideOnClick: !1, renderBullet: null, renderProgressbar: null, renderFraction: null, renderCustom: null, type: "bullets", dynamicBullets: !1, bulletClass: "swiper-pagination-bullet", bulletActiveClass: "swiper-pagination-bullet-active", modifierClass: "swiper-pagination-", currentClass: "swiper-pagination-current", totalClass: "swiper-pagination-total", hiddenClass: "swiper-pagination-hidden", progressbarFillClass: "swiper-pagination-progressbar-fill", clickableClass: "swiper-pagination-clickable", lockClass: "swiper-pagination-lock" } }, create: function() { l.extend(this, { pagination: { init: R.init.bind(this), render: R.render.bind(this), update: R.update.bind(this), destroy: R.destroy.bind(this) } }) }, on: { init: function() { this.pagination.init(), this.pagination.render(), this.pagination.update() }, activeIndexChange: function() { this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update() }, snapIndexChange: function() { this.params.loop || this.pagination.update() }, slidesLengthChange: function() { this.params.loop && (this.pagination.render(), this.pagination.update()) }, snapGridLengthChange: function() { this.params.loop || (this.pagination.render(), this.pagination.update()) }, destroy: function() { this.pagination.destroy() }, click: function(e) { this.params.pagination.el && this.params.pagination.hideOnClick && this.pagination.$el.length > 0 && !t(e.target).hasClass(this.params.pagination.bulletClass) && this.pagination.$el.toggleClass(this.params.pagination.hiddenClass) } } }, { name: "scrollbar", params: { scrollbar: { el: null, dragSize: "auto", hide: !1, draggable: !1, snapOnRelease: !0, lockClass: "swiper-scrollbar-lock" } }, create: function() { l.extend(this, { scrollbar: { init: F.init.bind(this), destroy: F.destroy.bind(this), updateSize: F.updateSize.bind(this), setTranslate: F.setTranslate.bind(this), setTransition: F.setTransition.bind(this), enableDraggable: F.enableDraggable.bind(this), disableDraggable: F.disableDraggable.bind(this), setDragPosition: F.setDragPosition.bind(this), onDragStart: F.onDragStart.bind(this), onDragMove: F.onDragMove.bind(this), onDragEnd: F.onDragEnd.bind(this), isTouched: !1, timeout: null, dragTimeout: null } }) }, on: { init: function() { this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate() }, update: function() { this.scrollbar.updateSize() }, resize: function() { this.scrollbar.updateSize() }, observerUpdate: function() { this.scrollbar.updateSize() }, setTranslate: function() { this.scrollbar.setTranslate() }, setTransition: function(e) { this.scrollbar.setTransition(e) }, destroy: function() { this.scrollbar.destroy() } } }, { name: "parallax", params: { parallax: { enabled: !1 } }, create: function() { l.extend(this, { parallax: { setTransform: W.setTransform.bind(this), setTranslate: W.setTranslate.bind(this), setTransition: W.setTransition.bind(this) } }) }, on: { beforeInit: function() { this.params.watchSlidesProgress = !0 }, init: function() { this.params.parallax && this.parallax.setTranslate() }, setTranslate: function() { this.params.parallax && this.parallax.setTranslate() }, setTransition: function(e) { this.params.parallax && this.parallax.setTransition(e) } } }, {
            name: "zoom",
            params: { zoom: { enabled: !1, maxRatio: 3, minRatio: 1, toggle: !0, containerClass: "swiper-zoom-container", zoomedSlideClass: "swiper-slide-zoomed" } },
            create: function() {
                var e = this,
                    t = { enabled: !1, scale: 1, currentScale: 1, isScaling: !1, gesture: { $slideEl: void 0, slideWidth: void 0, slideHeight: void 0, $imageEl: void 0, $imageWrapEl: void 0, maxRatio: 3 }, image: { isTouched: void 0, isMoved: void 0, currentX: void 0, currentY: void 0, minX: void 0, minY: void 0, maxX: void 0, maxY: void 0, width: void 0, height: void 0, startX: void 0, startY: void 0, touchesStart: {}, touchesCurrent: {} }, velocity: { x: void 0, y: void 0, prevPositionX: void 0, prevPositionY: void 0, prevTime: void 0 } };
                "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function(i) { t[i] = j[i].bind(e) }), l.extend(e, { zoom: t })
            },
            on: { init: function() { this.params.zoom.enabled && this.zoom.enable() }, destroy: function() { this.zoom.disable() }, touchStart: function(e) { this.zoom.enabled && this.zoom.onTouchStart(e) }, touchEnd: function(e) { this.zoom.enabled && this.zoom.onTouchEnd(e) }, doubleTap: function(e) { this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e) }, transitionEnd: function() { this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd() } }
        }, { name: "lazy", params: { lazy: { enabled: !1, loadPrevNext: !1, loadPrevNextAmount: 1, loadOnTransitionStart: !1, elementClass: "swiper-lazy", loadingClass: "swiper-lazy-loading", loadedClass: "swiper-lazy-loaded", preloaderClass: "swiper-lazy-preloader" } }, create: function() { l.extend(this, { lazy: { initialImageLoaded: !1, load: q.load.bind(this), loadInSlide: q.loadInSlide.bind(this) } }) }, on: { beforeInit: function() { this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1) }, init: function() { this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load() }, scroll: function() { this.params.freeMode && !this.params.freeModeSticky && this.lazy.load() }, resize: function() { this.params.lazy.enabled && this.lazy.load() }, scrollbarDragMove: function() { this.params.lazy.enabled && this.lazy.load() }, transitionStart: function() { this.params.lazy.enabled && (this.params.lazy.loadOnTransitionStart || !this.params.lazy.loadOnTransitionStart && !this.lazy.initialImageLoaded) && this.lazy.load() }, transitionEnd: function() { this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load() } } }, { name: "controller", params: { controller: { control: void 0, inverse: !1, by: "slide" } }, create: function() { l.extend(this, { controller: { control: this.params.controller.control, getInterpolateFunction: K.getInterpolateFunction.bind(this), setTranslate: K.setTranslate.bind(this), setTransition: K.setTransition.bind(this) } }) }, on: { update: function() { this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline) }, resize: function() { this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline) }, observerUpdate: function() { this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline) }, setTranslate: function(e, t) { this.controller.control && this.controller.setTranslate(e, t) }, setTransition: function(e, t) { this.controller.control && this.controller.setTransition(e, t) } } }, {
            name: "a11y",
            params: { a11y: { enabled: !1, notificationClass: "swiper-notification", prevSlideMessage: "Previous slide", nextSlideMessage: "Next slide", firstSlideMessage: "This is the first slide", lastSlideMessage: "This is the last slide", paginationBulletMessage: "Go to slide {{index}}" } },
            create: function() {
                var e = this;
                l.extend(e, { a11y: { liveRegion: t('<span class="' + e.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>') } }), Object.keys(U).forEach(function(t) { e.a11y[t] = U[t].bind(e) })
            },
            on: { init: function() { this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation()) }, toEdge: function() { this.params.a11y.enabled && this.a11y.updateNavigation() }, fromEdge: function() { this.params.a11y.enabled && this.a11y.updateNavigation() }, paginationUpdate: function() { this.params.a11y.enabled && this.a11y.updatePagination() }, destroy: function() { this.params.a11y.enabled && this.a11y.destroy() } }
        }, { name: "history", params: { history: { enabled: !1, replaceState: !1, key: "slides" } }, create: function() { l.extend(this, { history: { init: _.init.bind(this), setHistory: _.setHistory.bind(this), setHistoryPopState: _.setHistoryPopState.bind(this), scrollToSlide: _.scrollToSlide.bind(this), destroy: _.destroy.bind(this) } }) }, on: { init: function() { this.params.history.enabled && this.history.init() }, destroy: function() { this.params.history.enabled && this.history.destroy() }, transitionEnd: function() { this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex) } } }, { name: "hash-navigation", params: { hashNavigation: { enabled: !1, replaceState: !1, watchState: !1 } }, create: function() { l.extend(this, { hashNavigation: { initialized: !1, init: Z.init.bind(this), destroy: Z.destroy.bind(this), setHash: Z.setHash.bind(this), onHashCange: Z.onHashCange.bind(this) } }) }, on: { init: function() { this.params.hashNavigation.enabled && this.hashNavigation.init() }, destroy: function() { this.params.hashNavigation.enabled && this.hashNavigation.destroy() }, transitionEnd: function() { this.hashNavigation.initialized && this.hashNavigation.setHash() } } }, { name: "autoplay", params: { autoplay: { enabled: !1, delay: 3e3, waitForTransition: !0, disableOnInteraction: !0, stopOnLastSlide: !1, reverseDirection: !1 } }, create: function() { l.extend(this, { autoplay: { running: !1, paused: !1, run: Q.run.bind(this), start: Q.start.bind(this), stop: Q.stop.bind(this), pause: Q.pause.bind(this) } }) }, on: { init: function() { this.params.autoplay.enabled && this.autoplay.start() }, beforeTransitionStart: function(e, t) { this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop()) }, sliderFirstMove: function() { this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause()) }, destroy: function() { this.autoplay.running && this.autoplay.stop() } } }, {
            name: "effect-fade",
            params: { fadeEffect: { crossFade: !1 } },
            create: function() { l.extend(this, { fadeEffect: { setTranslate: J.setTranslate.bind(this), setTransition: J.setTransition.bind(this) } }) },
            on: {
                beforeInit: function() {
                    if ("fade" === this.params.effect) {
                        this.classNames.push(this.params.containerModifierClass + "fade");
                        var e = { slidesPerView: 1, slidesPerColumn: 1, slidesPerGroup: 1, watchSlidesProgress: !0, spaceBetween: 0, virtualTranslate: !0 };
                        l.extend(this.params, e), l.extend(this.originalParams, e)
                    }
                },
                setTranslate: function() { "fade" === this.params.effect && this.fadeEffect.setTranslate() },
                setTransition: function(e) { "fade" === this.params.effect && this.fadeEffect.setTransition(e) }
            }
        }, {
            name: "effect-cube",
            params: { cubeEffect: { slideShadows: !0, shadow: !0, shadowOffset: 20, shadowScale: .94 } },
            create: function() { l.extend(this, { cubeEffect: { setTranslate: ee.setTranslate.bind(this), setTransition: ee.setTransition.bind(this) } }) },
            on: {
                beforeInit: function() {
                    if ("cube" === this.params.effect) {
                        this.classNames.push(this.params.containerModifierClass + "cube"), this.classNames.push(this.params.containerModifierClass + "3d");
                        var e = { slidesPerView: 1, slidesPerColumn: 1, slidesPerGroup: 1, watchSlidesProgress: !0, resistanceRatio: 0, spaceBetween: 0, centeredSlides: !1, virtualTranslate: !0 };
                        l.extend(this.params, e), l.extend(this.originalParams, e)
                    }
                },
                setTranslate: function() { "cube" === this.params.effect && this.cubeEffect.setTranslate() },
                setTransition: function(e) { "cube" === this.params.effect && this.cubeEffect.setTransition(e) }
            }
        }, {
            name: "effect-flip",
            params: { flipEffect: { slideShadows: !0, limitRotation: !0 } },
            create: function() { l.extend(this, { flipEffect: { setTranslate: te.setTranslate.bind(this), setTransition: te.setTransition.bind(this) } }) },
            on: {
                beforeInit: function() {
                    if ("flip" === this.params.effect) {
                        this.classNames.push(this.params.containerModifierClass + "flip"), this.classNames.push(this.params.containerModifierClass + "3d");
                        var e = { slidesPerView: 1, slidesPerColumn: 1, slidesPerGroup: 1, watchSlidesProgress: !0, spaceBetween: 0, virtualTranslate: !0 };
                        l.extend(this.params, e), l.extend(this.originalParams, e)
                    }
                },
                setTranslate: function() { "flip" === this.params.effect && this.flipEffect.setTranslate() },
                setTransition: function(e) { "flip" === this.params.effect && this.flipEffect.setTransition(e) }
            }
        }, { name: "effect-coverflow", params: { coverflowEffect: { rotate: 50, stretch: 0, depth: 100, modifier: 1, slideShadows: !0 } }, create: function() { l.extend(this, { coverflowEffect: { setTranslate: ie.setTranslate.bind(this), setTransition: ie.setTransition.bind(this) } }) }, on: { beforeInit: function() { "coverflow" === this.params.effect && (this.classNames.push(this.params.containerModifierClass + "coverflow"), this.classNames.push(this.params.containerModifierClass + "3d"), this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0) }, setTranslate: function() { "coverflow" === this.params.effect && this.coverflowEffect.setTranslate() }, setTransition: function(e) { "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e) } } }];
    return void 0 === k.use && (k.use = k.Class.use, k.installModule = k.Class.installModule), k.use(se), k
});
(function($) {
    $.fn.extend({
        mouseParallax: function(options) {
            var defaults = { moveFactor: 5, zIndexValue: "-1", targetContainer: '.moving-background, .moving-background2' };
            var options = $.extend(defaults, options);
            return this.each(function() {
                var o = options;
                var background = $(this);
                $(o.targetContainer).on('mousemove', function(e) {
                    mouseX = e.pageX;
                    mouseY = e.pageY;
                    windowWidth = $(window).width();
                    windowHeight = $(window).height();
                    percentX = ((mouseX / windowWidth) * o.moveFactor) - (o.moveFactor / 2);
                    percentY = ((mouseY / windowHeight) * o.moveFactor) - (o.moveFactor / 2);
                    leftString = (0 - percentX - o.moveFactor) + "%";
                    rightString = (0 - percentX - o.moveFactor) + "%";
                    topString = (0 - percentY - o.moveFactor) + "%";
                    bottomString = (0 - percentY - o.moveFactor) + "%";
                    background[0].style.left = leftString;
                    background[0].style.right = rightString;
                    background[0].style.top = topString;
                    background[0].style.bottom = bottomString;
                    if (o.zIndexValue) { background[0].style.zIndex = o.zIndexValue; }
                });
            });
        }
    });
}(jQuery));
(function() {
    var e;
    e = function() {
            function e(e, t) {
                var n, r;
                this.options = { target: "instafeed", get: "popular", resolution: "thumbnail", sortBy: "none", links: !0, mock: !1, useHttp: !1 };
                if (typeof e == "object")
                    for (n in e) r = e[n], this.options[n] = r;
                this.context = t != null ? t : this, this.unique = this._genKey()
            }
            return e.prototype.hasNext = function() { return typeof this.context.nextUrl == "string" && this.context.nextUrl.length > 0 }, e.prototype.next = function() { return this.hasNext() ? this.run(this.context.nextUrl) : !1 }, e.prototype.run = function(t) { var n, r, i; if (typeof this.options.clientId != "string" && typeof this.options.accessToken != "string") throw new Error("Missing clientId or accessToken."); if (typeof this.options.accessToken != "string" && typeof this.options.clientId != "string") throw new Error("Missing clientId or accessToken."); return this.options.before != null && typeof this.options.before == "function" && this.options.before.call(this), typeof document != "undefined" && document !== null && (i = document.createElement("script"), i.id = "instafeed-fetcher", i.src = t || this._buildUrl(), n = document.getElementsByTagName("head"), n[0].appendChild(i), r = "instafeedCache" + this.unique, window[r] = new e(this.options, this), window[r].unique = this.unique), !0 }, e.prototype.parse = function(e) {
                var t, n, r, i, s, o, u, a, f, l, c, h, p, d, v, m, g, y, b, w, E, S, x, T, N, C, k, L, A, O, M, _, D;
                if (typeof e != "object") { if (this.options.error != null && typeof this.options.error == "function") return this.options.error.call(this, "Invalid JSON data"), !1; throw new Error("Invalid JSON response") }
                if (e.meta.code !== 200) { if (this.options.error != null && typeof this.options.error == "function") return this.options.error.call(this, e.meta.error_message), !1; throw new Error("Error from Instagram: " + e.meta.error_message) }
                if (e.data.length === 0) { if (this.options.error != null && typeof this.options.error == "function") return this.options.error.call(this, "No images were returned from Instagram"), !1; throw new Error("No images were returned from Instagram") }
                this.options.success != null && typeof this.options.success == "function" && this.options.success.call(this, e), this.context.nextUrl = "", e.pagination != null && (this.context.nextUrl = e.pagination.next_url);
                if (this.options.sortBy !== "none") {
                    this.options.sortBy === "random" ? M = ["", "random"] : M = this.options.sortBy.split("-"), O = M[0] === "least" ? !0 : !1;
                    switch (M[1]) {
                        case "random":
                            e.data.sort(function() { return .5 - Math.random() });
                            break;
                        case "recent":
                            e.data = this._sortBy(e.data, "created_time", O);
                            break;
                        case "liked":
                            e.data = this._sortBy(e.data, "likes.count", O);
                            break;
                        case "commented":
                            e.data = this._sortBy(e.data, "comments.count", O);
                            break;
                        default:
                            throw new Error("Invalid option for sortBy: '" + this.options.sortBy + "'.")
                    }
                }
                if (typeof document != "undefined" && document !== null && this.options.mock === !1) {
                    m = e.data, A = parseInt(this.options.limit, 10), this.options.limit != null && m.length > A && (m = m.slice(0, A)), u = document.createDocumentFragment(), this.options.filter != null && typeof this.options.filter == "function" && (m = this._filter(m, this.options.filter));
                    if (this.options.template != null && typeof this.options.template == "string") {
                        f = "", d = "", w = "", D = document.createElement("div");
                        for (c = 0, N = m.length; c < N; c++) {
                            h = m[c], p = h.images[this.options.resolution];
                            if (typeof p != "object") throw o = "No image found for resolution: " + this.options.resolution + ".", new Error(o);
                            E = p.width, y = p.height, b = "square", E > y && (b = "landscape"), E < y && (b = "portrait"), v = p.url, l = window.location.protocol.indexOf("http") >= 0, l && !this.options.useHttp && (v = v.replace(/https?:\/\//, "//")), d = this._makeTemplate(this.options.template, { model: h, id: h.id, link: h.link, type: h.type, image: v, width: E, height: y, orientation: b, caption: this._getObjectProperty(h, "caption.text"), likes: h.likes.count, comments: h.comments.count, location: this._getObjectProperty(h, "location.name") }), f += d
                        }
                        D.innerHTML = f, i = [], r = 0, n = D.childNodes.length;
                        while (r < n) i.push(D.childNodes[r]), r += 1;
                        for (x = 0, C = i.length; x < C; x++) L = i[x], u.appendChild(L)
                    } else
                        for (T = 0, k = m.length; T < k; T++) {
                            h = m[T], g = document.createElement("img"), p = h.images[this.options.resolution];
                            if (typeof p != "object") throw o = "No image found for resolution: " + this.options.resolution + ".", new Error(o);
                            v = p.url, l = window.location.protocol.indexOf("http") >= 0, l && !this.options.useHttp && (v = v.replace(/https?:\/\//, "//")), g.src = v, this.options.links === !0 ? (t = document.createElement("a"), t.href = h.link, t.appendChild(g), u.appendChild(t)) : u.appendChild(g)
                        }
                    _ = this.options.target, typeof _ == "string" && (_ = document.getElementById(_));
                    if (_ == null) throw o = 'No element with id="' + this.options.target + '" on page.', new Error(o);
                    _.appendChild(u), a = document.getElementsByTagName("head")[0], a.removeChild(document.getElementById("instafeed-fetcher")), S = "instafeedCache" + this.unique, window[S] = void 0;
                    try { delete window[S] } catch (P) { s = P }
                }
                return this.options.after != null && typeof this.options.after == "function" && this.options.after.call(this), !0
            }, e.prototype._buildUrl = function() {
                var e, t, n;
                e = "https://api.instagram.com/v1";
                switch (this.options.get) {
                    case "popular":
                        t = "media/popular";
                        break;
                    case "tagged":
                        if (!this.options.tagName) throw new Error("No tag name specified. Use the 'tagName' option.");
                        t = "tags/" + this.options.tagName + "/media/recent";
                        break;
                    case "location":
                        if (!this.options.locationId) throw new Error("No location specified. Use the 'locationId' option.");
                        t = "locations/" + this.options.locationId + "/media/recent";
                        break;
                    case "user":
                        if (!this.options.userId) throw new Error("No user specified. Use the 'userId' option.");
                        t = "users/" + this.options.userId + "/media/recent";
                        break;
                    default:
                        throw new Error("Invalid option for get: '" + this.options.get + "'.")
                }
                return n = e + "/" + t, this.options.accessToken != null ? n += "?access_token=" + this.options.accessToken : n += "?client_id=" + this.options.clientId, this.options.limit != null && (n += "&count=" + this.options.limit), n += "&callback=instafeedCache" + this.unique + ".parse", n
            }, e.prototype._genKey = function() { var e; return e = function() { return ((1 + Math.random()) * 65536 | 0).toString(16).substring(1) }, "" + e() + e() + e() + e() }, e.prototype._makeTemplate = function(e, t) {
                var n, r, i, s, o;
                r = /(?:\{{2})([\w\[\]\.]+)(?:\}{2})/, n = e;
                while (r.test(n)) s = n.match(r)[1], o = (i = this._getObjectProperty(t, s)) != null ? i : "", n = n.replace(r, function() { return "" + o });
                return n
            }, e.prototype._getObjectProperty = function(e, t) {
                var n, r;
                t = t.replace(/\[(\w+)\]/g, ".$1"), r = t.split(".");
                while (r.length) {
                    n = r.shift();
                    if (!(e != null && n in e)) return null;
                    e = e[n]
                }
                return e
            }, e.prototype._sortBy = function(e, t, n) { var r; return r = function(e, r) { var i, s; return i = this._getObjectProperty(e, t), s = this._getObjectProperty(r, t), n ? i > s ? 1 : -1 : i < s ? 1 : -1 }, e.sort(r.bind(this)), e }, e.prototype._filter = function(e, t) {
                var n, r, i, s, o;
                n = [], r = function(e) { if (t(e)) return n.push(e) };
                for (i = 0, o = e.length; i < o; i++) s = e[i], r(s);
                return n
            }, e
        }(),
        function(e, t) { return typeof define == "function" && define.amd ? define([], t) : typeof module == "object" && module.exports ? module.exports = t() : e.Instafeed = t() }(this, function() { return e })
}).call(this);
jQuery('#background-1').mouseParallax({ moveFactor: 2 });
jQuery('#background-2').mouseParallax({ moveFactor: 10 });
jQuery('#background-3').mouseParallax({ moveFactor: 7 });
jQuery('#background-4').mouseParallax({ moveFactor: 1 });
jQuery('#background-5').mouseParallax({ moveFactor: 3 });
jQuery('#background-6').mouseParallax({ moveFactor: 1 });
jQuery('#background-7').mouseParallax({ moveFactor: 3 });
jQuery('#background-8').mouseParallax({ moveFactor: 10 });
jQuery('#background-9').mouseParallax({ moveFactor: 5 });
jQuery('#background-10').mouseParallax({ moveFactor: 1 });
jQuery('#background-11').mouseParallax({ moveFactor: 1 });
jQuery('#background-12').mouseParallax({ moveFactor: 5 });
jQuery('#background-13').mouseParallax({ moveFactor: 1 });
jQuery('#background-14').mouseParallax({ moveFactor: 4 });
jQuery('#background-15').mouseParallax({ moveFactor: 5 });
jQuery('#background-16').mouseParallax({ moveFactor: 6 });
jQuery('#background-17').mouseParallax({ moveFactor: 5 });
jQuery('#background-18').mouseParallax({ moveFactor: 2 });
jQuery('#background-19').mouseParallax({ moveFactor: 3 });
jQuery('#background-20').mouseParallax({ moveFactor: 5 });
jQuery('#background-21').mouseParallax({ moveFactor: 7 });
jQuery('#background-22').mouseParallax({ moveFactor: 6 });
jQuery('#background-23').mouseParallax({ moveFactor: 2 });
jQuery('#background-24').mouseParallax({ moveFactor: 1 });
jQuery('#background-25').mouseParallax({ moveFactor: 3 });
jQuery('#background-26').mouseParallax({ moveFactor: 4 });
jQuery('#background-27').mouseParallax({ moveFactor: 2 });
jQuery('#background-28').mouseParallax({ moveFactor: 3 });
jQuery('#background-29').mouseParallax({ moveFactor: 4 });
jQuery('#background-30').mouseParallax({ moveFactor: 5 });
jQuery('#background-31').mouseParallax({ moveFactor: 6 });
jQuery('#background-32').mouseParallax({ moveFactor: 4 });
jQuery('#background-33').mouseParallax({ moveFactor: 4 });
jQuery('#background-34').mouseParallax({ moveFactor: 4 });
jQuery('#background-35').mouseParallax({ moveFactor: 4 });
jQuery('#background-36').mouseParallax({ moveFactor: 4 });
/*!
 * jQuery meanMenu v2.0.8
 * @Copyright (C) 2012-2014 Chris Wharton @ MeanThemes (https://github.com/meanthemes/meanMenu)
 *
 */
(function($) {
    "use strict";
    $.fn.meanmenu = function(options) {
        var defaults = { meanMenuTarget: jQuery(this), meanMenuContainer: 'body', meanMenuClose: "X", meanMenuCloseSize: "18px", meanMenuOpen: "<span/><span/><span/>", meanRevealPosition: "right", meanRevealPositionDistance: "0", meanRevealColour: "", meanScreenWidth: "480", meanNavPush: "", meanShowChildren: true, meanExpandableChildren: true, meanExpand: "+", meanContract: "-", meanRemoveAttrs: false, onePage: false, meanDisplay: "block", removeElements: "" };
        options = $.extend(defaults, options);
        var currentWidth = window.innerWidth || document.documentElement.clientWidth;
        return this.each(function() {
            var meanMenu = options.meanMenuTarget;
            var meanContainer = options.meanMenuContainer;
            var meanMenuClose = options.meanMenuClose;
            var meanMenuCloseSize = options.meanMenuCloseSize;
            var meanMenuOpen = options.meanMenuOpen;
            var meanRevealPosition = options.meanRevealPosition;
            var meanRevealPositionDistance = options.meanRevealPositionDistance;
            var meanRevealColour = options.meanRevealColour;
            var meanScreenWidth = options.meanScreenWidth;
            var meanNavPush = options.meanNavPush;
            var meanRevealClass = ".meanmenu-reveal";
            var meanShowChildren = options.meanShowChildren;
            var meanExpandableChildren = options.meanExpandableChildren;
            var meanExpand = options.meanExpand;
            var meanContract = options.meanContract;
            var meanRemoveAttrs = options.meanRemoveAttrs;
            var onePage = options.onePage;
            var meanDisplay = options.meanDisplay;
            var removeElements = options.removeElements;
            var isMobile = false;
            if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i)) || (navigator.userAgent.match(/Blackberry/i)) || (navigator.userAgent.match(/Windows Phone/i))) { isMobile = true; }
            if ((navigator.userAgent.match(/MSIE 8/i)) || (navigator.userAgent.match(/MSIE 7/i))) { jQuery('html').css("overflow-y", "scroll"); }
            var meanRevealPos = "";
            var meanCentered = function() {
                if (meanRevealPosition === "center") {
                    var newWidth = window.innerWidth || document.documentElement.clientWidth;
                    var meanCenter = ((newWidth / 2) - 22) + "px";
                    meanRevealPos = "left:" + meanCenter + ";right:auto;";
                    if (!isMobile) { jQuery('.meanmenu-reveal').css("left", meanCenter); } else { jQuery('.meanmenu-reveal').animate({ left: meanCenter }); }
                }
            };
            var menuOn = false;
            var meanMenuExist = false;
            if (meanRevealPosition === "right") { meanRevealPos = "right:" + meanRevealPositionDistance + ";left:auto;"; }
            if (meanRevealPosition === "left") { meanRevealPos = "left:" + meanRevealPositionDistance + ";right:auto;"; }
            meanCentered();
            var $navreveal = "";
            var meanInner = function() { if (jQuery($navreveal).is(".meanmenu-reveal.meanclose")) { $navreveal.html(meanMenuClose); } else { $navreveal.html(meanMenuOpen); } };
            var meanOriginal = function() {
                jQuery('.mean-bar,.mean-push').remove();
                jQuery(meanContainer).removeClass("mean-container");
                jQuery(meanMenu).css('display', meanDisplay);
                menuOn = false;
                meanMenuExist = false;
                jQuery(removeElements).removeClass('mean-remove');
            };
            var showMeanMenu = function() {
                var meanStyles = "background:" + meanRevealColour + ";color:" + meanRevealColour + ";" + meanRevealPos;
                if (currentWidth <= meanScreenWidth) {
                    jQuery(removeElements).addClass('mean-remove');
                    meanMenuExist = true;
                    jQuery(meanContainer).addClass("mean-container");
                    jQuery('.mean-container').prepend('<div class="mean-bar"><a href="#nav" class="meanmenu-reveal" style="' + meanStyles + '">Show Navigation</a><nav class="mean-nav"></nav></div>');
                    var meanMenuContents = jQuery(meanMenu).html();
                    jQuery('.mean-nav').html(meanMenuContents);
                    if (meanRemoveAttrs) {
                        jQuery('nav.mean-nav ul, nav.mean-nav ul *').each(function() {
                            if (jQuery(this).is('.mean-remove')) { jQuery(this).attr('class', 'mean-remove'); } else { jQuery(this).removeAttr("class"); }
                            jQuery(this).removeAttr("id");
                        });
                    }
                    jQuery(meanMenu).before('<div class="mean-push" />');
                    jQuery('.mean-push').css("margin-top", meanNavPush);
                    jQuery(meanMenu).hide();
                    jQuery(".meanmenu-reveal").show();
                    jQuery(meanRevealClass).html(meanMenuOpen);
                    $navreveal = jQuery(meanRevealClass);
                    jQuery('.mean-nav ul').hide();
                    if (meanShowChildren) {
                        if (meanExpandableChildren) {
                            jQuery('.mean-nav ul ul').each(function() { if (jQuery(this).children().length) { jQuery(this, 'li:first').parent().append('<a class="mean-expand" href="#" style="font-size: ' + meanMenuCloseSize + '">' + meanExpand + '</a>'); } });
                            jQuery('.mean-expand').on("click", function(e) {
                                e.preventDefault();
                                if (jQuery(this).hasClass("mean-clicked")) {
                                    jQuery(this).text(meanExpand);
                                    jQuery(this).prev('ul').slideUp(300, function() {});
                                } else {
                                    jQuery(this).text(meanContract);
                                    jQuery(this).prev('ul').slideDown(300, function() {});
                                }
                                jQuery(this).toggleClass("mean-clicked");
                            });
                        } else { jQuery('.mean-nav ul ul').show(); }
                    } else { jQuery('.mean-nav ul ul').hide(); }
                    jQuery('.mean-nav ul li').last().addClass('mean-last');
                    $navreveal.removeClass("meanclose");
                    jQuery($navreveal).click(function(e) {
                        e.preventDefault();
                        if (menuOn === false) {
                            $navreveal.css("text-align", "center");
                            $navreveal.css("text-indent", "0");
                            $navreveal.css("font-size", meanMenuCloseSize);
                            jQuery('.mean-nav ul:first').slideDown();
                            menuOn = true;
                        } else {
                            jQuery('.mean-nav ul:first').slideUp();
                            menuOn = false;
                        }
                        $navreveal.toggleClass("meanclose");
                        meanInner();
                        jQuery(removeElements).addClass('mean-remove');
                    });
                    if (onePage) {
                        jQuery('.mean-nav ul > li > a:first-child').on("click", function() {
                            jQuery('.mean-nav ul:first').slideUp();
                            menuOn = false;
                            jQuery($navreveal).toggleClass("meanclose").html(meanMenuOpen);
                        });
                    }
                } else { meanOriginal(); }
            };
            if (!isMobile) {
                jQuery(window).resize(function() {
                    currentWidth = window.innerWidth || document.documentElement.clientWidth;
                    if (currentWidth > meanScreenWidth) { meanOriginal(); } else { meanOriginal(); }
                    if (currentWidth <= meanScreenWidth) {
                        showMeanMenu();
                        meanCentered();
                    } else { meanOriginal(); }
                });
            }
            jQuery(window).resize(function() {
                currentWidth = window.innerWidth || document.documentElement.clientWidth;
                if (!isMobile) {
                    meanOriginal();
                    if (currentWidth <= meanScreenWidth) {
                        showMeanMenu();
                        meanCentered();
                    }
                } else { meanCentered(); if (currentWidth <= meanScreenWidth) { if (meanMenuExist === false) { showMeanMenu(); } } else { meanOriginal(); } }
            });
            showMeanMenu();
        });
    };
})(jQuery);
(function() {
    var t = [].indexOf || function(t) { for (var e = 0, n = this.length; e < n; e++) { if (e in this && this[e] === t) return e } return -1 },
        e = [].slice;
    (function(t, e) { if (typeof define === "function" && define.amd) { return define("waypoints", ["jquery"], function(n) { return e(n, t) }) } else { return e(t.jQuery, t) } })(this, function(n, r) {
        var i, o, l, s, f, u, a, c, h, d, p, y, v, w, g, m;
        i = n(r);
        c = t.call(r, "ontouchstart") >= 0;
        s = { horizontal: {}, vertical: {} };
        f = 1;
        a = {};
        u = "waypoints-context-id";
        p = "resize.waypoints";
        y = "scroll.waypoints";
        v = 1;
        w = "waypoints-waypoint-ids";
        g = "waypoint";
        m = "waypoints";
        o = function() {
            function t(t) {
                var e = this;
                this.$element = t;
                this.element = t[0];
                this.didResize = false;
                this.didScroll = false;
                this.id = "context" + f++;
                this.oldScroll = { x: t.scrollLeft(), y: t.scrollTop() };
                this.waypoints = { horizontal: {}, vertical: {} };
                t.data(u, this.id);
                a[this.id] = this;
                t.bind(y, function() {
                    var t;
                    if (!(e.didScroll || c)) {
                        e.didScroll = true;
                        t = function() { e.doScroll(); return e.didScroll = false };
                        return r.setTimeout(t, n[m].settings.scrollThrottle)
                    }
                });
                t.bind(p, function() {
                    var t;
                    if (!e.didResize) {
                        e.didResize = true;
                        t = function() { n[m]("refresh"); return e.didResize = false };
                        return r.setTimeout(t, n[m].settings.resizeThrottle)
                    }
                })
            }
            t.prototype.doScroll = function() {
                var t, e = this;
                t = { horizontal: { newScroll: this.$element.scrollLeft(), oldScroll: this.oldScroll.x, forward: "right", backward: "left" }, vertical: { newScroll: this.$element.scrollTop(), oldScroll: this.oldScroll.y, forward: "down", backward: "up" } };
                if (c && (!t.vertical.oldScroll || !t.vertical.newScroll)) { n[m]("refresh") }
                n.each(t, function(t, r) {
                    var i, o, l;
                    l = [];
                    o = r.newScroll > r.oldScroll;
                    i = o ? r.forward : r.backward;
                    n.each(e.waypoints[t], function(t, e) { var n, i; if (r.oldScroll < (n = e.offset) && n <= r.newScroll) { return l.push(e) } else if (r.newScroll < (i = e.offset) && i <= r.oldScroll) { return l.push(e) } });
                    l.sort(function(t, e) { return t.offset - e.offset });
                    if (!o) { l.reverse() }
                    return n.each(l, function(t, e) { if (e.options.continuous || t === l.length - 1) { return e.trigger([i]) } })
                });
                return this.oldScroll = { x: t.horizontal.newScroll, y: t.vertical.newScroll }
            };
            t.prototype.refresh = function() {
                var t, e, r, i = this;
                r = n.isWindow(this.element);
                e = this.$element.offset();
                this.doScroll();
                t = { horizontal: { contextOffset: r ? 0 : e.left, contextScroll: r ? 0 : this.oldScroll.x, contextDimension: this.$element.width(), oldScroll: this.oldScroll.x, forward: "right", backward: "left", offsetProp: "left" }, vertical: { contextOffset: r ? 0 : e.top, contextScroll: r ? 0 : this.oldScroll.y, contextDimension: r ? n[m]("viewportHeight") : this.$element.height(), oldScroll: this.oldScroll.y, forward: "down", backward: "up", offsetProp: "top" } };
                return n.each(t, function(t, e) {
                    return n.each(i.waypoints[t], function(t, r) {
                        var i, o, l, s, f;
                        i = r.options.offset;
                        l = r.offset;
                        o = n.isWindow(r.element) ? 0 : r.$element.offset()[e.offsetProp];
                        if (n.isFunction(i)) { i = i.apply(r.element) } else if (typeof i === "string") { i = parseFloat(i); if (r.options.offset.indexOf("%") > -1) { i = Math.ceil(e.contextDimension * i / 100) } }
                        r.offset = o - e.contextOffset + e.contextScroll - i;
                        if (r.options.onlyOnScroll && l != null || !r.enabled) { return }
                        if (l !== null && l < (s = e.oldScroll) && s <= r.offset) { return r.trigger([e.backward]) } else if (l !== null && l > (f = e.oldScroll) && f >= r.offset) { return r.trigger([e.forward]) } else if (l === null && e.oldScroll >= r.offset) { return r.trigger([e.forward]) }
                    })
                })
            };
            t.prototype.checkEmpty = function() { if (n.isEmptyObject(this.waypoints.horizontal) && n.isEmptyObject(this.waypoints.vertical)) { this.$element.unbind([p, y].join(" ")); return delete a[this.id] } };
            return t
        }();
        l = function() {
            function t(t, e, r) {
                var i, o;
                r = n.extend({}, n.fn[g].defaults, r);
                if (r.offset === "bottom-in-view") {
                    r.offset = function() {
                        var t;
                        t = n[m]("viewportHeight");
                        if (!n.isWindow(e.element)) { t = e.$element.height() }
                        return t - n(this).outerHeight()
                    }
                }
                this.$element = t;
                this.element = t[0];
                this.axis = r.horizontal ? "horizontal" : "vertical";
                this.callback = r.handler;
                this.context = e;
                this.enabled = r.enabled;
                this.id = "waypoints" + v++;
                this.offset = null;
                this.options = r;
                e.waypoints[this.axis][this.id] = this;
                s[this.axis][this.id] = this;
                i = (o = t.data(w)) != null ? o : [];
                i.push(this.id);
                t.data(w, i)
            }
            t.prototype.trigger = function(t) { if (!this.enabled) { return } if (this.callback != null) { this.callback.apply(this.element, t) } if (this.options.triggerOnce) { return this.destroy() } };
            t.prototype.disable = function() { return this.enabled = false };
            t.prototype.enable = function() { this.context.refresh(); return this.enabled = true };
            t.prototype.destroy = function() {
                delete s[this.axis][this.id];
                delete this.context.waypoints[this.axis][this.id];
                return this.context.checkEmpty()
            };
            t.getWaypointsByElement = function(t) {
                var e, r;
                r = n(t).data(w);
                if (!r) { return [] }
                e = n.extend({}, s.horizontal, s.vertical);
                return n.map(r, function(t) { return e[t] })
            };
            return t
        }();
        d = {
            init: function(t, e) {
                var r;
                if (e == null) { e = {} }
                if ((r = e.handler) == null) { e.handler = t }
                this.each(function() {
                    var t, r, i, s;
                    t = n(this);
                    i = (s = e.context) != null ? s : n.fn[g].defaults.context;
                    if (!n.isWindow(i)) { i = t.closest(i) }
                    i = n(i);
                    r = a[i.data(u)];
                    if (!r) { r = new o(i) }
                    return new l(t, r, e)
                });
                n[m]("refresh");
                return this
            },
            disable: function() { return d._invoke(this, "disable") },
            enable: function() { return d._invoke(this, "enable") },
            destroy: function() { return d._invoke(this, "destroy") },
            prev: function(t, e) { return d._traverse.call(this, t, e, function(t, e, n) { if (e > 0) { return t.push(n[e - 1]) } }) },
            next: function(t, e) { return d._traverse.call(this, t, e, function(t, e, n) { if (e < n.length - 1) { return t.push(n[e + 1]) } }) },
            _traverse: function(t, e, i) {
                var o, l;
                if (t == null) { t = "vertical" }
                if (e == null) { e = r }
                l = h.aggregate(e);
                o = [];
                this.each(function() {
                    var e;
                    e = n.inArray(this, l[t]);
                    return i(o, e, l[t])
                });
                return this.pushStack(o)
            },
            _invoke: function(t, e) {
                t.each(function() {
                    var t;
                    t = l.getWaypointsByElement(this);
                    return n.each(t, function(t, n) { n[e](); return true })
                });
                return this
            }
        };
        n.fn[g] = function() {
            var t, r;
            r = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
            if (d[r]) { return d[r].apply(this, t) } else if (n.isFunction(r)) { return d.init.apply(this, arguments) } else if (n.isPlainObject(r)) { return d.init.apply(this, [null, r]) } else if (!r) { return n.error("jQuery Waypoints needs a callback function or handler option.") } else { return n.error("The " + r + " method does not exist in jQuery Waypoints.") }
        };
        n.fn[g].defaults = { context: r, continuous: true, enabled: true, horizontal: false, offset: 0, triggerOnce: false };
        h = {
            refresh: function() { return n.each(a, function(t, e) { return e.refresh() }) },
            viewportHeight: function() { var t; return (t = r.innerHeight) != null ? t : i.height() },
            aggregate: function(t) {
                var e, r, i;
                e = s;
                if (t) { e = (i = a[n(t).data(u)]) != null ? i.waypoints : void 0 }
                if (!e) { return [] }
                r = { horizontal: [], vertical: [] };
                n.each(r, function(t, i) {
                    n.each(e[t], function(t, e) { return i.push(e) });
                    i.sort(function(t, e) { return t.offset - e.offset });
                    r[t] = n.map(i, function(t) { return t.element });
                    return r[t] = n.unique(r[t])
                });
                return r
            },
            above: function(t) { if (t == null) { t = r } return h._filter(t, "vertical", function(t, e) { return e.offset <= t.oldScroll.y }) },
            below: function(t) { if (t == null) { t = r } return h._filter(t, "vertical", function(t, e) { return e.offset > t.oldScroll.y }) },
            left: function(t) { if (t == null) { t = r } return h._filter(t, "horizontal", function(t, e) { return e.offset <= t.oldScroll.x }) },
            right: function(t) { if (t == null) { t = r } return h._filter(t, "horizontal", function(t, e) { return e.offset > t.oldScroll.x }) },
            enable: function() { return h._invoke("enable") },
            disable: function() { return h._invoke("disable") },
            destroy: function() { return h._invoke("destroy") },
            extendFn: function(t, e) { return d[t] = e },
            _invoke: function(t) {
                var e;
                e = n.extend({}, s.vertical, s.horizontal);
                return n.each(e, function(e, n) { n[t](); return true })
            },
            _filter: function(t, e, r) {
                var i, o;
                i = a[n(t).data(u)];
                if (!i) { return [] }
                o = [];
                n.each(i.waypoints[e], function(t, e) { if (r(i, e)) { return o.push(e) } });
                o.sort(function(t, e) { return t.offset - e.offset });
                return n.map(o, function(t) { return t.element })
            }
        };
        n[m] = function() {
            var t, n;
            n = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
            if (h[n]) { return h[n].apply(null, t) } else { return h.aggregate.call(null, n) }
        };
        n[m].settings = { resizeThrottle: 100, scrollThrottle: 30 };
        return i.load(function() { return n[m]("refresh") })
    })
}).call(this);
/*!
 * jquery.counterup.js 1.0
 *
 * Copyright 2013, Benjamin Intal http://gambit.ph @bfintal
 * Released under the GPL v2 License
 *
 * Date: Nov 26, 2013
 */
(function(e) {
    "use strict";
    e.fn.counterUp = function(t) {
        var n = e.extend({ time: 400, delay: 10 }, t);
        return this.each(function() {
            var t = e(this),
                r = n,
                i = function() {
                    var e = [],
                        n = r.time / r.delay,
                        i = t.text(),
                        s = /[0-9]+,[0-9]+/.test(i);
                    i = i.replace(/,/g, "");
                    var o = /^[0-9]+$/.test(i),
                        u = /^[0-9]+\.[0-9]+$/.test(i),
                        a = u ? (i.split(".")[1] || []).length : 0;
                    for (var f = n; f >= 1; f--) {
                        var l = parseInt(i / n * f);
                        u && (l = parseFloat(i / n * f).toFixed(a));
                        if (s)
                            while (/(\d+)(\d{3})/.test(l.toString())) l = l.toString().replace(/(\d+)(\d{3})/, "$1,$2");
                        e.unshift(l)
                    }
                    t.data("counterup-nums", e);
                    t.text("0");
                    var c = function() {
                        t.text(t.data("counterup-nums").shift());
                        if (t.data("counterup-nums").length) setTimeout(t.data("counterup-func"), r.delay);
                        else {
                            delete t.data("counterup-nums");
                            t.data("counterup-nums", null);
                            t.data("counterup-func", null)
                        }
                    };
                    t.data("counterup-func", c);
                    setTimeout(t.data("counterup-func"), r.delay)
                };
            t.waypoint(i, { offset: "100%", triggerOnce: !0 })
        })
    }
})(jQuery);
(function($) { 'use strict'; var swiper = new Swiper('.swiper-container', { effect: 'coverflow', grabCursor: false, centeredSlides: true, slidesPerView: 'auto', autoplay: true, navigation: { nextEl: '.icon-next', prevEl: '.icon-prev', }, autoplay: { delay: 3000, }, loop: true, coverflowEffect: { rotate: 1, stretch: 600, depth: 180, modifier: 1, slideShadows: true, }, }); })(jQuery);

(function(i) { "use strict"; "function" == typeof define && define.amd ? define(["jquery"], i) : "undefined" != typeof exports ? module.exports = i(require("jquery")) : i(jQuery) })(function(i) {
    "use strict";
    var e = window.Slick || {};
    e = function() {
        function e(e, o) {
            var s, n = this;
            n.defaults = { accessibility: !0, adaptiveHeight: !1, appendArrows: i(e), appendDots: i(e), arrows: !0, asNavFor: null, prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>', nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>', autoplay: !1, autoplaySpeed: 3e3, centerMode: !1, centerPadding: "50px", cssEase: "ease", customPaging: function(e, t) { return i('<button type="button" />').text(t + 1) }, dots: !1, dotsClass: "slick-dots", draggable: !0, easing: "linear", edgeFriction: .35, fade: !1, focusOnSelect: !1, focusOnChange: !1, infinite: !0, initialSlide: 0, lazyLoad: "ondemand", mobileFirst: !1, pauseOnHover: !0, pauseOnFocus: !0, pauseOnDotsHover: !1, respondTo: "window", responsive: null, rows: 1, rtl: !1, slide: "", slidesPerRow: 1, slidesToShow: 1, slidesToScroll: 1, speed: 500, swipe: !0, swipeToSlide: !1, touchMove: !0, touchThreshold: 5, useCSS: !0, useTransform: !0, variableWidth: !1, vertical: !1, verticalSwiping: !1, waitForAnimate: !0, zIndex: 1e3 }, n.initials = { animating: !1, dragging: !1, autoPlayTimer: null, currentDirection: 0, currentLeft: null, currentSlide: 0, direction: 1, $dots: null, listWidth: null, listHeight: null, loadIndex: 0, $nextArrow: null, $prevArrow: null, scrolling: !1, slideCount: null, slideWidth: null, $slideTrack: null, $slides: null, sliding: !1, slideOffset: 0, swipeLeft: null, swiping: !1, $list: null, touchObject: {}, transformsEnabled: !1, unslicked: !1 }, i.extend(n, n.initials), n.activeBreakpoint = null, n.animType = null, n.animProp = null, n.breakpoints = [], n.breakpointSettings = [], n.cssTransitions = !1, n.focussed = !1, n.interrupted = !1, n.hidden = "hidden", n.paused = !0, n.positionProp = null, n.respondTo = null, n.rowCount = 1, n.shouldClick = !0, n.$slider = i(e), n.$slidesCache = null, n.transformType = null, n.transitionType = null, n.visibilityChange = "visibilitychange", n.windowWidth = 0, n.windowTimer = null, s = i(e).data("slick") || {}, n.options = i.extend({}, n.defaults, o, s), n.currentSlide = n.options.initialSlide, n.originalSettings = n.options, "undefined" != typeof document.mozHidden ? (n.hidden = "mozHidden", n.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (n.hidden = "webkitHidden", n.visibilityChange = "webkitvisibilitychange"), n.autoPlay = i.proxy(n.autoPlay, n), n.autoPlayClear = i.proxy(n.autoPlayClear, n), n.autoPlayIterator = i.proxy(n.autoPlayIterator, n), n.changeSlide = i.proxy(n.changeSlide, n), n.clickHandler = i.proxy(n.clickHandler, n), n.selectHandler = i.proxy(n.selectHandler, n), n.setPosition = i.proxy(n.setPosition, n), n.swipeHandler = i.proxy(n.swipeHandler, n), n.dragHandler = i.proxy(n.dragHandler, n), n.keyHandler = i.proxy(n.keyHandler, n), n.instanceUid = t++, n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, n.registerBreakpoints(), n.init(!0)
        }
        var t = 0;
        return e
    }(), e.prototype.activateADA = function() {
        var i = this;
        i.$slideTrack.find(".slick-active").attr({ "aria-hidden": "false" }).find("a, input, button, select").attr({ tabindex: "0" })
    }, e.prototype.addSlide = e.prototype.slickAdd = function(e, t, o) {
        var s = this;
        if ("boolean" == typeof t) o = t, t = null;
        else if (t < 0 || t >= s.slideCount) return !1;
        s.unload(), "number" == typeof t ? 0 === t && 0 === s.$slides.length ? i(e).appendTo(s.$slideTrack) : o ? i(e).insertBefore(s.$slides.eq(t)) : i(e).insertAfter(s.$slides.eq(t)) : o === !0 ? i(e).prependTo(s.$slideTrack) : i(e).appendTo(s.$slideTrack), s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(), s.$slideTrack.append(s.$slides), s.$slides.each(function(e, t) { i(t).attr("data-slick-index", e) }), s.$slidesCache = s.$slides, s.reinit()
    }, e.prototype.animateHeight = function() {
        var i = this;
        if (1 === i.options.slidesToShow && i.options.adaptiveHeight === !0 && i.options.vertical === !1) {
            var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
            i.$list.animate({ height: e }, i.options.speed)
        }
    }, e.prototype.animateSlide = function(e, t) {
        var o = {},
            s = this;
        s.animateHeight(), s.options.rtl === !0 && s.options.vertical === !1 && (e = -e), s.transformsEnabled === !1 ? s.options.vertical === !1 ? s.$slideTrack.animate({ left: e }, s.options.speed, s.options.easing, t) : s.$slideTrack.animate({ top: e }, s.options.speed, s.options.easing, t) : s.cssTransitions === !1 ? (s.options.rtl === !0 && (s.currentLeft = -s.currentLeft), i({ animStart: s.currentLeft }).animate({ animStart: e }, { duration: s.options.speed, easing: s.options.easing, step: function(i) { i = Math.ceil(i), s.options.vertical === !1 ? (o[s.animType] = "translate(" + i + "px, 0px)", s.$slideTrack.css(o)) : (o[s.animType] = "translate(0px," + i + "px)", s.$slideTrack.css(o)) }, complete: function() { t && t.call() } })) : (s.applyTransition(), e = Math.ceil(e), s.options.vertical === !1 ? o[s.animType] = "translate3d(" + e + "px, 0px, 0px)" : o[s.animType] = "translate3d(0px," + e + "px, 0px)", s.$slideTrack.css(o), t && setTimeout(function() { s.disableTransition(), t.call() }, s.options.speed))
    }, e.prototype.getNavTarget = function() {
        var e = this,
            t = e.options.asNavFor;
        return t && null !== t && (t = i(t).not(e.$slider)), t
    }, e.prototype.asNavFor = function(e) {
        var t = this,
            o = t.getNavTarget();
        null !== o && "object" == typeof o && o.each(function() {
            var t = i(this).slick("getSlick");
            t.unslicked || t.slideHandler(e, !0)
        })
    }, e.prototype.applyTransition = function(i) {
        var e = this,
            t = {};
        e.options.fade === !1 ? t[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : t[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, e.options.fade === !1 ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t)
    }, e.prototype.autoPlay = function() {
        var i = this;
        i.autoPlayClear(), i.slideCount > i.options.slidesToShow && (i.autoPlayTimer = setInterval(i.autoPlayIterator, i.options.autoplaySpeed))
    }, e.prototype.autoPlayClear = function() {
        var i = this;
        i.autoPlayTimer && clearInterval(i.autoPlayTimer)
    }, e.prototype.autoPlayIterator = function() {
        var i = this,
            e = i.currentSlide + i.options.slidesToScroll;
        i.paused || i.interrupted || i.focussed || (i.options.infinite === !1 && (1 === i.direction && i.currentSlide + 1 === i.slideCount - 1 ? i.direction = 0 : 0 === i.direction && (e = i.currentSlide - i.options.slidesToScroll, i.currentSlide - 1 === 0 && (i.direction = 1))), i.slideHandler(e))
    }, e.prototype.buildArrows = function() {
        var e = this;
        e.options.arrows === !0 && (e.$prevArrow = i(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = i(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), e.options.infinite !== !0 && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({ "aria-disabled": "true", tabindex: "-1" }))
    }, e.prototype.buildDots = function() {
        var e, t, o = this;
        if (o.options.dots === !0 && o.slideCount > o.options.slidesToShow) {
            for (o.$slider.addClass("slick-dotted"), t = i("<ul />").addClass(o.options.dotsClass), e = 0; e <= o.getDotCount(); e += 1) t.append(i("<li />").append(o.options.customPaging.call(this, o, e)));
            o.$dots = t.appendTo(o.options.appendDots), o.$dots.find("li").first().addClass("slick-active")
        }
    }, e.prototype.buildOut = function() {
        var e = this;
        e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function(e, t) { i(t).attr("data-slick-index", e).data("originalStyling", i(t).attr("style") || "") }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? i('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), e.options.centerMode !== !0 && e.options.swipeToSlide !== !0 || (e.options.slidesToScroll = 1), i("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.options.draggable === !0 && e.$list.addClass("draggable")
    }, e.prototype.buildRows = function() {
        var i, e, t, o, s, n, r, l = this;
        if (o = document.createDocumentFragment(), n = l.$slider.children(), l.options.rows > 0) {
            for (r = l.options.slidesPerRow * l.options.rows, s = Math.ceil(n.length / r), i = 0; i < s; i++) {
                var d = document.createElement("div");
                for (e = 0; e < l.options.rows; e++) {
                    var a = document.createElement("div");
                    for (t = 0; t < l.options.slidesPerRow; t++) {
                        var c = i * r + (e * l.options.slidesPerRow + t);
                        n.get(c) && a.appendChild(n.get(c))
                    }
                    d.appendChild(a)
                }
                o.appendChild(d)
            }
            l.$slider.empty().append(o), l.$slider.children().children().children().css({ width: 100 / l.options.slidesPerRow + "%", display: "inline-block" })
        }
    }, e.prototype.checkResponsive = function(e, t) {
        var o, s, n, r = this,
            l = !1,
            d = r.$slider.width(),
            a = window.innerWidth || i(window).width();
        if ("window" === r.respondTo ? n = a : "slider" === r.respondTo ? n = d : "min" === r.respondTo && (n = Math.min(a, d)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
            s = null;
            for (o in r.breakpoints) r.breakpoints.hasOwnProperty(o) && (r.originalSettings.mobileFirst === !1 ? n < r.breakpoints[o] && (s = r.breakpoints[o]) : n > r.breakpoints[o] && (s = r.breakpoints[o]));
            null !== s ? null !== r.activeBreakpoint ? (s !== r.activeBreakpoint || t) && (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), e === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), e === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, e === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(e), l = s), e || l === !1 || r.$slider.trigger("breakpoint", [r, l])
        }
    }, e.prototype.changeSlide = function(e, t) {
        var o, s, n, r = this,
            l = i(e.currentTarget);
        switch (l.is("a") && e.preventDefault(), l.is("li") || (l = l.closest("li")), n = r.slideCount % r.options.slidesToScroll !== 0, o = n ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, e.data.message) {
            case "previous":
                s = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - s, !1, t);
                break;
            case "next":
                s = 0 === o ? r.options.slidesToScroll : o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + s, !1, t);
                break;
            case "index":
                var d = 0 === e.data.index ? 0 : e.data.index || l.index() * r.options.slidesToScroll;
                r.slideHandler(r.checkNavigable(d), !1, t), l.children().trigger("focus");
                break;
            default:
                return
        }
    }, e.prototype.checkNavigable = function(i) {
        var e, t, o = this;
        if (e = o.getNavigableIndexes(), t = 0, i > e[e.length - 1]) i = e[e.length - 1];
        else
            for (var s in e) {
                if (i < e[s]) { i = t; break }
                t = e[s]
            }
        return i
    }, e.prototype.cleanUpEvents = function() {
        var e = this;
        e.options.dots && null !== e.$dots && (i("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", i.proxy(e.interrupt, e, !0)).off("mouseleave.slick", i.proxy(e.interrupt, e, !1)), e.options.accessibility === !0 && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), e.options.accessibility === !0 && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), i(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), e.options.accessibility === !0 && e.$list.off("keydown.slick", e.keyHandler), e.options.focusOnSelect === !0 && i(e.$slideTrack).children().off("click.slick", e.selectHandler), i(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), i(window).off("resize.slick.slick-" + e.instanceUid, e.resize), i("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), i(window).off("load.slick.slick-" + e.instanceUid, e.setPosition)
    }, e.prototype.cleanUpSlideEvents = function() {
        var e = this;
        e.$list.off("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", i.proxy(e.interrupt, e, !1))
    }, e.prototype.cleanUpRows = function() {
        var i, e = this;
        e.options.rows > 0 && (i = e.$slides.children().children(), i.removeAttr("style"), e.$slider.empty().append(i))
    }, e.prototype.clickHandler = function(i) {
        var e = this;
        e.shouldClick === !1 && (i.stopImmediatePropagation(), i.stopPropagation(), i.preventDefault())
    }, e.prototype.destroy = function(e) {
        var t = this;
        t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), i(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() { i(this).attr("style", i(this).data("originalStyling")) }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
    }, e.prototype.disableTransition = function(i) {
        var e = this,
            t = {};
        t[e.transitionType] = "", e.options.fade === !1 ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t)
    }, e.prototype.fadeSlide = function(i, e) {
        var t = this;
        t.cssTransitions === !1 ? (t.$slides.eq(i).css({ zIndex: t.options.zIndex }), t.$slides.eq(i).animate({ opacity: 1 }, t.options.speed, t.options.easing, e)) : (t.applyTransition(i), t.$slides.eq(i).css({ opacity: 1, zIndex: t.options.zIndex }), e && setTimeout(function() { t.disableTransition(i), e.call() }, t.options.speed))
    }, e.prototype.fadeSlideOut = function(i) {
        var e = this;
        e.cssTransitions === !1 ? e.$slides.eq(i).animate({ opacity: 0, zIndex: e.options.zIndex - 2 }, e.options.speed, e.options.easing) : (e.applyTransition(i), e.$slides.eq(i).css({ opacity: 0, zIndex: e.options.zIndex - 2 }))
    }, e.prototype.filterSlides = e.prototype.slickFilter = function(i) {
        var e = this;
        null !== i && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(i).appendTo(e.$slideTrack), e.reinit())
    }, e.prototype.focusHandler = function() {
        var e = this;
        e.$slider.off("focus.slick blur.slick").on("focus.slick", "*", function(t) {
            var o = i(this);
            setTimeout(function() { e.options.pauseOnFocus && o.is(":focus") && (e.focussed = !0, e.autoPlay()) }, 0)
        }).on("blur.slick", "*", function(t) {
            i(this);
            e.options.pauseOnFocus && (e.focussed = !1, e.autoPlay())
        })
    }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function() { var i = this; return i.currentSlide }, e.prototype.getDotCount = function() {
        var i = this,
            e = 0,
            t = 0,
            o = 0;
        if (i.options.infinite === !0)
            if (i.slideCount <= i.options.slidesToShow) ++o;
            else
                for (; e < i.slideCount;) ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
        else if (i.options.centerMode === !0) o = i.slideCount;
        else if (i.options.asNavFor)
            for (; e < i.slideCount;) ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
        else o = 1 + Math.ceil((i.slideCount - i.options.slidesToShow) / i.options.slidesToScroll);
        return o - 1
    }, e.prototype.getLeft = function(i) {
        var e, t, o, s, n = this,
            r = 0;
        return n.slideOffset = 0, t = n.$slides.first().outerHeight(!0), n.options.infinite === !0 ? (n.slideCount > n.options.slidesToShow && (n.slideOffset = n.slideWidth * n.options.slidesToShow * -1, s = -1, n.options.vertical === !0 && n.options.centerMode === !0 && (2 === n.options.slidesToShow ? s = -1.5 : 1 === n.options.slidesToShow && (s = -2)), r = t * n.options.slidesToShow * s), n.slideCount % n.options.slidesToScroll !== 0 && i + n.options.slidesToScroll > n.slideCount && n.slideCount > n.options.slidesToShow && (i > n.slideCount ? (n.slideOffset = (n.options.slidesToShow - (i - n.slideCount)) * n.slideWidth * -1, r = (n.options.slidesToShow - (i - n.slideCount)) * t * -1) : (n.slideOffset = n.slideCount % n.options.slidesToScroll * n.slideWidth * -1, r = n.slideCount % n.options.slidesToScroll * t * -1))) : i + n.options.slidesToShow > n.slideCount && (n.slideOffset = (i + n.options.slidesToShow - n.slideCount) * n.slideWidth, r = (i + n.options.slidesToShow - n.slideCount) * t), n.slideCount <= n.options.slidesToShow && (n.slideOffset = 0, r = 0), n.options.centerMode === !0 && n.slideCount <= n.options.slidesToShow ? n.slideOffset = n.slideWidth * Math.floor(n.options.slidesToShow) / 2 - n.slideWidth * n.slideCount / 2 : n.options.centerMode === !0 && n.options.infinite === !0 ? n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2) - n.slideWidth : n.options.centerMode === !0 && (n.slideOffset = 0, n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2)), e = n.options.vertical === !1 ? i * n.slideWidth * -1 + n.slideOffset : i * t * -1 + r, n.options.variableWidth === !0 && (o = n.slideCount <= n.options.slidesToShow || n.options.infinite === !1 ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow), e = n.options.rtl === !0 ? o[0] ? (n.$slideTrack.width() - o[0].offsetLeft - o.width()) * -1 : 0 : o[0] ? o[0].offsetLeft * -1 : 0, n.options.centerMode === !0 && (o = n.slideCount <= n.options.slidesToShow || n.options.infinite === !1 ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow + 1), e = n.options.rtl === !0 ? o[0] ? (n.$slideTrack.width() - o[0].offsetLeft - o.width()) * -1 : 0 : o[0] ? o[0].offsetLeft * -1 : 0, e += (n.$list.width() - o.outerWidth()) / 2)), e
    }, e.prototype.getOption = e.prototype.slickGetOption = function(i) { var e = this; return e.options[i] }, e.prototype.getNavigableIndexes = function() {
        var i, e = this,
            t = 0,
            o = 0,
            s = [];
        for (e.options.infinite === !1 ? i = e.slideCount : (t = e.options.slidesToScroll * -1, o = e.options.slidesToScroll * -1, i = 2 * e.slideCount); t < i;) s.push(t), t = o + e.options.slidesToScroll, o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        return s
    }, e.prototype.getSlick = function() { return this }, e.prototype.getSlideCount = function() { var e, t, o, s, n = this; return s = n.options.centerMode === !0 ? Math.floor(n.$list.width() / 2) : 0, o = n.swipeLeft * -1 + s, n.options.swipeToSlide === !0 ? (n.$slideTrack.find(".slick-slide").each(function(e, s) { var r, l, d; if (r = i(s).outerWidth(), l = s.offsetLeft, n.options.centerMode !== !0 && (l += r / 2), d = l + r, o < d) return t = s, !1 }), e = Math.abs(i(t).attr("data-slick-index") - n.currentSlide) || 1) : n.options.slidesToScroll }, e.prototype.goTo = e.prototype.slickGoTo = function(i, e) {
        var t = this;
        t.changeSlide({ data: { message: "index", index: parseInt(i) } }, e)
    }, e.prototype.init = function(e) {
        var t = this;
        i(t.$slider).hasClass("slick-initialized") || (i(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), t.options.accessibility === !0 && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay())
    }, e.prototype.initADA = function() {
        var e = this,
            t = Math.ceil(e.slideCount / e.options.slidesToShow),
            o = e.getNavigableIndexes().filter(function(i) { return i >= 0 && i < e.slideCount });
        e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({ "aria-hidden": "true", tabindex: "-1" }).find("a, input, button, select").attr({ tabindex: "-1" }), null !== e.$dots && (e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(t) {
            var s = o.indexOf(t);
            if (i(this).attr({ role: "tabpanel", id: "slick-slide" + e.instanceUid + t, tabindex: -1 }), s !== -1) {
                var n = "slick-slide-control" + e.instanceUid + s;
                i("#" + n).length && i(this).attr({ "aria-describedby": n })
            }
        }), e.$dots.attr("role", "tablist").find("li").each(function(s) {
            var n = o[s];
            i(this).attr({ role: "presentation" }), i(this).find("button").first().attr({ role: "tab", id: "slick-slide-control" + e.instanceUid + s, "aria-controls": "slick-slide" + e.instanceUid + n, "aria-label": s + 1 + " of " + t, "aria-selected": null, tabindex: "-1" })
        }).eq(e.currentSlide).find("button").attr({ "aria-selected": "true", tabindex: "0" }).end());
        for (var s = e.currentSlide, n = s + e.options.slidesToShow; s < n; s++) e.options.focusOnChange ? e.$slides.eq(s).attr({ tabindex: "0" }) : e.$slides.eq(s).removeAttr("tabindex");
        e.activateADA()
    }, e.prototype.initArrowEvents = function() {
        var i = this;
        i.options.arrows === !0 && i.slideCount > i.options.slidesToShow && (i.$prevArrow.off("click.slick").on("click.slick", { message: "previous" }, i.changeSlide), i.$nextArrow.off("click.slick").on("click.slick", { message: "next" }, i.changeSlide), i.options.accessibility === !0 && (i.$prevArrow.on("keydown.slick", i.keyHandler), i.$nextArrow.on("keydown.slick", i.keyHandler)))
    }, e.prototype.initDotEvents = function() {
        var e = this;
        e.options.dots === !0 && e.slideCount > e.options.slidesToShow && (i("li", e.$dots).on("click.slick", { message: "index" }, e.changeSlide), e.options.accessibility === !0 && e.$dots.on("keydown.slick", e.keyHandler)), e.options.dots === !0 && e.options.pauseOnDotsHover === !0 && e.slideCount > e.options.slidesToShow && i("li", e.$dots).on("mouseenter.slick", i.proxy(e.interrupt, e, !0)).on("mouseleave.slick", i.proxy(e.interrupt, e, !1))
    }, e.prototype.initSlideEvents = function() {
        var e = this;
        e.options.pauseOnHover && (e.$list.on("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", i.proxy(e.interrupt, e, !1)))
    }, e.prototype.initializeEvents = function() {
        var e = this;
        e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", { action: "start" }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", { action: "move" }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", { action: "end" }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", { action: "end" }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), i(document).on(e.visibilityChange, i.proxy(e.visibility, e)), e.options.accessibility === !0 && e.$list.on("keydown.slick", e.keyHandler), e.options.focusOnSelect === !0 && i(e.$slideTrack).children().on("click.slick", e.selectHandler), i(window).on("orientationchange.slick.slick-" + e.instanceUid, i.proxy(e.orientationChange, e)), i(window).on("resize.slick.slick-" + e.instanceUid, i.proxy(e.resize, e)), i("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), i(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), i(e.setPosition)
    }, e.prototype.initUI = function() {
        var i = this;
        i.options.arrows === !0 && i.slideCount > i.options.slidesToShow && (i.$prevArrow.show(), i.$nextArrow.show()), i.options.dots === !0 && i.slideCount > i.options.slidesToShow && i.$dots.show()
    }, e.prototype.keyHandler = function(i) {
        var e = this;
        i.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === i.keyCode && e.options.accessibility === !0 ? e.changeSlide({ data: { message: e.options.rtl === !0 ? "next" : "previous" } }) : 39 === i.keyCode && e.options.accessibility === !0 && e.changeSlide({ data: { message: e.options.rtl === !0 ? "previous" : "next" } }))
    }, e.prototype.lazyLoad = function() {
        function e(e) {
            i("img[data-lazy]", e).each(function() {
                var e = i(this),
                    t = i(this).attr("data-lazy"),
                    o = i(this).attr("data-srcset"),
                    s = i(this).attr("data-sizes") || r.$slider.attr("data-sizes"),
                    n = document.createElement("img");
                n.onload = function() { e.animate({ opacity: 0 }, 100, function() { o && (e.attr("srcset", o), s && e.attr("sizes", s)), e.attr("src", t).animate({ opacity: 1 }, 200, function() { e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading") }), r.$slider.trigger("lazyLoaded", [r, e, t]) }) }, n.onerror = function() { e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), r.$slider.trigger("lazyLoadError", [r, e, t]) }, n.src = t
            })
        }
        var t, o, s, n, r = this;
        if (r.options.centerMode === !0 ? r.options.infinite === !0 ? (s = r.currentSlide + (r.options.slidesToShow / 2 + 1), n = s + r.options.slidesToShow + 2) : (s = Math.max(0, r.currentSlide - (r.options.slidesToShow / 2 + 1)), n = 2 + (r.options.slidesToShow / 2 + 1) + r.currentSlide) : (s = r.options.infinite ? r.options.slidesToShow + r.currentSlide : r.currentSlide, n = Math.ceil(s + r.options.slidesToShow), r.options.fade === !0 && (s > 0 && s--, n <= r.slideCount && n++)), t = r.$slider.find(".slick-slide").slice(s, n), "anticipated" === r.options.lazyLoad)
            for (var l = s - 1, d = n, a = r.$slider.find(".slick-slide"), c = 0; c < r.options.slidesToScroll; c++) l < 0 && (l = r.slideCount - 1), t = t.add(a.eq(l)), t = t.add(a.eq(d)), l--, d++;
        e(t), r.slideCount <= r.options.slidesToShow ? (o = r.$slider.find(".slick-slide"), e(o)) : r.currentSlide >= r.slideCount - r.options.slidesToShow ? (o = r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow), e(o)) : 0 === r.currentSlide && (o = r.$slider.find(".slick-cloned").slice(r.options.slidesToShow * -1), e(o))
    }, e.prototype.loadSlider = function() {
        var i = this;
        i.setPosition(), i.$slideTrack.css({ opacity: 1 }), i.$slider.removeClass("slick-loading"), i.initUI(), "progressive" === i.options.lazyLoad && i.progressiveLazyLoad()
    }, e.prototype.next = e.prototype.slickNext = function() {
        var i = this;
        i.changeSlide({ data: { message: "next" } })
    }, e.prototype.orientationChange = function() {
        var i = this;
        i.checkResponsive(), i.setPosition()
    }, e.prototype.pause = e.prototype.slickPause = function() {
        var i = this;
        i.autoPlayClear(), i.paused = !0
    }, e.prototype.play = e.prototype.slickPlay = function() {
        var i = this;
        i.autoPlay(), i.options.autoplay = !0, i.paused = !1, i.focussed = !1, i.interrupted = !1
    }, e.prototype.postSlide = function(e) {
        var t = this;
        if (!t.unslicked && (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), t.options.accessibility === !0 && (t.initADA(), t.options.focusOnChange))) {
            var o = i(t.$slides.get(t.currentSlide));
            o.attr("tabindex", 0).focus()
        }
    }, e.prototype.prev = e.prototype.slickPrev = function() {
        var i = this;
        i.changeSlide({ data: { message: "previous" } })
    }, e.prototype.preventDefault = function(i) { i.preventDefault() }, e.prototype.progressiveLazyLoad = function(e) {
        e = e || 1;
        var t, o, s, n, r, l = this,
            d = i("img[data-lazy]", l.$slider);
        d.length ? (t = d.first(), o = t.attr("data-lazy"), s = t.attr("data-srcset"), n = t.attr("data-sizes") || l.$slider.attr("data-sizes"), r = document.createElement("img"), r.onload = function() { s && (t.attr("srcset", s), n && t.attr("sizes", n)), t.attr("src", o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), l.options.adaptiveHeight === !0 && l.setPosition(), l.$slider.trigger("lazyLoaded", [l, t, o]), l.progressiveLazyLoad() }, r.onerror = function() { e < 3 ? setTimeout(function() { l.progressiveLazyLoad(e + 1) }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), l.$slider.trigger("lazyLoadError", [l, t, o]), l.progressiveLazyLoad()) }, r.src = o) : l.$slider.trigger("allImagesLoaded", [l])
    }, e.prototype.refresh = function(e) {
        var t, o, s = this;
        o = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > o && (s.currentSlide = o), s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), t = s.currentSlide, s.destroy(!0), i.extend(s, s.initials, { currentSlide: t }), s.init(), e || s.changeSlide({ data: { message: "index", index: t } }, !1)
    }, e.prototype.registerBreakpoints = function() {
        var e, t, o, s = this,
            n = s.options.responsive || null;
        if ("array" === i.type(n) && n.length) {
            s.respondTo = s.options.respondTo || "window";
            for (e in n)
                if (o = s.breakpoints.length - 1, n.hasOwnProperty(e)) {
                    for (t = n[e].breakpoint; o >= 0;) s.breakpoints[o] && s.breakpoints[o] === t && s.breakpoints.splice(o, 1), o--;
                    s.breakpoints.push(t), s.breakpointSettings[t] = n[e].settings
                }
            s.breakpoints.sort(function(i, e) { return s.options.mobileFirst ? i - e : e - i })
        }
    }, e.prototype.reinit = function() {
        var e = this;
        e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), e.options.focusOnSelect === !0 && i(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
    }, e.prototype.resize = function() {
        var e = this;
        i(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function() { e.windowWidth = i(window).width(), e.checkResponsive(), e.unslicked || e.setPosition() }, 50))
    }, e.prototype.removeSlide = e.prototype.slickRemove = function(i, e, t) { var o = this; return "boolean" == typeof i ? (e = i, i = e === !0 ? 0 : o.slideCount - 1) : i = e === !0 ? --i : i, !(o.slideCount < 1 || i < 0 || i > o.slideCount - 1) && (o.unload(), t === !0 ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(i).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, void o.reinit()) }, e.prototype.setCSS = function(i) {
        var e, t, o = this,
            s = {};
        o.options.rtl === !0 && (i = -i), e = "left" == o.positionProp ? Math.ceil(i) + "px" : "0px", t = "top" == o.positionProp ? Math.ceil(i) + "px" : "0px", s[o.positionProp] = i, o.transformsEnabled === !1 ? o.$slideTrack.css(s) : (s = {}, o.cssTransitions === !1 ? (s[o.animType] = "translate(" + e + ", " + t + ")", o.$slideTrack.css(s)) : (s[o.animType] = "translate3d(" + e + ", " + t + ", 0px)", o.$slideTrack.css(s)))
    }, e.prototype.setDimensions = function() {
        var i = this;
        i.options.vertical === !1 ? i.options.centerMode === !0 && i.$list.css({ padding: "0px " + i.options.centerPadding }) : (i.$list.height(i.$slides.first().outerHeight(!0) * i.options.slidesToShow), i.options.centerMode === !0 && i.$list.css({ padding: i.options.centerPadding + " 0px" })), i.listWidth = i.$list.width(), i.listHeight = i.$list.height(), i.options.vertical === !1 && i.options.variableWidth === !1 ? (i.slideWidth = Math.ceil(i.listWidth / i.options.slidesToShow), i.$slideTrack.width(Math.ceil(i.slideWidth * i.$slideTrack.children(".slick-slide").length))) : i.options.variableWidth === !0 ? i.$slideTrack.width(5e3 * i.slideCount) : (i.slideWidth = Math.ceil(i.listWidth), i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0) * i.$slideTrack.children(".slick-slide").length)));
        var e = i.$slides.first().outerWidth(!0) - i.$slides.first().width();
        i.options.variableWidth === !1 && i.$slideTrack.children(".slick-slide").width(i.slideWidth - e)
    }, e.prototype.setFade = function() {
        var e, t = this;
        t.$slides.each(function(o, s) { e = t.slideWidth * o * -1, t.options.rtl === !0 ? i(s).css({ position: "relative", right: e, top: 0, zIndex: t.options.zIndex - 2, opacity: 0 }) : i(s).css({ position: "relative", left: e, top: 0, zIndex: t.options.zIndex - 2, opacity: 0 }) }), t.$slides.eq(t.currentSlide).css({ zIndex: t.options.zIndex - 1, opacity: 1 })
    }, e.prototype.setHeight = function() {
        var i = this;
        if (1 === i.options.slidesToShow && i.options.adaptiveHeight === !0 && i.options.vertical === !1) {
            var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
            i.$list.css("height", e)
        }
    }, e.prototype.setOption = e.prototype.slickSetOption = function() {
        var e, t, o, s, n, r = this,
            l = !1;
        if ("object" === i.type(arguments[0]) ? (o = arguments[0], l = arguments[1], n = "multiple") : "string" === i.type(arguments[0]) && (o = arguments[0], s = arguments[1], l = arguments[2], "responsive" === arguments[0] && "array" === i.type(arguments[1]) ? n = "responsive" : "undefined" != typeof arguments[1] && (n = "single")), "single" === n) r.options[o] = s;
        else if ("multiple" === n) i.each(o, function(i, e) { r.options[i] = e });
        else if ("responsive" === n)
            for (t in s)
                if ("array" !== i.type(r.options.responsive)) r.options.responsive = [s[t]];
                else {
                    for (e = r.options.responsive.length - 1; e >= 0;) r.options.responsive[e].breakpoint === s[t].breakpoint && r.options.responsive.splice(e, 1), e--;
                    r.options.responsive.push(s[t])
                }
        l && (r.unload(), r.reinit())
    }, e.prototype.setPosition = function() {
        var i = this;
        i.setDimensions(), i.setHeight(), i.options.fade === !1 ? i.setCSS(i.getLeft(i.currentSlide)) : i.setFade(), i.$slider.trigger("setPosition", [i])
    }, e.prototype.setProps = function() {
        var i = this,
            e = document.body.style;
        i.positionProp = i.options.vertical === !0 ? "top" : "left",
            "top" === i.positionProp ? i.$slider.addClass("slick-vertical") : i.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || i.options.useCSS === !0 && (i.cssTransitions = !0), i.options.fade && ("number" == typeof i.options.zIndex ? i.options.zIndex < 3 && (i.options.zIndex = 3) : i.options.zIndex = i.defaults.zIndex), void 0 !== e.OTransform && (i.animType = "OTransform", i.transformType = "-o-transform", i.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.MozTransform && (i.animType = "MozTransform", i.transformType = "-moz-transform", i.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (i.animType = !1)), void 0 !== e.webkitTransform && (i.animType = "webkitTransform", i.transformType = "-webkit-transform", i.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.msTransform && (i.animType = "msTransform", i.transformType = "-ms-transform", i.transitionType = "msTransition", void 0 === e.msTransform && (i.animType = !1)), void 0 !== e.transform && i.animType !== !1 && (i.animType = "transform", i.transformType = "transform", i.transitionType = "transition"), i.transformsEnabled = i.options.useTransform && null !== i.animType && i.animType !== !1
    }, e.prototype.setSlideClasses = function(i) {
        var e, t, o, s, n = this;
        if (t = n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), n.$slides.eq(i).addClass("slick-current"), n.options.centerMode === !0) {
            var r = n.options.slidesToShow % 2 === 0 ? 1 : 0;
            e = Math.floor(n.options.slidesToShow / 2), n.options.infinite === !0 && (i >= e && i <= n.slideCount - 1 - e ? n.$slides.slice(i - e + r, i + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = n.options.slidesToShow + i, t.slice(o - e + 1 + r, o + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === i ? t.eq(t.length - 1 - n.options.slidesToShow).addClass("slick-center") : i === n.slideCount - 1 && t.eq(n.options.slidesToShow).addClass("slick-center")), n.$slides.eq(i).addClass("slick-center")
        } else i >= 0 && i <= n.slideCount - n.options.slidesToShow ? n.$slides.slice(i, i + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : t.length <= n.options.slidesToShow ? t.addClass("slick-active").attr("aria-hidden", "false") : (s = n.slideCount % n.options.slidesToShow, o = n.options.infinite === !0 ? n.options.slidesToShow + i : i, n.options.slidesToShow == n.options.slidesToScroll && n.slideCount - i < n.options.slidesToShow ? t.slice(o - (n.options.slidesToShow - s), o + s).addClass("slick-active").attr("aria-hidden", "false") : t.slice(o, o + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
        "ondemand" !== n.options.lazyLoad && "anticipated" !== n.options.lazyLoad || n.lazyLoad()
    }, e.prototype.setupInfinite = function() {
        var e, t, o, s = this;
        if (s.options.fade === !0 && (s.options.centerMode = !1), s.options.infinite === !0 && s.options.fade === !1 && (t = null, s.slideCount > s.options.slidesToShow)) {
            for (o = s.options.centerMode === !0 ? s.options.slidesToShow + 1 : s.options.slidesToShow, e = s.slideCount; e > s.slideCount - o; e -= 1) t = e - 1, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");
            for (e = 0; e < o + s.slideCount; e += 1) t = e, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");
            s.$slideTrack.find(".slick-cloned").find("[id]").each(function() { i(this).attr("id", "") })
        }
    }, e.prototype.interrupt = function(i) {
        var e = this;
        i || e.autoPlay(), e.interrupted = i
    }, e.prototype.selectHandler = function(e) {
        var t = this,
            o = i(e.target).is(".slick-slide") ? i(e.target) : i(e.target).parents(".slick-slide"),
            s = parseInt(o.attr("data-slick-index"));
        return s || (s = 0), t.slideCount <= t.options.slidesToShow ? void t.slideHandler(s, !1, !0) : void t.slideHandler(s)
    }, e.prototype.slideHandler = function(i, e, t) {
        var o, s, n, r, l, d = null,
            a = this;
        if (e = e || !1, !(a.animating === !0 && a.options.waitForAnimate === !0 || a.options.fade === !0 && a.currentSlide === i)) return e === !1 && a.asNavFor(i), o = i, d = a.getLeft(o), r = a.getLeft(a.currentSlide), a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft, a.options.infinite === !1 && a.options.centerMode === !1 && (i < 0 || i > a.getDotCount() * a.options.slidesToScroll) ? void(a.options.fade === !1 && (o = a.currentSlide, t !== !0 && a.slideCount > a.options.slidesToShow ? a.animateSlide(r, function() { a.postSlide(o) }) : a.postSlide(o))) : a.options.infinite === !1 && a.options.centerMode === !0 && (i < 0 || i > a.slideCount - a.options.slidesToScroll) ? void(a.options.fade === !1 && (o = a.currentSlide, t !== !0 && a.slideCount > a.options.slidesToShow ? a.animateSlide(r, function() { a.postSlide(o) }) : a.postSlide(o))) : (a.options.autoplay && clearInterval(a.autoPlayTimer), s = o < 0 ? a.slideCount % a.options.slidesToScroll !== 0 ? a.slideCount - a.slideCount % a.options.slidesToScroll : a.slideCount + o : o >= a.slideCount ? a.slideCount % a.options.slidesToScroll !== 0 ? 0 : o - a.slideCount : o, a.animating = !0, a.$slider.trigger("beforeChange", [a, a.currentSlide, s]), n = a.currentSlide, a.currentSlide = s, a.setSlideClasses(a.currentSlide), a.options.asNavFor && (l = a.getNavTarget(), l = l.slick("getSlick"), l.slideCount <= l.options.slidesToShow && l.setSlideClasses(a.currentSlide)), a.updateDots(), a.updateArrows(), a.options.fade === !0 ? (t !== !0 ? (a.fadeSlideOut(n), a.fadeSlide(s, function() { a.postSlide(s) })) : a.postSlide(s), void a.animateHeight()) : void(t !== !0 && a.slideCount > a.options.slidesToShow ? a.animateSlide(d, function() { a.postSlide(s) }) : a.postSlide(s)))
    }, e.prototype.startLoad = function() {
        var i = this;
        i.options.arrows === !0 && i.slideCount > i.options.slidesToShow && (i.$prevArrow.hide(), i.$nextArrow.hide()), i.options.dots === !0 && i.slideCount > i.options.slidesToShow && i.$dots.hide(), i.$slider.addClass("slick-loading")
    }, e.prototype.swipeDirection = function() { var i, e, t, o, s = this; return i = s.touchObject.startX - s.touchObject.curX, e = s.touchObject.startY - s.touchObject.curY, t = Math.atan2(e, i), o = Math.round(180 * t / Math.PI), o < 0 && (o = 360 - Math.abs(o)), o <= 45 && o >= 0 ? s.options.rtl === !1 ? "left" : "right" : o <= 360 && o >= 315 ? s.options.rtl === !1 ? "left" : "right" : o >= 135 && o <= 225 ? s.options.rtl === !1 ? "right" : "left" : s.options.verticalSwiping === !0 ? o >= 35 && o <= 135 ? "down" : "up" : "vertical" }, e.prototype.swipeEnd = function(i) {
        var e, t, o = this;
        if (o.dragging = !1, o.swiping = !1, o.scrolling) return o.scrolling = !1, !1;
        if (o.interrupted = !1, o.shouldClick = !(o.touchObject.swipeLength > 10), void 0 === o.touchObject.curX) return !1;
        if (o.touchObject.edgeHit === !0 && o.$slider.trigger("edge", [o, o.swipeDirection()]), o.touchObject.swipeLength >= o.touchObject.minSwipe) {
            switch (t = o.swipeDirection()) {
                case "left":
                case "down":
                    e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), o.currentDirection = 0;
                    break;
                case "right":
                case "up":
                    e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), o.currentDirection = 1
            }
            "vertical" != t && (o.slideHandler(e), o.touchObject = {}, o.$slider.trigger("swipe", [o, t]))
        } else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), o.touchObject = {})
    }, e.prototype.swipeHandler = function(i) {
        var e = this;
        if (!(e.options.swipe === !1 || "ontouchend" in document && e.options.swipe === !1 || e.options.draggable === !1 && i.type.indexOf("mouse") !== -1)) switch (e.touchObject.fingerCount = i.originalEvent && void 0 !== i.originalEvent.touches ? i.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, e.options.verticalSwiping === !0 && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), i.data.action) {
            case "start":
                e.swipeStart(i);
                break;
            case "move":
                e.swipeMove(i);
                break;
            case "end":
                e.swipeEnd(i)
        }
    }, e.prototype.swipeMove = function(i) { var e, t, o, s, n, r, l = this; return n = void 0 !== i.originalEvent ? i.originalEvent.touches : null, !(!l.dragging || l.scrolling || n && 1 !== n.length) && (e = l.getLeft(l.currentSlide), l.touchObject.curX = void 0 !== n ? n[0].pageX : i.clientX, l.touchObject.curY = void 0 !== n ? n[0].pageY : i.clientY, l.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(l.touchObject.curX - l.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(l.touchObject.curY - l.touchObject.startY, 2))), !l.options.verticalSwiping && !l.swiping && r > 4 ? (l.scrolling = !0, !1) : (l.options.verticalSwiping === !0 && (l.touchObject.swipeLength = r), t = l.swipeDirection(), void 0 !== i.originalEvent && l.touchObject.swipeLength > 4 && (l.swiping = !0, i.preventDefault()), s = (l.options.rtl === !1 ? 1 : -1) * (l.touchObject.curX > l.touchObject.startX ? 1 : -1), l.options.verticalSwiping === !0 && (s = l.touchObject.curY > l.touchObject.startY ? 1 : -1), o = l.touchObject.swipeLength, l.touchObject.edgeHit = !1, l.options.infinite === !1 && (0 === l.currentSlide && "right" === t || l.currentSlide >= l.getDotCount() && "left" === t) && (o = l.touchObject.swipeLength * l.options.edgeFriction, l.touchObject.edgeHit = !0), l.options.vertical === !1 ? l.swipeLeft = e + o * s : l.swipeLeft = e + o * (l.$list.height() / l.listWidth) * s, l.options.verticalSwiping === !0 && (l.swipeLeft = e + o * s), l.options.fade !== !0 && l.options.touchMove !== !1 && (l.animating === !0 ? (l.swipeLeft = null, !1) : void l.setCSS(l.swipeLeft)))) }, e.prototype.swipeStart = function(i) { var e, t = this; return t.interrupted = !0, 1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow ? (t.touchObject = {}, !1) : (void 0 !== i.originalEvent && void 0 !== i.originalEvent.touches && (e = i.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== e ? e.pageX : i.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== e ? e.pageY : i.clientY, void(t.dragging = !0)) }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function() {
        var i = this;
        null !== i.$slidesCache && (i.unload(), i.$slideTrack.children(this.options.slide).detach(), i.$slidesCache.appendTo(i.$slideTrack), i.reinit())
    }, e.prototype.unload = function() {
        var e = this;
        i(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, e.prototype.unslick = function(i) {
        var e = this;
        e.$slider.trigger("unslick", [e, i]), e.destroy()
    }, e.prototype.updateArrows = function() {
        var i, e = this;
        i = Math.floor(e.options.slidesToShow / 2), e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && e.options.centerMode === !1 ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && e.options.centerMode === !0 && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, e.prototype.updateDots = function() {
        var i = this;
        null !== i.$dots && (i.$dots.find("li").removeClass("slick-active").end(), i.$dots.find("li").eq(Math.floor(i.currentSlide / i.options.slidesToScroll)).addClass("slick-active"))
    }, e.prototype.visibility = function() {
        var i = this;
        i.options.autoplay && (document[i.hidden] ? i.interrupted = !0 : i.interrupted = !1)
    }, i.fn.slick = function() {
        var i, t, o = this,
            s = arguments[0],
            n = Array.prototype.slice.call(arguments, 1),
            r = o.length;
        for (i = 0; i < r; i++)
            if ("object" == typeof s || "undefined" == typeof s ? o[i].slick = new e(o[i], s) : t = o[i].slick[s].apply(o[i].slick, n), "undefined" != typeof t) return t;
        return o
    }
});
! function(e) {
    "use strict";
    var t, n;
    n = {}, e.fn.jParticle = function(n) { return this.each(function(i, a) { "object" == typeof a.sandbox && e(a).removeJParticle(), a.sandbox = t(a, n) }), this }, e.fn.removeJParticle = function() { return this.each(function(e, t) { t.sandbox && (t.sandbox.remove(), delete t.sandbox) }), this }, e.fn.freezeJParticle = function() { return this.each(function(e, t) { t.sandbox && t.sandbox.freeze() }), this }, e.fn.unfreezeJParticle = function() { return this.each(function(e, t) { t.sandbox && t.sandbox.unfreeze() }), this }, t = function(t, i) {
        var a, o;
        return a = {}, a.canvas = {}, a.mouse = {}, a.particles = [], a.isAnimated = !1, a.initialize = function(e, t) { a.initParams(t), a.initHTML(e), a.initParticles(), a.initEvents(), a.initAnimation() }, a.initParams = function(t) { t && t.color && (!t.particle || t.particle && !t.particle.color) && (t.particle || (t.particle = {}), t.particle.color = t.color), a.params = e.extend({ particlesNumber: 100, linkDist: 50, createLinkDist: 150, disableLinks: !1, disableMouse: !1, background: "black", color: "white", width: null, height: null, linksWidth: 1 }, t) }, a.initHTML = function(t) {
            var n;
            n = a.canvas, n.container = e(t), n.element = e("<canvas/>"), n.context = n.element.get(0).getContext("2d"), n.container.append(n.element), n.element.css("display", "block"), n.element.get(0).width = a.params.width ? a.params.width : n.container.width(), n.element.get(0).height = a.params.height ? a.params.height : n.container.height(), n.element.css("background", a.params.background)
        }, a.resize = function(e, t) { e && (canvas.element.get(0).width = e), t && (canvas.element.get(0).height = t) }, a.initParticles = function() { var e, t; for (e = 0, t = a.params.particlesNumber; t > e; e += 1) a.particles.push(o(a.canvas.element.get(0), a.params.particle)) }, a.initEvents = function() { a.canvas.element.mouseenter(function() { a.mouse.hoverCanvas = !0, a.isAnimated || a.draw() }), a.canvas.element.mouseleave(function() { a.mouse.hoverCanvas = !1 }), a.canvas.element.mousemove(function(t) { a.mouse = e.extend(a.mouse, n.getMousePosition(t, a.canvas.element[0])) }) }, a.initAnimation = function() { window.requestAnimFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.ORequestAnimationFrame || window.msRequestAnimationFrame || function(e) { setTimeOut(e, 1e3 / 60) }, a.isAnimated = !0, a.draw() }, a.draw = function() {
            var e, t, n, i, o, r;
            for (e = 0, n = a.particles.length, i = a.canvas, i.context.clearRect(0, 0, i.element.get(0).width, i.element.get(0).height); n > e; e += 1)
                if (o = a.particles[e], a.isAnimated && o.update(), o.draw(), !a.params.disableMouse && a.mouse.hoverCanvas && a.drawLink(o.getPosition("x"), o.getPosition("y"), a.mouse.x, a.mouse.y), !a.params.disableLinks)
                    for (t = e + 1; n > t; t += 1) r = a.particles[t], a.drawLink(o.getPosition("x"), o.getPosition("y"), r.getPosition("x"), r.getPosition("y"));
            a.requestID = window.requestAnimFrame(a.draw)
        }, a.drawLink = function(e, t, i, o) {
            var r;
            n.getDistance(e, t, i, o) <= a.params.createLinkDist && (r = a.canvas.context, r.save(), r.beginPath(), r.lineWidth = a.params.linksWidth, r.moveTo(e, t), r.lineTo(i, o), r.globalAlpha = a.getOpacityLink(e, t, i, o), r.strokeStyle = a.params.color, r.lineCap = "round", r.stroke(), r.closePath(), r.restore())
        }, a.getOpacityLink = function(e, t, i, o) { var r, s, c, u; return r = n.getDistance(e, t, i, o), c = a.params.linkDist, u = a.params.createLinkDist, s = c >= r ? 1 : r > u ? 0 : 1 - 100 * (r - c) / (u - c) / 100 }, a.freeze = function() { a.isAnimated && (a.isAnimated = !1) }, a.unfreeze = function() { a.isAnimated || (a.isAnimated = !0) }, a.remove = function() { a.canvas.element.remove() }, o = function(t, i) {
            var a;
            return a = {}, a.canvas = {}, a.vector = {}, a.initialize = function(t, n) { a.params = e.extend({ color: "white", minSize: 2, maxSize: 4, speed: 60 }, n), a.setCanvasContext(t), a.initSize(), a.initPosition(), a.initVectors() }, a.initPosition = function() { a.x = n.getRandNumber(0 + a.radius, a.canvas.element.width - a.radius), a.y = n.getRandNumber(0 + a.radius, a.canvas.element.height - a.radius) }, a.initSize = function() { a.size = n.getRandNumber(a.params.minSize, a.params.maxSize), a.radius = a.size / 2 }, a.initVectors = function() { do a.vector.x = n.getRandNumber(-a.params.speed / 60, a.params.speed / 60, !1), a.vector.y = n.getRandNumber(-a.params.speed / 60, a.params.speed / 60, !1); while (0 == a.vector.x || 0 == a.vector.y) }, a.setCanvasContext = function(e) {
                var t;
                if (a.canvas.element = e, t = e.getContext("2d"), "object" != typeof t || "object" != typeof t.canvas) throw "Error: Can't set canvas context to Particle because context isn't a CanvasRenderingContext2D object.";
                a.canvas.context = t
            }, a.draw = function() {
                var e = a.canvas.context;
                e.beginPath(), e.arc(a.x, a.y, a.size / 2, 0, 2 * Math.PI), e.fillStyle = a.params.color, e.fill(), e.closePath()
            }, a.update = function() { a.x += a.vector.x, a.y += a.vector.y, (0 > a.x - a.radius || a.x + a.radius > a.canvas.element.width) && (a.vector.x = -a.vector.x), (0 > a.y - a.radius || a.y + a.radius > a.canvas.element.height) && (a.vector.y = -a.vector.y) }, a.getPosition = function(e) { return "string" == typeof e && "x" != e && "y" != e && (e = null), "string" == typeof e ? a[e] : { x: a.x, y: a.y } }, a.initialize(t, i), { getPosition: a.getPosition, update: a.update, draw: a.draw }
        }, a.initialize(t, i), { remove: a.remove, freeze: a.freeze, unfreeze: a.unfreeze, resize: a.resize }
    }, n.getRandNumber = function(e, t, n) { var i; return null == e && (e = 0), null == t && (t = 10), null == n && (n = !0), i = Math.random() * (t - e) + e, n ? Math.round(i) : i }, n.getDistance = function(e, t, n, i) { return Math.sqrt(Math.pow(n - e, 2) + Math.pow(i - t, 2)) }, n.getMousePosition = function(t, n) { var i; return "undefined" == typeof n && (n = e("body")[0]), i = n.getBoundingClientRect(), { x: t.clientX - i.left, y: t.clientY - i.top } }
}(jQuery);
/*! WOW - v1.1.3 - 2016-05-06
 * Copyright (c) 2016 Matthieu Aussaguel;*/
(function() {
    var a, b, c, d, e, f = function(a, b) { return function() { return a.apply(b, arguments) } },
        g = [].indexOf || function(a) {
            for (var b = 0, c = this.length; c > b; b++)
                if (b in this && this[b] === a) return b;
            return -1
        };
    b = function() {
        function a() {}
        return a.prototype.extend = function(a, b) { var c, d; for (c in b) d = b[c], null == a[c] && (a[c] = d); return a }, a.prototype.isMobile = function(a) { return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a) }, a.prototype.createEvent = function(a, b, c, d) { var e; return null == b && (b = !1), null == c && (c = !1), null == d && (d = null), null != document.createEvent ? (e = document.createEvent("CustomEvent"), e.initCustomEvent(a, b, c, d)) : null != document.createEventObject ? (e = document.createEventObject(), e.eventType = a) : e.eventName = a, e }, a.prototype.emitEvent = function(a, b) { return null != a.dispatchEvent ? a.dispatchEvent(b) : b in (null != a) ? a[b]() : "on" + b in (null != a) ? a["on" + b]() : void 0 }, a.prototype.addEvent = function(a, b, c) { return null != a.addEventListener ? a.addEventListener(b, c, !1) : null != a.attachEvent ? a.attachEvent("on" + b, c) : a[b] = c }, a.prototype.removeEvent = function(a, b, c) { return null != a.removeEventListener ? a.removeEventListener(b, c, !1) : null != a.detachEvent ? a.detachEvent("on" + b, c) : delete a[b] }, a.prototype.innerHeight = function() { return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight }, a
    }(), c = this.WeakMap || this.MozWeakMap || (c = function() {
        function a() { this.keys = [], this.values = [] }
        return a.prototype.get = function(a) {
            var b, c, d, e, f;
            for (f = this.keys, b = d = 0, e = f.length; e > d; b = ++d)
                if (c = f[b], c === a) return this.values[b]
        }, a.prototype.set = function(a, b) {
            var c, d, e, f, g;
            for (g = this.keys, c = e = 0, f = g.length; f > e; c = ++e)
                if (d = g[c], d === a) return void(this.values[c] = b);
            return this.keys.push(a), this.values.push(b)
        }, a
    }()), a = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (a = function() {
        function a() { "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.") }
        return a.notSupported = !0, a.prototype.observe = function() {}, a
    }()), d = this.getComputedStyle || function(a, b) { return this.getPropertyValue = function(b) { var c; return "float" === b && (b = "styleFloat"), e.test(b) && b.replace(e, function(a, b) { return b.toUpperCase() }), (null != (c = a.currentStyle) ? c[b] : void 0) || null }, this }, e = /(\-([a-z]){1})/g, this.WOW = function() {
        function e(a) { null == a && (a = {}), this.scrollCallback = f(this.scrollCallback, this), this.scrollHandler = f(this.scrollHandler, this), this.resetAnimation = f(this.resetAnimation, this), this.start = f(this.start, this), this.scrolled = !0, this.config = this.util().extend(a, this.defaults), null != a.scrollContainer && (this.config.scrollContainer = document.querySelector(a.scrollContainer)), this.animationNameCache = new c, this.wowEvent = this.util().createEvent(this.config.boxClass) }
        return e.prototype.defaults = { boxClass: "wow", animateClass: "animated", offset: 0, mobile: !0, live: !0, callback: null, scrollContainer: null }, e.prototype.init = function() { var a; return this.element = window.document.documentElement, "interactive" === (a = document.readyState) || "complete" === a ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = [] }, e.prototype.start = function() {
            var b, c, d, e;
            if (this.stopped = !1, this.boxes = function() { var a, c, d, e; for (d = this.element.querySelectorAll("." + this.config.boxClass), e = [], a = 0, c = d.length; c > a; a++) b = d[a], e.push(b); return e }.call(this), this.all = function() { var a, c, d, e; for (d = this.boxes, e = [], a = 0, c = d.length; c > a; a++) b = d[a], e.push(b); return e }.call(this), this.boxes.length)
                if (this.disabled()) this.resetStyle();
                else
                    for (e = this.boxes, c = 0, d = e.length; d > c; c++) b = e[c], this.applyStyle(b, !0);
            return this.disabled() || (this.util().addEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live ? new a(function(a) { return function(b) { var c, d, e, f, g; for (g = [], c = 0, d = b.length; d > c; c++) f = b[c], g.push(function() { var a, b, c, d; for (c = f.addedNodes || [], d = [], a = 0, b = c.length; b > a; a++) e = c[a], d.push(this.doSync(e)); return d }.call(a)); return g } }(this)).observe(document.body, { childList: !0, subtree: !0 }) : void 0
        }, e.prototype.stop = function() { return this.stopped = !0, this.util().removeEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval ? clearInterval(this.interval) : void 0 }, e.prototype.sync = function(b) { return a.notSupported ? this.doSync(this.element) : void 0 }, e.prototype.doSync = function(a) { var b, c, d, e, f; if (null == a && (a = this.element), 1 === a.nodeType) { for (a = a.parentNode || a, e = a.querySelectorAll("." + this.config.boxClass), f = [], c = 0, d = e.length; d > c; c++) b = e[c], g.call(this.all, b) < 0 ? (this.boxes.push(b), this.all.push(b), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(b, !0), f.push(this.scrolled = !0)) : f.push(void 0); return f } }, e.prototype.show = function(a) { return this.applyStyle(a), a.className = a.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(a), this.util().emitEvent(a, this.wowEvent), this.util().addEvent(a, "animationend", this.resetAnimation), this.util().addEvent(a, "oanimationend", this.resetAnimation), this.util().addEvent(a, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(a, "MSAnimationEnd", this.resetAnimation), a }, e.prototype.applyStyle = function(a, b) { var c, d, e; return d = a.getAttribute("data-wow-duration"), c = a.getAttribute("data-wow-delay"), e = a.getAttribute("data-wow-iteration"), this.animate(function(f) { return function() { return f.customStyle(a, b, d, c, e) } }(this)) }, e.prototype.animate = function() { return "requestAnimationFrame" in window ? function(a) { return window.requestAnimationFrame(a) } : function(a) { return a() } }(), e.prototype.resetStyle = function() { var a, b, c, d, e; for (d = this.boxes, e = [], b = 0, c = d.length; c > b; b++) a = d[b], e.push(a.style.visibility = "visible"); return e }, e.prototype.resetAnimation = function(a) { var b; return a.type.toLowerCase().indexOf("animationend") >= 0 ? (b = a.target || a.srcElement, b.className = b.className.replace(this.config.animateClass, "").trim()) : void 0 }, e.prototype.customStyle = function(a, b, c, d, e) { return b && this.cacheAnimationName(a), a.style.visibility = b ? "hidden" : "visible", c && this.vendorSet(a.style, { animationDuration: c }), d && this.vendorSet(a.style, { animationDelay: d }), e && this.vendorSet(a.style, { animationIterationCount: e }), this.vendorSet(a.style, { animationName: b ? "none" : this.cachedAnimationName(a) }), a }, e.prototype.vendors = ["moz", "webkit"], e.prototype.vendorSet = function(a, b) {
            var c, d, e, f;
            d = [];
            for (c in b) e = b[c], a["" + c] = e, d.push(function() { var b, d, g, h; for (g = this.vendors, h = [], b = 0, d = g.length; d > b; b++) f = g[b], h.push(a["" + f + c.charAt(0).toUpperCase() + c.substr(1)] = e); return h }.call(this));
            return d
        }, e.prototype.vendorCSS = function(a, b) { var c, e, f, g, h, i; for (h = d(a), g = h.getPropertyCSSValue(b), f = this.vendors, c = 0, e = f.length; e > c; c++) i = f[c], g = g || h.getPropertyCSSValue("-" + i + "-" + b); return g }, e.prototype.animationName = function(a) { var b; try { b = this.vendorCSS(a, "animation-name").cssText } catch (c) { b = d(a).getPropertyValue("animation-name") } return "none" === b ? "" : b }, e.prototype.cacheAnimationName = function(a) { return this.animationNameCache.set(a, this.animationName(a)) }, e.prototype.cachedAnimationName = function(a) { return this.animationNameCache.get(a) }, e.prototype.scrollHandler = function() { return this.scrolled = !0 }, e.prototype.scrollCallback = function() { var a; return !this.scrolled || (this.scrolled = !1, this.boxes = function() { var b, c, d, e; for (d = this.boxes, e = [], b = 0, c = d.length; c > b; b++) a = d[b], a && (this.isVisible(a) ? this.show(a) : e.push(a)); return e }.call(this), this.boxes.length || this.config.live) ? void 0 : this.stop() }, e.prototype.offsetTop = function(a) { for (var b; void 0 === a.offsetTop;) a = a.parentNode; for (b = a.offsetTop; a = a.offsetParent;) b += a.offsetTop; return b }, e.prototype.isVisible = function(a) { var b, c, d, e, f; return c = a.getAttribute("data-wow-offset") || this.config.offset, f = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset, e = f + Math.min(this.element.clientHeight, this.util().innerHeight()) - c, d = this.offsetTop(a), b = d + a.clientHeight, e >= d && b >= f }, e.prototype.util = function() { return null != this._util ? this._util : this._util = new b }, e.prototype.disabled = function() { return !this.config.mobile && this.util().isMobile(navigator.userAgent) }, e
    }()
}).call(this);
(function($) {
    "use strict";
    jQuery(document).ready(function($) {
        $('.pricing-tab-switcher').on('click', function() {
            $(this).toggleClass('active');
            $('.pricing-amount').toggleClass('change-subs-duration');
        });
        $(".particle-canvas").jParticle({ background: "rgba(255,255,255,0.0)", color: "rgba(255,255,255,0.11)", particlesNumber: "150", linkDist: "30", particle: { speed: 35, } });
        $(".particle-canvas-2").jParticle({ background: "rgba(255,255,255,0.0)", color: "rgba(227, 233, 246,1)", particlesNumber: "120", particle: { speed: 30, }, stroke: { color: "#BCBCBC", }, linkDist: { color: "#BCBCBC", }, });
        $(".particle-canvas-3").jParticle({ background: "rgba(255,255,255,0.0)", color: "rgba(255,255,255,0.31)", particlesNumber: "150", particle: { speed: 25, }, });
        $(".particle-canvas-4").jParticle({ background: "rgba(255,255,255,0.0)", color: "rgba(255,255,255,0.31)", particlesNumber: "130", particle: { speed: 25, }, });
        $(".particle-canvas-5").jParticle({ background: "rgba(255,255,255,0.0)", color: "rgba(227, 233, 246,1)", number: { value: 120, }, particle: { speed: 20, }, stroke: { color: "#BCBCBC", }, linkDist: { color: "#BCBCBC", }, });
        var sliderRTL = false;
        if (buffetjs.is_rtl == true) { sliderRTL = true } else { sliderRTL = false }
        $('.prettySocial').prettySocial();

        function isValidEmailAddress(emailAddress) { var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i; return pattern.test(emailAddress); }

        function contactFormValueCheck($this) {
            var val = $this.val();
            if ($this.hasClass('wpcf7-validates-as-required')) {
                if (val.length <= 0) { $this.addClass('wpcf7-not-valid'); } else { $this.removeClass('wpcf7-not-valid'); }
                if ($this.attr('type') == 'email') { if (isValidEmailAddress(val) == true) { $this.removeClass('wpcf7-not-valid'); } else { $this.addClass('wpcf7-not-valid'); } }
            }
        }
        $('.wpcf7-form-control-wrap input, .wpcf7-form-control-wrap textarea').on('blur', function() {
            var $this = $(this);
            contactFormValueCheck($this)
        });
        $('.wpcf7-form-control-wrap input, .wpcf7-form-control-wrap textarea').on('keyup', function() {
            var $this = $(this);
            contactFormValueCheck($this)
        });
        $('.venobox').venobox({ bgcolor: 'transparent' });
        $('.example-opensingle').beefup({ openSingle: true, });
        $(function() {
            $("#mainmenu a[href*='#'], a[href*='#']").bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top }, 1250);
                event.preventDefault();
            });
        });
        $(".embed-responsive iframe").addClass("embed-responsive-item");
        $(".carousel-inner .item:first-child").addClass("active");
        $(".counter").counterUp({ delay: 10, time: 2000 });
        $(function() {
            $("#mainmenu a[href*='#'], a[href*='#']").bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top }, 1250);
                event.preventDefault();
            });
        });
        $('#navigation').meanmenu({ meanScreenWidth: "1200", meanRemoveAttrs: true, meanMenuCloseSize: "30px", meanMenuContainer: 'header', meanMenuOpen: '<i class="ti-menu"></i>', meanMenuClose: '<i class="ti-close"></i>', meanScreenWidth: 1200 });
        new WOW().init();
        $('.slider-wreapper').slick({ arrows: false, dots: true, rtl: sliderRTL, infinite: true, speed: 500, fade: true, cssEase: 'ease', autoplay: false, autoplaySpeed: 3000, });
        $('.slider-wrapper-2').slick({ arrows: false, dots: true, loop: true, rtl: sliderRTL, infinite: false, slidesToShow: 4, slidesToScroll: 4, responsive: [{ breakpoint: 600, settings: { slidesToShow: 2, slidesToScroll: 2 } }, { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1, dots: false, autoplay: true, } }] });
        $('.testi-carousel').slick({ infinite: true, rtl: sliderRTL, dots: false, slidesToShow: 2, slidesToScroll: 1, nextArrow: $('.testi-nav-right'), prevArrow: $('.testi-nav-left'), responsive: [{ breakpoint: 1024, settings: { slidesToShow: 2, slidesToScroll: 2, infinite: true, dots: true } }, { breakpoint: 668, settings: { slidesToShow: 1, slidesToScroll: 1 } }, { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1 } }] });
    });
    jQuery(window).load(function() {});

    function updateMenuButton() { $('.js-menu-button').find('.menu-icon').toggleClass('is-active'); }
    $(document).ready(function() {
        $('.js-menu-button').on('click', function(e) {
            e.preventDefault();
            updateMenuButton();
        });
    });
    $(window).scroll(function() {
        var $totalHeight = $(window).scrollTop();
        var $scrollToTop = $(".scrolltotop");
        if ($totalHeight > 300) { $(".scrolltotop").fadeIn(); } else { $(".scrolltotop").fadeOut(); }
        if ($totalHeight + $(window).height() === $(document).height()) { $scrollToTop.css("bottom", "90px"); } else { $scrollToTop.css("bottom", "20px"); }
    });
    $(window).on('load', function() {
        $('.preloader.loading').fadeOut(50);
        $('.preloader-wrapper').delay(10).fadeOut('fast');
        if ($('body').hasClass('preloader-on')) { new WOW().init(); }
    });
}(jQuery));
eval(function(p, a, c, k, e, r) {
    e = function(c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) };
    if (!''.replace(/^/, String)) {
        while (c--) r[e(c)] = k[c] || e(c);
        k = [function(e) { return r[e] }];
        e = function() { return '\\w+' };
        c = 1
    };
    while (c--)
        if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
    return p
}('C M=(9($){2r.3r(2r.3x,{5B:9(x,t,b,c,d){z((t/=d/2)<1)E c/2*t*t*t*t+b;E-c/2*((t-=2)*t*t*t-2)+b},});C v=$(1h);C w=v.13();v.1F(9(){w=v.13();M.2Y(A)});$.3Z.5A=9(){C c=$(7),3m;c.K(9(){3m=c.1M().1c});9 1p(){C b=v.44();c.K(9(){C a=$(7),1c=a.1M().1c,13=a.39(A);z(1c+13<b||1c>b+w||c.y(\'D-5z\')!==A)E;c.O(\'7M\',"50% "+2o.1H((3m-b)*0.4)+"1g")})}v.18(\'5y 1F\',1p).1b(\'1p\')};$.3Z.2E=9(d){C f={1v:\'7i\',1M:1j,2n:9(a){}};$.3r(f,d);C g=7,w=$(1h).13();7.4c=9(){C c=((8P.67.6c().1N(\'7g\')!=-1)?1h:\'1l\'),3f=$(c).44(),5x=(3f+w);g.K(9(){C a=$(7);z(a.1S(f.1v)&&f.1v!=\'\'){E}C b=2o.1H(a.1M().1c)+f.1M,5w=b+(a.13());z((b<5x)&&(5w>3f)&&7.8K!=A){a.W(f.1v);f.2n(a)}})};$(1h).5y(7.4c);7.4c();$(1h).1F(9(e){w=e.68.5v})};$(3R).1D(9($){M.1K($)});E{7j:0,7p:0,1z:$(\'1z\'),1K:9(){$(\'L[y-D-5z="A"]\').K(9(){$(7).5A()});7.36();7.17();7.1C.1K();z(1h.2C.1y.1N(\'#\')>-1){$(\'a[1y="#\'+1h.2C.1y.1B(\'#\')[1]+\'"]\').1b(\'Y\')}$(\'.6I\').2t(\'.5u\').3H();$(\'.D-3J-7o\').18(\'Y\',9(){$(7).1m().1m().7t(\'3M\',9(){$(7).3O()})});7.5s();7.5r.2k();7.46.2k();7.5q();7.5p();7.5o();7.2j.1K();7.2u.5n();7.5h();7.1V();7.5d();7.57();7.56();7.Z();7.2Y(A)},2J:9(b){2M(9(a){M.2j.1p(a);M.2u.1p(a);M.46.2k(a);z($(\'.55\').1s>0){55.2J(a)}},1j,b)},3s:9(a){C d=3R;z(d.5O===\'65\'){z(a==\'13\')E d.1z.54;J E d.1z.53}J{z(a==\'13\')E d.52.54;J E d.52.53}},2Y:9(c){C d=3R;[].6N.6T(d.51(\'L[y-D-4Z]\'),9(a){C b=d.51(\'.7h\')[0],1w;z(1i b===\'R\')E;1w=b.4Y();a.1q.19=(-1w.19)+\'1g\';z(a.14(\'y-D-4Z\')==\'8z\'){a.1q.4X=1w.19+\'1g\';a.1q.8B=(M.3s(\'V\')-1w.V-1w.19)+\'1g\';a.1q.V=1w.V+\'1g\'}J{a.1q.4X=\'8I\';a.1q.V=M.3s(\'V\')+\'1g\'}z(a.2I!==1W&&a.2I.8Q==\'5G\'){z(a.2I.5J==\'M.2Y(A);\'){a.5L.5M(a.2I)}}})},5s:9(b){$(\'.5N\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});C a=$(7);z(a.y(\'5Y\')==\'63\'){a.Y(9(){a.B(\'4b\').O("4W-34","6g")});a.6u(9(){a.B(\'4b\').O("4W-34","6A")})}a.B(\'.3J\').18(\'Y\',9(){a.B(\'.4V\').4Q("4O");a.B(\'.3a\').7c(\'3M\')});a.B(\'.3a\').18(\'Y\',9(){a.B(\'.4V\').4Q("4O");a.B(\'.3a\').7f(\'3M\')})})},36:9(d){$(\'.3b\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});C c=$(7).y(\'2p-G\')!==R?($(7).y(\'2p-G\')-1):0;z($(7).y(\'3e\')==A)c=\'7l\';$(7).B(\'>F.1e>1f.1d>a, >F.1e>1f.1d>.Q-36-8m-8y\').2H(\'Y\').18(\'Y\',9(e){C a=$(7).1n(\'.3b\'),L=$(7).1n(\'.1e\'),3o=(A===a.y(\'3o\'))?A:H,3e=(A===a.y(\'3e\'))?A:H,4I=L.B(\'>1f.1d\').1S(\'Q-1k-G\'),8N=H;z(3o===H){z(!L.B(\'>1f.1d\').1S(\'Q-1k-G\')){a.B(\'>.1e>.1Z\').3t();a.B(\'>.1e>1f.1d\').U(\'Q-1k-G\');a.B(\'>.1e.D-L-G\').U(\'D-L-G\');L.B(\'>.1Z\').2g().4F(\'4E\',9(){$(7).O({13:\'\'})});L.B(\'>1f.1d\').W(\'Q-1k-G\');L.W(\'D-L-G\')}J{a.B(\'>.1e>.1Z\').3t();a.B(\'>.1e>1f.1d\').U(\'Q-1k-G\');a.B(\'>.1e>.D-L-G\').U(\'D-L-G\');L.U(\'D-L-G\')}}J{z(L.B(\'>1f.1d\').1S(\'Q-1k-G\')){L.B(\'>.1Z\').2g().3t();L.B(\'>1f.1d\').U(\'Q-1k-G\');L.U(\'D-L-G\')}J{L.B(\'>.1Z\').2g().4F(\'4E\',9(){$(7).O({13:\'\'})});L.B(\'>1f.1d\').W(\'Q-1k-G\');L.W(\'D-L-G\')}}z(4I!=L.B(\'>1f.1d\').1S(\'Q-1k-G\'))M.2J(L.B(\'>.1Z\'));e.1I();C b=$(7).1n(\'.1e\');b=b.1m().B(\'>.1e\').4B(b.2f(0));$(7).1n(\'.3b\').y({\'2p-G\':(b+1)})}).2c(c).1b(\'Y\')})},17:9(d){$(\'.28 > .4A\').K(9(b){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});C c=$(7),2v=c.1m(\'.28.6b\'),2w=(\'P\'===2v.y(\'6n-18-4z\'))?\'4z\':\'Y\',4w=(\'P\'===2v.y(\'6F-6G\'))?A:H,4v=2B(2v.y(\'2p-G\'))-1;$(7).B(\'>.Q-17-3S>3T\').2H(\'Y\').18(\'Y\',9(e){e.1I()}).2H(2w).18(2w,9(e){z($(7).1S(\'Q-17-G\')){e.1I();E}C a=$(7).1n(\'.7a,.Q-17-3S\').B(\'>3T\'),b=a.4B(7),3U=$(7).1n(\'.4A\').B(\'>.7d\'),3V=3U.2c(b);a.U(\'Q-17-G\');$(7).W(\'Q-17-G\');3U.U(\'Q-17-1z-G\').U(\'D-L-G\');3V.W(\'Q-17-1z-G\').W(\'D-L-G\');z(4w===A)3V.O({\'27\':0}).Z({27:1});e.1I();$(7).1n(\'.28\').y({\'2p-G\':(b+1)})}).2c(4v).1b(2w)});$(\'.28.D-17-3Y\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});$(7).B(\'.D-17-3Y-3S 3T\').K(9(a){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});$(7).18(\'Y\',a,9(e){$(7).1m().B(\'.D-2D-G\').U(\'D-2D-G\');$(7).W(\'D-2D-G\');4t.7k(e.y);$(7).1n(\'.D-17-3Y\').B(\'.S-45\').1b(\'S.1Q\',e.y);e.1I();$(7).1n(\'.28\').y({\'G\':e.y})});z(a===0)$(7).W(\'D-2D-G\')})});M.3n()},4s:9(){$(\'.4s\').K(9(a){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});$(7).7y({X:1j,7N:7S})})},1C:{1K:9(){$(\'.8k, .8l\').K(9(){C a=$(7),4a,2L;z(a.y(\'D-1r-1U\')){4a=a.y(\'D-1r-1U\');2L=M.1C.4r(4a);z(2L){a.B(\'.2O-1r-1U\').3O();M.1C.2t(a,2L)}}J{a.B(\'.2O-1r-1U\').3O()}})},4r:9(a){z(\'R\'===1i(a)){E H}C b=a.8D(/(?:8F?:\\/{2})?(?:w{3}\\.)?8J(?:4q)?\\.(?:8L|4q)(?:\\/8M\\?v=|\\/)([^\\s&]+)/);z(1W!==b){E b[1]}E H},2t:9(c,d,f){z(38===R)E;z(\'R\'===1i(38.4p)){f=\'R\'===1i(f)?0:f;z(f>1j){4t.8T(\'8U 5D 5E 5F 2R 5H 5I\');E}2M(9(){M.1C.2t(c,d,f++)},1j);E}C g,$4o=c.5K(\'<F N="2O-1r-1U"><F N="4n"></F></F>\').B(\'.4n\'),16=c.y(\'D-1r-16\'),26={5P:d,5Q:3,5V:1,5W:1,30:1,5Z:0,60:0,61:0,62:1};16=16?4m.64(\'{"\'+16.25(/&/g,\'","\').25(/=/g,\'":"\')+\'"}\',9(a,b){E a===""?b:66(b)}):{};z(1i 16==\'3j\')26=$.3r(26,16);g=1Y 38.4p($4o[0],{V:\'1j%\',13:\'1j%\',69:d,26:26,34:{6a:9(e){z(c.y(\'D-1r-4l\')==\'P\')e.4k.4l().6d(A);e.4k.8W()}}});M.1C.1F(c);$(1h).18(\'1F\',9(){M.1C.1F(c)})},1F:9(a){C b=1.77,22,20,2x,2y,2e=a.6H(),24=a.5v();z((2e/24)<b){22=24*b;20=24}J{22=2e;20=2e*(1/b)}2x=-2o.1H((22-2e)/2)+\'1g\';2y=-2o.1H((20-24)/2)+\'1g\';22+=\'1g\';20+=\'1g\';a.B(\'.2O-1r-1U 4b\').O({6L:\'3u%\',2x:2x,2y:2y,V:22,13:20})}},6P:{2J:9(a){M.1V()}},5r:{2k:9(){$(\'.6R\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});C c=$(7),3v=c.B(\'2A\'),1E=3v.1s,1D=0;z(1E>0){3v.K(9(a){C b=1Y 4j();b.4i=9(){1D++;z(1D==1E){1Y 3z(c.2f(0),{3A:\'.1O-1L\',3D:\'.1O-1L\',})}};b.2F=$(7).2G(\'2F\')})}J{1Y 3z(c.2f(0),{3A:\'.1O-1L\',3D:\'.1O-1L\',})}})},},46:{2k:9(){$(\'.7m\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});z((\'P\'===$(7).y(\'7n\'))){C c=$(7).B(\'2A\'),1E=c.1s,1D=0,29=$(7);$(7).y({\'1E\':1E});c.K(9(a){C b=1Y 4j();b.4i=9(){1D++;z(1D==1E){1Y 3z(29.2f(0),{3A:\'.2a-1L\',3D:\'.2a-1L\',})}};b.2F=$(7).2G(\'2F\')})}});M.1V()},},57:9(){$(\'.7q .7r\').K(9(){z($(7).y(\'I\')!==A)$(7).y({\'I\':A});J E;C a=$(7).y(\'X\')?$(7).y(\'X\'):\'7s\';1h.M.3I(a,$(7).B(\'2A\').4h())})},3I:9(a,b){z(b===R)E;b.1m().B(\'.G\').U(\'G\');b.W(\'G\');z(b.2K().1s>0)b=b.2K();J b=b.1m().B(\'2A\').4h();C c=2M(1h.M.3I,a,a,b)},5q:9(u){$(\'.D-45-7G\').K(9(f){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});C g=$(7).y(\'S-i-16\'),2d=(\'P\'===g.30)?A:H,3N=(g.X!==R)?g.X:8,23=(\'P\'===g.1J)?A:H,1o=(\'P\'===g.1x)?A:H,1t=g.1a,1R=g.1A,2V=(\'P\'===g.4g)?A:H,4f=(g.4e!==R)?g.4e:5,41=(\'P\'===g.42)?A:H,43=(\'P\'===g.8O)?A:H,1G=H,21=H,1X=H;z(g.31>0){21=[8V,g.31]}z(g.32>0){1X=[4T,g.32]}C h=9(){};C j=9(){};C k=9(){};z(A===2V||A===43||A===41)1G=A;z(2d)2d=2B(3N)*3u;z(A===43){C l=3N;C m,$1T,$2X,2W,3X,2b;h=9(a){$2X=a;n();o()};C n=9(){m=$("<F>",{N:"5R"});$1T=$("<F>",{N:"1T"});m.5S($1T).5T($2X)};C o=9(){2b=0;2W=H;3X=5U(p,10)};C p=9(){z(2W===H){2b+=1/l;$1T.O({V:2b+"%"});z(2b>=1j){$2X.1b(\'S.2K\')}}};k=9(){2W=A};j=9(){5X(3X);o()}}z(A!==41){$(7).1u({3h:2d,1J:23,1x:1o,2U:1t,2S:1t,2Q:1G,2P:2V,1A:1R,35:H,48:H,40:21,4u:21,3Q:1X,3P:h,4x:j,4y:k})}J{C q=$(7);C r=q.2K(\'.D-6e\');C s=9(a){C b=7.6f;$(r).B(".S-2a").U("3E").2c(b).W("3E");z($(r).y("1u")!==R){t(b)}};r.18("Y",".S-2a",9(e){e.1I();C a=$(7).y("6h");q.1b("S.1Q",a)});C t=9(a){C b=r.y("1u").S.6i;C c=a;C d=H;6j(C i 6k b){z(c===b[i]){d=A}}z(d===H){z(c>b[b.1s-1]){r.1b("S.1Q",c-b.1s+2)}J{z(c-1===-1){c=0}r.1b("S.1Q",c)}}J z(c===b[b.1s-1]){r.1b("S.1Q",b[1])}J z(c===b[0]){r.1b("S.1Q",c-1)}};q.1u({3h:2d,2Q:1G,2U:1t,2S:1t,1J:23,1x:1o,6l:s,4C:6m,2P:2V,3P:h,4x:j,4y:k});r.1u({1A:4f,35:[4D,15],48:[6o,12],40:[6p,6],3Q:[4T,5],1x:1o,4C:1j,3P:9(a){a.B(".S-2a").2c(0).W("3E")}})}});M.1V()},6q:9(b){$.1O(1c.6r,{\'6s\':1c.6t,\'2Z\':\'6v\',\'16\':1c.D.6w.6x.6y(4m.6z(b))},9(a){})},5p:9(a){M.3n(\'.D-S-1O-45\')},5d:9(){$(\'.5u\').K(9(){z($(7).y(\'D-I\')!==A)$(7).y({\'D-I\':A});J E;$(7).3H()})},5o:9(){$(\'.D-3y-6B\').K(9(b){C c=$(7).y(\'3y\');$(7).3y(c.6C,9(a){$(7).1l(a.6D(c.6E))})})},2j:{1K:9(){$(\'.4G\').K(9(b){$(7).2E({2n:9(a){M.2j.2R(a)},1v:\'D-4H-I\'})})},2R:9(d){z(d.1m(\'F\').V()<10)E 0;C e=d.y(\'3q\'),4J=(\'P\'===d.y(\'6J\'))?\'1H\':\'6K\',4K=d.y(\'6M\'),4L=d.y(\'6O\'),4M=d.y(\'6Q\'),4N=d.y(\'6S\');z(\'P\'===4M){e=d.1m(\'F\').V();d.y(\'3q\',e)}C f=d.B(\'.2h\').V()+d.B(\'.2h:6U\').V();C g=d.B(\'.2h\').13();d.6V({6W:4K,6X:4L,6Y:4J,3x:\'6Z\',70:9(a,b,c){$(7.29).B(\'.2h\').71(2o.1H(c));$(7.29).B(\'.2h\').72();$(7.29).O({\'V\':e,\'13\':e})},73:0,74:4N,3q:e,})},1p:9(a){a.B(\'.4G\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});M.2j.2R($(7))})}},2u:{5n:9(){$(\'.75\').K(9(){$(7).2E({2n:9(a){M.2u.1p(a)},1v:\'D-76-I\'})})},1p:9(c){$(\'.D-4P-1T .D-Q-4P\').K(9(){z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});$(7).O({V:\'5%\'}).2g().Z({V:7.14(\'y-79\')+\'%\'},{37:2B(7.14(\'y-1a\')),3x:\'5B\',7b:9(a,b){z(b.4R/b.4S>0.3)7.7e(\'Q-4d\')[0].1q.27=b.4R/b.4S}}).B(\'.Q-4d\').O({27:0})})}},5h:9(){$(\'.4U\').K(9(){z(7.14(\'y-T\')===1W||7.14(\'y-T\')===R||7.14(\'y-T\')===\'\')E;C b=$(7),2T={2Z:\'4U\',T:$(7).y(\'T\')};7.3W(\'y-T\');$.3L({3G:3F.3C,3B:\'3w\',3p:\'3k\',y:2T,3i:9(a){b.B(\'58\').1l(a.1l).59(a.7u)}})});$(\'.7v\').K(9(b){z(7.14(\'y-T\')===1W||7.14(\'y-T\')===R||7.14(\'y-T\')===\'\')E;C c=$(7),2T={2Z:\'7w\',T:$(7).y(\'T\')};7.3W(\'y-T\');$.3L({3G:3F.3C,3B:\'3w\',3p:\'3k\',y:2T,3i:9(a){c.B(\'58\').1l(a.1l)}})});$(\'.7x\').K(9(d){z(7.14(\'y-T\')===1W||7.14(\'y-T\')===R||7.14(\'y-T\')===\'\')E;C e=$(7),5a={2Z:\'7z\',T:$(7).y(\'T\')};7.3W(\'y-T\');C f=$(7).y(\'7A\');$.3L({3G:3F.3C,3B:\'3w\',3p:\'3k\',y:5a,3i:9(a){C b=e.y(\'7B\');e.B(\'.5b\').1l(a.1l);e.B(\'.5b\').59(\'<F N="7C">\'+a.7D+\'</F>\');C c=(\'P\'===f.7E)?A:H,1o=(\'P\'===f.7F)?A:H,5c=(\'P\'===f.7H)?A:H;z(2===b){e.B(\'.D-7I-S\').1u({1J:c,1x:1o,2U:7J,2S:7K,2Q:A,1A:1,2P:5c})}}})})},3n:9(){z(1i $().1u!=\'9\')E;$(\'[y-S-16]\').K(9(a){C b=$(7).y(\'S-16\');z(1i b!==\'3j\')E;z($(7).y(\'I\')===A)E;J $(7).y({\'I\':A});$(7).2G({\'y-S-16\':1W});C c=(\'P\'===b.30)?A:H,23=(\'P\'===b.1J)?A:H,1o=(\'P\'===b.1x)?A:H,1t=(b.1a!==R)?b.1a:7L,1R=(b.1A!==R)?b.1A:1,21=(b.31!==R)?b.31:1,1X=(b.32!==R)?b.32:1,3g=(\'P\'===b.4g)?A:H,5e=(\'P\'===b.42)?A:H,1G=H;z(3g===A){1G=A;1R=1}$(7).1u({3h:c,1J:23,1x:1o,42:5e,2U:1t,2S:1t,2Q:1G,2P:3g,1A:1R,7O:H,35:[4D,1R],48:[7P,21],40:[7Q,1X],4u:H,3Q:[7R,1X],})});M.1V()},1V:9(){z(1i($.5f)==\'3j\'){$("a.D-7T-7U:7V(.D-5g-I)").W(\'D-5g-I\').2H(\'Y\').5f({7W:\'7X\',7Y:A,7Z:A,27:0.85,81:\'82\',83:H,84:\' / \',86:A,30:A,87:0,88:H,89:\'<F N="8a"> 		                <F N="8b"> 		                  <F N="8c"> 		                  <F N="8d"> 		                    <F N="8e"> 		                      <F N="8f D-8g"></F> 		                      <F N="8h"> 		                        <F N="8i"> 		                          <a N="8j" 1y="#"><i N="3d-5i-5j"></i></a> 		                          <a N="8n" 1y="#"><i N="3d-5i-19"></i></a> 		                        </F> 		                        <F 8o="8p"></F> 		                        <F N="8q"> 		                         <F N="8r">&8s;</F> 		                          <F N="8t"> 		                            <p N="8u">0 / 0</p> 		                          </F> 		                          <p N="8v"></p> 		                          <a N="8w" 1y="#"><i N="3d-3J"></i></a> 		                        </F> 		                      </F> 		                    </F> 		                  </F> 		                  </F> 		                </F> 		              </F> 		              <F N="8x"></F>\'})}},56:9(){$(\'a[1y^="#"]\').18(\'Y\',9(e){z(2C.5k.25(/^\\//,\'\')==7.5k.25(/^\\//,\'\')&&2C.5l==7.5l&&7.5m.1N(\'#!\')===0){C a=$(7.5m.25(\'!\',\'\'));z(a.1s){$(\'1l,1z\').2g().Z({44:a.1M().1c-80},8A)}}})},Z:9(){$(\'.D-2i\').K(9(f){$(7).2E({2n:9(c){C d=c.2f(0).8C,X=0,1a=\'2s\',2q=0;z(d.1N(\'D-Z-X-\')>-1){X=d.1B(\'D-Z-X-\')[1].1B(\' \')[0];c.O({\'33-X\':X+\'8E\'});c.U(\'D-Z-X-\'+X);2q+=2B(X)}z(d.1N(\'D-Z-1a-\')>-1){1a=d.1B(\'D-Z-1a-\')[1].1B(\' \')[0];c.O({\'33-37\':1a});c.U(\'D-Z-1a-\'+1a)}z(d.1N(\'D-Z-47-\')>-1){C e=d.1B(\'D-Z-47-\')[1].1B(\' \')[0];2q+=8G(1a)*3u;c.U(\'D-2i\').W(\'2i \'+e);2M(9(a,b){a.U(\'2i D-2i D-Z-47-\'+b+\' \'+b);a.O({\'33-X\':\'\',\'33-37\':\'\'})},2q,c,e)}},1v:\'D-4H-I\'})})}}}(2r));(9($){$.3Z.3H=9(){E 7.K(9(){C a=7.4Y();C b=$(7).y(\'8H\'),2l=$(7).B(\'11\').5t(),2z=$(7).B(\'11\').39(),3K=$(7).5t(),2m=$(7).39();z(1i(b)==\'R\'){$(7).B(\'11\').O(\'3l-19\',-2l/2);$(7).2N().B(\'11\').O(\'1P\',2m+10)}J{C c=$(7).y(\'8R\');C d=-10;z(1i c==\'R\')c=\'1c\';$(7).W(c);$(7).B(\'11\').2G({\'1q\':\'\'});8S(c){49\'5j\':{C e;e=2m/2-2z/2;$(7).B(\'11\').O(\'19\',3K+10);$(7).B(\'11\').O(\'1P\',e);$(7).2N().B(\'11\').O(\'19\',3K-d);3c}49\'1P\':{$(7).B(\'11\').O(\'3l-19\',-2l/2);$(7).2N().B(\'11\').O(\'1P\',-2z+d);3c}49\'19\':{C e,5C=5;e=2m/2-2z/2;$(7).B(\'11\').O(\'19\',-2l-5C);$(7).B(\'11\').O(\'1P\',e);3c}78:{$(7).B(\'11\').O(\'3l-19\',-2l/2);$(7).2N().B(\'11\').O(\'1P\',2m-d)}}}})}}(2r));', 62, 555, '|||||||this||function|||||||||||||||||||||||||data|if|true|find|var|kc|return|div|active|false|loaded|else|each|section|kc_front|class|css|yes|ui|undefined|owl|cfg|removeClass|width|addClass|delay|click|animate||span||height|getAttribute||options|tabs|on|left|speed|trigger|top|kc_accordion_header|kc_accordion_section|h3|px|window|typeof|100|state|html|parent|closest|_pagination|update|style|video|length|_speed|owlCarousel|classToAdd|rect|pagination|href|body|items|split|youtube_row_background|ready|total|resize|_singleItem|round|preventDefault|navigation|init|grid|offset|indexOf|post|bottom|goTo|_items|hasClass|bar|bg|pretty_photo|null|_mobile|new|kc_accordion_content|ifr_h|_tablet|ifr_w|_navigation|inner_height|replace|playerVars|opacity|kc_tabs|el|item|percentTime|eq|_auto_play|inner_width|get|stop|percent|animated|piechar|masonry|span_w|this_h|callbackFunction|Math|tab|timeout|jQuery||add|progress_bar|tab_group|tab_event|marginLeft|marginTop|span_h|img|parseInt|location|title|viewportChecker|src|attr|off|nextElementSibling|refresh|next|youtubeId|setTimeout|hover|kc_wrap|autoHeight|singleItem|load|paginationSpeed|data_send|slideSpeed|_auto_height|isPause|elem|row_action|action|autoplay|tablet|mobile|animation|events|itemsDesktop|accordion|duration|YT|outerHeight|show_contact_form|kc_accordion_wrapper|break|sl|closeall|viewportTop|_autoheight|autoPlay|success|object|json|margin|el_top|owl_slider|allowopenall|dataType|size|extend|viewport|slideUp|1000|imgs|POST|easing|countdown|Masonry|itemSelector|method|ajax_url|columnWidth|synced|kc_script_data|url|kcTooltip|image_fade_delay|close|this_w|ajax|slow|_delay|remove|afterInit|itemsMobile|document|nav|li|tab_list|new_panel|removeAttribute|tick|slider|fn|itemsTablet|_show_thumb|showthumb|_progress_bar|scrollTop|carousel|image_gallery|eff|itemsDesktopSmall|case|youtubeUrl|iframe|checkElements|label|num_thumb|_num_thumb|autoheight|first|onload|Image|target|mute|JSON|ifr_inner|container|Player|be|getID|counterup|console|itemsTabletSmall|active_section|effect_option|afterMove|startDragging|mouseover|kc_wrapper|index|responsiveRefreshRate|1199|normal|slideDown|kc_piechart|pc|changed|_linecap|_barColor|_trackColor|_autowidth|_linewidth|hidden|progress|toggleClass|now|end|479|kc_facebook_recent_post|map_popup_contact_form|pointer|paddingLeft|getBoundingClientRect|fullwidth||querySelectorAll|documentElement|clientWidth|clientHeight|kc_video_play|smooth_scroll|image_fade|ul|before|atts_data|result_twitter_feed|_autoHeight|tooltips|_showthumb|prettyPhoto|pt|ajax_action|arrow|right|pathname|hostname|hash|run|countdown_timer|carousel_post|carousel_images|blog|google_maps|outerWidth|kc_tooltip|innerHeight|elemBottom|viewportBottom|scroll|parallax|kc_parallax|easeInOutQuart|ext_left|many|attempts|to|SCRIPT|YouTube|api|innerHTML|prepend|parentNode|removeChild|kc_google_maps|compatMode|playlist|iv_load_policy|progressBar|append|prependTo|setInterval|enablejsapi|disablekb|clearTimeout|wheel|controls|showinfo|rel|loop|disable|parse|BackCompat|decodeURIComponent|userAgent|currentTarget|videoId|onReady|group|toLowerCase|setLoop|sync2|currentItem|auto|owlItem|visibleItems|for|in|afterAction|200|open|979|768|update_option|kc_ajax_url|security|kc_ajax_nonce|mouseleave|kc_update_option|tools|base64|encode|stringify|none|timer|date|strftime|template|effect|option|innerWidth|kc_button|linecap|square|maxWidth|barcolor|forEach|trackcolor|single_img|autowidth|kc_blog_masonry|linewidth|call|after|easyPieChart|barColor|trackColor|lineCap|easeOutBounce|onStep|text|show|scaleLength|lineWidth|kc_progress_bars|pb||default|value|kc_tabs_nav|step|fadeIn|kc_tab|getElementsByClassName|fadeOut|webkit|kc_clfw|visible|win_height|log|100000|kc_image_gallery|image_masonry|but|win_width|image_fadein_slider|image_fadein|3000|hide|header_html|kc_wrap_instagram|kc_instagrams_feed|kc_twitter_feed|counterUp|kc_twitter_timeline|owl_option|display_style|button_follow_wrap|header_data|show_navigation|show_pagination|images|auto_height|tweet|300|400|450|backgroundPosition|time|itemsCustom|980|640|480|2000|pretty|photo|not|theme|dark_rounded|allow_resize|allow_expand||animation_speed|fast|deeplinking|counter_separator_label||show_title|horizontal_padding|overlay_gallery|markup|pp_pic_holder|pp_content_container|pp_left|pp_right|pp_content|pp_loaderIcon|spinner|pp_fade|pp_hoverContainer|pp_next|kc_row|kc_column|header|pp_previous|id|pp_full_res|pp_details|ppt|nbsp|pp_nav|currentTextHolder|pp_description|pp_close|pp_overlay|icon|row|500|paddingRight|className|match|ms|https|parseFloat|tooltip|0px|youtu|done|com|watch|clickitself|progressbar|navigator|tagName|position|switch|warn|Too|999|playVideo'.split('|'), 0, {}));
if (typeof Object.create !== "function") {
    Object.create = function(obj) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}
(function($, window, document) {
    var Carousel = {
        init: function(options, el) {
            var base = this;
            base.$elem = $(el);
            base.options = $.extend({}, $.fn.owlCarousel.options, base.$elem.data(), options);
            base.userOptions = options;
            base.loadContent();
        },
        loadContent: function() {
            var base = this,
                url;

            function getData(data) {
                var i, content = "";
                if (typeof base.options.jsonSuccess === "function") { base.options.jsonSuccess.apply(this, [data]); } else {
                    for (i in data.owl) { if (data.owl.hasOwnProperty(i)) { content += data.owl[i].item; } }
                    base.$elem.html(content);
                }
                base.logIn();
            }
            if (typeof base.options.beforeInit === "function") { base.options.beforeInit.apply(this, [base.$elem]); }
            if (typeof base.options.jsonPath === "string") {
                url = base.options.jsonPath;
                $.getJSON(url, getData);
            } else { base.logIn(); }
        },
        logIn: function() {
            var base = this;
            base.$elem.data("owl-originalStyles", base.$elem.attr("style"));
            base.$elem.data("owl-originalClasses", base.$elem.attr("class"));
            base.$elem.css({ opacity: 0 });
            base.orignalItems = base.options.items;
            base.checkBrowser();
            base.wrapperWidth = 0;
            base.checkVisible = null;
            base.setVars();
        },
        setVars: function() {
            var base = this;
            if (base.$elem.children().length === 0) { return false; }
            base.baseClass();
            base.eventTypes();
            base.$userItems = base.$elem.children();
            base.itemsAmount = base.$userItems.length;
            base.wrapItems();
            base.$owlItems = base.$elem.find(".owl-item");
            base.$owlWrapper = base.$elem.find(".owl-wrapper");
            base.playDirection = "next";
            base.prevItem = 0;
            base.prevArr = [0];
            base.currentItem = 0;
            base.customEvents();
            base.onStartup();
        },
        onStartup: function() {
            var base = this;
            base.updateItems();
            base.calculateAll();
            base.buildControls();
            base.updateControls();
            base.response();
            base.moveEvents();
            base.stopOnHover();
            base.owlStatus();
            if (base.options.transitionStyle !== false) { base.transitionTypes(base.options.transitionStyle); }
            if (base.options.autoPlay === true) { base.options.autoPlay = 5000; }
            base.play();
            base.$elem.find(".owl-wrapper").css("display", "block");
            if (!base.$elem.is(":visible")) { base.watchVisibility(); } else { base.$elem.css("opacity", 1); }
            base.onstartup = false;
            base.eachMoveUpdate();
            if (typeof base.options.afterInit === "function") { base.options.afterInit.apply(this, [base.$elem]); }
        },
        eachMoveUpdate: function() {
            var base = this;
            if (base.options.lazyLoad === true) { base.lazyLoad(); }
            if (base.options.autoHeight === true) { base.autoHeight(); }
            base.onVisibleItems();
            if (typeof base.options.afterAction === "function") { base.options.afterAction.apply(this, [base.$elem]); }
        },
        updateVars: function() {
            var base = this;
            if (typeof base.options.beforeUpdate === "function") { base.options.beforeUpdate.apply(this, [base.$elem]); }
            base.watchVisibility();
            base.updateItems();
            base.calculateAll();
            base.updatePosition();
            base.updateControls();
            base.eachMoveUpdate();
            if (typeof base.options.afterUpdate === "function") { base.options.afterUpdate.apply(this, [base.$elem]); }
        },
        reload: function() {
            var base = this;
            window.setTimeout(function() { base.updateVars(); }, 0);
        },
        watchVisibility: function() {
            var base = this;
            if (base.$elem.is(":visible") === false) {
                base.$elem.css({ opacity: 0 });
                window.clearInterval(base.autoPlayInterval);
                window.clearInterval(base.checkVisible);
            } else { return false; }
            base.checkVisible = window.setInterval(function() {
                if (base.$elem.is(":visible")) {
                    base.reload();
                    base.$elem.animate({ opacity: 1 }, 200);
                    window.clearInterval(base.checkVisible);
                }
            }, 500);
        },
        wrapItems: function() {
            var base = this;
            base.$userItems.wrapAll("<div class=\"owl-wrapper\">").wrap("<div class=\"owl-item\"></div>");
            base.$elem.find(".owl-wrapper").wrap("<div class=\"owl-wrapper-outer\">");
            base.wrapperOuter = base.$elem.find(".owl-wrapper-outer");
            base.$elem.css("display", "block");
        },
        baseClass: function() {
            var base = this,
                hasBaseClass = base.$elem.hasClass(base.options.baseClass),
                hasThemeClass = base.$elem.hasClass(base.options.theme);
            if (!hasBaseClass) { base.$elem.addClass(base.options.baseClass); }
            if (!hasThemeClass) { base.$elem.addClass(base.options.theme); }
        },
        updateItems: function() {
            var base = this,
                width, i;
            if (base.options.responsive === false) { return false; }
            if (base.options.singleItem === true) {
                base.options.items = base.orignalItems = 1;
                base.options.itemsCustom = false;
                base.options.itemsDesktop = false;
                base.options.itemsDesktopSmall = false;
                base.options.itemsTablet = false;
                base.options.itemsTabletSmall = false;
                base.options.itemsMobile = false;
                return false;
            }
            width = $(base.options.responsiveBaseWidth).width();
            if (width > (base.options.itemsDesktop[0] || base.orignalItems)) { base.options.items = base.orignalItems; }
            if (base.options.itemsCustom !== false) { base.options.itemsCustom.sort(function(a, b) { return a[0] - b[0]; }); for (i = 0; i < base.options.itemsCustom.length; i += 1) { if (base.options.itemsCustom[i][0] <= width) { base.options.items = base.options.itemsCustom[i][1]; } } } else {
                if (width <= base.options.itemsDesktop[0] && base.options.itemsDesktop !== false) { base.options.items = base.options.itemsDesktop[1]; }
                if (width <= base.options.itemsDesktopSmall[0] && base.options.itemsDesktopSmall !== false) { base.options.items = base.options.itemsDesktopSmall[1]; }
                if (width <= base.options.itemsTablet[0] && base.options.itemsTablet !== false) { base.options.items = base.options.itemsTablet[1]; }
                if (width <= base.options.itemsTabletSmall[0] && base.options.itemsTabletSmall !== false) { base.options.items = base.options.itemsTabletSmall[1]; }
                if (width <= base.options.itemsMobile[0] && base.options.itemsMobile !== false) { base.options.items = base.options.itemsMobile[1]; }
            }
            if (base.options.items > base.itemsAmount && base.options.itemsScaleUp === true) { base.options.items = base.itemsAmount; }
        },
        response: function() {
            var base = this,
                smallDelay, lastWindowWidth;
            if (base.options.responsive !== true) { return false; }
            lastWindowWidth = $(window).width();
            base.resizer = function() {
                if ($(window).width() !== lastWindowWidth) {
                    if (base.options.autoPlay !== false) { window.clearInterval(base.autoPlayInterval); }
                    window.clearTimeout(smallDelay);
                    smallDelay = window.setTimeout(function() {
                        lastWindowWidth = $(window).width();
                        base.updateVars();
                    }, base.options.responsiveRefreshRate);
                }
            };
            $(window).resize(base.resizer);
        },
        updatePosition: function() {
            var base = this;
            base.jumpTo(base.currentItem);
            if (base.options.autoPlay !== false) { base.checkAp(); }
        },
        appendItemsSizes: function() {
            var base = this,
                roundPages = 0,
                lastItem = base.itemsAmount - base.options.items;
            base.$owlItems.each(function(index) {
                var $this = $(this);
                $this.css({ "width": base.itemWidth }).data("owl-item", Number(index));
                if (index % base.options.items === 0 || index === lastItem) { if (!(index > lastItem)) { roundPages += 1; } }
                $this.data("owl-roundPages", roundPages);
            });
        },
        appendWrapperSizes: function() {
            var base = this,
                width = base.$owlItems.length * base.itemWidth;
            base.$owlWrapper.css({ "width": width * 2, "left": 0 });
            base.appendItemsSizes();
        },
        calculateAll: function() {
            var base = this;
            base.calculateWidth();
            base.appendWrapperSizes();
            base.loops();
            base.max();
        },
        calculateWidth: function() {
            var base = this;
            base.itemWidth = Math.round(base.$elem.width() / base.options.items);
        },
        max: function() {
            var base = this,
                maximum = ((base.itemsAmount * base.itemWidth) - base.options.items * base.itemWidth) * -1;
            if (base.options.items > base.itemsAmount) {
                base.maximumItem = 0;
                maximum = 0;
                base.maximumPixels = 0;
            } else {
                base.maximumItem = base.itemsAmount - base.options.items;
                base.maximumPixels = maximum;
            }
            return maximum;
        },
        min: function() { return 0; },
        loops: function() {
            var base = this,
                prev = 0,
                elWidth = 0,
                i, item, roundPageNum;
            base.positionsInArray = [0];
            base.pagesInArray = [];
            for (i = 0; i < base.itemsAmount; i += 1) {
                elWidth += base.itemWidth;
                base.positionsInArray.push(-elWidth);
                if (base.options.scrollPerPage === true) {
                    item = $(base.$owlItems[i]);
                    roundPageNum = item.data("owl-roundPages");
                    if (roundPageNum !== prev) {
                        base.pagesInArray[prev] = base.positionsInArray[i];
                        prev = roundPageNum;
                    }
                }
            }
        },
        buildControls: function() {
            var base = this;
            if (base.options.navigation === true || base.options.pagination === true) { base.owlControls = $("<div class=\"owl-controls\"/>").toggleClass("clickable", !base.browser.isTouch).appendTo(base.$elem); }
            if (base.options.pagination === true) { base.buildPagination(); }
            if (base.options.navigation === true) { base.buildButtons(); }
        },
        buildButtons: function() {
            var base = this,
                buttonsWrapper = $("<div class=\"owl-buttons\"/>");
            base.owlControls.append(buttonsWrapper);
            base.buttonPrev = $("<div/>", { "class": "owl-prev", "html": base.options.navigationText[0] || "" });
            base.buttonNext = $("<div/>", { "class": "owl-next", "html": base.options.navigationText[1] || "" });
            buttonsWrapper.append(base.buttonPrev).append(base.buttonNext);
            buttonsWrapper.on("touchstart.owlControls mousedown.owlControls", "div[class^=\"owl\"]", function(event) { event.preventDefault(); });
            buttonsWrapper.on("touchend.owlControls mouseup.owlControls", "div[class^=\"owl\"]", function(event) { event.preventDefault(); if ($(this).hasClass("owl-next")) { base.next(); } else { base.prev(); } });
        },
        buildPagination: function() {
            var base = this;
            base.paginationWrapper = $("<div class=\"owl-pagination\"/>");
            base.owlControls.append(base.paginationWrapper);
            base.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(event) { event.preventDefault(); if (Number($(this).data("owl-page")) !== base.currentItem) { base.goTo(Number($(this).data("owl-page")), true); } });
        },
        updatePagination: function() {
            var base = this,
                counter, lastPage, lastItem, i, paginationButton, paginationButtonInner;
            if (base.options.pagination === false) { return false; }
            base.paginationWrapper.html("");
            counter = 0;
            lastPage = base.itemsAmount - base.itemsAmount % base.options.items;
            for (i = 0; i < base.itemsAmount; i += 1) {
                if (i % base.options.items === 0) {
                    counter += 1;
                    if (lastPage === i) { lastItem = base.itemsAmount - base.options.items; }
                    paginationButton = $("<div/>", { "class": "owl-page" });
                    paginationButtonInner = $("<span></span>", { "text": base.options.paginationNumbers === true ? counter : "", "class": base.options.paginationNumbers === true ? "owl-numbers" : "" });
                    paginationButton.append(paginationButtonInner);
                    paginationButton.data("owl-page", lastPage === i ? lastItem : i);
                    paginationButton.data("owl-roundPages", counter);
                    base.paginationWrapper.append(paginationButton);
                }
            }
            base.checkPagination();
        },
        checkPagination: function() {
            var base = this;
            if (base.options.pagination === false) { return false; }
            base.paginationWrapper.find(".owl-page").each(function() {
                if ($(this).data("owl-roundPages") === $(base.$owlItems[base.currentItem]).data("owl-roundPages")) {
                    base.paginationWrapper.find(".owl-page").removeClass("active");
                    $(this).addClass("active");
                }
            });
        },
        checkNavigation: function() {
            var base = this;
            if (base.options.navigation === false) { return false; }
            if (base.options.rewindNav === false) {
                if (base.currentItem === 0 && base.maximumItem === 0) {
                    base.buttonPrev.addClass("disabled");
                    base.buttonNext.addClass("disabled");
                } else if (base.currentItem === 0 && base.maximumItem !== 0) {
                    base.buttonPrev.addClass("disabled");
                    base.buttonNext.removeClass("disabled");
                } else if (base.currentItem === base.maximumItem) {
                    base.buttonPrev.removeClass("disabled");
                    base.buttonNext.addClass("disabled");
                } else if (base.currentItem !== 0 && base.currentItem !== base.maximumItem) {
                    base.buttonPrev.removeClass("disabled");
                    base.buttonNext.removeClass("disabled");
                }
            }
        },
        updateControls: function() {
            var base = this;
            base.updatePagination();
            base.checkNavigation();
            if (base.owlControls) { if (base.options.items >= base.itemsAmount) { base.owlControls.hide(); } else { base.owlControls.show(); } }
        },
        destroyControls: function() { var base = this; if (base.owlControls) { base.owlControls.remove(); } },
        next: function(speed) {
            var base = this;
            if (base.isTransition) { return false; }
            base.currentItem += base.options.scrollPerPage === true ? base.options.items : 1;
            if (base.currentItem > base.maximumItem + (base.options.scrollPerPage === true ? (base.options.items - 1) : 0)) {
                if (base.options.rewindNav === true) {
                    base.currentItem = 0;
                    speed = "rewind";
                } else { base.currentItem = base.maximumItem; return false; }
            }
            base.goTo(base.currentItem, speed);
        },
        prev: function(speed) {
            var base = this;
            if (base.isTransition) { return false; }
            if (base.options.scrollPerPage === true && base.currentItem > 0 && base.currentItem < base.options.items) { base.currentItem = 0; } else { base.currentItem -= base.options.scrollPerPage === true ? base.options.items : 1; }
            if (base.currentItem < 0) {
                if (base.options.rewindNav === true) {
                    base.currentItem = base.maximumItem;
                    speed = "rewind";
                } else { base.currentItem = 0; return false; }
            }
            base.goTo(base.currentItem, speed);
        },
        goTo: function(position, speed, drag) {
            var base = this,
                goToPixel;
            if (base.isTransition) { return false; }
            if (typeof base.options.beforeMove === "function") { base.options.beforeMove.apply(this, [base.$elem]); }
            if (position >= base.maximumItem) { position = base.maximumItem; } else if (position <= 0) { position = 0; }
            base.currentItem = base.owl.currentItem = position;
            if (base.options.transitionStyle !== false && drag !== "drag" && base.options.items === 1 && base.browser.support3d === true) {
                base.swapSpeed(0);
                if (base.browser.support3d === true) { base.transition3d(base.positionsInArray[position]); } else { base.css2slide(base.positionsInArray[position], 1); }
                base.afterGo();
                base.singleItemTransition();
                return false;
            }
            goToPixel = base.positionsInArray[position];
            if (base.browser.support3d === true) {
                base.isCss3Finish = false;
                if (speed === true) {
                    base.swapSpeed("paginationSpeed");
                    window.setTimeout(function() { base.isCss3Finish = true; }, base.options.paginationSpeed);
                } else if (speed === "rewind") {
                    base.swapSpeed(base.options.rewindSpeed);
                    window.setTimeout(function() { base.isCss3Finish = true; }, base.options.rewindSpeed);
                } else {
                    base.swapSpeed("slideSpeed");
                    window.setTimeout(function() { base.isCss3Finish = true; }, base.options.slideSpeed);
                }
                base.transition3d(goToPixel);
            } else { if (speed === true) { base.css2slide(goToPixel, base.options.paginationSpeed); } else if (speed === "rewind") { base.css2slide(goToPixel, base.options.rewindSpeed); } else { base.css2slide(goToPixel, base.options.slideSpeed); } }
            base.afterGo();
        },
        jumpTo: function(position) {
            var base = this;
            if (typeof base.options.beforeMove === "function") { base.options.beforeMove.apply(this, [base.$elem]); }
            if (position >= base.maximumItem || position === -1) { position = base.maximumItem; } else if (position <= 0) { position = 0; }
            base.swapSpeed(0);
            if (base.browser.support3d === true) { base.transition3d(base.positionsInArray[position]); } else { base.css2slide(base.positionsInArray[position], 1); }
            base.currentItem = base.owl.currentItem = position;
            base.afterGo();
        },
        afterGo: function() {
            var base = this;
            base.prevArr.push(base.currentItem);
            base.prevItem = base.owl.prevItem = base.prevArr[base.prevArr.length - 2];
            base.prevArr.shift(0);
            if (base.prevItem !== base.currentItem) {
                base.checkPagination();
                base.checkNavigation();
                base.eachMoveUpdate();
                if (base.options.autoPlay !== false) { base.checkAp(); }
            }
            if (typeof base.options.afterMove === "function" && base.prevItem !== base.currentItem) { base.options.afterMove.apply(this, [base.$elem]); }
        },
        stop: function() {
            var base = this;
            base.apStatus = "stop";
            window.clearInterval(base.autoPlayInterval);
        },
        checkAp: function() { var base = this; if (base.apStatus !== "stop") { base.play(); } },
        play: function() {
            var base = this;
            base.apStatus = "play";
            if (base.options.autoPlay === false) { return false; }
            window.clearInterval(base.autoPlayInterval);
            base.autoPlayInterval = window.setInterval(function() { base.next(true); }, base.options.autoPlay);
        },
        swapSpeed: function(action) { var base = this; if (action === "slideSpeed") { base.$owlWrapper.css(base.addCssSpeed(base.options.slideSpeed)); } else if (action === "paginationSpeed") { base.$owlWrapper.css(base.addCssSpeed(base.options.paginationSpeed)); } else if (typeof action !== "string") { base.$owlWrapper.css(base.addCssSpeed(action)); } },
        addCssSpeed: function(speed) { return { "-webkit-transition": "all " + speed + "ms ease", "-moz-transition": "all " + speed + "ms ease", "-o-transition": "all " + speed + "ms ease", "transition": "all " + speed + "ms ease" }; },
        removeTransition: function() { return { "-webkit-transition": "", "-moz-transition": "", "-o-transition": "", "transition": "" }; },
        doTranslate: function(pixels) { return { "-webkit-transform": "translate3d(" + pixels + "px, 0px, 0px)", "-moz-transform": "translate3d(" + pixels + "px, 0px, 0px)", "-o-transform": "translate3d(" + pixels + "px, 0px, 0px)", "-ms-transform": "translate3d(" + pixels + "px, 0px, 0px)", "transform": "translate3d(" + pixels + "px, 0px,0px)" }; },
        transition3d: function(value) {
            var base = this;
            base.$owlWrapper.css(base.doTranslate(value));
        },
        css2move: function(value) {
            var base = this;
            base.$owlWrapper.css({ "left": value });
        },
        css2slide: function(value, speed) {
            var base = this;
            base.isCssFinish = false;
            base.$owlWrapper.stop(true, true).animate({ "left": value }, { duration: speed || base.options.slideSpeed, complete: function() { base.isCssFinish = true; } });
        },
        checkBrowser: function() {
            var base = this,
                translate3D = "translate3d(0px, 0px, 0px)",
                tempElem = document.createElement("div"),
                regex, asSupport, support3d, isTouch;
            tempElem.style.cssText = "  -moz-transform:" + translate3D + "; -ms-transform:" + translate3D + "; -o-transform:" + translate3D + "; -webkit-transform:" + translate3D + "; transform:" + translate3D;
            regex = /translate3d\(0px, 0px, 0px\)/g;
            asSupport = tempElem.style.cssText.match(regex);
            support3d = (asSupport !== null && asSupport.length === 1);
            isTouch = "ontouchstart" in window || window.navigator.msMaxTouchPoints;
            base.browser = { "support3d": support3d, "isTouch": isTouch };
        },
        moveEvents: function() {
            var base = this;
            if (base.options.mouseDrag !== false || base.options.touchDrag !== false) {
                base.gestures();
                base.disabledEvents();
            }
        },
        eventTypes: function() {
            var base = this,
                types = ["s", "e", "x"];
            base.ev_types = {};
            if (base.options.mouseDrag === true && base.options.touchDrag === true) { types = ["touchstart.owl mousedown.owl", "touchmove.owl mousemove.owl", "touchend.owl touchcancel.owl mouseup.owl"]; } else if (base.options.mouseDrag === false && base.options.touchDrag === true) { types = ["touchstart.owl", "touchmove.owl", "touchend.owl touchcancel.owl"]; } else if (base.options.mouseDrag === true && base.options.touchDrag === false) { types = ["mousedown.owl", "mousemove.owl", "mouseup.owl"]; }
            base.ev_types.start = types[0];
            base.ev_types.move = types[1];
            base.ev_types.end = types[2];
        },
        disabledEvents: function() {
            var base = this;
            base.$elem.on("dragstart.owl", function(event) { event.preventDefault(); });
            base.$elem.on("mousedown.disableTextSelect", function(e) {
                if (e.which !== 3)
                    return $(e.target).is('input, textarea, select, option');
            });
        },
        gestures: function() {
            var base = this,
                locals = { offsetX: 0, offsetY: 0, baseElWidth: 0, relativePos: 0, position: null, minSwipe: null, maxSwipe: null, sliding: null, dargging: null, targetElement: null };
            base.isCssFinish = true;

            function getTouches(event) {
                if (event.touches !== undefined) { return { x: event.touches[0].pageX, y: event.touches[0].pageY }; }
                if (event.touches === undefined) {
                    if (event.pageX !== undefined) { return { x: event.pageX, y: event.pageY }; }
                    if (event.pageX === undefined) { return { x: event.clientX, y: event.clientY }; }
                }
            }

            function swapEvents(type) {
                if (type === "on") {
                    $(document).on(base.ev_types.move, dragMove);
                    $(document).on(base.ev_types.end, dragEnd);
                } else if (type === "off") {
                    $(document).off(base.ev_types.move);
                    $(document).off(base.ev_types.end);
                }
            }

            function dragStart(event) {
                var ev = event.originalEvent || event || window.event,
                    position;
                if (ev.which === 3) { return true; }
                if (base.itemsAmount <= base.options.items) { return; }
                if (base.isCssFinish === false && !base.options.dragBeforeAnimFinish) { return false; }
                if (base.isCss3Finish === false && !base.options.dragBeforeAnimFinish) { return false; }
                if (base.options.autoPlay !== false) { window.clearInterval(base.autoPlayInterval); }
                if (base.browser.isTouch !== true && !base.$owlWrapper.hasClass("grabbing")) { base.$owlWrapper.addClass("grabbing"); }
                base.newPosX = 0;
                base.newRelativeX = 0;
                $(this).css(base.removeTransition());
                position = $(this).position();
                locals.relativePos = position.left;
                locals.offsetX = getTouches(ev).x - position.left;
                locals.offsetY = getTouches(ev).y - position.top;
                swapEvents("on");
                locals.sliding = false;
                locals.targetElement = ev.target || ev.srcElement;
            }

            function dragMove(event) {
                var ev = event.originalEvent || event || window.event,
                    minSwipe, maxSwipe;
                base.newPosX = getTouches(ev).x - locals.offsetX;
                base.newPosY = getTouches(ev).y - locals.offsetY;
                base.newRelativeX = base.newPosX - locals.relativePos;
                if (typeof base.options.startDragging === "function" && locals.dragging !== true && base.newRelativeX !== 0) {
                    locals.dragging = true;
                    base.options.startDragging.apply(base, [base.$elem]);
                }
                if ((base.newRelativeX > 8 || base.newRelativeX < -8) && (base.browser.isTouch === true)) {
                    if (ev.preventDefault !== undefined) { ev.preventDefault(); } else { ev.returnValue = false; }
                    locals.sliding = true;
                }
                if ((base.newPosY > 10 || base.newPosY < -10) && locals.sliding === false) { $(document).off("touchmove.owl"); }
                minSwipe = function() { return base.newRelativeX / 5; };
                maxSwipe = function() { return base.maximumPixels + base.newRelativeX / 5; };
                base.newPosX = Math.max(Math.min(base.newPosX, minSwipe()), maxSwipe());
                if (base.browser.support3d === true) { base.transition3d(base.newPosX); } else { base.css2move(base.newPosX); }
            }

            function dragEnd(event) {
                var ev = event.originalEvent || event || window.event,
                    newPosition, handlers, owlStopEvent;
                ev.target = ev.target || ev.srcElement;
                locals.dragging = false;
                if (base.browser.isTouch !== true) { base.$owlWrapper.removeClass("grabbing"); }
                if (base.newRelativeX < 0) { base.dragDirection = base.owl.dragDirection = "left"; } else { base.dragDirection = base.owl.dragDirection = "right"; }
                if (base.newRelativeX !== 0) {
                    newPosition = base.getNewPosition();
                    base.goTo(newPosition, false, "drag");
                    if (locals.targetElement === ev.target && base.browser.isTouch !== true) {
                        $(ev.target).on("click.disable", function(ev) {
                            ev.stopImmediatePropagation();
                            ev.stopPropagation();
                            ev.preventDefault();
                            $(ev.target).off("click.disable");
                        });
                        handlers = $._data(ev.target, "events").click;
                        owlStopEvent = handlers.pop();
                        handlers.splice(0, 0, owlStopEvent);
                    }
                }
                swapEvents("off");
            }
            base.$elem.on(base.ev_types.start, ".owl-wrapper", dragStart);
        },
        getNewPosition: function() {
            var base = this,
                newPosition = base.closestItem();
            if (newPosition > base.maximumItem) {
                base.currentItem = base.maximumItem;
                newPosition = base.maximumItem;
            } else if (base.newPosX >= 0) {
                newPosition = 0;
                base.currentItem = 0;
            }
            return newPosition;
        },
        closestItem: function() {
            var base = this,
                array = base.options.scrollPerPage === true ? base.pagesInArray : base.positionsInArray,
                goal = base.newPosX,
                closest = null;
            $.each(array, function(i, v) {
                if (goal - (base.itemWidth / 20) > array[i + 1] && goal - (base.itemWidth / 20) < v && base.moveDirection() === "left") { closest = v; if (base.options.scrollPerPage === true) { base.currentItem = $.inArray(closest, base.positionsInArray); } else { base.currentItem = i; } } else if (goal + (base.itemWidth / 20) < v && goal + (base.itemWidth / 20) > (array[i + 1] || array[i] - base.itemWidth) && base.moveDirection() === "right") {
                    if (base.options.scrollPerPage === true) {
                        closest = array[i + 1] || array[array.length - 1];
                        base.currentItem = $.inArray(closest, base.positionsInArray);
                    } else {
                        closest = array[i + 1];
                        base.currentItem = i + 1;
                    }
                }
            });
            return base.currentItem;
        },
        moveDirection: function() {
            var base = this,
                direction;
            if (base.newRelativeX < 0) {
                direction = "right";
                base.playDirection = "next";
            } else {
                direction = "left";
                base.playDirection = "prev";
            }
            return direction;
        },
        customEvents: function() {
            var base = this;
            base.$elem.on("owl.next", function() { base.next(); });
            base.$elem.on("owl.prev", function() { base.prev(); });
            base.$elem.on("owl.play", function(event, speed) {
                base.options.autoPlay = speed;
                base.play();
                base.hoverStatus = "play";
            });
            base.$elem.on("owl.stop", function() {
                base.stop();
                base.hoverStatus = "stop";
            });
            base.$elem.on("owl.goTo", function(event, item) { base.goTo(item); });
            base.$elem.on("owl.jumpTo", function(event, item) { base.jumpTo(item); });
        },
        stopOnHover: function() {
            var base = this;
            if (base.options.stopOnHover === true && base.browser.isTouch !== true && base.options.autoPlay !== false) {
                base.$elem.on("mouseover", function() { base.stop(); });
                base.$elem.on("mouseout", function() { if (base.hoverStatus !== "stop") { base.play(); } });
            }
        },
        lazyLoad: function() {
            var base = this,
                i, $item, itemNumber, $lazyImg, follow;
            if (base.options.lazyLoad === false) { return false; }
            for (i = 0; i < base.itemsAmount; i += 1) {
                $item = $(base.$owlItems[i]);
                if ($item.data("owl-loaded") === "loaded") { continue; }
                itemNumber = $item.data("owl-item");
                $lazyImg = $item.find(".lazyOwl");
                if (typeof $lazyImg.data("src") !== "string") { $item.data("owl-loaded", "loaded"); continue; }
                if ($item.data("owl-loaded") === undefined) {
                    $lazyImg.hide();
                    $item.addClass("loading").data("owl-loaded", "checked");
                }
                if (base.options.lazyFollow === true) { follow = itemNumber >= base.currentItem; } else { follow = true; }
                if (follow && itemNumber < base.currentItem + base.options.items && $lazyImg.length) { base.lazyPreload($item, $lazyImg); }
            }
        },
        lazyPreload: function($item, $lazyImg) {
            var base = this,
                iterations = 0,
                isBackgroundImg;
            if ($lazyImg.prop("tagName") === "DIV") {
                $lazyImg.css("background-image", "url(" + $lazyImg.data("src") + ")");
                isBackgroundImg = true;
            } else { $lazyImg[0].src = $lazyImg.data("src"); }

            function showImage() {
                $item.data("owl-loaded", "loaded").removeClass("loading");
                $lazyImg.removeAttr("data-src");
                if (base.options.lazyEffect === "fade") { $lazyImg.fadeIn(400); } else { $lazyImg.show(); }
                if (typeof base.options.afterLazyLoad === "function") { base.options.afterLazyLoad.apply(this, [base.$elem]); }
            }

            function checkLazyImage() { iterations += 1; if (base.completeImg($lazyImg.get(0)) || isBackgroundImg === true) { showImage(); } else if (iterations <= 100) { window.setTimeout(checkLazyImage, 100); } else { showImage(); } }
            checkLazyImage();
        },
        autoHeight: function() {
            var base = this,
                $currentimg = $(base.$owlItems[base.currentItem]).find("img"),
                iterations;

            function addHeight() {
                var $currentItem = $(base.$owlItems[base.currentItem]).height();
                base.wrapperOuter.css("height", $currentItem + "px");
                if (!base.wrapperOuter.hasClass("autoHeight")) { window.setTimeout(function() { base.wrapperOuter.addClass("autoHeight"); }, 0); }
            }

            function checkImage() { iterations += 1; if (base.completeImg($currentimg.get(0))) { addHeight(); } else if (iterations <= 100) { window.setTimeout(checkImage, 100); } else { base.wrapperOuter.css("height", ""); } }
            if ($currentimg.get(0) !== undefined) {
                iterations = 0;
                checkImage();
            } else { addHeight(); }
        },
        completeImg: function(img) {
            var naturalWidthType;
            if (!img.complete) { return false; }
            naturalWidthType = typeof img.naturalWidth;
            if (naturalWidthType !== "undefined" && img.naturalWidth === 0) { return false; }
            return true;
        },
        onVisibleItems: function() {
            var base = this,
                i;
            if (base.options.addClassActive === true) { base.$owlItems.removeClass("active"); }
            base.visibleItems = [];
            for (i = base.currentItem; i < base.currentItem + base.options.items; i += 1) { base.visibleItems.push(i); if (base.options.addClassActive === true) { $(base.$owlItems[i]).addClass("active"); } }
            base.owl.visibleItems = base.visibleItems;
        },
        transitionTypes: function(className) {
            var base = this;
            base.outClass = "owl-" + className + "-out";
            base.inClass = "owl-" + className + "-in";
        },
        singleItemTransition: function() {
            var base = this,
                outClass = base.outClass,
                inClass = base.inClass,
                $currentItem = base.$owlItems.eq(base.currentItem),
                $prevItem = base.$owlItems.eq(base.prevItem),
                prevPos = Math.abs(base.positionsInArray[base.currentItem]) + base.positionsInArray[base.prevItem],
                origin = Math.abs(base.positionsInArray[base.currentItem]) + base.itemWidth / 2,
                animEnd = 'webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend';
            base.isTransition = true;
            base.$owlWrapper.addClass('owl-origin').css({ "-webkit-transform-origin": origin + "px", "-moz-perspective-origin": origin + "px", "perspective-origin": origin + "px" });

            function transStyles(prevPos) { return { "position": "relative", "left": prevPos + "px" }; }
            $prevItem.css(transStyles(prevPos, 10)).addClass(outClass).on(animEnd, function() {
                base.endPrev = true;
                $prevItem.off(animEnd);
                base.clearTransStyle($prevItem, outClass);
            });
            $currentItem.addClass(inClass).on(animEnd, function() {
                base.endCurrent = true;
                $currentItem.off(animEnd);
                base.clearTransStyle($currentItem, inClass);
            });
        },
        clearTransStyle: function(item, classToRemove) {
            var base = this;
            item.css({ "position": "", "left": "" }).removeClass(classToRemove);
            if (base.endPrev && base.endCurrent) {
                base.$owlWrapper.removeClass('owl-origin');
                base.endPrev = false;
                base.endCurrent = false;
                base.isTransition = false;
            }
        },
        owlStatus: function() {
            var base = this;
            base.owl = { "userOptions": base.userOptions, "baseElement": base.$elem, "userItems": base.$userItems, "owlItems": base.$owlItems, "currentItem": base.currentItem, "prevItem": base.prevItem, "visibleItems": base.visibleItems, "isTouch": base.browser.isTouch, "browser": base.browser, "dragDirection": base.dragDirection };
        },
        clearEvents: function() {
            var base = this;
            base.$elem.off(".owl owl mousedown.disableTextSelect");
            $(document).off(".owl owl");
            $(window).off("resize", base.resizer);
        },
        unWrap: function() {
            var base = this;
            if (base.$elem.children().length !== 0) {
                base.$owlWrapper.unwrap();
                base.$userItems.unwrap().unwrap();
                if (base.owlControls) { base.owlControls.remove(); }
            }
            base.clearEvents();
            base.$elem.attr("style", base.$elem.data("owl-originalStyles") || "").attr("class", base.$elem.data("owl-originalClasses"));
        },
        destroy: function() {
            var base = this;
            base.stop();
            window.clearInterval(base.checkVisible);
            base.unWrap();
            base.$elem.removeData();
        },
        reinit: function(newOptions) {
            var base = this,
                options = $.extend({}, base.userOptions, newOptions);
            base.unWrap();
            base.init(options, base.$elem);
        },
        addItem: function(htmlString, targetPosition) {
            var base = this,
                position;
            if (!htmlString) { return false; }
            if (base.$elem.children().length === 0) {
                base.$elem.append(htmlString);
                base.setVars();
                return false;
            }
            base.unWrap();
            if (targetPosition === undefined || targetPosition === -1) { position = -1; } else { position = targetPosition; }
            if (position >= base.$userItems.length || position === -1) { base.$userItems.eq(-1).after(htmlString); } else { base.$userItems.eq(position).before(htmlString); }
            base.setVars();
        },
        removeItem: function(targetPosition) {
            var base = this,
                position;
            if (base.$elem.children().length === 0) { return false; }
            if (targetPosition === undefined || targetPosition === -1) { position = -1; } else { position = targetPosition; }
            base.unWrap();
            base.$userItems.eq(position).remove();
            base.setVars();
        }
    };
    $.fn.owlCarousel = function(options) {
        return this.each(function() {
            if ($(this).data("owl-init") === true) { return false; }
            $(this).data("owl-init", true);
            var carousel = Object.create(Carousel);
            carousel.init(options, this);
            $.data(this, "owlCarousel", carousel);
        });
    };
    $.fn.owlCarousel.options = { items: 5, itemsCustom: false, itemsDesktop: [1199, 4], itemsDesktopSmall: [979, 3], itemsTablet: [768, 2], itemsTabletSmall: false, itemsMobile: [479, 1], singleItem: false, itemsScaleUp: false, slideSpeed: 200, paginationSpeed: 800, rewindSpeed: 1000, autoPlay: false, stopOnHover: false, navigation: false, navigationText: ["prev", "next"], rewindNav: true, scrollPerPage: false, pagination: true, paginationNumbers: false, responsive: true, responsiveRefreshRate: 200, responsiveBaseWidth: window, baseClass: "owl-carousel", theme: "owl-theme", lazyLoad: false, lazyFollow: true, lazyEffect: "fade", autoHeight: false, jsonPath: false, jsonSuccess: false, dragBeforeAnimFinish: true, mouseDrag: true, touchDrag: true, addClassActive: false, transitionStyle: false, beforeUpdate: false, afterUpdate: false, beforeInit: false, afterInit: false, beforeMove: false, afterMove: false, afterAction: false, startDragging: false, afterLazyLoad: false };
}(jQuery, window, document));
! function(a, b) {
    "use strict";

    function c() {
        if (!e) {
            e = !0;
            var a, c, d, f, g = -1 !== navigator.appVersion.indexOf("MSIE 10"),
                h = !!navigator.userAgent.match(/Trident.*rv:11\./),
                i = b.querySelectorAll("iframe.wp-embedded-content");
            for (c = 0; c < i.length; c++) { if (d = i[c], !d.getAttribute("data-secret")) f = Math.random().toString(36).substr(2, 10), d.src += "#?secret=" + f, d.setAttribute("data-secret", f); if (g || h) a = d.cloneNode(!0), a.removeAttribute("security"), d.parentNode.replaceChild(a, d) }
        }
    }
    var d = !1,
        e = !1;
    if (b.querySelector)
        if (a.addEventListener) d = !0;
    if (a.wp = a.wp || {}, !a.wp.receiveEmbedMessage)
        if (a.wp.receiveEmbedMessage = function(c) {
                var d = c.data;
                if (d)
                    if (d.secret || d.message || d.value)
                        if (!/[^a-zA-Z0-9]/.test(d.secret)) {
                            var e, f, g, h, i, j = b.querySelectorAll('iframe[data-secret="' + d.secret + '"]'),
                                k = b.querySelectorAll('blockquote[data-secret="' + d.secret + '"]');
                            for (e = 0; e < k.length; e++) k[e].style.display = "none";
                            for (e = 0; e < j.length; e++)
                                if (f = j[e], c.source === f.contentWindow) {
                                    if (f.removeAttribute("style"), "height" === d.message) {
                                        if (g = parseInt(d.value, 10), g > 1e3) g = 1e3;
                                        else if (~~g < 200) g = 200;
                                        f.height = g
                                    }
                                    if ("link" === d.message)
                                        if (h = b.createElement("a"), i = b.createElement("a"), h.href = f.getAttribute("src"), i.href = d.value, i.host === h.host)
                                            if (b.activeElement === f) a.top.location.href = d.value
                                } else;
                        }
            }, d) a.addEventListener("message", a.wp.receiveEmbedMessage, !1), b.addEventListener("DOMContentLoaded", c, !1), a.addEventListener("load", c, !1)
}(window, document);

! function(a) { "object" == typeof module && module.exports ? module.exports = a(require("jquery"), window, document) : "function" == typeof define && define.amd ? define(["jquery"], function(b) { a(b, window, document) }) : a(jQuery, window, document) }(function(a, b, c, d) {
    "use strict";

    function e(a, b) { if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function") }

    function f(a, b) {
        for (var c = 0; c < b.length; c++) {
            var d = b[c];
            d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), Object.defineProperty(a, d.key, d)
        }
    }

    function g(a, b, c) { return b && f(a.prototype, b), c && f(a, c), a }
    for (var h = [
            ["Afghanistan (â€«Ø§ÙØºØ§Ù†Ø³ØªØ§Ù†â€¬â€Ž)", "af", "93"],
            ["Albania (ShqipÃ«ri)", "al", "355"],
            ["Algeria (â€«Ø§Ù„Ø¬Ø²Ø§Ø¦Ø±â€¬â€Ž)", "dz", "213"],
            ["American Samoa", "as", "1684"],
            ["Andorra", "ad", "376"],
            ["Angola", "ao", "244"],
            ["Anguilla", "ai", "1264"],
            ["Antigua and Barbuda", "ag", "1268"],
            ["Argentina", "ar", "54"],
            ["Armenia (Õ€Õ¡ÕµÕ¡Õ½Õ¿Õ¡Õ¶)", "am", "374"],
            ["Aruba", "aw", "297"],
            ["Australia", "au", "61", 0],
            ["Austria (Ã–sterreich)", "at", "43"],
            ["Azerbaijan (AzÉ™rbaycan)", "az", "994"],
            ["Bahamas", "bs", "1242"],
            ["Bahrain (â€«Ø§Ù„Ø¨Ø­Ø±ÙŠÙ†â€¬â€Ž)", "bh", "973"],
            ["Bangladesh (à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶)", "bd", "880"],
            ["Barbados", "bb", "1246"],
            ["Belarus (Ð‘ÐµÐ»Ð°Ñ€ÑƒÑÑŒ)", "by", "375"],
            ["Belgium (BelgiÃ«)", "be", "32"],
            ["Belize", "bz", "501"],
            ["Benin (BÃ©nin)", "bj", "229"],
            ["Bermuda", "bm", "1441"],
            ["Bhutan (à½ à½–à¾²à½´à½‚)", "bt", "975"],
            ["Bolivia", "bo", "591"],
            ["Bosnia and Herzegovina (Ð‘Ð¾ÑÐ½Ð° Ð¸ Ð¥ÐµÑ€Ñ†ÐµÐ³Ð¾Ð²Ð¸Ð½Ð°)", "ba", "387"],
            ["Botswana", "bw", "267"],
            ["Brazil (Brasil)", "br", "55"],
            ["British Indian Ocean Territory", "io", "246"],
            ["British Virgin Islands", "vg", "1284"],
            ["Brunei", "bn", "673"],
            ["Bulgaria (Ð‘ÑŠÐ»Ð³Ð°Ñ€Ð¸Ñ)", "bg", "359"],
            ["Burkina Faso", "bf", "226"],
            ["Burundi (Uburundi)", "bi", "257"],
            ["Cambodia (áž€áž˜áŸ’áž–áž»áž‡áž¶)", "kh", "855"],
            ["Cameroon (Cameroun)", "cm", "237"],
            ["Canada", "ca", "1", 1, ["204", "226", "236", "249", "250", "289", "306", "343", "365", "387", "403", "416", "418", "431", "437", "438", "450", "506", "514", "519", "548", "579", "581", "587", "604", "613", "639", "647", "672", "705", "709", "742", "778", "780", "782", "807", "819", "825", "867", "873", "902", "905"]],
            ["Cape Verde (Kabu Verdi)", "cv", "238"],
            ["Caribbean Netherlands", "bq", "599", 1],
            ["Cayman Islands", "ky", "1345"],
            ["Central African Republic (RÃ©publique centrafricaine)", "cf", "236"],
            ["Chad (Tchad)", "td", "235"],
            ["Chile", "cl", "56"],
            ["China (ä¸­å›½)", "cn", "86"],
            ["Christmas Island", "cx", "61", 2],
            ["Cocos (Keeling) Islands", "cc", "61", 1],
            ["Colombia", "co", "57"],
            ["Comoros (â€«Ø¬Ø²Ø± Ø§Ù„Ù‚Ù…Ø±â€¬â€Ž)", "km", "269"],
            ["Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)", "cd", "243"],
            ["Congo (Republic) (Congo-Brazzaville)", "cg", "242"],
            ["Cook Islands", "ck", "682"],
            ["Costa Rica", "cr", "506"],
            ["CÃ´te dâ€™Ivoire", "ci", "225"],
            ["Croatia (Hrvatska)", "hr", "385"],
            ["Cuba", "cu", "53"],
            ["CuraÃ§ao", "cw", "599", 0],
            ["Cyprus (ÎšÏÏ€ÏÎ¿Ï‚)", "cy", "357"],
            ["Czech Republic (ÄŒeskÃ¡ republika)", "cz", "420"],
            ["Denmark (Danmark)", "dk", "45"],
            ["Djibouti", "dj", "253"],
            ["Dominica", "dm", "1767"],
            ["Dominican Republic (RepÃºblica Dominicana)", "do", "1", 2, ["809", "829", "849"]],
            ["Ecuador", "ec", "593"],
            ["Egypt (â€«Ù…ØµØ±â€¬â€Ž)", "eg", "20"],
            ["El Salvador", "sv", "503"],
            ["Equatorial Guinea (Guinea Ecuatorial)", "gq", "240"],
            ["Eritrea", "er", "291"],
            ["Estonia (Eesti)", "ee", "372"],
            ["Ethiopia", "et", "251"],
            ["Falkland Islands (Islas Malvinas)", "fk", "500"],
            ["Faroe Islands (FÃ¸royar)", "fo", "298"],
            ["Fiji", "fj", "679"],
            ["Finland (Suomi)", "fi", "358", 0],
            ["France", "fr", "33"],
            ["French Guiana (Guyane franÃ§aise)", "gf", "594"],
            ["French Polynesia (PolynÃ©sie franÃ§aise)", "pf", "689"],
            ["Gabon", "ga", "241"],
            ["Gambia", "gm", "220"],
            ["Georgia (áƒ¡áƒáƒ¥áƒáƒ áƒ—áƒ•áƒ”áƒšáƒ)", "ge", "995"],
            ["Germany (Deutschland)", "de", "49"],
            ["Ghana (Gaana)", "gh", "233"],
            ["Gibraltar", "gi", "350"],
            ["Greece (Î•Î»Î»Î¬Î´Î±)", "gr", "30"],
            ["Greenland (Kalaallit Nunaat)", "gl", "299"],
            ["Grenada", "gd", "1473"],
            ["Guadeloupe", "gp", "590", 0],
            ["Guam", "gu", "1671"],
            ["Guatemala", "gt", "502"],
            ["Guernsey", "gg", "44", 1],
            ["Guinea (GuinÃ©e)", "gn", "224"],
            ["Guinea-Bissau (GuinÃ© Bissau)", "gw", "245"],
            ["Guyana", "gy", "592"],
            ["Haiti", "ht", "509"],
            ["Honduras", "hn", "504"],
            ["Hong Kong (é¦™æ¸¯)", "hk", "852"],
            ["Hungary (MagyarorszÃ¡g)", "hu", "36"],
            ["Iceland (Ãsland)", "is", "354"],
            ["India (à¤­à¤¾à¤°à¤¤)", "in", "91"],
            ["Indonesia", "id", "62"],
            ["Iran (â€«Ø§ÛŒØ±Ø§Ù†â€¬â€Ž)", "ir", "98"],
            ["Iraq (â€«Ø§Ù„Ø¹Ø±Ø§Ù‚â€¬â€Ž)", "iq", "964"],
            ["Ireland", "ie", "353"],
            ["Isle of Man", "im", "44", 2],
            ["Israel (â€«×™×©×¨××œâ€¬â€Ž)", "il", "972"],
            ["Italy (Italia)", "it", "39", 0],
            ["Jamaica", "jm", "1", 4, ["876", "658"]],
            ["Japan (æ—¥æœ¬)", "jp", "81"],
            ["Jersey", "je", "44", 3],
            ["Jordan (â€«Ø§Ù„Ø£Ø±Ø¯Ù†â€¬â€Ž)", "jo", "962"],
            ["Kazakhstan (ÐšÐ°Ð·Ð°Ñ…ÑÑ‚Ð°Ð½)", "kz", "7", 1],
            ["Kenya", "ke", "254"],
            ["Kiribati", "ki", "686"],
            ["Kosovo", "xk", "383"],
            ["Kuwait (â€«Ø§Ù„ÙƒÙˆÙŠØªâ€¬â€Ž)", "kw", "965"],
            ["Kyrgyzstan (ÐšÑ‹Ñ€Ð³Ñ‹Ð·ÑÑ‚Ð°Ð½)", "kg", "996"],
            ["Laos (àº¥àº²àº§)", "la", "856"],
            ["Latvia (Latvija)", "lv", "371"],
            ["Lebanon (â€«Ù„Ø¨Ù†Ø§Ù†â€¬â€Ž)", "lb", "961"],
            ["Lesotho", "ls", "266"],
            ["Liberia", "lr", "231"],
            ["Libya (â€«Ù„ÙŠØ¨ÙŠØ§â€¬â€Ž)", "ly", "218"],
            ["Liechtenstein", "li", "423"],
            ["Lithuania (Lietuva)", "lt", "370"],
            ["Luxembourg", "lu", "352"],
            ["Macau (æ¾³é–€)", "mo", "853"],
            ["Macedonia (FYROM) (ÐœÐ°ÐºÐµÐ´Ð¾Ð½Ð¸Ñ˜Ð°)", "mk", "389"],
            ["Madagascar (Madagasikara)", "mg", "261"],
            ["Malawi", "mw", "265"],
            ["Malaysia", "my", "60"],
            ["Maldives", "mv", "960"],
            ["Mali", "ml", "223"],
            ["Malta", "mt", "356"],
            ["Marshall Islands", "mh", "692"],
            ["Martinique", "mq", "596"],
            ["Mauritania (â€«Ù…ÙˆØ±ÙŠØªØ§Ù†ÙŠØ§â€¬â€Ž)", "mr", "222"],
            ["Mauritius (Moris)", "mu", "230"],
            ["Mayotte", "yt", "262", 1],
            ["Mexico (MÃ©xico)", "mx", "52"],
            ["Micronesia", "fm", "691"],
            ["Moldova (Republica Moldova)", "md", "373"],
            ["Monaco", "mc", "377"],
            ["Mongolia (ÐœÐ¾Ð½Ð³Ð¾Ð»)", "mn", "976"],
            ["Montenegro (Crna Gora)", "me", "382"],
            ["Montserrat", "ms", "1664"],
            ["Morocco (â€«Ø§Ù„Ù…ØºØ±Ø¨â€¬â€Ž)", "ma", "212", 0],
            ["Mozambique (MoÃ§ambique)", "mz", "258"],
            ["Myanmar (Burma) (á€™á€¼á€”á€ºá€™á€¬)", "mm", "95"],
            ["Namibia (NamibiÃ«)", "na", "264"],
            ["Nauru", "nr", "674"],
            ["Nepal (à¤¨à¥‡à¤ªà¤¾à¤²)", "np", "977"],
            ["Netherlands (Nederland)", "nl", "31"],
            ["New Caledonia (Nouvelle-CalÃ©donie)", "nc", "687"],
            ["New Zealand", "nz", "64"],
            ["Nicaragua", "ni", "505"],
            ["Niger (Nijar)", "ne", "227"],
            ["Nigeria", "ng", "234"],
            ["Niue", "nu", "683"],
            ["Norfolk Island", "nf", "672"],
            ["North Korea (ì¡°ì„  ë¯¼ì£¼ì£¼ì˜ ì¸ë¯¼ ê³µí™”êµ­)", "kp", "850"],
            ["Northern Mariana Islands", "mp", "1670"],
            ["Norway (Norge)", "no", "47", 0],
            ["Oman (â€«Ø¹ÙÙ…Ø§Ù†â€¬â€Ž)", "om", "968"],
            ["Pakistan (â€«Ù¾Ø§Ú©Ø³ØªØ§Ù†â€¬â€Ž)", "pk", "92"],
            ["Palau", "pw", "680"],
            ["Palestine (â€«ÙÙ„Ø³Ø·ÙŠÙ†â€¬â€Ž)", "ps", "970"],
            ["Panama (PanamÃ¡)", "pa", "507"],
            ["Papua New Guinea", "pg", "675"],
            ["Paraguay", "py", "595"],
            ["Peru (PerÃº)", "pe", "51"],
            ["Philippines", "ph", "63"],
            ["Poland (Polska)", "pl", "48"],
            ["Portugal", "pt", "351"],
            ["Puerto Rico", "pr", "1", 3, ["787", "939"]],
            ["Qatar (â€«Ù‚Ø·Ø±â€¬â€Ž)", "qa", "974"],
            ["RÃ©union (La RÃ©union)", "re", "262", 0],
            ["Romania (RomÃ¢nia)", "ro", "40"],
            ["Russia (Ð Ð¾ÑÑÐ¸Ñ)", "ru", "7", 0],
            ["Rwanda", "rw", "250"],
            ["Saint BarthÃ©lemy", "bl", "590", 1],
            ["Saint Helena", "sh", "290"],
            ["Saint Kitts and Nevis", "kn", "1869"],
            ["Saint Lucia", "lc", "1758"],
            ["Saint Martin (Saint-Martin (partie franÃ§aise))", "mf", "590", 2],
            ["Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)", "pm", "508"],
            ["Saint Vincent and the Grenadines", "vc", "1784"],
            ["Samoa", "ws", "685"],
            ["San Marino", "sm", "378"],
            ["SÃ£o TomÃ© and PrÃ­ncipe (SÃ£o TomÃ© e PrÃ­ncipe)", "st", "239"],
            ["Saudi Arabia (â€«Ø§Ù„Ù…Ù…Ù„ÙƒØ© Ø§Ù„Ø¹Ø±Ø¨ÙŠØ© Ø§Ù„Ø³Ø¹ÙˆØ¯ÙŠØ©â€¬â€Ž)", "sa", "966"],
            ["Senegal (SÃ©nÃ©gal)", "sn", "221"],
            ["Serbia (Ð¡Ñ€Ð±Ð¸Ñ˜Ð°)", "rs", "381"],
            ["Seychelles", "sc", "248"],
            ["Sierra Leone", "sl", "232"],
            ["Singapore", "sg", "65"],
            ["Sint Maarten", "sx", "1721"],
            ["Slovakia (Slovensko)", "sk", "421"],
            ["Slovenia (Slovenija)", "si", "386"],
            ["Solomon Islands", "sb", "677"],
            ["Somalia (Soomaaliya)", "so", "252"],
            ["South Africa", "za", "27"],
            ["South Korea (ëŒ€í•œë¯¼êµ­)", "kr", "82"],
            ["South Sudan (â€«Ø¬Ù†ÙˆØ¨ Ø§Ù„Ø³ÙˆØ¯Ø§Ù†â€¬â€Ž)", "ss", "211"],
            ["Spain (EspaÃ±a)", "es", "34"],
            ["Sri Lanka (à·à·Šâ€à¶»à·“ à¶½à¶‚à¶šà·à·€)", "lk", "94"],
            ["Sudan (â€«Ø§Ù„Ø³ÙˆØ¯Ø§Ù†â€¬â€Ž)", "sd", "249"],
            ["Suriname", "sr", "597"],
            ["Svalbard and Jan Mayen", "sj", "47", 1],
            ["Swaziland", "sz", "268"],
            ["Sweden (Sverige)", "se", "46"],
            ["Switzerland (Schweiz)", "ch", "41"],
            ["Syria (â€«Ø³ÙˆØ±ÙŠØ§â€¬â€Ž)", "sy", "963"],
            ["Taiwan (å°ç£)", "tw", "886"],
            ["Tajikistan", "tj", "992"],
            ["Tanzania", "tz", "255"],
            ["Thailand (à¹„à¸—à¸¢)", "th", "66"],
            ["Timor-Leste", "tl", "670"],
            ["Togo", "tg", "228"],
            ["Tokelau", "tk", "690"],
            ["Tonga", "to", "676"],
            ["Trinidad and Tobago", "tt", "1868"],
            ["Tunisia (â€«ØªÙˆÙ†Ø³â€¬â€Ž)", "tn", "216"],
            ["Turkey (TÃ¼rkiye)", "tr", "90"],
            ["Turkmenistan", "tm", "993"],
            ["Turks and Caicos Islands", "tc", "1649"],
            ["Tuvalu", "tv", "688"],
            ["U.S. Virgin Islands", "vi", "1340"],
            ["Uganda", "ug", "256"],
            ["Ukraine (Ð£ÐºÑ€Ð°Ñ—Ð½Ð°)", "ua", "380"],
            ["United Arab Emirates (â€«Ø§Ù„Ø¥Ù…Ø§Ø±Ø§Øª Ø§Ù„Ø¹Ø±Ø¨ÙŠØ© Ø§Ù„Ù…ØªØ­Ø¯Ø©â€¬â€Ž)", "ae", "971"],
            ["United Kingdom", "gb", "44", 0],
            ["United States", "us", "1", 0],
            ["Uruguay", "uy", "598"],
            ["Uzbekistan (OÊ»zbekiston)", "uz", "998"],
            ["Vanuatu", "vu", "678"],
            ["Vatican City (CittÃ  del Vaticano)", "va", "39", 1],
            ["Venezuela", "ve", "58"],
            ["Vietnam (Viá»‡t Nam)", "vn", "84"],
            ["Wallis and Futuna (Wallis-et-Futuna)", "wf", "681"],
            ["Western Sahara (â€«Ø§Ù„ØµØ­Ø±Ø§Ø¡ Ø§Ù„ØºØ±Ø¨ÙŠØ©â€¬â€Ž)", "eh", "212", 1],
            ["Yemen (â€«Ø§Ù„ÙŠÙ…Ù†â€¬â€Ž)", "ye", "967"],
            ["Zambia", "zm", "260"],
            ["Zimbabwe", "zw", "263"],
            ["Ã…land Islands", "ax", "358", 1]
        ], i = 0; i < h.length; i++) {
        var j = h[i];
        h[i] = { name: j[0], iso2: j[1], dialCode: j[2], priority: j[3] || 0, areaCodes: j[4] || null }
    }
    b.intlTelInputGlobals = { getInstance: function(a) { var c = a.getAttribute("data-intl-tel-input-id"); return b.intlTelInputGlobals.instances[c] }, instances: {} };
    var k = 0,
        l = { allowDropdown: !0, autoHideDialCode: !0, autoPlaceholder: "polite", customContainer: "", customPlaceholder: null, dropdownContainer: null, excludeCountries: [], formatOnDisplay: !0, geoIpLookup: null, hiddenInput: "", initialCountry: "", localizedCountries: null, nationalMode: !0, onlyCountries: [], placeholderNumberType: "MOBILE", preferredCountries: ["us", "gb"], separateDialCode: !1, utilsScript: "" },
        m = ["800", "822", "833", "844", "855", "866", "877", "880", "881", "882", "883", "884", "885", "886", "887", "888", "889"];
    b.addEventListener("load", function() { b.intlTelInputGlobals.windowLoaded = !0 });
    var n = function(a, b) { for (var c = Object.keys(a), d = 0; d < c.length; d++) b(c[d], a[c[d]]) },
        o = function(a) { n(b.intlTelInputGlobals.instances, function(c) { b.intlTelInputGlobals.instances[c][a]() }) },
        p = function() {
            function a(b, c) {
                var d = this;
                e(this, a), this.id = k++, this.a = b, this.b = null, this.c = null;
                var f = c || {};
                this.d = {}, n(l, function(a, b) { d.d[a] = f.hasOwnProperty(a) ? f[a] : b }), this.e = Boolean(b.getAttribute("placeholder"))
            }
            return g(a, [{
                key: "_init",
                value: function() {
                    var a = this;
                    if (this.d.nationalMode && (this.d.autoHideDialCode = !1), this.d.separateDialCode && (this.d.autoHideDialCode = this.d.nationalMode = !1), this.g = /Android.+Mobile|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent), this.g && (c.body.classList.add("iti-mobile"), this.d.dropdownContainer || (this.d.dropdownContainer = c.body)), "undefined" != typeof Promise) {
                        var b = new Promise(function(b, c) { a.h = b, a.i = c }),
                            d = new Promise(function(b, c) { a.i0 = b, a.i1 = c });
                        this.promise = Promise.all([b, d])
                    } else this.h = this.i = function() {}, this.i0 = this.i1 = function() {};
                    this.s = {}, this._b(), this._f(), this._h(), this._i(), this._i3()
                }
            }, { key: "_b", value: function() { this._d(), this._d2(), this._e(), this.d.localizedCountries && this._d0(), (this.d.onlyCountries.length || this.d.localizedCountries) && this.p.sort(this._d1) } }, {
                key: "_c",
                value: function(a, b, c) {
                    this.q.hasOwnProperty(b) || (this.q[b] = []);
                    var d = c || 0;
                    this.q[b][d] = a
                }
            }, {
                key: "_d",
                value: function() {
                    if (this.d.onlyCountries.length) {
                        var a = this.d.onlyCountries.map(function(a) { return a.toLowerCase() });
                        this.p = h.filter(function(b) { return a.indexOf(b.iso2) > -1 })
                    } else if (this.d.excludeCountries.length) {
                        var b = this.d.excludeCountries.map(function(a) { return a.toLowerCase() });
                        this.p = h.filter(function(a) { return -1 === b.indexOf(a.iso2) })
                    } else this.p = h
                }
            }, {
                key: "_d0",
                value: function() {
                    for (var a = 0; a < this.p.length; a++) {
                        var b = this.p[a].iso2.toLowerCase();
                        this.d.localizedCountries.hasOwnProperty(b) && (this.p[a].name = this.d.localizedCountries[b])
                    }
                }
            }, { key: "_d1", value: function(a, b) { return a.name.localeCompare(b.name) } }, {
                key: "_d2",
                value: function() {
                    this.q = {};
                    for (var a = 0; a < this.p.length; a++) {
                        var b = this.p[a];
                        if (this._c(b.iso2, b.dialCode, b.priority), b.areaCodes)
                            for (var c = 0; c < b.areaCodes.length; c++) this._c(b.iso2, b.dialCode + b.areaCodes[c])
                    }
                }
            }, {
                key: "_e",
                value: function() {
                    this.preferredCountries = [];
                    for (var a = 0; a < this.d.preferredCountries.length; a++) {
                        var b = this.d.preferredCountries[a].toLowerCase(),
                            c = this._y(b, !1, !0);
                        c && this.preferredCountries.push(c)
                    }
                }
            }, { key: "_e2", value: function(a, b, d) { var e = c.createElement(a); return b && n(b, function(a, b) { return e.setAttribute(a, b) }), d && d.appendChild(e), e } }, {
                key: "_f",
                value: function() {
                    this.a.setAttribute("autocomplete", "off");
                    var a = "intl-tel-input";
                    this.d.allowDropdown && (a += " allow-dropdown"), this.d.separateDialCode && (a += " separate-dial-code"), this.d.customContainer && (a += " ", a += this.d.customContainer);
                    var b = this._e2("div", { "class": a });
                    if (this.a.parentNode.insertBefore(b, this.a), this.k = this._e2("div", { "class": "flag-container" }, b), b.appendChild(this.a), this.selectedFlag = this._e2("div", { "class": "selected-flag", role: "combobox", "aria-owns": "country-listbox" }, this.k), this.l = this._e2("div", { "class": "iti-flag" }, this.selectedFlag), this.d.separateDialCode && (this.t = this._e2("div", { "class": "selected-dial-code" }, this.selectedFlag)), this.d.allowDropdown && (this.selectedFlag.setAttribute("tabindex", "0"), this.u = this._e2("div", { "class": "iti-arrow" }, this.selectedFlag), this.m = this._e2("ul", { "class": "country-list hide", id: "country-listbox", "aria-expanded": "false", role: "listbox" }), this.preferredCountries.length && (this._g(this.preferredCountries, "preferred"), this._e2("li", { "class": "divider", role: "separator", "aria-disabled": "true" }, this.m)), this._g(this.p, "standard"), this.d.dropdownContainer ? (this.dropdown = this._e2("div", { "class": "intl-tel-input iti-container" }), this.dropdown.appendChild(this.m)) : this.k.appendChild(this.m)), this.d.hiddenInput) {
                        var c = this.d.hiddenInput,
                            d = this.a.getAttribute("name");
                        if (d) { var e = d.lastIndexOf("["); - 1 !== e && (c = "".concat(d.substr(0, e), "[").concat(c, "]")) }
                        this.hiddenInput = this._e2("input", { type: "hidden", name: c }), b.appendChild(this.hiddenInput)
                    }
                }
            }, {
                key: "_g",
                value: function(a, b) {
                    for (var c = "", d = 0; d < a.length; d++) {
                        var e = a[d];
                        c += "<li class='country ".concat(b, "' id='iti-item-").concat(e.iso2, "' role='option' data-dial-code='").concat(e.dialCode, "' data-country-code='").concat(e.iso2, "'>"), c += "<div class='flag-box'><div class='iti-flag ".concat(e.iso2, "'></div></div>"), c += "<span class='country-name'>".concat(e.name, "</span>"), c += "<span class='dial-code'>+".concat(e.dialCode, "</span>"), c += "</li>"
                    }
                    this.m.insertAdjacentHTML("beforeend", c)
                }
            }, {
                key: "_h",
                value: function() {
                    var a = this.a.value,
                        b = this._5(a),
                        c = this._w(a),
                        d = this.d,
                        e = d.initialCountry,
                        f = d.nationalMode,
                        g = d.autoHideDialCode,
                        h = d.separateDialCode;
                    b && !c ? this._v(a) : "auto" !== e && (e ? this._z(e.toLowerCase()) : b && c ? this._z("us") : (this.j = this.preferredCountries.length ? this.preferredCountries[0].iso2 : this.p[0].iso2, a || this._z(this.j)), a || f || g || h || (this.a.value = "+".concat(this.s.dialCode))), a && this._u(a)
                }
            }, { key: "_i", value: function() { this._j(), this.d.autoHideDialCode && this._l(), this.d.allowDropdown && this._i2(), this.hiddenInput && this._i0() } }, {
                key: "_i0",
                value: function() {
                    var a = this;
                    this._a14 = function() { a.hiddenInput.value = a.getNumber() }, this.a.form && this.a.form.addEventListener("submit", this._a14)
                }
            }, { key: "_i1", value: function() { for (var a = this.a; a && "LABEL" !== a.tagName;) a = a.parentNode; return a } }, {
                key: "_i2",
                value: function() {
                    var a = this;
                    this._a9 = function(b) { a.m.classList.contains("hide") ? a.a.focus() : b.preventDefault() };
                    var b = this._i1();
                    b && b.addEventListener("click", this._a9), this._a10 = function() {!a.m.classList.contains("hide") || a.a.disabled || a.a.readOnly || a._n() }, this.selectedFlag.addEventListener("click", this._a10), this._a11 = function(b) { a.m.classList.contains("hide") && -1 !== ["ArrowUp", "ArrowDown", " ", "Enter"].indexOf(b.key) && (b.preventDefault(), b.stopPropagation(), a._n()), "Tab" === b.key && a._2() }, this.k.addEventListener("keydown", this._a11)
                }
            }, {
                key: "_i3",
                value: function() {
                    var a = this;
                    this.d.utilsScript && !b.intlTelInputUtils ? b.intlTelInputGlobals.windowLoaded ? b.intlTelInputGlobals.loadUtils(this.d.utilsScript) : b.addEventListener("load", function() { b.intlTelInputGlobals.loadUtils(a.d.utilsScript) }) : this.i0(), "auto" === this.d.initialCountry ? this._i4() : this.h()
                }
            }, { key: "_i4", value: function() { b.intlTelInputGlobals.autoCountry ? this.handleAutoCountry() : b.intlTelInputGlobals.startedLoadingAutoCountry || (b.intlTelInputGlobals.startedLoadingAutoCountry = !0, "function" == typeof this.d.geoIpLookup && this.d.geoIpLookup(function(a) { b.intlTelInputGlobals.autoCountry = a.toLowerCase(), setTimeout(function() { return o("handleAutoCountry") }) }, function() { return o("rejectAutoCountryPromise") })) } }, {
                key: "_j",
                value: function() {
                    var a = this;
                    this._a12 = function() { a._v(a.a.value) && a._8() }, this.a.addEventListener("keyup", this._a12), this._a13 = function() { setTimeout(a._a12) }, this.a.addEventListener("cut", this._a13), this.a.addEventListener("paste", this._a13)
                }
            }, { key: "_j2", value: function(a) { var b = this.a.getAttribute("maxlength"); return b && a.length > b ? a.substr(0, b) : a } }, {
                key: "_l",
                value: function() {
                    var a = this;
                    this._a8 = function() { a._l2() }, this.a.form && this.a.form.addEventListener("submit", this._a8), this.a.addEventListener("blur", this._a8)
                }
            }, {
                key: "_l2",
                value: function() {
                    if ("+" === this.a.value.charAt(0)) {
                        var a = this._m(this.a.value);
                        a && this.s.dialCode !== a || (this.a.value = "")
                    }
                }
            }, { key: "_m", value: function(a) { return a.replace(/\D/g, "") } }, {
                key: "_m2",
                value: function(a) {
                    var b = c.createEvent("Event");
                    b.initEvent(a, !0, !0), this.a.dispatchEvent(b)
                }
            }, { key: "_n", value: function() { this.m.classList.remove("hide"), this.m.setAttribute("aria-expanded", "true"), this._o(), this.b && (this._x(this.b), this._3(this.b)), this._p(), this.u.classList.add("up"), this._m2("open:countrydropdown") } }, { key: "_n2", value: function(a, b, c) { c && !a.classList.contains(b) ? a.classList.add(b) : !c && a.classList.contains(b) && a.classList.remove(b) } }, {
                key: "_o",
                value: function() {
                    var a = this;
                    if (this.d.dropdownContainer && this.d.dropdownContainer.appendChild(this.dropdown), !this.g) {
                        var d = this.a.getBoundingClientRect(),
                            e = b.pageYOffset || c.documentElement.scrollTop,
                            f = d.top + e,
                            g = this.m.offsetHeight,
                            h = f + this.a.offsetHeight + g < e + b.innerHeight,
                            i = f - g > e;
                        if (this._n2(this.m, "dropup", !h && i), this.d.dropdownContainer) {
                            var j = !h && i ? 0 : this.a.offsetHeight;
                            this.dropdown.style.top = "".concat(f + j, "px"), this.dropdown.style.left = "".concat(d.left + c.body.scrollLeft, "px"), this._a4 = function() { return a._2() }, b.addEventListener("scroll", this._a4)
                        }
                    }
                }
            }, { key: "_o2", value: function(a) { for (var b = a; b && b !== this.m && !b.classList.contains("country");) b = b.parentNode; return b === this.m ? null : b } }, {
                key: "_p",
                value: function() {
                    var a = this;
                    this._a0 = function(b) {
                        var c = a._o2(b.target);
                        c && a._x(c)
                    }, this.m.addEventListener("mouseover", this._a0), this._a1 = function(b) {
                        var c = a._o2(b.target);
                        c && a._1(c)
                    }, this.m.addEventListener("click", this._a1);
                    var b = !0;
                    this._a2 = function() { b || a._2(), b = !1 }, c.documentElement.addEventListener("click", this._a2);
                    var d = "",
                        e = null;
                    this._a3 = function(b) { b.preventDefault(), "ArrowUp" === b.key || "ArrowDown" === b.key ? a._q(b.key) : "Enter" === b.key ? a._r() : "Escape" === b.key ? a._2() : /^[a-zA-ZÃ€-Ã¿ ]$/.test(b.key) && (e && clearTimeout(e), d += b.key.toLowerCase(), a._s(d), e = setTimeout(function() { d = "" }, 1e3)) }, c.addEventListener("keydown", this._a3)
                }
            }, {
                key: "_q",
                value: function(a) {
                    var b = "ArrowUp" === a ? this.c.previousElementSibling : this.c.nextElementSibling;
                    b && (b.classList.contains("divider") && (b = "ArrowUp" === a ? b.previousElementSibling : b.nextElementSibling), this._x(b), this._3(b))
                }
            }, { key: "_r", value: function() { this.c && this._1(this.c) } }, {
                key: "_s",
                value: function(a) {
                    for (var b = 0; b < this.p.length; b++)
                        if (this._t(this.p[b].name, a)) {
                            var c = this.m.querySelector("#iti-item-".concat(this.p[b].iso2));
                            this._x(c), this._3(c, !0);
                            break
                        }
                }
            }, { key: "_t", value: function(a, b) { return a.substr(0, b.length).toLowerCase() === b } }, {
                key: "_u",
                value: function(a) {
                    var c = a;
                    if (this.d.formatOnDisplay && b.intlTelInputUtils && this.s) {
                        var d = !this.d.separateDialCode && (this.d.nationalMode || "+" !== c.charAt(0)),
                            e = intlTelInputUtils.numberFormat,
                            f = e.NATIONAL,
                            g = e.INTERNATIONAL,
                            h = d ? f : g;
                        c = intlTelInputUtils.formatNumber(c, this.s.iso2, h)
                    }
                    c = this._7(c), this.a.value = c
                }
            }, {
                key: "_v",
                value: function(a) {
                    var b = a,
                        c = "1" === this.s.dialCode;
                    b && this.d.nationalMode && c && "+" !== b.charAt(0) && ("1" !== b.charAt(0) && (b = "1".concat(b)), b = "+".concat(b));
                    var d = this._5(b),
                        e = this._m(b),
                        f = null;
                    if (d) {
                        var g = this.q[this._m(d)],
                            h = -1 !== g.indexOf(this.s.iso2),
                            i = "+1" === d && e.length >= 4;
                        if (!("1" === this.s.dialCode && this._w(e)) && (!h || i))
                            for (var j = 0; j < g.length; j++)
                                if (g[j]) { f = g[j]; break }
                    } else "+" === b.charAt(0) && e.length ? f = "" : b && "+" !== b || (f = this.j);
                    return null !== f && this._z(f)
                }
            }, { key: "_w", value: function(a) { var b = this._m(a); if ("1" === b.charAt(0)) { var c = b.substr(1, 3); return -1 !== m.indexOf(c) } return !1 } }, {
                key: "_x",
                value: function(a) {
                    var b = this.c;
                    b && b.classList.remove("highlight"), this.c = a, this.c.classList.add("highlight")
                }
            }, {
                key: "_y",
                value: function(a, b, c) {
                    for (var d = b ? h : this.p, e = 0; e < d.length; e++)
                        if (d[e].iso2 === a) return d[e];
                    if (c) return null;
                    throw new Error("No country data for '".concat(a, "'"))
                }
            }, {
                key: "_z",
                value: function(a) {
                    var b = this.s.iso2 ? this.s : {};
                    this.s = a ? this._y(a, !1, !1) : {}, this.s.iso2 && (this.j = this.s.iso2), this.l.setAttribute("class", "iti-flag ".concat(a));
                    var c = a ? "".concat(this.s.name, ": +").concat(this.s.dialCode) : "Unknown";
                    if (this.selectedFlag.setAttribute("title", c), this.d.separateDialCode) {
                        var d = this.s.dialCode ? "+".concat(this.s.dialCode) : "";
                        this.t.innerHTML = d, this.a.style.paddingLeft = "".concat(this.selectedFlag.offsetWidth + 6, "px")
                    }
                    if (this._0(), this.d.allowDropdown) {
                        var e = this.b;
                        if (e && (e.classList.remove("active"), e.setAttribute("aria-selected", "false")), a) {
                            var f = this.m.querySelector("#iti-item-".concat(a));
                            f.setAttribute("aria-selected", "true"), f.classList.add("active"), this.b = f, this.m.setAttribute("aria-activedescendant", f.getAttribute("id"))
                        }
                    }
                    return b.iso2 !== a
                }
            }, {
                key: "_0",
                value: function() {
                    var a = "aggressive" === this.d.autoPlaceholder || !this.e && "polite" === this.d.autoPlaceholder;
                    if (b.intlTelInputUtils && a) {
                        var c = intlTelInputUtils.numberType[this.d.placeholderNumberType],
                            d = this.s.iso2 ? intlTelInputUtils.getExampleNumber(this.s.iso2, this.d.nationalMode, c) : "";
                        d = this._7(d), "function" == typeof this.d.customPlaceholder && (d = this.d.customPlaceholder(d, this.s)), this.a.setAttribute("placeholder", d)
                    }
                }
            }, {
                key: "_1",
                value: function(a) {
                    var b = this._z(a.getAttribute("data-country-code"));
                    this._2(), this._4(a.getAttribute("data-dial-code"), !0), this.a.focus();
                    var c = this.a.value.length;
                    this.a.setSelectionRange(c, c), b && this._8()
                }
            }, { key: "_2", value: function() { this.m.classList.add("hide"), this.m.setAttribute("aria-expanded", "false"), this.u.classList.remove("up"), c.removeEventListener("keydown", this._a3), c.documentElement.removeEventListener("click", this._a2), this.m.removeEventListener("mouseover", this._a0), this.m.removeEventListener("click", this._a1), this.d.dropdownContainer && (this.g || b.removeEventListener("scroll", this._a4), this.dropdown.parentNode && this.dropdown.parentNode.removeChild(this.dropdown)), this._m2("close:countrydropdown") } }, {
                key: "_3",
                value: function(a, d) {
                    var e = this.m,
                        f = b.pageYOffset || c.documentElement.scrollTop,
                        g = e.offsetHeight,
                        h = e.getBoundingClientRect().top + f,
                        i = h + g,
                        j = a.offsetHeight,
                        k = a.getBoundingClientRect().top + f,
                        l = k + j,
                        m = k - h + e.scrollTop,
                        n = g / 2 - j / 2;
                    if (k < h) d && (m -= n), e.scrollTop = m;
                    else if (l > i) {
                        d && (m += n);
                        var o = g - j;
                        e.scrollTop = m - o
                    }
                }
            }, {
                key: "_4",
                value: function(a, b) {
                    var c, d = this.a.value,
                        e = "+".concat(a);
                    if ("+" === d.charAt(0)) {
                        var f = this._5(d);
                        c = f ? d.replace(f, e) : e
                    } else {
                        if (this.d.nationalMode || this.d.separateDialCode) return;
                        if (d) c = e + d;
                        else {
                            if (!b && this.d.autoHideDialCode) return;
                            c = e
                        }
                    }
                    this.a.value = c
                }
            }, {
                key: "_5",
                value: function(a) {
                    var b = "";
                    if ("+" === a.charAt(0))
                        for (var c = "", d = 0; d < a.length; d++) { var e = a.charAt(d); if (!isNaN(parseInt(e, 10)) && (c += e, this.q[c] && (b = a.substr(0, d + 1)), 4 === c.length)) break }
                    return b
                }
            }, {
                key: "_6",
                value: function() {
                    var a = this.a.value.trim(),
                        b = this.s.dialCode,
                        c = this._m(a),
                        d = "1" === c.charAt(0) ? c : "1".concat(c);
                    return (this.d.separateDialCode && "+" !== a.charAt(0) ? "+".concat(b) : a && "+" !== a.charAt(0) && "1" !== a.charAt(0) && b && "1" === b.charAt(0) && 4 === b.length && b !== d.substr(0, 4) ? b.substr(1) : "") + a
                }
            }, {
                key: "_7",
                value: function(a) {
                    var b = a;
                    if (this.d.separateDialCode) {
                        var c = this._5(b);
                        if (c) {
                            null !== this.s.areaCodes && (c = "+".concat(this.s.dialCode));
                            var d = " " === b[c.length] || "-" === b[c.length] ? c.length + 1 : c.length;
                            b = b.substr(d)
                        }
                    }
                    return this._j2(b)
                }
            }, { key: "_8", value: function() { this._m2("countrychange") } }, { key: "handleAutoCountry", value: function() { "auto" === this.d.initialCountry && (this.j = b.intlTelInputGlobals.autoCountry, this.a.value || this.setCountry(this.j), this.h()) } }, { key: "handleUtils", value: function() { b.intlTelInputUtils && (this.a.value && this._u(this.a.value), this._0()), this.i0() } }, {
                key: "destroy",
                value: function() {
                    var a = this.a.form;
                    if (this.d.allowDropdown) {
                        this._2(), this.selectedFlag.removeEventListener("click", this._a10), this.k.removeEventListener("keydown", this._a11);
                        var c = this._i1();
                        c && c.removeEventListener("click", this._a9)
                    }
                    this.hiddenInput && a && a.removeEventListener("submit", this._a14), this.d.autoHideDialCode && (a && a.removeEventListener("submit", this._a8), this.a.removeEventListener("blur", this._a8)), this.a.removeEventListener("keyup", this._a12), this.a.removeEventListener("cut", this._a13), this.a.removeEventListener("paste", this._a13), this.a.removeAttribute("data-intl-tel-input-id");
                    var d = this.a.parentNode;
                    d.parentNode.insertBefore(this.a, d), d.parentNode.removeChild(d), delete b.intlTelInputGlobals.instances[this.id]
                }
            }, { key: "getExtension", value: function() { return b.intlTelInputUtils ? intlTelInputUtils.getExtension(this._6(), this.s.iso2) : "" } }, { key: "getNumber", value: function(a) { if (b.intlTelInputUtils) { var c = this.s.iso2; return intlTelInputUtils.formatNumber(this._6(), c, a) } return "" } }, { key: "getNumberType", value: function() { return b.intlTelInputUtils ? intlTelInputUtils.getNumberType(this._6(), this.s.iso2) : -99 } }, { key: "getSelectedCountryData", value: function() { return this.s } }, { key: "getValidationError", value: function() { if (b.intlTelInputUtils) { var a = this.s.iso2; return intlTelInputUtils.getValidationError(this._6(), a) } return -99 } }, {
                key: "isValidNumber",
                value: function() {
                    var a = this._6().trim(),
                        c = this.d.nationalMode ? this.s.iso2 : "";
                    return b.intlTelInputUtils ? intlTelInputUtils.isValidNumber(a, c) : null
                }
            }, {
                key: "setCountry",
                value: function(a) {
                    var b = a.toLowerCase();
                    this.l.classList.contains(b) || (this._z(b), this._4(this.s.dialCode, !1), this._8())
                }
            }, {
                key: "setNumber",
                value: function(a) {
                    var b = this._v(a);
                    this._u(a), b && this._8()
                }
            }, { key: "setPlaceholderNumberType", value: function(a) { this.d.placeholderNumberType = a, this._0() } }]), a
        }();
    b.intlTelInputGlobals.getCountryData = function() { return h };
    var q = function(a, b, d) {
        var e = c.createElement("script");
        e.onload = function() { o("handleUtils"), b && b() }, e.onerror = function() { o("rejectUtilsScriptPromise"), d && d() }, e.className = "iti-load-utils", e.async = !0, e.src = a, c.body.appendChild(e)
    };
    b.intlTelInputGlobals.loadUtils = function(a) {
        if (!b.intlTelInputUtils && !b.intlTelInputGlobals.startedLoadingUtilsScript) {
            if (b.intlTelInputGlobals.startedLoadingUtilsScript = !0, "undefined" != typeof Promise) return new Promise(function(b, c) { return q(a, b, c) });
            q(a)
        }
        return null
    }, b.intlTelInputGlobals.defaults = l, b.intlTelInputGlobals.version = "15.0.0";
    a.fn.intlTelInput = function(c) {
        var e = arguments;
        if (c === d || "object" == typeof c) return this.each(function() {
            if (!a.data(this, "plugin_intlTelInput")) {
                var d = new p(this, c);
                d._init(), b.intlTelInputGlobals.instances[d.id] = d, a.data(this, "plugin_intlTelInput", d)
            }
        });
        if ("string" == typeof c && "_" !== c[0]) {
            var f;
            return this.each(function() {
                var b = a.data(this, "plugin_intlTelInput");
                b instanceof p && "function" == typeof b[c] && (f = b[c].apply(b, Array.prototype.slice.call(e, 1))), "destroy" === c && a.data(this, "plugin_intlTelInput", null)
            }), f !== d ? f : this
        }
    }
});
/*! jQuery Validation Plugin - v1.19.0 - 11/28/2018
 * https://jqueryvalidation.org/
 * Copyright (c) 2018 JÃ¶rn Zaefferer; Licensed MIT */
! function(a) { "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof module && module.exports ? module.exports = a(require("jquery")) : a(jQuery) }(function(a) {
    a.extend(a.fn, {
        validate: function(b) {
            if (!this.length) return void(b && b.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
            var c = a.data(this[0], "validator");
            return c ? c : (this.attr("novalidate", "novalidate"), c = new a.validator(b, this[0]), a.data(this[0], "validator", c), c.settings.onsubmit && (this.on("click.validate", ":submit", function(b) { c.submitButton = b.currentTarget, a(this).hasClass("cancel") && (c.cancelSubmit = !0), void 0 !== a(this).attr("formnovalidate") && (c.cancelSubmit = !0) }), this.on("submit.validate", function(b) {
                function d() { var d, e; return c.submitButton && (c.settings.submitHandler || c.formSubmitted) && (d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)), !(c.settings.submitHandler && !c.settings.debug) || (e = c.settings.submitHandler.call(c, c.currentForm, b), d && d.remove(), void 0 !== e && e) }
                return c.settings.debug && b.preventDefault(), c.cancelSubmit ? (c.cancelSubmit = !1, d()) : c.form() ? c.pendingRequest ? (c.formSubmitted = !0, !1) : d() : (c.focusInvalid(), !1)
            })), c)
        },
        valid: function() { var b, c, d; return a(this[0]).is("form") ? b = this.validate().form() : (d = [], b = !0, c = a(this[0].form).validate(), this.each(function() { b = c.element(this) && b, b || (d = d.concat(c.errorList)) }), c.errorList = d), b },
        rules: function(b, c) {
            var d, e, f, g, h, i, j = this[0],
                k = "undefined" != typeof this.attr("contenteditable") && "false" !== this.attr("contenteditable");
            if (null != j && (!j.form && k && (j.form = this.closest("form")[0], j.name = this.attr("name")), null != j.form)) {
                if (b) switch (d = a.data(j.form, "validator").settings, e = d.rules, f = a.validator.staticRules(j), b) {
                    case "add":
                        a.extend(f, a.validator.normalizeRule(c)), delete f.messages, e[j.name] = f, c.messages && (d.messages[j.name] = a.extend(d.messages[j.name], c.messages));
                        break;
                    case "remove":
                        return c ? (i = {}, a.each(c.split(/\s/), function(a, b) { i[b] = f[b], delete f[b] }), i) : (delete e[j.name], f)
                }
                return g = a.validator.normalizeRules(a.extend({}, a.validator.classRules(j), a.validator.attributeRules(j), a.validator.dataRules(j), a.validator.staticRules(j)), j), g.required && (h = g.required, delete g.required, g = a.extend({ required: h }, g)), g.remote && (h = g.remote, delete g.remote, g = a.extend(g, { remote: h })), g
            }
        }
    }), a.extend(a.expr.pseudos || a.expr[":"], { blank: function(b) { return !a.trim("" + a(b).val()) }, filled: function(b) { var c = a(b).val(); return null !== c && !!a.trim("" + c) }, unchecked: function(b) { return !a(b).prop("checked") } }), a.validator = function(b, c) { this.settings = a.extend(!0, {}, a.validator.defaults, b), this.currentForm = c, this.init() }, a.validator.format = function(b, c) { return 1 === arguments.length ? function() { var c = a.makeArray(arguments); return c.unshift(b), a.validator.format.apply(this, c) } : void 0 === c ? b : (arguments.length > 2 && c.constructor !== Array && (c = a.makeArray(arguments).slice(1)), c.constructor !== Array && (c = [c]), a.each(c, function(a, c) { b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function() { return c }) }), b) }, a.extend(a.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            pendingClass: "pending",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: !1,
            focusInvalid: !0,
            errorContainer: a([]),
            errorLabelContainer: a([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function(a) { this.lastActive = a, this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(a))) },
            onfocusout: function(a) { this.checkable(a) || !(a.name in this.submitted) && this.optional(a) || this.element(a) },
            onkeyup: function(b, c) {
                var d = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];
                9 === c.which && "" === this.elementValue(b) || a.inArray(c.keyCode, d) !== -1 || (b.name in this.submitted || b.name in this.invalid) && this.element(b)
            },
            onclick: function(a) { a.name in this.submitted ? this.element(a) : a.parentNode.name in this.submitted && this.element(a.parentNode) },
            highlight: function(b, c, d) { "radio" === b.type ? this.findByName(b.name).addClass(c).removeClass(d) : a(b).addClass(c).removeClass(d) },
            unhighlight: function(b, c, d) { "radio" === b.type ? this.findByName(b.name).removeClass(c).addClass(d) : a(b).removeClass(c).addClass(d) }
        },
        setDefaults: function(b) { a.extend(a.validator.defaults, b) },
        messages: { required: "This field is required.", remote: "Please fix this field.", email: "Please enter a valid email address.", url: "Please enter a valid URL.", date: "Please enter a valid date.", dateISO: "Please enter a valid date (ISO).", number: "Please enter a valid number.", digits: "Please enter only digits.", equalTo: "Please enter the same value again.", maxlength: a.validator.format("Please enter no more than {0} characters."), minlength: a.validator.format("Please enter at least {0} characters."), rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."), range: a.validator.format("Please enter a value between {0} and {1}."), max: a.validator.format("Please enter a value less than or equal to {0}."), min: a.validator.format("Please enter a value greater than or equal to {0}."), step: a.validator.format("Please enter a multiple of {0}.") },
        autoCreateRanges: !1,
        prototype: {
            init: function() {
                function b(b) {
                    var c = "undefined" != typeof a(this).attr("contenteditable") && "false" !== a(this).attr("contenteditable");
                    if (!this.form && c && (this.form = a(this).closest("form")[0], this.name = a(this).attr("name")), d === this.form) {
                        var e = a.data(this.form, "validator"),
                            f = "on" + b.type.replace(/^validate/, ""),
                            g = e.settings;
                        g[f] && !a(this).is(g.ignore) && g[f].call(e, this, b)
                    }
                }
                this.labelContainer = a(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm), this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();
                var c, d = this.currentForm,
                    e = this.groups = {};
                a.each(this.settings.groups, function(b, c) { "string" == typeof c && (c = c.split(/\s/)), a.each(c, function(a, c) { e[c] = b }) }), c = this.settings.rules, a.each(c, function(b, d) { c[b] = a.validator.normalizeRule(d) }), a(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']", b).on("click.validate", "select, option, [type='radio'], [type='checkbox']", b), this.settings.invalidHandler && a(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler)
            },
            form: function() { return this.checkForm(), a.extend(this.submitted, this.errorMap), this.invalid = a.extend({}, this.errorMap), this.valid() || a(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid() },
            checkForm: function() { this.prepareForm(); for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) this.check(b[a]); return this.valid() },
            element: function(b) {
                var c, d, e = this.clean(b),
                    f = this.validationTargetFor(e),
                    g = this,
                    h = !0;
                return void 0 === f ? delete this.invalid[e.name] : (this.prepareElement(f), this.currentElements = a(f), d = this.groups[f.name], d && a.each(this.groups, function(a, b) { b === d && a !== f.name && (e = g.validationTargetFor(g.clean(g.findByName(a))), e && e.name in g.invalid && (g.currentElements.push(e), h = g.check(e) && h)) }), c = this.check(f) !== !1, h = h && c, c ? this.invalid[f.name] = !1 : this.invalid[f.name] = !0, this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), a(b).attr("aria-invalid", !c)), h
            },
            showErrors: function(b) {
                if (b) {
                    var c = this;
                    a.extend(this.errorMap, b), this.errorList = a.map(this.errorMap, function(a, b) { return { message: a, element: c.findByName(b)[0] } }), this.successList = a.grep(this.successList, function(a) { return !(a.name in b) })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            },
            resetForm: function() {
                a.fn.resetForm && a(this.currentForm).resetForm(), this.invalid = {}, this.submitted = {}, this.prepareForm(), this.hideErrors();
                var b = this.elements().removeData("previousValue").removeAttr("aria-invalid");
                this.resetElements(b)
            },
            resetElements: function(a) {
                var b;
                if (this.settings.unhighlight)
                    for (b = 0; a[b]; b++) this.settings.unhighlight.call(this, a[b], this.settings.errorClass, ""), this.findByName(a[b].name).removeClass(this.settings.validClass);
                else a.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)
            },
            numberOfInvalids: function() { return this.objectLength(this.invalid) },
            objectLength: function(a) { var b, c = 0; for (b in a) void 0 !== a[b] && null !== a[b] && a[b] !== !1 && c++; return c },
            hideErrors: function() { this.hideThese(this.toHide) },
            hideThese: function(a) { a.not(this.containers).text(""), this.addWrapper(a).hide() },
            valid: function() { return 0 === this.size() },
            size: function() { return this.errorList.length },
            focusInvalid: function() { if (this.settings.focusInvalid) try { a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin") } catch (b) {} },
            findLastActive: function() { var b = this.lastActive; return b && 1 === a.grep(this.errorList, function(a) { return a.element.name === b.name }).length && b },
            elements: function() {
                var b = this,
                    c = {};
                return a(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function() {
                    var d = this.name || a(this).attr("name"),
                        e = "undefined" != typeof a(this).attr("contenteditable") && "false" !== a(this).attr("contenteditable");
                    return !d && b.settings.debug && window.console && console.error("%o has no name assigned", this), e && (this.form = a(this).closest("form")[0], this.name = d), this.form === b.currentForm && (!(d in c || !b.objectLength(a(this).rules())) && (c[d] = !0, !0))
                })
            },
            clean: function(b) { return a(b)[0] },
            errors: function() { var b = this.settings.errorClass.split(" ").join("."); return a(this.settings.errorElement + "." + b, this.errorContext) },
            resetInternals: function() { this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = a([]), this.toHide = a([]) },
            reset: function() { this.resetInternals(), this.currentElements = a([]) },
            prepareForm: function() { this.reset(), this.toHide = this.errors().add(this.containers) },
            prepareElement: function(a) { this.reset(), this.toHide = this.errorsFor(a) },
            elementValue: function(b) {
                var c, d, e = a(b),
                    f = b.type,
                    g = "undefined" != typeof e.attr("contenteditable") && "false" !== e.attr("contenteditable");
                return "radio" === f || "checkbox" === f ? this.findByName(b.name).filter(":checked").val() : "number" === f && "undefined" != typeof b.validity ? b.validity.badInput ? "NaN" : e.val() : (c = g ? e.text() : e.val(), "file" === f ? "C:\\fakepath\\" === c.substr(0, 12) ? c.substr(12) : (d = c.lastIndexOf("/"), d >= 0 ? c.substr(d + 1) : (d = c.lastIndexOf("\\"), d >= 0 ? c.substr(d + 1) : c)) : "string" == typeof c ? c.replace(/\r/g, "") : c)
            },
            check: function(b) {
                b = this.validationTargetFor(this.clean(b));
                var c, d, e, f, g = a(b).rules(),
                    h = a.map(g, function(a, b) { return b }).length,
                    i = !1,
                    j = this.elementValue(b);
                "function" == typeof g.normalizer ? f = g.normalizer : "function" == typeof this.settings.normalizer && (f = this.settings.normalizer), f && (j = f.call(b, j), delete g.normalizer);
                for (d in g) { e = { method: d, parameters: g[d] }; try { if (c = a.validator.methods[d].call(this, j, b, e.parameters), "dependency-mismatch" === c && 1 === h) { i = !0; continue } if (i = !1, "pending" === c) return void(this.toHide = this.toHide.not(this.errorsFor(b))); if (!c) return this.formatAndAdd(b, e), !1 } catch (k) { throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method.", k), k instanceof TypeError && (k.message += ".  Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method."), k } }
                if (!i) return this.objectLength(g) && this.successList.push(b), !0
            },
            customDataMessage: function(b, c) { return a(b).data("msg" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg") },
            customMessage: function(a, b) { var c = this.settings.messages[a]; return c && (c.constructor === String ? c : c[b]) },
            findDefined: function() {
                for (var a = 0; a < arguments.length; a++)
                    if (void 0 !== arguments[a]) return arguments[a]
            },
            defaultMessage: function(b, c) {
                "string" == typeof c && (c = { method: c });
                var d = this.findDefined(this.customMessage(b.name, c.method), this.customDataMessage(b, c.method), !this.settings.ignoreTitle && b.title || void 0, a.validator.messages[c.method], "<strong>Warning: No message defined for " + b.name + "</strong>"),
                    e = /\$?\{(\d+)\}/g;
                return "function" == typeof d ? d = d.call(this, c.parameters, b) : e.test(d) && (d = a.validator.format(d.replace(e, "{$1}"), c.parameters)), d
            },
            formatAndAdd: function(a, b) {
                var c = this.defaultMessage(a, b);
                this.errorList.push({ message: c, element: a, method: b.method }), this.errorMap[a.name] = c, this.submitted[a.name] = c
            },
            addWrapper: function(a) { return this.settings.wrapper && (a = a.add(a.parent(this.settings.wrapper))), a },
            defaultShowErrors: function() {
                var a, b, c;
                for (a = 0; this.errorList[a]; a++) c = this.errorList[a], this.settings.highlight && this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass), this.showLabel(c.element, c.message);
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success)
                    for (a = 0; this.successList[a]; a++) this.showLabel(this.successList[a]);
                if (this.settings.unhighlight)
                    for (a = 0, b = this.validElements(); b[a]; a++) this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
                this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show()
            },
            validElements: function() { return this.currentElements.not(this.invalidElements()) },
            invalidElements: function() { return a(this.errorList).map(function() { return this.element }) },
            showLabel: function(b, c) {
                var d, e, f, g, h = this.errorsFor(b),
                    i = this.idOrName(b),
                    j = a(b).attr("aria-describedby");
                h.length ? (h.removeClass(this.settings.validClass).addClass(this.settings.errorClass), h.html(c)) : (h = a("<" + this.settings.errorElement + ">").attr("id", i + "-error").addClass(this.settings.errorClass).html(c || ""), d = h, this.settings.wrapper && (d = h.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(d) : this.settings.errorPlacement ? this.settings.errorPlacement.call(this, d, a(b)) : d.insertAfter(b), h.is("label") ? h.attr("for", i) : 0 === h.parents("label[for='" + this.escapeCssMeta(i) + "']").length && (f = h.attr("id"), j ? j.match(new RegExp("\\b" + this.escapeCssMeta(f) + "\\b")) || (j += " " + f) : j = f, a(b).attr("aria-describedby", j), e = this.groups[b.name], e && (g = this, a.each(g.groups, function(b, c) { c === e && a("[name='" + g.escapeCssMeta(b) + "']", g.currentForm).attr("aria-describedby", h.attr("id")) })))), !c && this.settings.success && (h.text(""), "string" == typeof this.settings.success ? h.addClass(this.settings.success) : this.settings.success(h, b)), this.toShow = this.toShow.add(h)
            },
            errorsFor: function(b) {
                var c = this.escapeCssMeta(this.idOrName(b)),
                    d = a(b).attr("aria-describedby"),
                    e = "label[for='" + c + "'], label[for='" + c + "'] *";
                return d && (e = e + ", #" + this.escapeCssMeta(d).replace(/\s+/g, ", #")), this.errors().filter(e)
            },
            escapeCssMeta: function(a) { return a.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g, "\\$1") },
            idOrName: function(a) { return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name) },
            validationTargetFor: function(b) { return this.checkable(b) && (b = this.findByName(b.name)), a(b).not(this.settings.ignore)[0] },
            checkable: function(a) { return /radio|checkbox/i.test(a.type) },
            findByName: function(b) { return a(this.currentForm).find("[name='" + this.escapeCssMeta(b) + "']") },
            getLength: function(b, c) {
                switch (c.nodeName.toLowerCase()) {
                    case "select":
                        return a("option:selected", c).length;
                    case "input":
                        if (this.checkable(c)) return this.findByName(c.name).filter(":checked").length
                }
                return b.length
            },
            depend: function(a, b) { return !this.dependTypes[typeof a] || this.dependTypes[typeof a](a, b) },
            dependTypes: { "boolean": function(a) { return a }, string: function(b, c) { return !!a(b, c.form).length }, "function": function(a, b) { return a(b) } },
            optional: function(b) { var c = this.elementValue(b); return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch" },
            startRequest: function(b) { this.pending[b.name] || (this.pendingRequest++, a(b).addClass(this.settings.pendingClass), this.pending[b.name] = !0) },
            stopRequest: function(b, c) { this.pendingRequest--, this.pendingRequest < 0 && (this.pendingRequest = 0), delete this.pending[b.name], a(b).removeClass(this.settings.pendingClass), c && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (a(this.currentForm).submit(), this.submitButton && a("input:hidden[name='" + this.submitButton.name + "']", this.currentForm).remove(), this.formSubmitted = !1) : !c && 0 === this.pendingRequest && this.formSubmitted && (a(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1) },
            previousValue: function(b, c) { return c = "string" == typeof c && c || "remote", a.data(b, "previousValue") || a.data(b, "previousValue", { old: null, valid: !0, message: this.defaultMessage(b, { method: c }) }) },
            destroy: function() { this.resetForm(), a(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur").find(".validate-lessThan-blur").off(".validate-lessThan").removeClass("validate-lessThan-blur").find(".validate-lessThanEqual-blur").off(".validate-lessThanEqual").removeClass("validate-lessThanEqual-blur").find(".validate-greaterThanEqual-blur").off(".validate-greaterThanEqual").removeClass("validate-greaterThanEqual-blur").find(".validate-greaterThan-blur").off(".validate-greaterThan").removeClass("validate-greaterThan-blur") }
        },
        classRuleSettings: { required: { required: !0 }, email: { email: !0 }, url: { url: !0 }, date: { date: !0 }, dateISO: { dateISO: !0 }, number: { number: !0 }, digits: { digits: !0 }, creditcard: { creditcard: !0 } },
        addClassRules: function(b, c) { b.constructor === String ? this.classRuleSettings[b] = c : a.extend(this.classRuleSettings, b) },
        classRules: function(b) {
            var c = {},
                d = a(b).attr("class");
            return d && a.each(d.split(" "), function() { this in a.validator.classRuleSettings && a.extend(c, a.validator.classRuleSettings[this]) }), c
        },
        normalizeAttributeRule: function(a, b, c, d) { /min|max|step/.test(c) && (null === b || /number|range|text/.test(b)) && (d = Number(d), isNaN(d) && (d = void 0)), d || 0 === d ? a[c] = d : b === c && "range" !== b && (a[c] = !0) },
        attributeRules: function(b) {
            var c, d, e = {},
                f = a(b),
                g = b.getAttribute("type");
            for (c in a.validator.methods) "required" === c ? (d = b.getAttribute(c), "" === d && (d = !0), d = !!d) : d = f.attr(c), this.normalizeAttributeRule(e, g, c, d);
            return e.maxlength && /-1|2147483647|524288/.test(e.maxlength) && delete e.maxlength, e
        },
        dataRules: function(b) {
            var c, d, e = {},
                f = a(b),
                g = b.getAttribute("type");
            for (c in a.validator.methods) d = f.data("rule" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()), "" === d && (d = !0), this.normalizeAttributeRule(e, g, c, d);
            return e
        },
        staticRules: function(b) {
            var c = {},
                d = a.data(b.form, "validator");
            return d.settings.rules && (c = a.validator.normalizeRule(d.settings.rules[b.name]) || {}), c
        },
        normalizeRules: function(b, c) {
            return a.each(b, function(d, e) {
                if (e === !1) return void delete b[d];
                if (e.param || e.depends) {
                    var f = !0;
                    switch (typeof e.depends) {
                        case "string":
                            f = !!a(e.depends, c.form).length;
                            break;
                        case "function":
                            f = e.depends.call(c, c)
                    }
                    f ? b[d] = void 0 === e.param || e.param : (a.data(c.form, "validator").resetElements(a(c)), delete b[d])
                }
            }), a.each(b, function(d, e) { b[d] = a.isFunction(e) && "normalizer" !== d ? e(c) : e }), a.each(["minlength", "maxlength"], function() { b[this] && (b[this] = Number(b[this])) }), a.each(["rangelength", "range"], function() {
                var c;
                b[this] && (a.isArray(b[this]) ? b[this] = [Number(b[this][0]), Number(b[this][1])] : "string" == typeof b[this] && (c = b[this].replace(/[\[\]]/g, "").split(/[\s,]+/), b[this] = [Number(c[0]), Number(c[1])]))
            }), a.validator.autoCreateRanges && (null != b.min && null != b.max && (b.range = [b.min, b.max], delete b.min, delete b.max), null != b.minlength && null != b.maxlength && (b.rangelength = [b.minlength, b.maxlength], delete b.minlength, delete b.maxlength)), b
        },
        normalizeRule: function(b) {
            if ("string" == typeof b) {
                var c = {};
                a.each(b.split(/\s/), function() { c[this] = !0 }), b = c
            }
            return b
        },
        addMethod: function(b, c, d) { a.validator.methods[b] = c, a.validator.messages[b] = void 0 !== d ? d : a.validator.messages[b], c.length < 3 && a.validator.addClassRules(b, a.validator.normalizeRule(b)) },
        methods: {
            required: function(b, c, d) { if (!this.depend(d, c)) return "dependency-mismatch"; if ("select" === c.nodeName.toLowerCase()) { var e = a(c).val(); return e && e.length > 0 } return this.checkable(c) ? this.getLength(b, c) > 0 : void 0 !== b && null !== b && b.length > 0 },
            email: function(a, b) { return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a) },
            url: function(a, b) { return this.optional(b) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(a) },
            date: function() { var a = !1; return function(b, c) { return a || (a = !0, this.settings.debug && window.console && console.warn("The `date` method is deprecated and will be removed in version '2.0.0'.\nPlease don't use it, since it relies on the Date constructor, which\nbehaves very differently across browsers and locales. Use `dateISO`\ninstead or one of the locale specific methods in `localizations/`\nand `additional-methods.js`.")), this.optional(c) || !/Invalid|NaN/.test(new Date(b).toString()) } }(),
            dateISO: function(a, b) { return this.optional(b) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a) },
            number: function(a, b) { return this.optional(b) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a) },
            digits: function(a, b) { return this.optional(b) || /^\d+$/.test(a) },
            minlength: function(b, c, d) { var e = a.isArray(b) ? b.length : this.getLength(b, c); return this.optional(c) || e >= d },
            maxlength: function(b, c, d) { var e = a.isArray(b) ? b.length : this.getLength(b, c); return this.optional(c) || e <= d },
            rangelength: function(b, c, d) { var e = a.isArray(b) ? b.length : this.getLength(b, c); return this.optional(c) || e >= d[0] && e <= d[1] },
            min: function(a, b, c) { return this.optional(b) || a >= c },
            max: function(a, b, c) { return this.optional(b) || a <= c },
            range: function(a, b, c) { return this.optional(b) || a >= c[0] && a <= c[1] },
            step: function(b, c, d) {
                var e, f = a(c).attr("type"),
                    g = "Step attribute on input type " + f + " is not supported.",
                    h = ["text", "number", "range"],
                    i = new RegExp("\\b" + f + "\\b"),
                    j = f && !i.test(h.join()),
                    k = function(a) { var b = ("" + a).match(/(?:\.(\d+))?$/); return b && b[1] ? b[1].length : 0 },
                    l = function(a) { return Math.round(a * Math.pow(10, e)) },
                    m = !0;
                if (j) throw new Error(g);
                return e = k(d), (k(b) > e || l(b) % l(d) !== 0) && (m = !1), this.optional(c) || m
            },
            equalTo: function(b, c, d) { var e = a(d); return this.settings.onfocusout && e.not(".validate-equalTo-blur").length && e.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function() { a(c).valid() }), b === e.val() },
            remote: function(b, c, d, e) {
                if (this.optional(c)) return "dependency-mismatch";
                e = "string" == typeof e && e || "remote";
                var f, g, h, i = this.previousValue(c, e);
                return this.settings.messages[c.name] || (this.settings.messages[c.name] = {}), i.originalMessage = i.originalMessage || this.settings.messages[c.name][e], this.settings.messages[c.name][e] = i.message, d = "string" == typeof d && { url: d } || d, h = a.param(a.extend({ data: b }, d.data)), i.old === h ? i.valid : (i.old = h, f = this, this.startRequest(c), g = {}, g[c.name] = b, a.ajax(a.extend(!0, {
                    mode: "abort",
                    port: "validate" + c.name,
                    dataType: "json",
                    data: g,
                    context: f.currentForm,
                    success: function(a) {
                        var d, g, h, j = a === !0 || "true" === a;
                        f.settings.messages[c.name][e] = i.originalMessage, j ? (h = f.formSubmitted, f.resetInternals(), f.toHide = f.errorsFor(c), f.formSubmitted = h, f.successList.push(c), f.invalid[c.name] = !1, f.showErrors()) : (d = {}, g = a || f.defaultMessage(c, { method: e, parameters: b }), d[c.name] = i.message = g, f.invalid[c.name] = !0, f.showErrors(d)), i.valid = j, f.stopRequest(c, j)
                    }
                }, d)), "pending")
            }
        }
    });
    var b, c = {};
    return a.ajaxPrefilter ? a.ajaxPrefilter(function(a, b, d) { var e = a.port; "abort" === a.mode && (c[e] && c[e].abort(), c[e] = d) }) : (b = a.ajax, a.ajax = function(d) {
        var e = ("mode" in d ? d : a.ajaxSettings).mode,
            f = ("port" in d ? d : a.ajaxSettings).port;
        return "abort" === e ? (c[f] && c[f].abort(), c[f] = b.apply(this, arguments), c[f]) : b.apply(this, arguments)
    }), a
});
/*!
 * jquery.inputmask.bundle.js
 * https://github.com/RobinHerbots/Inputmask
 * Copyright (c) 2010 - 2019 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 4.0.6
 */
(function(modules) {
    var installedModules = {};

    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) { return installedModules[moduleId].exports }
        var module = installedModules[moduleId] = { i: moduleId, l: false, exports: {} };
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
        module.l = true;
        return module.exports
    }
    __webpack_require__.m = modules;
    __webpack_require__.c = installedModules;
    __webpack_require__.d = function(exports, name, getter) { if (!__webpack_require__.o(exports, name)) { Object.defineProperty(exports, name, { enumerable: true, get: getter }) } };
    __webpack_require__.r = function(exports) {
        if (typeof Symbol !== "undefined" && Symbol.toStringTag) { Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" }) }
        Object.defineProperty(exports, "__esModule", { value: true })
    };
    __webpack_require__.t = function(value, mode) {
        if (mode & 1) value = __webpack_require__(value);
        if (mode & 8) return value;
        if (mode & 4 && typeof value === "object" && value && value.__esModule) return value;
        var ns = Object.create(null);
        __webpack_require__.r(ns);
        Object.defineProperty(ns, "default", { enumerable: true, value: value });
        if (mode & 2 && typeof value != "string")
            for (var key in value) __webpack_require__.d(ns, key, function(key) { return value[key] }.bind(null, key));
        return ns
    };
    __webpack_require__.n = function(module) {
        var getter = module && module.__esModule ? function getDefault() { return module["default"] } : function getModuleExports() { return module };
        __webpack_require__.d(getter, "a", getter);
        return getter
    };
    __webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property) };
    __webpack_require__.p = "";
    return __webpack_require__(__webpack_require__.s = 0)
})([function(module, exports, __webpack_require__) {
    "use strict";
    __webpack_require__(1);
    __webpack_require__(6);
    __webpack_require__(7);
    var _inputmask = __webpack_require__(2);
    var _inputmask2 = _interopRequireDefault(_inputmask);
    var _inputmask3 = __webpack_require__(3);
    var _inputmask4 = _interopRequireDefault(_inputmask3);
    var _jquery = __webpack_require__(4);
    var _jquery2 = _interopRequireDefault(_jquery);

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj } }
    if (_inputmask4.default === _jquery2.default) { __webpack_require__(8) }
    window.Inputmask = _inputmask2.default
}, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    (function(factory) { if (true) {!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) } else {} })(function(Inputmask) {
        Inputmask.extendDefinitions({ A: { validator: "[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]", casing: "upper" }, "&": { validator: "[0-9A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]", casing: "upper" }, "#": { validator: "[0-9A-Fa-f]", casing: "upper" } });
        Inputmask.extendAliases({ cssunit: { regex: "[+-]?[0-9]+\\.?([0-9]+)?(px|em|rem|ex|%|in|cm|mm|pt|pc)" }, url: { regex: "(https?|ftp)//.*", autoUnmask: false }, ip: { mask: "i[i[i]].i[i[i]].i[i[i]].i[i[i]]", definitions: { i: { validator: function validator(chrs, maskset, pos, strict, opts) { if (pos - 1 > -1 && maskset.buffer[pos - 1] !== ".") { chrs = maskset.buffer[pos - 1] + chrs; if (pos - 2 > -1 && maskset.buffer[pos - 2] !== ".") { chrs = maskset.buffer[pos - 2] + chrs } else chrs = "0" + chrs } else chrs = "00" + chrs; return new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(chrs) } } }, onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) { return maskedValue }, inputmode: "numeric" }, email: { mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]", greedy: false, casing: "lower", onBeforePaste: function onBeforePaste(pastedValue, opts) { pastedValue = pastedValue.toLowerCase(); return pastedValue.replace("mailto:", "") }, definitions: { "*": { validator: "[0-9\uff11-\uff19A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5!#$%&'*+/=?^_`{|}~-]" }, "-": { validator: "[0-9A-Za-z-]" } }, onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) { return maskedValue }, inputmode: "email" }, mac: { mask: "##:##:##:##:##:##" }, vin: { mask: "V{13}9{4}", definitions: { V: { validator: "[A-HJ-NPR-Za-hj-npr-z\\d]", casing: "upper" } }, clearIncomplete: true, autoUnmask: true } });
        return Inputmask
    })
}, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    (function(factory) { if (true) {!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(3), __webpack_require__(5)], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) } else {} })(function($, window, undefined) {
        var document = window.document,
            ua = navigator.userAgent,
            ie = ua.indexOf("MSIE ") > 0 || ua.indexOf("Trident/") > 0,
            mobile = isInputEventSupported("touchstart"),
            iemobile = /iemobile/i.test(ua),
            iphone = /iphone/i.test(ua) && !iemobile;

        function Inputmask(alias, options, internal) {
            if (!(this instanceof Inputmask)) { return new Inputmask(alias, options, internal) }
            this.el = undefined;
            this.events = {};
            this.maskset = undefined;
            this.refreshValue = false;
            if (internal !== true) {
                if ($.isPlainObject(alias)) { options = alias } else { options = options || {}; if (alias) options.alias = alias }
                this.opts = $.extend(true, {}, this.defaults, options);
                this.noMasksCache = options && options.definitions !== undefined;
                this.userOptions = options || {};
                this.isRTL = this.opts.numericInput;
                resolveAlias(this.opts.alias, options, this.opts)
            }
        }
        Inputmask.prototype = {
            dataAttribute: "data-inputmask",
            defaults: { placeholder: "_", optionalmarker: ["[", "]"], quantifiermarker: ["{", "}"], groupmarker: ["(", ")"], alternatormarker: "|", escapeChar: "\\", mask: null, regex: null, oncomplete: $.noop, onincomplete: $.noop, oncleared: $.noop, repeat: 0, greedy: false, autoUnmask: false, removeMaskOnSubmit: false, clearMaskOnLostFocus: true, insertMode: true, clearIncomplete: false, alias: null, onKeyDown: $.noop, onBeforeMask: null, onBeforePaste: function onBeforePaste(pastedValue, opts) { return $.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(this, pastedValue, opts) : pastedValue }, onBeforeWrite: null, onUnMask: null, showMaskOnFocus: true, showMaskOnHover: true, onKeyValidation: $.noop, skipOptionalPartCharacter: " ", numericInput: false, rightAlign: false, undoOnEscape: true, radixPoint: "", _radixDance: false, groupSeparator: "", keepStatic: null, positionCaretOnTab: true, tabThrough: false, supportsInputType: ["text", "tel", "url", "password", "search"], ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 0, 229], isComplete: null, preValidation: null, postValidation: null, staticDefinitionSymbol: undefined, jitMasking: false, nullable: true, inputEventOnly: false, noValuePatching: false, positionCaretOnClick: "lvp", casing: null, inputmode: "verbatim", colorMask: false, disablePredictiveText: false, importDataAttributes: true, shiftPositions: true },
            definitions: { 9: { validator: "[0-9\uff11-\uff19]", definitionSymbol: "*" }, a: { validator: "[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]", definitionSymbol: "*" }, "*": { validator: "[0-9\uff11-\uff19A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]" } },
            aliases: {},
            masksCache: {},
            mask: function mask(elems) {
                var that = this;

                function importAttributeOptions(npt, opts, userOptions, dataAttribute) {
                    if (opts.importDataAttributes === true) {
                        var importOption = function importOption(option, optionData) {
                            optionData = optionData !== undefined ? optionData : npt.getAttribute(dataAttribute + "-" + option);
                            if (optionData !== null) {
                                if (typeof optionData === "string") {
                                    if (option.indexOf("on") === 0) optionData = window[optionData];
                                    else if (optionData === "false") optionData = false;
                                    else if (optionData === "true") optionData = true
                                }
                                userOptions[option] = optionData
                            }
                        };
                        var attrOptions = npt.getAttribute(dataAttribute),
                            option, dataoptions, optionData, p;
                        if (attrOptions && attrOptions !== "") {
                            attrOptions = attrOptions.replace(/'/g, '"');
                            dataoptions = JSON.parse("{" + attrOptions + "}")
                        }
                        if (dataoptions) { optionData = undefined; for (p in dataoptions) { if (p.toLowerCase() === "alias") { optionData = dataoptions[p]; break } } }
                        importOption("alias", optionData);
                        if (userOptions.alias) { resolveAlias(userOptions.alias, userOptions, opts) }
                        for (option in opts) {
                            if (dataoptions) { optionData = undefined; for (p in dataoptions) { if (p.toLowerCase() === option.toLowerCase()) { optionData = dataoptions[p]; break } } }
                            importOption(option, optionData)
                        }
                    }
                    $.extend(true, opts, userOptions);
                    if (npt.dir === "rtl" || opts.rightAlign) { npt.style.textAlign = "right" }
                    if (npt.dir === "rtl" || opts.numericInput) {
                        npt.dir = "ltr";
                        npt.removeAttribute("dir");
                        opts.isRTL = true
                    }
                    return Object.keys(userOptions).length
                }
                if (typeof elems === "string") { elems = document.getElementById(elems) || document.querySelectorAll(elems) }
                elems = elems.nodeName ? [elems] : elems;
                $.each(elems, function(ndx, el) {
                    var scopedOpts = $.extend(true, {}, that.opts);
                    if (importAttributeOptions(el, scopedOpts, $.extend(true, {}, that.userOptions), that.dataAttribute)) {
                        var maskset = generateMaskSet(scopedOpts, that.noMasksCache);
                        if (maskset !== undefined) {
                            if (el.inputmask !== undefined) {
                                el.inputmask.opts.autoUnmask = true;
                                el.inputmask.remove()
                            }
                            el.inputmask = new Inputmask(undefined, undefined, true);
                            el.inputmask.opts = scopedOpts;
                            el.inputmask.noMasksCache = that.noMasksCache;
                            el.inputmask.userOptions = $.extend(true, {}, that.userOptions);
                            el.inputmask.isRTL = scopedOpts.isRTL || scopedOpts.numericInput;
                            el.inputmask.el = el;
                            el.inputmask.maskset = maskset;
                            $.data(el, "_inputmask_opts", scopedOpts);
                            maskScope.call(el.inputmask, { action: "mask" })
                        }
                    }
                });
                return elems && elems[0] ? elems[0].inputmask || this : this
            },
            option: function option(options, noremask) { if (typeof options === "string") { return this.opts[options] } else if ((typeof options === "undefined" ? "undefined" : _typeof(options)) === "object") { $.extend(this.userOptions, options); if (this.el && noremask !== true) { this.mask(this.el) } return this } },
            unmaskedvalue: function unmaskedvalue(value) { this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache); return maskScope.call(this, { action: "unmaskedvalue", value: value }) },
            remove: function remove() { return maskScope.call(this, { action: "remove" }) },
            getemptymask: function getemptymask() { this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache); return maskScope.call(this, { action: "getemptymask" }) },
            hasMaskedValue: function hasMaskedValue() { return !this.opts.autoUnmask },
            isComplete: function isComplete() { this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache); return maskScope.call(this, { action: "isComplete" }) },
            getmetadata: function getmetadata() { this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache); return maskScope.call(this, { action: "getmetadata" }) },
            isValid: function isValid(value) { this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache); return maskScope.call(this, { action: "isValid", value: value }) },
            format: function format(value, metadata) { this.maskset = this.maskset || generateMaskSet(this.opts, this.noMasksCache); return maskScope.call(this, { action: "format", value: value, metadata: metadata }) },
            setValue: function setValue(value) { if (this.el) { $(this.el).trigger("setvalue", [value]) } },
            analyseMask: function analyseMask(mask, regexMask, opts) {
                var tokenizer = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?(?:\|[0-9\+\*]*)?\})|[^.?*+^${[]()|\\]+|./g,
                    regexTokenizer = /\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,
                    escaped = false,
                    currentToken = new MaskToken,
                    match, m, openenings = [],
                    maskTokens = [],
                    openingToken, currentOpeningToken, alternator, lastMatch, groupToken;

                function MaskToken(isGroup, isOptional, isQuantifier, isAlternator) {
                    this.matches = [];
                    this.openGroup = isGroup || false;
                    this.alternatorGroup = false;
                    this.isGroup = isGroup || false;
                    this.isOptional = isOptional || false;
                    this.isQuantifier = isQuantifier || false;
                    this.isAlternator = isAlternator || false;
                    this.quantifier = { min: 1, max: 1 }
                }

                function insertTestDefinition(mtoken, element, position) {
                    position = position !== undefined ? position : mtoken.matches.length;
                    var prevMatch = mtoken.matches[position - 1];
                    if (regexMask) {
                        if (element.indexOf("[") === 0 || escaped && /\\d|\\s|\\w]/i.test(element) || element === ".") { mtoken.matches.splice(position++, 0, { fn: new RegExp(element, opts.casing ? "i" : ""), optionality: false, newBlockMarker: prevMatch === undefined ? "master" : prevMatch.def !== element, casing: null, def: element, placeholder: undefined, nativeDef: element }) } else {
                            if (escaped) element = element[element.length - 1];
                            $.each(element.split(""), function(ndx, lmnt) {
                                prevMatch = mtoken.matches[position - 1];
                                mtoken.matches.splice(position++, 0, { fn: null, optionality: false, newBlockMarker: prevMatch === undefined ? "master" : prevMatch.def !== lmnt && prevMatch.fn !== null, casing: null, def: opts.staticDefinitionSymbol || lmnt, placeholder: opts.staticDefinitionSymbol !== undefined ? lmnt : undefined, nativeDef: (escaped ? "'" : "") + lmnt })
                            })
                        }
                        escaped = false
                    } else {
                        var maskdef = (opts.definitions ? opts.definitions[element] : undefined) || Inputmask.prototype.definitions[element];
                        if (maskdef && !escaped) { mtoken.matches.splice(position++, 0, { fn: maskdef.validator ? typeof maskdef.validator == "string" ? new RegExp(maskdef.validator, opts.casing ? "i" : "") : new function() { this.test = maskdef.validator } : new RegExp("."), optionality: false, newBlockMarker: prevMatch === undefined ? "master" : prevMatch.def !== (maskdef.definitionSymbol || element), casing: maskdef.casing, def: maskdef.definitionSymbol || element, placeholder: maskdef.placeholder, nativeDef: element }) } else {
                            mtoken.matches.splice(position++, 0, { fn: null, optionality: false, newBlockMarker: prevMatch === undefined ? "master" : prevMatch.def !== element && prevMatch.fn !== null, casing: null, def: opts.staticDefinitionSymbol || element, placeholder: opts.staticDefinitionSymbol !== undefined ? element : undefined, nativeDef: (escaped ? "'" : "") + element });
                            escaped = false
                        }
                    }
                }

                function verifyGroupMarker(maskToken) {
                    if (maskToken && maskToken.matches) {
                        $.each(maskToken.matches, function(ndx, token) {
                            var nextToken = maskToken.matches[ndx + 1];
                            if ((nextToken === undefined || nextToken.matches === undefined || nextToken.isQuantifier === false) && token && token.isGroup) { token.isGroup = false; if (!regexMask) { insertTestDefinition(token, opts.groupmarker[0], 0); if (token.openGroup !== true) { insertTestDefinition(token, opts.groupmarker[1]) } } }
                            verifyGroupMarker(token)
                        })
                    }
                }

                function defaultCase() {
                    if (openenings.length > 0) {
                        currentOpeningToken = openenings[openenings.length - 1];
                        insertTestDefinition(currentOpeningToken, m);
                        if (currentOpeningToken.isAlternator) {
                            alternator = openenings.pop();
                            for (var mndx = 0; mndx < alternator.matches.length; mndx++) { if (alternator.matches[mndx].isGroup) alternator.matches[mndx].isGroup = false }
                            if (openenings.length > 0) {
                                currentOpeningToken = openenings[openenings.length - 1];
                                currentOpeningToken.matches.push(alternator)
                            } else { currentToken.matches.push(alternator) }
                        }
                    } else { insertTestDefinition(currentToken, m) }
                }

                function reverseTokens(maskToken) {
                    function reverseStatic(st) {
                        if (st === opts.optionalmarker[0]) st = opts.optionalmarker[1];
                        else if (st === opts.optionalmarker[1]) st = opts.optionalmarker[0];
                        else if (st === opts.groupmarker[0]) st = opts.groupmarker[1];
                        else if (st === opts.groupmarker[1]) st = opts.groupmarker[0];
                        return st
                    }
                    maskToken.matches = maskToken.matches.reverse();
                    for (var match in maskToken.matches) {
                        if (maskToken.matches.hasOwnProperty(match)) {
                            var intMatch = parseInt(match);
                            if (maskToken.matches[match].isQuantifier && maskToken.matches[intMatch + 1] && maskToken.matches[intMatch + 1].isGroup) {
                                var qt = maskToken.matches[match];
                                maskToken.matches.splice(match, 1);
                                maskToken.matches.splice(intMatch + 1, 0, qt)
                            }
                            if (maskToken.matches[match].matches !== undefined) { maskToken.matches[match] = reverseTokens(maskToken.matches[match]) } else { maskToken.matches[match] = reverseStatic(maskToken.matches[match]) }
                        }
                    }
                    return maskToken
                }

                function groupify(matches) {
                    var groupToken = new MaskToken(true);
                    groupToken.openGroup = false;
                    groupToken.matches = matches;
                    return groupToken
                }
                if (regexMask) {
                    opts.optionalmarker[0] = undefined;
                    opts.optionalmarker[1] = undefined
                }
                while (match = regexMask ? regexTokenizer.exec(mask) : tokenizer.exec(mask)) {
                    m = match[0];
                    if (regexMask) {
                        switch (m.charAt(0)) {
                            case "?":
                                m = "{0,1}";
                                break;
                            case "+":
                            case "*":
                                m = "{" + m + "}";
                                break
                        }
                    }
                    if (escaped) { defaultCase(); continue }
                    switch (m.charAt(0)) {
                        case "(?=":
                            break;
                        case "(?!":
                            break;
                        case "(?<=":
                            break;
                        case "(?<!":
                            break;
                        case opts.escapeChar:
                            escaped = true;
                            if (regexMask) { defaultCase() }
                            break;
                        case opts.optionalmarker[1]:
                        case opts.groupmarker[1]:
                            openingToken = openenings.pop();
                            openingToken.openGroup = false;
                            if (openingToken !== undefined) {
                                if (openenings.length > 0) {
                                    currentOpeningToken = openenings[openenings.length - 1];
                                    currentOpeningToken.matches.push(openingToken);
                                    if (currentOpeningToken.isAlternator) {
                                        alternator = openenings.pop();
                                        for (var mndx = 0; mndx < alternator.matches.length; mndx++) {
                                            alternator.matches[mndx].isGroup = false;
                                            alternator.matches[mndx].alternatorGroup = false
                                        }
                                        if (openenings.length > 0) {
                                            currentOpeningToken = openenings[openenings.length - 1];
                                            currentOpeningToken.matches.push(alternator)
                                        } else { currentToken.matches.push(alternator) }
                                    }
                                } else { currentToken.matches.push(openingToken) }
                            } else defaultCase();
                            break;
                        case opts.optionalmarker[0]:
                            openenings.push(new MaskToken(false, true));
                            break;
                        case opts.groupmarker[0]:
                            openenings.push(new MaskToken(true));
                            break;
                        case opts.quantifiermarker[0]:
                            var quantifier = new MaskToken(false, false, true);
                            m = m.replace(/[{}]/g, "");
                            var mqj = m.split("|"),
                                mq = mqj[0].split(","),
                                mq0 = isNaN(mq[0]) ? mq[0] : parseInt(mq[0]),
                                mq1 = mq.length === 1 ? mq0 : isNaN(mq[1]) ? mq[1] : parseInt(mq[1]);
                            if (mq0 === "*" || mq0 === "+") { mq0 = mq1 === "*" ? 0 : 1 }
                            quantifier.quantifier = { min: mq0, max: mq1, jit: mqj[1] };
                            var matches = openenings.length > 0 ? openenings[openenings.length - 1].matches : currentToken.matches;
                            match = matches.pop();
                            if (match.isAlternator) {
                                matches.push(match);
                                matches = match.matches;
                                var groupToken = new MaskToken(true);
                                var tmpMatch = matches.pop();
                                matches.push(groupToken);
                                matches = groupToken.matches;
                                match = tmpMatch
                            }
                            if (!match.isGroup) { match = groupify([match]) }
                            matches.push(match);
                            matches.push(quantifier);
                            break;
                        case opts.alternatormarker:
                            var groupQuantifier = function groupQuantifier(matches) { var lastMatch = matches.pop(); if (lastMatch.isQuantifier) { lastMatch = groupify([matches.pop(), lastMatch]) } return lastMatch };
                            if (openenings.length > 0) { currentOpeningToken = openenings[openenings.length - 1]; var subToken = currentOpeningToken.matches[currentOpeningToken.matches.length - 1]; if (currentOpeningToken.openGroup && (subToken.matches === undefined || subToken.isGroup === false && subToken.isAlternator === false)) { lastMatch = openenings.pop() } else { lastMatch = groupQuantifier(currentOpeningToken.matches) } } else { lastMatch = groupQuantifier(currentToken.matches) }
                            if (lastMatch.isAlternator) { openenings.push(lastMatch) } else {
                                if (lastMatch.alternatorGroup) {
                                    alternator = openenings.pop();
                                    lastMatch.alternatorGroup = false
                                } else { alternator = new MaskToken(false, false, false, true) }
                                alternator.matches.push(lastMatch);
                                openenings.push(alternator);
                                if (lastMatch.openGroup) {
                                    lastMatch.openGroup = false;
                                    var alternatorGroup = new MaskToken(true);
                                    alternatorGroup.alternatorGroup = true;
                                    openenings.push(alternatorGroup)
                                }
                            }
                            break;
                        default:
                            defaultCase()
                    }
                }
                while (openenings.length > 0) {
                    openingToken = openenings.pop();
                    currentToken.matches.push(openingToken)
                }
                if (currentToken.matches.length > 0) {
                    verifyGroupMarker(currentToken);
                    maskTokens.push(currentToken)
                }
                if (opts.numericInput || opts.isRTL) { reverseTokens(maskTokens[0]) }
                return maskTokens
            }
        };
        Inputmask.extendDefaults = function(options) { $.extend(true, Inputmask.prototype.defaults, options) };
        Inputmask.extendDefinitions = function(definition) { $.extend(true, Inputmask.prototype.definitions, definition) };
        Inputmask.extendAliases = function(alias) { $.extend(true, Inputmask.prototype.aliases, alias) };
        Inputmask.format = function(value, options, metadata) { return Inputmask(options).format(value, metadata) };
        Inputmask.unmask = function(value, options) { return Inputmask(options).unmaskedvalue(value) };
        Inputmask.isValid = function(value, options) { return Inputmask(options).isValid(value) };
        Inputmask.remove = function(elems) {
            if (typeof elems === "string") { elems = document.getElementById(elems) || document.querySelectorAll(elems) }
            elems = elems.nodeName ? [elems] : elems;
            $.each(elems, function(ndx, el) { if (el.inputmask) el.inputmask.remove() })
        };
        Inputmask.setValue = function(elems, value) {
            if (typeof elems === "string") { elems = document.getElementById(elems) || document.querySelectorAll(elems) }
            elems = elems.nodeName ? [elems] : elems;
            $.each(elems, function(ndx, el) {
                if (el.inputmask) el.inputmask.setValue(value);
                else $(el).trigger("setvalue", [value])
            })
        };
        Inputmask.escapeRegex = function(str) { var specials = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"]; return str.replace(new RegExp("(\\" + specials.join("|\\") + ")", "gim"), "\\$1") };
        Inputmask.keyCode = { BACKSPACE: 8, BACKSPACE_SAFARI: 127, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, INSERT: 45, LEFT: 37, PAGE_DOWN: 34, PAGE_UP: 33, RIGHT: 39, SPACE: 32, TAB: 9, UP: 38, X: 88, CONTROL: 17 };
        Inputmask.dependencyLib = $;

        function resolveAlias(aliasStr, options, opts) {
            var aliasDefinition = Inputmask.prototype.aliases[aliasStr];
            if (aliasDefinition) {
                if (aliasDefinition.alias) resolveAlias(aliasDefinition.alias, undefined, opts);
                $.extend(true, opts, aliasDefinition);
                $.extend(true, opts, options);
                return true
            } else if (opts.mask === null) { opts.mask = aliasStr }
            return false
        }

        function generateMaskSet(opts, nocache) {
            function generateMask(mask, metadata, opts) {
                var regexMask = false;
                if (mask === null || mask === "") {
                    regexMask = opts.regex !== null;
                    if (regexMask) {
                        mask = opts.regex;
                        mask = mask.replace(/^(\^)(.*)(\$)$/, "$2")
                    } else {
                        regexMask = true;
                        mask = ".*"
                    }
                }
                if (mask.length === 1 && opts.greedy === false && opts.repeat !== 0) { opts.placeholder = "" }
                if (opts.repeat > 0 || opts.repeat === "*" || opts.repeat === "+") {
                    var repeatStart = opts.repeat === "*" ? 0 : opts.repeat === "+" ? 1 : opts.repeat;
                    mask = opts.groupmarker[0] + mask + opts.groupmarker[1] + opts.quantifiermarker[0] + repeatStart + "," + opts.repeat + opts.quantifiermarker[1]
                }
                var masksetDefinition, maskdefKey = regexMask ? "regex_" + opts.regex : opts.numericInput ? mask.split("").reverse().join("") : mask;
                if (Inputmask.prototype.masksCache[maskdefKey] === undefined || nocache === true) {
                    masksetDefinition = { mask: mask, maskToken: Inputmask.prototype.analyseMask(mask, regexMask, opts), validPositions: {}, _buffer: undefined, buffer: undefined, tests: {}, excludes: {}, metadata: metadata, maskLength: undefined, jitOffset: {} };
                    if (nocache !== true) {
                        Inputmask.prototype.masksCache[maskdefKey] = masksetDefinition;
                        masksetDefinition = $.extend(true, {}, Inputmask.prototype.masksCache[maskdefKey])
                    }
                } else masksetDefinition = $.extend(true, {}, Inputmask.prototype.masksCache[maskdefKey]);
                return masksetDefinition
            }
            var ms;
            if ($.isFunction(opts.mask)) { opts.mask = opts.mask(opts) }
            if ($.isArray(opts.mask)) {
                if (opts.mask.length > 1) {
                    if (opts.keepStatic === null) { opts.keepStatic = "auto"; for (var i = 0; i < opts.mask.length; i++) { if (opts.mask[i].charAt(0) !== opts.mask[0].charAt(0)) { opts.keepStatic = true; break } } }
                    var altMask = opts.groupmarker[0];
                    $.each(opts.isRTL ? opts.mask.reverse() : opts.mask, function(ndx, msk) { if (altMask.length > 1) { altMask += opts.groupmarker[1] + opts.alternatormarker + opts.groupmarker[0] } if (msk.mask !== undefined && !$.isFunction(msk.mask)) { altMask += msk.mask } else { altMask += msk } });
                    altMask += opts.groupmarker[1];
                    return generateMask(altMask, opts.mask, opts)
                } else opts.mask = opts.mask.pop()
            }
            if (opts.mask && opts.mask.mask !== undefined && !$.isFunction(opts.mask.mask)) { ms = generateMask(opts.mask.mask, opts.mask, opts) } else { ms = generateMask(opts.mask, opts.mask, opts) }
            return ms
        }

        function isInputEventSupported(eventName) {
            var el = document.createElement("input"),
                evName = "on" + eventName,
                isSupported = evName in el;
            if (!isSupported) {
                el.setAttribute(evName, "return;");
                isSupported = typeof el[evName] === "function"
            }
            el = null;
            return isSupported
        }

        function maskScope(actionObj, maskset, opts) {
            maskset = maskset || this.maskset;
            opts = opts || this.opts;
            var inputmask = this,
                el = this.el,
                isRTL = this.isRTL,
                undoValue, $el, skipKeyPressEvent = false,
                skipInputEvent = false,
                ignorable = false,
                maxLength, mouseEnter = false,
                colorMask, originalPlaceholder;

            function getMaskTemplate(baseOnInput, minimalPos, includeMode, noJit, clearOptionalTail) {
                var greedy = opts.greedy;
                if (clearOptionalTail) opts.greedy = false;
                minimalPos = minimalPos || 0;
                var maskTemplate = [],
                    ndxIntlzr, pos = 0,
                    test, testPos, lvp = getLastValidPosition();
                do {
                    if (baseOnInput === true && getMaskSet().validPositions[pos]) {
                        testPos = clearOptionalTail && getMaskSet().validPositions[pos].match.optionality === true && getMaskSet().validPositions[pos + 1] === undefined && (getMaskSet().validPositions[pos].generatedInput === true || getMaskSet().validPositions[pos].input == opts.skipOptionalPartCharacter && pos > 0) ? determineTestTemplate(pos, getTests(pos, ndxIntlzr, pos - 1)) : getMaskSet().validPositions[pos];
                        test = testPos.match;
                        ndxIntlzr = testPos.locator.slice();
                        maskTemplate.push(includeMode === true ? testPos.input : includeMode === false ? test.nativeDef : getPlaceholder(pos, test))
                    } else {
                        testPos = getTestTemplate(pos, ndxIntlzr, pos - 1);
                        test = testPos.match;
                        ndxIntlzr = testPos.locator.slice();
                        var jitMasking = noJit === true ? false : opts.jitMasking !== false ? opts.jitMasking : test.jit;
                        if (jitMasking === false || jitMasking === undefined || typeof jitMasking === "number" && isFinite(jitMasking) && jitMasking > pos) { maskTemplate.push(includeMode === false ? test.nativeDef : getPlaceholder(pos, test)) }
                    }
                    if (opts.keepStatic === "auto") { if (test.newBlockMarker && test.fn !== null) { opts.keepStatic = pos - 1 } }
                    pos++
                } while ((maxLength === undefined || pos < maxLength) && (test.fn !== null || test.def !== "") || minimalPos > pos);
                if (maskTemplate[maskTemplate.length - 1] === "") { maskTemplate.pop() }
                if (includeMode !== false || getMaskSet().maskLength === undefined) getMaskSet().maskLength = pos - 1;
                opts.greedy = greedy;
                return maskTemplate
            }

            function getMaskSet() { return maskset }

            function resetMaskSet(soft) {
                var maskset = getMaskSet();
                maskset.buffer = undefined;
                if (soft !== true) {
                    maskset.validPositions = {};
                    maskset.p = 0
                }
            }

            function getLastValidPosition(closestTo, strict, validPositions) {
                var before = -1,
                    after = -1,
                    valids = validPositions || getMaskSet().validPositions;
                if (closestTo === undefined) closestTo = -1;
                for (var posNdx in valids) { var psNdx = parseInt(posNdx); if (valids[psNdx] && (strict || valids[psNdx].generatedInput !== true)) { if (psNdx <= closestTo) before = psNdx; if (psNdx >= closestTo) after = psNdx } }
                return before === -1 || before == closestTo ? after : after == -1 ? before : closestTo - before < after - closestTo ? before : after
            }

            function getDecisionTaker(tst) { var decisionTaker = tst.locator[tst.alternation]; if (typeof decisionTaker == "string" && decisionTaker.length > 0) { decisionTaker = decisionTaker.split(",")[0] } return decisionTaker !== undefined ? decisionTaker.toString() : "" }

            function getLocator(tst, align) {
                var locator = (tst.alternation != undefined ? tst.mloc[getDecisionTaker(tst)] : tst.locator).join("");
                if (locator !== "")
                    while (locator.length < align) { locator += "0" }
                return locator
            }

            function determineTestTemplate(pos, tests) {
                pos = pos > 0 ? pos - 1 : 0;
                var altTest = getTest(pos),
                    targetLocator = getLocator(altTest),
                    tstLocator, closest, bestMatch;
                for (var ndx = 0; ndx < tests.length; ndx++) {
                    var tst = tests[ndx];
                    tstLocator = getLocator(tst, targetLocator.length);
                    var distance = Math.abs(tstLocator - targetLocator);
                    if (closest === undefined || tstLocator !== "" && distance < closest || bestMatch && !opts.greedy && bestMatch.match.optionality && bestMatch.match.newBlockMarker === "master" && (!tst.match.optionality || !tst.match.newBlockMarker) || bestMatch && bestMatch.match.optionalQuantifier && !tst.match.optionalQuantifier) {
                        closest = distance;
                        bestMatch = tst
                    }
                }
                return bestMatch
            }

            function getTestTemplate(pos, ndxIntlzr, tstPs) { return getMaskSet().validPositions[pos] || determineTestTemplate(pos, getTests(pos, ndxIntlzr ? ndxIntlzr.slice() : ndxIntlzr, tstPs)) }

            function getTest(pos, tests) { if (getMaskSet().validPositions[pos]) { return getMaskSet().validPositions[pos] } return (tests || getTests(pos))[0] }

            function positionCanMatchDefinition(pos, def) {
                var valid = false,
                    tests = getTests(pos);
                for (var tndx = 0; tndx < tests.length; tndx++) { if (tests[tndx].match && tests[tndx].match.def === def) { valid = true; break } }
                return valid
            }

            function getTests(pos, ndxIntlzr, tstPs) {
                var maskTokens = getMaskSet().maskToken,
                    testPos = ndxIntlzr ? tstPs : 0,
                    ndxInitializer = ndxIntlzr ? ndxIntlzr.slice() : [0],
                    matches = [],
                    insertStop = false,
                    latestMatch, cacheDependency = ndxIntlzr ? ndxIntlzr.join("") : "";

                function resolveTestFromToken(maskToken, ndxInitializer, loopNdx, quantifierRecurse) {
                    function handleMatch(match, loopNdx, quantifierRecurse) {
                        function isFirstMatch(latestMatch, tokenGroup) {
                            var firstMatch = $.inArray(latestMatch, tokenGroup.matches) === 0;
                            if (!firstMatch) {
                                $.each(tokenGroup.matches, function(ndx, match) {
                                    if (match.isQuantifier === true) firstMatch = isFirstMatch(latestMatch, tokenGroup.matches[ndx - 1]);
                                    else if (match.hasOwnProperty("matches")) firstMatch = isFirstMatch(latestMatch, match);
                                    if (firstMatch) return false
                                })
                            }
                            return firstMatch
                        }

                        function resolveNdxInitializer(pos, alternateNdx, targetAlternation) {
                            var bestMatch, indexPos;
                            if (getMaskSet().tests[pos] || getMaskSet().validPositions[pos]) {
                                $.each(getMaskSet().tests[pos] || [getMaskSet().validPositions[pos]], function(ndx, lmnt) {
                                    if (lmnt.mloc[alternateNdx]) { bestMatch = lmnt; return false }
                                    var alternation = targetAlternation !== undefined ? targetAlternation : lmnt.alternation,
                                        ndxPos = lmnt.locator[alternation] !== undefined ? lmnt.locator[alternation].toString().indexOf(alternateNdx) : -1;
                                    if ((indexPos === undefined || ndxPos < indexPos) && ndxPos !== -1) {
                                        bestMatch = lmnt;
                                        indexPos = ndxPos
                                    }
                                })
                            }
                            if (bestMatch) { var bestMatchAltIndex = bestMatch.locator[bestMatch.alternation]; var locator = bestMatch.mloc[alternateNdx] || bestMatch.mloc[bestMatchAltIndex] || bestMatch.locator; return locator.slice((targetAlternation !== undefined ? targetAlternation : bestMatch.alternation) + 1) } else { return targetAlternation !== undefined ? resolveNdxInitializer(pos, alternateNdx) : undefined }
                        }

                        function isSubsetOf(source, target) {
                            function expand(pattern) {
                                var expanded = [],
                                    start, end;
                                for (var i = 0, l = pattern.length; i < l; i++) {
                                    if (pattern.charAt(i) === "-") { end = pattern.charCodeAt(i + 1); while (++start < end) { expanded.push(String.fromCharCode(start)) } } else {
                                        start = pattern.charCodeAt(i);
                                        expanded.push(pattern.charAt(i))
                                    }
                                }
                                return expanded.join("")
                            }
                            if (opts.regex && source.match.fn !== null && target.match.fn !== null) { return expand(target.match.def.replace(/[\[\]]/g, "")).indexOf(expand(source.match.def.replace(/[\[\]]/g, ""))) !== -1 }
                            return source.match.def === target.match.nativeDef
                        }

                        function staticCanMatchDefinition(source, target) {
                            var sloc = source.locator.slice(source.alternation).join(""),
                                tloc = target.locator.slice(target.alternation).join(""),
                                canMatch = sloc == tloc;
                            canMatch = canMatch && source.match.fn === null && target.match.fn !== null ? target.match.fn.test(source.match.def, getMaskSet(), pos, false, opts, false) : false;
                            return canMatch
                        }

                        function setMergeLocators(targetMatch, altMatch) {
                            if (altMatch === undefined || targetMatch.alternation === altMatch.alternation && targetMatch.locator[targetMatch.alternation].toString().indexOf(altMatch.locator[altMatch.alternation]) === -1) {
                                targetMatch.mloc = targetMatch.mloc || {};
                                var locNdx = targetMatch.locator[targetMatch.alternation];
                                if (locNdx === undefined) targetMatch.alternation = undefined;
                                else {
                                    if (typeof locNdx === "string") locNdx = locNdx.split(",")[0];
                                    if (targetMatch.mloc[locNdx] === undefined) targetMatch.mloc[locNdx] = targetMatch.locator.slice();
                                    if (altMatch !== undefined) {
                                        for (var ndx in altMatch.mloc) { if (typeof ndx === "string") ndx = ndx.split(",")[0]; if (targetMatch.mloc[ndx] === undefined) targetMatch.mloc[ndx] = altMatch.mloc[ndx] }
                                        targetMatch.locator[targetMatch.alternation] = Object.keys(targetMatch.mloc).join(",")
                                    }
                                    return true
                                }
                            }
                            return false
                        }
                        if (testPos > 500 && quantifierRecurse !== undefined) { throw "Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + getMaskSet().mask }
                        if (testPos === pos && match.matches === undefined) { matches.push({ match: match, locator: loopNdx.reverse(), cd: cacheDependency, mloc: {} }); return true } else if (match.matches !== undefined) {
                            if (match.isGroup && quantifierRecurse !== match) { match = handleMatch(maskToken.matches[$.inArray(match, maskToken.matches) + 1], loopNdx, quantifierRecurse); if (match) return true } else if (match.isOptional) {
                                var optionalToken = match;
                                match = resolveTestFromToken(match, ndxInitializer, loopNdx, quantifierRecurse);
                                if (match) {
                                    $.each(matches, function(ndx, mtch) { mtch.match.optionality = true });
                                    latestMatch = matches[matches.length - 1].match;
                                    if (quantifierRecurse === undefined && isFirstMatch(latestMatch, optionalToken)) {
                                        insertStop = true;
                                        testPos = pos
                                    } else return true
                                }
                            } else if (match.isAlternator) {
                                var alternateToken = match,
                                    malternateMatches = [],
                                    maltMatches, currentMatches = matches.slice(),
                                    loopNdxCnt = loopNdx.length;
                                var altIndex = ndxInitializer.length > 0 ? ndxInitializer.shift() : -1;
                                if (altIndex === -1 || typeof altIndex === "string") {
                                    var currentPos = testPos,
                                        ndxInitializerClone = ndxInitializer.slice(),
                                        altIndexArr = [],
                                        amndx;
                                    if (typeof altIndex == "string") { altIndexArr = altIndex.split(",") } else { for (amndx = 0; amndx < alternateToken.matches.length; amndx++) { altIndexArr.push(amndx.toString()) } }
                                    if (getMaskSet().excludes[pos]) {
                                        var altIndexArrClone = altIndexArr.slice();
                                        for (var i = 0, el = getMaskSet().excludes[pos].length; i < el; i++) { altIndexArr.splice(altIndexArr.indexOf(getMaskSet().excludes[pos][i].toString()), 1) }
                                        if (altIndexArr.length === 0) {
                                            getMaskSet().excludes[pos] = undefined;
                                            altIndexArr = altIndexArrClone
                                        }
                                    }
                                    if (opts.keepStatic === true || isFinite(parseInt(opts.keepStatic)) && currentPos >= opts.keepStatic) altIndexArr = altIndexArr.slice(0, 1);
                                    var unMatchedAlternation = false;
                                    for (var ndx = 0; ndx < altIndexArr.length; ndx++) {
                                        amndx = parseInt(altIndexArr[ndx]);
                                        matches = [];
                                        ndxInitializer = typeof altIndex === "string" ? resolveNdxInitializer(testPos, amndx, loopNdxCnt) || ndxInitializerClone.slice() : ndxInitializerClone.slice();
                                        if (alternateToken.matches[amndx] && handleMatch(alternateToken.matches[amndx], [amndx].concat(loopNdx), quantifierRecurse)) match = true;
                                        else if (ndx === 0) { unMatchedAlternation = true }
                                        maltMatches = matches.slice();
                                        testPos = currentPos;
                                        matches = [];
                                        for (var ndx1 = 0; ndx1 < maltMatches.length; ndx1++) {
                                            var altMatch = maltMatches[ndx1],
                                                dropMatch = false;
                                            altMatch.match.jit = altMatch.match.jit || unMatchedAlternation;
                                            altMatch.alternation = altMatch.alternation || loopNdxCnt;
                                            setMergeLocators(altMatch);
                                            for (var ndx2 = 0; ndx2 < malternateMatches.length; ndx2++) {
                                                var altMatch2 = malternateMatches[ndx2];
                                                if (typeof altIndex !== "string" || altMatch.alternation !== undefined && $.inArray(altMatch.locator[altMatch.alternation].toString(), altIndexArr) !== -1) {
                                                    if (altMatch.match.nativeDef === altMatch2.match.nativeDef) {
                                                        dropMatch = true;
                                                        setMergeLocators(altMatch2, altMatch);
                                                        break
                                                    } else if (isSubsetOf(altMatch, altMatch2)) {
                                                        if (setMergeLocators(altMatch, altMatch2)) {
                                                            dropMatch = true;
                                                            malternateMatches.splice(malternateMatches.indexOf(altMatch2), 0, altMatch)
                                                        }
                                                        break
                                                    } else if (isSubsetOf(altMatch2, altMatch)) { setMergeLocators(altMatch2, altMatch); break } else if (staticCanMatchDefinition(altMatch, altMatch2)) {
                                                        if (setMergeLocators(altMatch, altMatch2)) {
                                                            dropMatch = true;
                                                            malternateMatches.splice(malternateMatches.indexOf(altMatch2), 0, altMatch)
                                                        }
                                                        break
                                                    }
                                                }
                                            }
                                            if (!dropMatch) { malternateMatches.push(altMatch) }
                                        }
                                    }
                                    matches = currentMatches.concat(malternateMatches);
                                    testPos = pos;
                                    insertStop = matches.length > 0;
                                    match = malternateMatches.length > 0;
                                    ndxInitializer = ndxInitializerClone.slice()
                                } else match = handleMatch(alternateToken.matches[altIndex] || maskToken.matches[altIndex], [altIndex].concat(loopNdx), quantifierRecurse);
                                if (match) return true
                            } else if (match.isQuantifier && quantifierRecurse !== maskToken.matches[$.inArray(match, maskToken.matches) - 1]) {
                                var qt = match;
                                for (var qndx = ndxInitializer.length > 0 ? ndxInitializer.shift() : 0; qndx < (isNaN(qt.quantifier.max) ? qndx + 1 : qt.quantifier.max) && testPos <= pos; qndx++) {
                                    var tokenGroup = maskToken.matches[$.inArray(qt, maskToken.matches) - 1];
                                    match = handleMatch(tokenGroup, [qndx].concat(loopNdx), tokenGroup);
                                    if (match) {
                                        latestMatch = matches[matches.length - 1].match;
                                        latestMatch.optionalQuantifier = qndx >= qt.quantifier.min;
                                        latestMatch.jit = (qndx || 1) * tokenGroup.matches.indexOf(latestMatch) >= qt.quantifier.jit;
                                        if (latestMatch.optionalQuantifier && isFirstMatch(latestMatch, tokenGroup)) {
                                            insertStop = true;
                                            testPos = pos;
                                            break
                                        }
                                        if (latestMatch.jit) { getMaskSet().jitOffset[pos] = tokenGroup.matches.indexOf(latestMatch) }
                                        return true
                                    }
                                }
                            } else { match = resolveTestFromToken(match, ndxInitializer, loopNdx, quantifierRecurse); if (match) return true }
                        } else { testPos++ }
                    }
                    for (var tndx = ndxInitializer.length > 0 ? ndxInitializer.shift() : 0; tndx < maskToken.matches.length; tndx++) { if (maskToken.matches[tndx].isQuantifier !== true) { var match = handleMatch(maskToken.matches[tndx], [tndx].concat(loopNdx), quantifierRecurse); if (match && testPos === pos) { return match } else if (testPos > pos) { break } } }
                }

                function mergeLocators(pos, tests) {
                    var locator = [];
                    if (!$.isArray(tests)) tests = [tests];
                    if (tests.length > 0) {
                        if (tests[0].alternation === undefined) { locator = determineTestTemplate(pos, tests.slice()).locator.slice(); if (locator.length === 0) locator = tests[0].locator.slice() } else {
                            $.each(tests, function(ndx, tst) {
                                if (tst.def !== "") {
                                    if (locator.length === 0) locator = tst.locator.slice();
                                    else { for (var i = 0; i < locator.length; i++) { if (tst.locator[i] && locator[i].toString().indexOf(tst.locator[i]) === -1) { locator[i] += "," + tst.locator[i] } } }
                                }
                            })
                        }
                    }
                    return locator
                }
                if (pos > -1) {
                    if (ndxIntlzr === undefined) {
                        var previousPos = pos - 1,
                            test;
                        while ((test = getMaskSet().validPositions[previousPos] || getMaskSet().tests[previousPos]) === undefined && previousPos > -1) { previousPos-- }
                        if (test !== undefined && previousPos > -1) {
                            ndxInitializer = mergeLocators(previousPos, test);
                            cacheDependency = ndxInitializer.join("");
                            testPos = previousPos
                        }
                    }
                    if (getMaskSet().tests[pos] && getMaskSet().tests[pos][0].cd === cacheDependency) { return getMaskSet().tests[pos] }
                    for (var mtndx = ndxInitializer.shift(); mtndx < maskTokens.length; mtndx++) { var match = resolveTestFromToken(maskTokens[mtndx], ndxInitializer, [mtndx]); if (match && testPos === pos || testPos > pos) { break } }
                }
                if (matches.length === 0 || insertStop) { matches.push({ match: { fn: null, optionality: false, casing: null, def: "", placeholder: "" }, locator: [], mloc: {}, cd: cacheDependency }) }
                if (ndxIntlzr !== undefined && getMaskSet().tests[pos]) { return $.extend(true, [], matches) }
                getMaskSet().tests[pos] = $.extend(true, [], matches);
                return getMaskSet().tests[pos]
            }

            function getBufferTemplate() { if (getMaskSet()._buffer === undefined) { getMaskSet()._buffer = getMaskTemplate(false, 1); if (getMaskSet().buffer === undefined) getMaskSet().buffer = getMaskSet()._buffer.slice() } return getMaskSet()._buffer }

            function getBuffer(noCache) { if (getMaskSet().buffer === undefined || noCache === true) { getMaskSet().buffer = getMaskTemplate(true, getLastValidPosition(), true); if (getMaskSet()._buffer === undefined) getMaskSet()._buffer = getMaskSet().buffer.slice() } return getMaskSet().buffer }

            function refreshFromBuffer(start, end, buffer) {
                var i, p;
                if (start === true) {
                    resetMaskSet();
                    start = 0;
                    end = buffer.length
                } else { for (i = start; i < end; i++) { delete getMaskSet().validPositions[i] } }
                p = start;
                for (i = start; i < end; i++) {
                    resetMaskSet(true);
                    if (buffer[i] !== opts.skipOptionalPartCharacter) {
                        var valResult = isValid(p, buffer[i], true, true);
                        if (valResult !== false) {
                            resetMaskSet(true);
                            p = valResult.caret !== undefined ? valResult.caret : valResult.pos + 1
                        }
                    }
                }
            }

            function casing(elem, test, pos) {
                switch (opts.casing || test.casing) {
                    case "upper":
                        elem = elem.toUpperCase();
                        break;
                    case "lower":
                        elem = elem.toLowerCase();
                        break;
                    case "title":
                        var posBefore = getMaskSet().validPositions[pos - 1];
                        if (pos === 0 || posBefore && posBefore.input === String.fromCharCode(Inputmask.keyCode.SPACE)) { elem = elem.toUpperCase() } else { elem = elem.toLowerCase() }
                        break;
                    default:
                        if ($.isFunction(opts.casing)) {
                            var args = Array.prototype.slice.call(arguments);
                            args.push(getMaskSet().validPositions);
                            elem = opts.casing.apply(this, args)
                        }
                }
                return elem
            }

            function checkAlternationMatch(altArr1, altArr2, na) {
                var altArrC = opts.greedy ? altArr2 : altArr2.slice(0, 1),
                    isMatch = false,
                    naArr = na !== undefined ? na.split(",") : [],
                    naNdx;
                for (var i = 0; i < naArr.length; i++) { if ((naNdx = altArr1.indexOf(naArr[i])) !== -1) { altArr1.splice(naNdx, 1) } }
                for (var alndx = 0; alndx < altArr1.length; alndx++) { if ($.inArray(altArr1[alndx], altArrC) !== -1) { isMatch = true; break } }
                return isMatch
            }

            function alternate(pos, c, strict, fromSetValid, rAltPos) {
                var validPsClone = $.extend(true, {}, getMaskSet().validPositions),
                    lastAlt, alternation, isValidRslt = false,
                    altPos, prevAltPos, i, validPos, decisionPos, lAltPos = rAltPos !== undefined ? rAltPos : getLastValidPosition();
                if (lAltPos === -1 && rAltPos === undefined) {
                    lastAlt = 0;
                    prevAltPos = getTest(lastAlt);
                    alternation = prevAltPos.alternation
                } else {
                    for (; lAltPos >= 0; lAltPos--) {
                        altPos = getMaskSet().validPositions[lAltPos];
                        if (altPos && altPos.alternation !== undefined) {
                            if (prevAltPos && prevAltPos.locator[altPos.alternation] !== altPos.locator[altPos.alternation]) { break }
                            lastAlt = lAltPos;
                            alternation = getMaskSet().validPositions[lastAlt].alternation;
                            prevAltPos = altPos
                        }
                    }
                }
                if (alternation !== undefined) {
                    decisionPos = parseInt(lastAlt);
                    getMaskSet().excludes[decisionPos] = getMaskSet().excludes[decisionPos] || [];
                    if (pos !== true) { getMaskSet().excludes[decisionPos].push(getDecisionTaker(prevAltPos)) }
                    var validInputsClone = [],
                        staticInputsBeforePos = 0;
                    for (i = decisionPos; i < getLastValidPosition(undefined, true) + 1; i++) {
                        validPos = getMaskSet().validPositions[i];
                        if (validPos && validPos.generatedInput !== true) { validInputsClone.push(validPos.input) } else if (i < pos) staticInputsBeforePos++;
                        delete getMaskSet().validPositions[i]
                    }
                    while (getMaskSet().excludes[decisionPos] && getMaskSet().excludes[decisionPos].length < 10) {
                        var posOffset = staticInputsBeforePos * -1,
                            validInputs = validInputsClone.slice();
                        getMaskSet().tests[decisionPos] = undefined;
                        resetMaskSet(true);
                        isValidRslt = true;
                        while (validInputs.length > 0) { var input = validInputs.shift(); if (!(isValidRslt = isValid(getLastValidPosition(undefined, true) + 1, input, false, fromSetValid, true))) { break } }
                        if (isValidRslt && c !== undefined) {
                            var targetLvp = getLastValidPosition(pos) + 1;
                            for (i = decisionPos; i < getLastValidPosition() + 1; i++) { validPos = getMaskSet().validPositions[i]; if ((validPos === undefined || validPos.match.fn == null) && i < pos + posOffset) { posOffset++ } }
                            pos = pos + posOffset;
                            isValidRslt = isValid(pos > targetLvp ? targetLvp : pos, c, strict, fromSetValid, true)
                        }
                        if (!isValidRslt) {
                            resetMaskSet();
                            prevAltPos = getTest(decisionPos);
                            getMaskSet().validPositions = $.extend(true, {}, validPsClone);
                            if (getMaskSet().excludes[decisionPos]) {
                                var decisionTaker = getDecisionTaker(prevAltPos);
                                if (getMaskSet().excludes[decisionPos].indexOf(decisionTaker) !== -1) { isValidRslt = alternate(pos, c, strict, fromSetValid, decisionPos - 1); break }
                                getMaskSet().excludes[decisionPos].push(decisionTaker);
                                for (i = decisionPos; i < getLastValidPosition(undefined, true) + 1; i++) { delete getMaskSet().validPositions[i] }
                            } else { isValidRslt = alternate(pos, c, strict, fromSetValid, decisionPos - 1); break }
                        } else break
                    }
                }
                getMaskSet().excludes[decisionPos] = undefined;
                return isValidRslt
            }

            function isValid(pos, c, strict, fromSetValid, fromAlternate, validateOnly) {
                function isSelection(posObj) { return isRTL ? posObj.begin - posObj.end > 1 || posObj.begin - posObj.end === 1 : posObj.end - posObj.begin > 1 || posObj.end - posObj.begin === 1 }
                strict = strict === true;
                var maskPos = pos;
                if (pos.begin !== undefined) { maskPos = isRTL ? pos.end : pos.begin }

                function _isValid(position, c, strict) {
                    var rslt = false;
                    $.each(getTests(position), function(ndx, tst) {
                        var test = tst.match;
                        getBuffer(true);
                        rslt = test.fn != null ? test.fn.test(c, getMaskSet(), position, strict, opts, isSelection(pos)) : (c === test.def || c === opts.skipOptionalPartCharacter) && test.def !== "" ? { c: getPlaceholder(position, test, true) || test.def, pos: position } : false;
                        if (rslt !== false) {
                            var elem = rslt.c !== undefined ? rslt.c : c,
                                validatedPos = position;
                            elem = elem === opts.skipOptionalPartCharacter && test.fn === null ? getPlaceholder(position, test, true) || test.def : elem;
                            if (rslt.remove !== undefined) {
                                if (!$.isArray(rslt.remove)) rslt.remove = [rslt.remove];
                                $.each(rslt.remove.sort(function(a, b) { return b - a }), function(ndx, lmnt) { revalidateMask({ begin: lmnt, end: lmnt + 1 }) })
                            }
                            if (rslt.insert !== undefined) {
                                if (!$.isArray(rslt.insert)) rslt.insert = [rslt.insert];
                                $.each(rslt.insert.sort(function(a, b) { return a - b }), function(ndx, lmnt) { isValid(lmnt.pos, lmnt.c, true, fromSetValid) })
                            }
                            if (rslt !== true && rslt.pos !== undefined && rslt.pos !== position) { validatedPos = rslt.pos }
                            if (rslt !== true && rslt.pos === undefined && rslt.c === undefined) { return false }
                            if (!revalidateMask(pos, $.extend({}, tst, { input: casing(elem, test, validatedPos) }), fromSetValid, validatedPos)) { rslt = false }
                            return false
                        }
                    });
                    return rslt
                }
                var result = true,
                    positionsClone = $.extend(true, {}, getMaskSet().validPositions);
                if ($.isFunction(opts.preValidation) && !strict && fromSetValid !== true && validateOnly !== true) { result = opts.preValidation(getBuffer(), maskPos, c, isSelection(pos), opts, getMaskSet()) }
                if (result === true) {
                    trackbackPositions(undefined, maskPos, true);
                    if (maxLength === undefined || maskPos < maxLength) {
                        result = _isValid(maskPos, c, strict);
                        if ((!strict || fromSetValid === true) && result === false && validateOnly !== true) {
                            var currentPosValid = getMaskSet().validPositions[maskPos];
                            if (currentPosValid && currentPosValid.match.fn === null && (currentPosValid.match.def === c || c === opts.skipOptionalPartCharacter)) { result = { caret: seekNext(maskPos) } } else {
                                if ((opts.insertMode || getMaskSet().validPositions[seekNext(maskPos)] === undefined) && (!isMask(maskPos, true) || getMaskSet().jitOffset[maskPos])) {
                                    if (getMaskSet().jitOffset[maskPos] && getMaskSet().validPositions[seekNext(maskPos)] === undefined) { result = isValid(maskPos + getMaskSet().jitOffset[maskPos], c, strict); if (result !== false) result.caret = maskPos } else
                                        for (var nPos = maskPos + 1, snPos = seekNext(maskPos); nPos <= snPos; nPos++) {
                                            result = _isValid(nPos, c, strict);
                                            if (result !== false) {
                                                result = trackbackPositions(maskPos, result.pos !== undefined ? result.pos : nPos) || result;
                                                maskPos = nPos;
                                                break
                                            }
                                        }
                                }
                            }
                        }
                    }
                    if (result === false && opts.keepStatic !== false && (opts.regex == null || isComplete(getBuffer())) && !strict && fromAlternate !== true) { result = alternate(maskPos, c, strict, fromSetValid) }
                    if (result === true) { result = { pos: maskPos } }
                }
                if ($.isFunction(opts.postValidation) && result !== false && !strict && fromSetValid !== true && validateOnly !== true) {
                    var postResult = opts.postValidation(getBuffer(true), pos.begin !== undefined ? isRTL ? pos.end : pos.begin : pos, result, opts);
                    if (postResult !== undefined) {
                        if (postResult.refreshFromBuffer && postResult.buffer) {
                            var refresh = postResult.refreshFromBuffer;
                            refreshFromBuffer(refresh === true ? refresh : refresh.start, refresh.end, postResult.buffer)
                        }
                        result = postResult === true ? result : postResult
                    }
                }
                if (result && result.pos === undefined) { result.pos = maskPos }
                if (result === false || validateOnly === true) {
                    resetMaskSet(true);
                    getMaskSet().validPositions = $.extend(true, {}, positionsClone)
                }
                return result
            }

            function trackbackPositions(originalPos, newPos, fillOnly) {
                var result;
                if (originalPos === undefined) { for (originalPos = newPos - 1; originalPos > 0; originalPos--) { if (getMaskSet().validPositions[originalPos]) break } }
                for (var ps = originalPos; ps < newPos; ps++) {
                    if (getMaskSet().validPositions[ps] === undefined && !isMask(ps, true)) {
                        var vp = ps == 0 ? getTest(ps) : getMaskSet().validPositions[ps - 1];
                        if (vp) {
                            var tests = getTests(ps).slice();
                            if (tests[tests.length - 1].match.def === "") tests.pop();
                            var bestMatch = determineTestTemplate(ps, tests);
                            bestMatch = $.extend({}, bestMatch, { input: getPlaceholder(ps, bestMatch.match, true) || bestMatch.match.def });
                            bestMatch.generatedInput = true;
                            revalidateMask(ps, bestMatch, true);
                            if (fillOnly !== true) {
                                var cvpInput = getMaskSet().validPositions[newPos].input;
                                getMaskSet().validPositions[newPos] = undefined;
                                result = isValid(newPos, cvpInput, true, true)
                            }
                        }
                    }
                }
                return result
            }

            function revalidateMask(pos, validTest, fromSetValid, validatedPos) {
                function IsEnclosedStatic(pos, valids, selection) {
                    var posMatch = valids[pos];
                    if (posMatch !== undefined && (posMatch.match.fn === null && posMatch.match.optionality !== true || posMatch.input === opts.radixPoint)) {
                        var prevMatch = selection.begin <= pos - 1 ? valids[pos - 1] && valids[pos - 1].match.fn === null && valids[pos - 1] : valids[pos - 1],
                            nextMatch = selection.end > pos + 1 ? valids[pos + 1] && valids[pos + 1].match.fn === null && valids[pos + 1] : valids[pos + 1];
                        return prevMatch && nextMatch
                    }
                    return false
                }
                var begin = pos.begin !== undefined ? pos.begin : pos,
                    end = pos.end !== undefined ? pos.end : pos;
                if (pos.begin > pos.end) {
                    begin = pos.end;
                    end = pos.begin
                }
                validatedPos = validatedPos !== undefined ? validatedPos : begin;
                if (begin !== end || opts.insertMode && getMaskSet().validPositions[validatedPos] !== undefined && fromSetValid === undefined) {
                    var positionsClone = $.extend(true, {}, getMaskSet().validPositions),
                        lvp = getLastValidPosition(undefined, true),
                        i;
                    getMaskSet().p = begin;
                    for (i = lvp; i >= begin; i--) {
                        if (getMaskSet().validPositions[i] && getMaskSet().validPositions[i].match.nativeDef === "+") { opts.isNegative = false }
                        delete getMaskSet().validPositions[i]
                    }
                    var valid = true,
                        j = validatedPos,
                        vps = getMaskSet().validPositions,
                        needsValidation = false,
                        posMatch = j,
                        i = j;
                    if (validTest) {
                        getMaskSet().validPositions[validatedPos] = $.extend(true, {}, validTest);
                        posMatch++;
                        j++;
                        if (begin < end) i++
                    }
                    for (; i <= lvp; i++) {
                        var t = positionsClone[i];
                        if (t !== undefined && (i >= end || i >= begin && t.generatedInput !== true && IsEnclosedStatic(i, positionsClone, { begin: begin, end: end }))) {
                            while (getTest(posMatch).match.def !== "") {
                                if (needsValidation === false && positionsClone[posMatch] && positionsClone[posMatch].match.nativeDef === t.match.nativeDef) {
                                    getMaskSet().validPositions[posMatch] = $.extend(true, {}, positionsClone[posMatch]);
                                    getMaskSet().validPositions[posMatch].input = t.input;
                                    trackbackPositions(undefined, posMatch, true);
                                    j = posMatch + 1;
                                    valid = true
                                } else if (opts.shiftPositions && positionCanMatchDefinition(posMatch, t.match.def)) {
                                    var result = isValid(posMatch, t.input, true, true);
                                    valid = result !== false;
                                    j = result.caret || result.insert ? getLastValidPosition() : posMatch + 1;
                                    needsValidation = true
                                } else { valid = t.generatedInput === true || t.input === opts.radixPoint && opts.numericInput === true }
                                if (valid) break;
                                if (!valid && posMatch > end && isMask(posMatch, true) && (t.match.fn !== null || posMatch > getMaskSet().maskLength)) { break }
                                posMatch++
                            }
                            if (getTest(posMatch).match.def == "") valid = false;
                            posMatch = j
                        }
                        if (!valid) break
                    }
                    if (!valid) {
                        getMaskSet().validPositions = $.extend(true, {}, positionsClone);
                        resetMaskSet(true);
                        return false
                    }
                } else if (validTest) { getMaskSet().validPositions[validatedPos] = $.extend(true, {}, validTest) }
                resetMaskSet(true);
                return true
            }

            function isMask(pos, strict) { var test = getTestTemplate(pos).match; if (test.def === "") test = getTest(pos).match; if (test.fn != null) { return test.fn } if (strict !== true && pos > -1) { var tests = getTests(pos); return tests.length > 1 + (tests[tests.length - 1].match.def === "" ? 1 : 0) } return false }

            function seekNext(pos, newBlock) { var position = pos + 1; while (getTest(position).match.def !== "" && (newBlock === true && (getTest(position).match.newBlockMarker !== true || !isMask(position)) || newBlock !== true && !isMask(position))) { position++ } return position }

            function seekPrevious(pos, newBlock) {
                var position = pos,
                    tests;
                if (position <= 0) return 0;
                while (--position > 0 && (newBlock === true && getTest(position).match.newBlockMarker !== true || newBlock !== true && !isMask(position) && (tests = getTests(position), tests.length < 2 || tests.length === 2 && tests[1].match.def === ""))) {}
                return position
            }

            function writeBuffer(input, buffer, caretPos, event, triggerEvents) {
                if (event && $.isFunction(opts.onBeforeWrite)) {
                    var result = opts.onBeforeWrite.call(inputmask, event, buffer, caretPos, opts);
                    if (result) {
                        if (result.refreshFromBuffer) {
                            var refresh = result.refreshFromBuffer;
                            refreshFromBuffer(refresh === true ? refresh : refresh.start, refresh.end, result.buffer || buffer);
                            buffer = getBuffer(true)
                        }
                        if (caretPos !== undefined) caretPos = result.caret !== undefined ? result.caret : caretPos
                    }
                }
                if (input !== undefined) {
                    input.inputmask._valueSet(buffer.join(""));
                    if (caretPos !== undefined && (event === undefined || event.type !== "blur")) { caret(input, caretPos) } else renderColorMask(input, caretPos, buffer.length === 0);
                    if (triggerEvents === true) {
                        var $input = $(input),
                            nptVal = input.inputmask._valueGet();
                        skipInputEvent = true;
                        $input.trigger("input");
                        setTimeout(function() { if (nptVal === getBufferTemplate().join("")) { $input.trigger("cleared") } else if (isComplete(buffer) === true) { $input.trigger("complete") } }, 0)
                    }
                }
            }

            function getPlaceholder(pos, test, returnPL) {
                test = test || getTest(pos).match;
                if (test.placeholder !== undefined || returnPL === true) { return $.isFunction(test.placeholder) ? test.placeholder(opts) : test.placeholder } else if (test.fn === null) {
                    if (pos > -1 && getMaskSet().validPositions[pos] === undefined) {
                        var tests = getTests(pos),
                            staticAlternations = [],
                            prevTest;
                        if (tests.length > 1 + (tests[tests.length - 1].match.def === "" ? 1 : 0)) { for (var i = 0; i < tests.length; i++) { if (tests[i].match.optionality !== true && tests[i].match.optionalQuantifier !== true && (tests[i].match.fn === null || prevTest === undefined || tests[i].match.fn.test(prevTest.match.def, getMaskSet(), pos, true, opts) !== false)) { staticAlternations.push(tests[i]); if (tests[i].match.fn === null) prevTest = tests[i]; if (staticAlternations.length > 1) { if (/[0-9a-bA-Z]/.test(staticAlternations[0].match.def)) { return opts.placeholder.charAt(pos % opts.placeholder.length) } } } } }
                    }
                    return test.def
                }
                return opts.placeholder.charAt(pos % opts.placeholder.length)
            }

            function HandleNativePlaceholder(npt, value) {
                if (ie) {
                    if (npt.inputmask._valueGet() !== value && (npt.placeholder !== value || npt.placeholder === "")) {
                        var buffer = getBuffer().slice(),
                            nptValue = npt.inputmask._valueGet();
                        if (nptValue !== value) {
                            var lvp = getLastValidPosition();
                            if (lvp === -1 && nptValue === getBufferTemplate().join("")) { buffer = [] } else if (lvp !== -1) { clearOptionalTail(buffer) }
                            writeBuffer(npt, buffer)
                        }
                    }
                } else if (npt.placeholder !== value) { npt.placeholder = value; if (npt.placeholder === "") npt.removeAttribute("placeholder") }
            }
            var EventRuler = {
                on: function on(input, eventName, eventHandler) {
                    var ev = function ev(e) {
                        var that = this;
                        if (that.inputmask === undefined && this.nodeName !== "FORM") {
                            var imOpts = $.data(that, "_inputmask_opts");
                            if (imOpts) new Inputmask(imOpts).mask(that);
                            else EventRuler.off(that)
                        } else if (e.type !== "setvalue" && this.nodeName !== "FORM" && (that.disabled || that.readOnly && !(e.type === "keydown" && e.ctrlKey && e.keyCode === 67 || opts.tabThrough === false && e.keyCode === Inputmask.keyCode.TAB))) { e.preventDefault() } else {
                            switch (e.type) {
                                case "input":
                                    if (skipInputEvent === true) { skipInputEvent = false; return e.preventDefault() }
                                    if (mobile) {
                                        var args = arguments;
                                        setTimeout(function() {
                                            eventHandler.apply(that, args);
                                            caret(that, that.inputmask.caretPos, undefined, true)
                                        }, 0);
                                        return false
                                    }
                                    break;
                                case "keydown":
                                    skipKeyPressEvent = false;
                                    skipInputEvent = false;
                                    break;
                                case "keypress":
                                    if (skipKeyPressEvent === true) { return e.preventDefault() }
                                    skipKeyPressEvent = true;
                                    break;
                                case "click":
                                    if (iemobile || iphone) {
                                        var args = arguments;
                                        setTimeout(function() { eventHandler.apply(that, args) }, 0);
                                        return false
                                    }
                                    break
                            }
                            var returnVal = eventHandler.apply(that, arguments);
                            if (returnVal === false) {
                                e.preventDefault();
                                e.stopPropagation()
                            }
                            return returnVal
                        }
                    };
                    input.inputmask.events[eventName] = input.inputmask.events[eventName] || [];
                    input.inputmask.events[eventName].push(ev);
                    if ($.inArray(eventName, ["submit", "reset"]) !== -1) { if (input.form !== null) $(input.form).on(eventName, ev) } else { $(input).on(eventName, ev) }
                },
                off: function off(input, event) {
                    if (input.inputmask && input.inputmask.events) {
                        var events;
                        if (event) {
                            events = [];
                            events[event] = input.inputmask.events[event]
                        } else { events = input.inputmask.events }
                        $.each(events, function(eventName, evArr) {
                            while (evArr.length > 0) { var ev = evArr.pop(); if ($.inArray(eventName, ["submit", "reset"]) !== -1) { if (input.form !== null) $(input.form).off(eventName, ev) } else { $(input).off(eventName, ev) } }
                            delete input.inputmask.events[eventName]
                        })
                    }
                }
            };
            var EventHandlers = {
                keydownEvent: function keydownEvent(e) {
                    var input = this,
                        $input = $(input),
                        k = e.keyCode,
                        pos = caret(input);
                    if (k === Inputmask.keyCode.BACKSPACE || k === Inputmask.keyCode.DELETE || iphone && k === Inputmask.keyCode.BACKSPACE_SAFARI || e.ctrlKey && k === Inputmask.keyCode.X && !isInputEventSupported("cut")) {
                        e.preventDefault();
                        handleRemove(input, k, pos);
                        writeBuffer(input, getBuffer(true), getMaskSet().p, e, input.inputmask._valueGet() !== getBuffer().join(""))
                    } else if (k === Inputmask.keyCode.END || k === Inputmask.keyCode.PAGE_DOWN) {
                        e.preventDefault();
                        var caretPos = seekNext(getLastValidPosition());
                        caret(input, e.shiftKey ? pos.begin : caretPos, caretPos, true)
                    } else if (k === Inputmask.keyCode.HOME && !e.shiftKey || k === Inputmask.keyCode.PAGE_UP) {
                        e.preventDefault();
                        caret(input, 0, e.shiftKey ? pos.begin : 0, true)
                    } else if ((opts.undoOnEscape && k === Inputmask.keyCode.ESCAPE || k === 90 && e.ctrlKey) && e.altKey !== true) {
                        checkVal(input, true, false, undoValue.split(""));
                        $input.trigger("click")
                    } else if (k === Inputmask.keyCode.INSERT && !(e.shiftKey || e.ctrlKey)) {
                        opts.insertMode = !opts.insertMode;
                        input.setAttribute("im-insert", opts.insertMode)
                    } else if (opts.tabThrough === true && k === Inputmask.keyCode.TAB) {
                        if (e.shiftKey === true) {
                            if (getTest(pos.begin).match.fn === null) { pos.begin = seekNext(pos.begin) }
                            pos.end = seekPrevious(pos.begin, true);
                            pos.begin = seekPrevious(pos.end, true)
                        } else {
                            pos.begin = seekNext(pos.begin, true);
                            pos.end = seekNext(pos.begin, true);
                            if (pos.end < getMaskSet().maskLength) pos.end--
                        }
                        if (pos.begin < getMaskSet().maskLength) {
                            e.preventDefault();
                            caret(input, pos.begin, pos.end)
                        }
                    }
                    opts.onKeyDown.call(this, e, getBuffer(), caret(input).begin, opts);
                    ignorable = $.inArray(k, opts.ignorables) !== -1
                },
                keypressEvent: function keypressEvent(e, checkval, writeOut, strict, ndx) {
                    var input = this,
                        $input = $(input),
                        k = e.which || e.charCode || e.keyCode;
                    if (checkval !== true && !(e.ctrlKey && e.altKey) && (e.ctrlKey || e.metaKey || ignorable)) {
                        if (k === Inputmask.keyCode.ENTER && undoValue !== getBuffer().join("")) {
                            undoValue = getBuffer().join("");
                            setTimeout(function() { $input.trigger("change") }, 0)
                        }
                        return true
                    } else {
                        if (k) {
                            if (k === 46 && e.shiftKey === false && opts.radixPoint !== "") k = opts.radixPoint.charCodeAt(0);
                            var pos = checkval ? { begin: ndx, end: ndx } : caret(input),
                                forwardPosition, c = String.fromCharCode(k),
                                offset = 0;
                            if (opts._radixDance && opts.numericInput) {
                                var caretPos = getBuffer().indexOf(opts.radixPoint.charAt(0)) + 1;
                                if (pos.begin <= caretPos) {
                                    if (k === opts.radixPoint.charCodeAt(0)) offset = 1;
                                    pos.begin -= 1;
                                    pos.end -= 1
                                }
                            }
                            getMaskSet().writeOutBuffer = true;
                            var valResult = isValid(pos, c, strict);
                            if (valResult !== false) {
                                resetMaskSet(true);
                                forwardPosition = valResult.caret !== undefined ? valResult.caret : seekNext(valResult.pos.begin ? valResult.pos.begin : valResult.pos);
                                getMaskSet().p = forwardPosition
                            }
                            forwardPosition = (opts.numericInput && valResult.caret === undefined ? seekPrevious(forwardPosition) : forwardPosition) + offset;
                            if (writeOut !== false) {
                                setTimeout(function() { opts.onKeyValidation.call(input, k, valResult, opts) }, 0);
                                if (getMaskSet().writeOutBuffer && valResult !== false) {
                                    var buffer = getBuffer();
                                    writeBuffer(input, buffer, forwardPosition, e, checkval !== true)
                                }
                            }
                            e.preventDefault();
                            if (checkval) { if (valResult !== false) valResult.forwardPosition = forwardPosition; return valResult }
                        }
                    }
                },
                pasteEvent: function pasteEvent(e) {
                    var input = this,
                        ev = e.originalEvent || e,
                        $input = $(input),
                        inputValue = input.inputmask._valueGet(true),
                        caretPos = caret(input),
                        tempValue;
                    if (isRTL) {
                        tempValue = caretPos.end;
                        caretPos.end = caretPos.begin;
                        caretPos.begin = tempValue
                    }
                    var valueBeforeCaret = inputValue.substr(0, caretPos.begin),
                        valueAfterCaret = inputValue.substr(caretPos.end, inputValue.length);
                    if (valueBeforeCaret === (isRTL ? getBufferTemplate().reverse() : getBufferTemplate()).slice(0, caretPos.begin).join("")) valueBeforeCaret = "";
                    if (valueAfterCaret === (isRTL ? getBufferTemplate().reverse() : getBufferTemplate()).slice(caretPos.end).join("")) valueAfterCaret = "";
                    if (window.clipboardData && window.clipboardData.getData) { inputValue = valueBeforeCaret + window.clipboardData.getData("Text") + valueAfterCaret } else if (ev.clipboardData && ev.clipboardData.getData) { inputValue = valueBeforeCaret + ev.clipboardData.getData("text/plain") + valueAfterCaret } else return true;
                    var pasteValue = inputValue;
                    if ($.isFunction(opts.onBeforePaste)) { pasteValue = opts.onBeforePaste.call(inputmask, inputValue, opts); if (pasteValue === false) { return e.preventDefault() } if (!pasteValue) { pasteValue = inputValue } }
                    checkVal(input, false, false, pasteValue.toString().split(""));
                    writeBuffer(input, getBuffer(), seekNext(getLastValidPosition()), e, undoValue !== getBuffer().join(""));
                    return e.preventDefault()
                },
                inputFallBackEvent: function inputFallBackEvent(e) {
                    function radixPointHandler(input, inputValue, caretPos) {
                        if (inputValue.charAt(caretPos.begin - 1) === "." && opts.radixPoint !== "") {
                            inputValue = inputValue.split("");
                            inputValue[caretPos.begin - 1] = opts.radixPoint.charAt(0);
                            inputValue = inputValue.join("")
                        }
                        return inputValue
                    }

                    function ieMobileHandler(input, inputValue, caretPos) {
                        if (iemobile) {
                            var inputChar = inputValue.replace(getBuffer().join(""), "");
                            if (inputChar.length === 1) {
                                var iv = inputValue.split("");
                                iv.splice(caretPos.begin, 0, inputChar);
                                inputValue = iv.join("")
                            }
                        }
                        return inputValue
                    }
                    var input = this,
                        inputValue = input.inputmask._valueGet();
                    if (getBuffer().join("") !== inputValue) {
                        var caretPos = caret(input);
                        inputValue = radixPointHandler(input, inputValue, caretPos);
                        inputValue = ieMobileHandler(input, inputValue, caretPos);
                        if (getBuffer().join("") !== inputValue) {
                            var buffer = getBuffer().join(""),
                                offset = !opts.numericInput && inputValue.length > buffer.length ? -1 : 0,
                                frontPart = inputValue.substr(0, caretPos.begin),
                                backPart = inputValue.substr(caretPos.begin),
                                frontBufferPart = buffer.substr(0, caretPos.begin + offset),
                                backBufferPart = buffer.substr(caretPos.begin + offset);
                            var selection = caretPos,
                                entries = "",
                                isEntry = false;
                            if (frontPart !== frontBufferPart) {
                                var fpl = (isEntry = frontPart.length >= frontBufferPart.length) ? frontPart.length : frontBufferPart.length,
                                    i;
                                for (i = 0; frontPart.charAt(i) === frontBufferPart.charAt(i) && i < fpl; i++) {}
                                if (isEntry) {
                                    selection.begin = i - offset;
                                    entries += frontPart.slice(i, selection.end)
                                }
                            }
                            if (backPart !== backBufferPart) {
                                if (backPart.length > backBufferPart.length) { entries += backPart.slice(0, 1) } else {
                                    if (backPart.length < backBufferPart.length) {
                                        selection.end += backBufferPart.length - backPart.length;
                                        if (!isEntry && opts.radixPoint !== "" && backPart === "" && frontPart.charAt(selection.begin + offset - 1) === opts.radixPoint) {
                                            selection.begin--;
                                            entries = opts.radixPoint
                                        }
                                    }
                                }
                            }
                            writeBuffer(input, getBuffer(), { begin: selection.begin + offset, end: selection.end + offset });
                            if (entries.length > 0) {
                                $.each(entries.split(""), function(ndx, entry) {
                                    var keypress = new $.Event("keypress");
                                    keypress.which = entry.charCodeAt(0);
                                    ignorable = false;
                                    EventHandlers.keypressEvent.call(input, keypress)
                                })
                            } else {
                                if (selection.begin === selection.end - 1) { selection.begin = seekPrevious(selection.begin + 1); if (selection.begin === selection.end - 1) { caret(input, selection.begin) } else { caret(input, selection.begin, selection.end) } }
                                var keydown = new $.Event("keydown");
                                keydown.keyCode = opts.numericInput ? Inputmask.keyCode.BACKSPACE : Inputmask.keyCode.DELETE;
                                EventHandlers.keydownEvent.call(input, keydown)
                            }
                            e.preventDefault()
                        }
                    }
                },
                beforeInputEvent: function beforeInputEvent(e) {
                    if (e.cancelable) {
                        var input = this;
                        switch (e.inputType) {
                            case "insertText":
                                $.each(e.data.split(""), function(ndx, entry) {
                                    var keypress = new $.Event("keypress");
                                    keypress.which = entry.charCodeAt(0);
                                    ignorable = false;
                                    EventHandlers.keypressEvent.call(input, keypress)
                                });
                                return e.preventDefault();
                            case "deleteContentBackward":
                                var keydown = new $.Event("keydown");
                                keydown.keyCode = Inputmask.keyCode.BACKSPACE;
                                EventHandlers.keydownEvent.call(input, keydown);
                                return e.preventDefault();
                            case "deleteContentForward":
                                var keydown = new $.Event("keydown");
                                keydown.keyCode = Inputmask.keyCode.DELETE;
                                EventHandlers.keydownEvent.call(input, keydown);
                                return e.preventDefault()
                        }
                    }
                },
                setValueEvent: function setValueEvent(e) {
                    this.inputmask.refreshValue = false;
                    var input = this,
                        value = e && e.detail ? e.detail[0] : arguments[1],
                        value = value || input.inputmask._valueGet(true);
                    if ($.isFunction(opts.onBeforeMask)) value = opts.onBeforeMask.call(inputmask, value, opts) || value;
                    value = value.split("");
                    checkVal(input, true, false, value);
                    undoValue = getBuffer().join("");
                    if ((opts.clearMaskOnLostFocus || opts.clearIncomplete) && input.inputmask._valueGet() === getBufferTemplate().join("")) { input.inputmask._valueSet("") }
                },
                focusEvent: function focusEvent(e) {
                    var input = this,
                        nptValue = input.inputmask._valueGet();
                    if (opts.showMaskOnFocus) { if (nptValue !== getBuffer().join("")) { writeBuffer(input, getBuffer(), seekNext(getLastValidPosition())) } else if (mouseEnter === false) { caret(input, seekNext(getLastValidPosition())) } }
                    if (opts.positionCaretOnTab === true && mouseEnter === false) { EventHandlers.clickEvent.apply(input, [e, true]) }
                    undoValue = getBuffer().join("")
                },
                mouseleaveEvent: function mouseleaveEvent(e) {
                    var input = this;
                    mouseEnter = false;
                    if (opts.clearMaskOnLostFocus && document.activeElement !== input) { HandleNativePlaceholder(input, originalPlaceholder) }
                },
                clickEvent: function clickEvent(e, tabbed) {
                    function doRadixFocus(clickPos) { if (opts.radixPoint !== "") { var vps = getMaskSet().validPositions; if (vps[clickPos] === undefined || vps[clickPos].input === getPlaceholder(clickPos)) { if (clickPos < seekNext(-1)) return true; var radixPos = $.inArray(opts.radixPoint, getBuffer()); if (radixPos !== -1) { for (var vp in vps) { if (radixPos < vp && vps[vp].input !== getPlaceholder(vp)) { return false } } return true } } } return false }
                    var input = this;
                    setTimeout(function() {
                        if (document.activeElement === input) {
                            var selectedCaret = caret(input);
                            if (tabbed) { if (isRTL) { selectedCaret.end = selectedCaret.begin } else { selectedCaret.begin = selectedCaret.end } }
                            if (selectedCaret.begin === selectedCaret.end) {
                                switch (opts.positionCaretOnClick) {
                                    case "none":
                                        break;
                                    case "select":
                                        caret(input, 0, getBuffer().length);
                                        break;
                                    case "ignore":
                                        caret(input, seekNext(getLastValidPosition()));
                                        break;
                                    case "radixFocus":
                                        if (doRadixFocus(selectedCaret.begin)) {
                                            var radixPos = getBuffer().join("").indexOf(opts.radixPoint);
                                            caret(input, opts.numericInput ? seekNext(radixPos) : radixPos);
                                            break
                                        }
                                    default:
                                        var clickPosition = selectedCaret.begin,
                                            lvclickPosition = getLastValidPosition(clickPosition, true),
                                            lastPosition = seekNext(lvclickPosition);
                                        if (clickPosition < lastPosition) { caret(input, !isMask(clickPosition, true) && !isMask(clickPosition - 1, true) ? seekNext(clickPosition) : clickPosition) } else {
                                            var lvp = getMaskSet().validPositions[lvclickPosition],
                                                tt = getTestTemplate(lastPosition, lvp ? lvp.match.locator : undefined, lvp),
                                                placeholder = getPlaceholder(lastPosition, tt.match);
                                            if (placeholder !== "" && getBuffer()[lastPosition] !== placeholder && tt.match.optionalQuantifier !== true && tt.match.newBlockMarker !== true || !isMask(lastPosition, opts.keepStatic) && tt.match.def === placeholder) { var newPos = seekNext(lastPosition); if (clickPosition >= newPos || clickPosition === lastPosition) { lastPosition = newPos } }
                                            caret(input, lastPosition)
                                        }
                                        break
                                }
                            }
                        }
                    }, 0)
                },
                cutEvent: function cutEvent(e) {
                    var input = this,
                        $input = $(input),
                        pos = caret(input),
                        ev = e.originalEvent || e;
                    var clipboardData = window.clipboardData || ev.clipboardData,
                        clipData = isRTL ? getBuffer().slice(pos.end, pos.begin) : getBuffer().slice(pos.begin, pos.end);
                    clipboardData.setData("text", isRTL ? clipData.reverse().join("") : clipData.join(""));
                    if (document.execCommand) document.execCommand("copy");
                    handleRemove(input, Inputmask.keyCode.DELETE, pos);
                    writeBuffer(input, getBuffer(), getMaskSet().p, e, undoValue !== getBuffer().join(""))
                },
                blurEvent: function blurEvent(e) {
                    var $input = $(this),
                        input = this;
                    if (input.inputmask) {
                        HandleNativePlaceholder(input, originalPlaceholder);
                        var nptValue = input.inputmask._valueGet(),
                            buffer = getBuffer().slice();
                        if (nptValue !== "" || colorMask !== undefined) {
                            if (opts.clearMaskOnLostFocus) { if (getLastValidPosition() === -1 && nptValue === getBufferTemplate().join("")) { buffer = [] } else { clearOptionalTail(buffer) } }
                            if (isComplete(buffer) === false) { setTimeout(function() { $input.trigger("incomplete") }, 0); if (opts.clearIncomplete) { resetMaskSet(); if (opts.clearMaskOnLostFocus) { buffer = [] } else { buffer = getBufferTemplate().slice() } } }
                            writeBuffer(input, buffer, undefined, e)
                        }
                        if (undoValue !== getBuffer().join("")) {
                            undoValue = buffer.join("");
                            $input.trigger("change")
                        }
                    }
                },
                mouseenterEvent: function mouseenterEvent(e) {
                    var input = this;
                    mouseEnter = true;
                    if (document.activeElement !== input && opts.showMaskOnHover) { HandleNativePlaceholder(input, (isRTL ? getBuffer().slice().reverse() : getBuffer()).join("")) }
                },
                submitEvent: function submitEvent(e) {
                    if (undoValue !== getBuffer().join("")) { $el.trigger("change") }
                    if (opts.clearMaskOnLostFocus && getLastValidPosition() === -1 && el.inputmask._valueGet && el.inputmask._valueGet() === getBufferTemplate().join("")) { el.inputmask._valueSet("") }
                    if (opts.clearIncomplete && isComplete(getBuffer()) === false) { el.inputmask._valueSet("") }
                    if (opts.removeMaskOnSubmit) {
                        el.inputmask._valueSet(el.inputmask.unmaskedvalue(), true);
                        setTimeout(function() { writeBuffer(el, getBuffer()) }, 0)
                    }
                },
                resetEvent: function resetEvent(e) {
                    el.inputmask.refreshValue = true;
                    setTimeout(function() { $el.trigger("setvalue") }, 0)
                }
            };

            function checkVal(input, writeOut, strict, nptvl, initiatingEvent) {
                var inputmask = this || input.inputmask,
                    inputValue = nptvl.slice(),
                    charCodes = "",
                    initialNdx = -1,
                    result = undefined;

                function isTemplateMatch(ndx, charCodes) { var charCodeNdx = getMaskTemplate(true, 0, false).slice(ndx, seekNext(ndx)).join("").replace(/'/g, "").indexOf(charCodes); return charCodeNdx !== -1 && !isMask(ndx) && (getTest(ndx).match.nativeDef === charCodes.charAt(0) || getTest(ndx).match.fn === null && getTest(ndx).match.nativeDef === "'" + charCodes.charAt(0) || getTest(ndx).match.nativeDef === " " && (getTest(ndx + 1).match.nativeDef === charCodes.charAt(0) || getTest(ndx + 1).match.fn === null && getTest(ndx + 1).match.nativeDef === "'" + charCodes.charAt(0))) }
                resetMaskSet();
                if (!strict && opts.autoUnmask !== true) {
                    var staticInput = getBufferTemplate().slice(0, seekNext(-1)).join(""),
                        matches = inputValue.join("").match(new RegExp("^" + Inputmask.escapeRegex(staticInput), "g"));
                    if (matches && matches.length > 0) {
                        inputValue.splice(0, matches.length * staticInput.length);
                        initialNdx = seekNext(initialNdx)
                    }
                } else { initialNdx = seekNext(initialNdx) }
                if (initialNdx === -1) {
                    getMaskSet().p = seekNext(initialNdx);
                    initialNdx = 0
                } else getMaskSet().p = initialNdx;
                inputmask.caretPos = { begin: initialNdx };
                $.each(inputValue, function(ndx, charCode) {
                    if (charCode !== undefined) {
                        if (getMaskSet().validPositions[ndx] === undefined && inputValue[ndx] === getPlaceholder(ndx) && isMask(ndx, true) && isValid(ndx, inputValue[ndx], true, undefined, undefined, true) === false) { getMaskSet().p++ } else {
                            var keypress = new $.Event("_checkval");
                            keypress.which = charCode.charCodeAt(0);
                            charCodes += charCode;
                            var lvp = getLastValidPosition(undefined, true);
                            if (!isTemplateMatch(initialNdx, charCodes)) {
                                result = EventHandlers.keypressEvent.call(input, keypress, true, false, strict, inputmask.caretPos.begin);
                                if (result) {
                                    initialNdx = inputmask.caretPos.begin + 1;
                                    charCodes = ""
                                }
                            } else { result = EventHandlers.keypressEvent.call(input, keypress, true, false, strict, lvp + 1) }
                            if (result) {
                                writeBuffer(undefined, getBuffer(), result.forwardPosition, keypress, false);
                                inputmask.caretPos = { begin: result.forwardPosition, end: result.forwardPosition }
                            }
                        }
                    }
                });
                if (writeOut) writeBuffer(input, getBuffer(), result ? result.forwardPosition : undefined, initiatingEvent || new $.Event("checkval"), initiatingEvent && initiatingEvent.type === "input")
            }

            function unmaskedvalue(input) {
                if (input) { if (input.inputmask === undefined) { return input.value } if (input.inputmask && input.inputmask.refreshValue) { EventHandlers.setValueEvent.call(input) } }
                var umValue = [],
                    vps = getMaskSet().validPositions;
                for (var pndx in vps) { if (vps[pndx].match && vps[pndx].match.fn != null) { umValue.push(vps[pndx].input) } }
                var unmaskedValue = umValue.length === 0 ? "" : (isRTL ? umValue.reverse() : umValue).join("");
                if ($.isFunction(opts.onUnMask)) {
                    var bufferValue = (isRTL ? getBuffer().slice().reverse() : getBuffer()).join("");
                    unmaskedValue = opts.onUnMask.call(inputmask, bufferValue, unmaskedValue, opts)
                }
                return unmaskedValue
            }

            function caret(input, begin, end, notranslate) {
                function translatePosition(pos) { if (isRTL && typeof pos === "number" && (!opts.greedy || opts.placeholder !== "") && el) { pos = el.inputmask._valueGet().length - pos } return pos }
                var range;
                if (begin !== undefined) {
                    if ($.isArray(begin)) {
                        end = isRTL ? begin[0] : begin[1];
                        begin = isRTL ? begin[1] : begin[0]
                    }
                    if (begin.begin !== undefined) {
                        end = isRTL ? begin.begin : begin.end;
                        begin = isRTL ? begin.end : begin.begin
                    }
                    if (typeof begin === "number") {
                        begin = notranslate ? begin : translatePosition(begin);
                        end = notranslate ? end : translatePosition(end);
                        end = typeof end == "number" ? end : begin;
                        var scrollCalc = parseInt(((input.ownerDocument.defaultView || window).getComputedStyle ? (input.ownerDocument.defaultView || window).getComputedStyle(input, null) : input.currentStyle).fontSize) * end;
                        input.scrollLeft = scrollCalc > input.scrollWidth ? scrollCalc : 0;
                        input.inputmask.caretPos = { begin: begin, end: end };
                        if (input === document.activeElement) {
                            if ("selectionStart" in input) {
                                input.selectionStart = begin;
                                input.selectionEnd = end
                            } else if (window.getSelection) {
                                range = document.createRange();
                                if (input.firstChild === undefined || input.firstChild === null) {
                                    var textNode = document.createTextNode("");
                                    input.appendChild(textNode)
                                }
                                range.setStart(input.firstChild, begin < input.inputmask._valueGet().length ? begin : input.inputmask._valueGet().length);
                                range.setEnd(input.firstChild, end < input.inputmask._valueGet().length ? end : input.inputmask._valueGet().length);
                                range.collapse(true);
                                var sel = window.getSelection();
                                sel.removeAllRanges();
                                sel.addRange(range)
                            } else if (input.createTextRange) {
                                range = input.createTextRange();
                                range.collapse(true);
                                range.moveEnd("character", end);
                                range.moveStart("character", begin);
                                range.select()
                            }
                            renderColorMask(input, { begin: begin, end: end })
                        }
                    }
                } else {
                    if ("selectionStart" in input) {
                        begin = input.selectionStart;
                        end = input.selectionEnd
                    } else if (window.getSelection) {
                        range = window.getSelection().getRangeAt(0);
                        if (range.commonAncestorContainer.parentNode === input || range.commonAncestorContainer === input) {
                            begin = range.startOffset;
                            end = range.endOffset
                        }
                    } else if (document.selection && document.selection.createRange) {
                        range = document.selection.createRange();
                        begin = 0 - range.duplicate().moveStart("character", -input.inputmask._valueGet().length);
                        end = begin + range.text.length
                    }
                    return { begin: notranslate ? begin : translatePosition(begin), end: notranslate ? end : translatePosition(end) }
                }
            }

            function determineLastRequiredPosition(returnDefinition) {
                var buffer = getMaskTemplate(true, getLastValidPosition(), true, true),
                    bl = buffer.length,
                    pos, lvp = getLastValidPosition(),
                    positions = {},
                    lvTest = getMaskSet().validPositions[lvp],
                    ndxIntlzr = lvTest !== undefined ? lvTest.locator.slice() : undefined,
                    testPos;
                for (pos = lvp + 1; pos < buffer.length; pos++) {
                    testPos = getTestTemplate(pos, ndxIntlzr, pos - 1);
                    ndxIntlzr = testPos.locator.slice();
                    positions[pos] = $.extend(true, {}, testPos)
                }
                var lvTestAlt = lvTest && lvTest.alternation !== undefined ? lvTest.locator[lvTest.alternation] : undefined;
                for (pos = bl - 1; pos > lvp; pos--) { testPos = positions[pos]; if ((testPos.match.optionality || testPos.match.optionalQuantifier && testPos.match.newBlockMarker || lvTestAlt && (lvTestAlt !== positions[pos].locator[lvTest.alternation] && testPos.match.fn != null || testPos.match.fn === null && testPos.locator[lvTest.alternation] && checkAlternationMatch(testPos.locator[lvTest.alternation].toString().split(","), lvTestAlt.toString().split(",")) && getTests(pos)[0].def !== "")) && buffer[pos] === getPlaceholder(pos, testPos.match)) { bl-- } else break }
                return returnDefinition ? { l: bl, def: positions[bl] ? positions[bl].match : undefined } : bl
            }

            function clearOptionalTail(buffer) {
                buffer.length = 0;
                var template = getMaskTemplate(true, 0, true, undefined, true),
                    lmnt, validPos;
                while (lmnt = template.shift(), lmnt !== undefined) { buffer.push(lmnt) }
                return buffer
            }

            function isComplete(buffer) {
                if ($.isFunction(opts.isComplete)) return opts.isComplete(buffer, opts);
                if (opts.repeat === "*") return undefined;
                var complete = false,
                    lrp = determineLastRequiredPosition(true),
                    aml = seekPrevious(lrp.l);
                if (lrp.def === undefined || lrp.def.newBlockMarker || lrp.def.optionality || lrp.def.optionalQuantifier) { complete = true; for (var i = 0; i <= aml; i++) { var test = getTestTemplate(i).match; if (test.fn !== null && getMaskSet().validPositions[i] === undefined && test.optionality !== true && test.optionalQuantifier !== true || test.fn === null && buffer[i] !== getPlaceholder(i, test)) { complete = false; break } } }
                return complete
            }

            function handleRemove(input, k, pos, strict, fromIsValid) {
                if (opts.numericInput || isRTL) {
                    if (k === Inputmask.keyCode.BACKSPACE) { k = Inputmask.keyCode.DELETE } else if (k === Inputmask.keyCode.DELETE) { k = Inputmask.keyCode.BACKSPACE }
                    if (isRTL) {
                        var pend = pos.end;
                        pos.end = pos.begin;
                        pos.begin = pend
                    }
                }
                if (k === Inputmask.keyCode.BACKSPACE && pos.end - pos.begin < 1) { pos.begin = seekPrevious(pos.begin); if (getMaskSet().validPositions[pos.begin] !== undefined && getMaskSet().validPositions[pos.begin].input === opts.groupSeparator) { pos.begin-- } } else if (k === Inputmask.keyCode.DELETE && pos.begin === pos.end) { pos.end = isMask(pos.end, true) && getMaskSet().validPositions[pos.end] && getMaskSet().validPositions[pos.end].input !== opts.radixPoint ? pos.end + 1 : seekNext(pos.end) + 1; if (getMaskSet().validPositions[pos.begin] !== undefined && getMaskSet().validPositions[pos.begin].input === opts.groupSeparator) { pos.end++ } }
                revalidateMask(pos);
                if (strict !== true && opts.keepStatic !== false || opts.regex !== null) { var result = alternate(true); if (result) { var newPos = result.caret !== undefined ? result.caret : result.pos ? seekNext(result.pos.begin ? result.pos.begin : result.pos) : getLastValidPosition(-1, true); if (k !== Inputmask.keyCode.DELETE || pos.begin > newPos) { pos.begin == newPos } } }
                var lvp = getLastValidPosition(pos.begin, true);
                if (lvp < pos.begin || pos.begin === -1) { getMaskSet().p = seekNext(lvp) } else if (strict !== true) { getMaskSet().p = pos.begin; if (fromIsValid !== true) { while (getMaskSet().p < lvp && getMaskSet().validPositions[getMaskSet().p] === undefined) { getMaskSet().p++ } } }
            }

            function initializeColorMask(input) {
                var computedStyle = (input.ownerDocument.defaultView || window).getComputedStyle(input, null);

                function findCaretPos(clientx) {
                    var e = document.createElement("span"),
                        caretPos;
                    for (var style in computedStyle) { if (isNaN(style) && style.indexOf("font") !== -1) { e.style[style] = computedStyle[style] } }
                    e.style.textTransform = computedStyle.textTransform;
                    e.style.letterSpacing = computedStyle.letterSpacing;
                    e.style.position = "absolute";
                    e.style.height = "auto";
                    e.style.width = "auto";
                    e.style.visibility = "hidden";
                    e.style.whiteSpace = "nowrap";
                    document.body.appendChild(e);
                    var inputText = input.inputmask._valueGet(),
                        previousWidth = 0,
                        itl;
                    for (caretPos = 0, itl = inputText.length; caretPos <= itl; caretPos++) {
                        e.innerHTML += inputText.charAt(caretPos) || "_";
                        if (e.offsetWidth >= clientx) {
                            var offset1 = clientx - previousWidth;
                            var offset2 = e.offsetWidth - clientx;
                            e.innerHTML = inputText.charAt(caretPos);
                            offset1 -= e.offsetWidth / 3;
                            caretPos = offset1 < offset2 ? caretPos - 1 : caretPos;
                            break
                        }
                        previousWidth = e.offsetWidth
                    }
                    document.body.removeChild(e);
                    return caretPos
                }
                var template = document.createElement("div");
                template.style.width = computedStyle.width;
                template.style.textAlign = computedStyle.textAlign;
                colorMask = document.createElement("div");
                input.inputmask.colorMask = colorMask;
                colorMask.className = "im-colormask";
                input.parentNode.insertBefore(colorMask, input);
                input.parentNode.removeChild(input);
                colorMask.appendChild(input);
                colorMask.appendChild(template);
                input.style.left = template.offsetLeft + "px";
                $(colorMask).on("mouseleave", function(e) { return EventHandlers.mouseleaveEvent.call(input, [e]) });
                $(colorMask).on("mouseenter", function(e) { return EventHandlers.mouseenterEvent.call(input, [e]) });
                $(colorMask).on("click", function(e) { caret(input, findCaretPos(e.clientX)); return EventHandlers.clickEvent.call(input, [e]) })
            }
            Inputmask.prototype.positionColorMask = function(input, template) { input.style.left = template.offsetLeft + "px" };

            function renderColorMask(input, caretPos, clear) {
                var maskTemplate = [],
                    isStatic = false,
                    test, testPos, ndxIntlzr, pos = 0;

                function setEntry(entry) {
                    if (entry === undefined) entry = "";
                    if (!isStatic && (test.fn === null || testPos.input === undefined)) {
                        isStatic = true;
                        maskTemplate.push("<span class='im-static'>" + entry)
                    } else if (isStatic && (test.fn !== null && testPos.input !== undefined || test.def === "")) {
                        isStatic = false;
                        var mtl = maskTemplate.length;
                        maskTemplate[mtl - 1] = maskTemplate[mtl - 1] + "</span>";
                        maskTemplate.push(entry)
                    } else maskTemplate.push(entry)
                }

                function setCaret() {
                    if (document.activeElement === input) {
                        maskTemplate.splice(caretPos.begin, 0, caretPos.begin === caretPos.end || caretPos.end > getMaskSet().maskLength ? '<mark class="im-caret" style="border-right-width: 1px;border-right-style: solid;">' : '<mark class="im-caret-select">');
                        maskTemplate.splice(caretPos.end + 1, 0, "</mark>")
                    }
                }
                if (colorMask !== undefined) {
                    var buffer = getBuffer();
                    if (caretPos === undefined) { caretPos = caret(input) } else if (caretPos.begin === undefined) { caretPos = { begin: caretPos, end: caretPos } }
                    if (clear !== true) {
                        var lvp = getLastValidPosition();
                        do {
                            if (getMaskSet().validPositions[pos]) {
                                testPos = getMaskSet().validPositions[pos];
                                test = testPos.match;
                                ndxIntlzr = testPos.locator.slice();
                                setEntry(buffer[pos])
                            } else {
                                testPos = getTestTemplate(pos, ndxIntlzr, pos - 1);
                                test = testPos.match;
                                ndxIntlzr = testPos.locator.slice();
                                if (opts.jitMasking === false || pos < lvp || typeof opts.jitMasking === "number" && isFinite(opts.jitMasking) && opts.jitMasking > pos) { setEntry(getPlaceholder(pos, test)) } else isStatic = false
                            }
                            pos++
                        } while ((maxLength === undefined || pos < maxLength) && (test.fn !== null || test.def !== "") || lvp > pos || isStatic);
                        if (isStatic) setEntry();
                        setCaret()
                    }
                    var template = colorMask.getElementsByTagName("div")[0];
                    template.innerHTML = maskTemplate.join("");
                    input.inputmask.positionColorMask(input, template)
                }
            }

            function mask(elem) {
                function isElementTypeSupported(input, opts) {
                    function patchValueProperty(npt) {
                        var valueGet;
                        var valueSet;

                        function patchValhook(type) {
                            if ($.valHooks && ($.valHooks[type] === undefined || $.valHooks[type].inputmaskpatch !== true)) {
                                var valhookGet = $.valHooks[type] && $.valHooks[type].get ? $.valHooks[type].get : function(elem) { return elem.value };
                                var valhookSet = $.valHooks[type] && $.valHooks[type].set ? $.valHooks[type].set : function(elem, value) { elem.value = value; return elem };
                                $.valHooks[type] = {
                                    get: function get(elem) { if (elem.inputmask) { if (elem.inputmask.opts.autoUnmask) { return elem.inputmask.unmaskedvalue() } else { var result = valhookGet(elem); return getLastValidPosition(undefined, undefined, elem.inputmask.maskset.validPositions) !== -1 || opts.nullable !== true ? result : "" } } else return valhookGet(elem) },
                                    set: function set(elem, value) {
                                        var $elem = $(elem),
                                            result;
                                        result = valhookSet(elem, value);
                                        if (elem.inputmask) { $elem.trigger("setvalue", [value]) }
                                        return result
                                    },
                                    inputmaskpatch: true
                                }
                            }
                        }

                        function getter() { if (this.inputmask) { return this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : getLastValidPosition() !== -1 || opts.nullable !== true ? document.activeElement === this && opts.clearMaskOnLostFocus ? (isRTL ? clearOptionalTail(getBuffer().slice()).reverse() : clearOptionalTail(getBuffer().slice())).join("") : valueGet.call(this) : "" } else return valueGet.call(this) }

                        function setter(value) { valueSet.call(this, value); if (this.inputmask) { $(this).trigger("setvalue", [value]) } }

                        function installNativeValueSetFallback(npt) {
                            EventRuler.on(npt, "mouseenter", function(event) {
                                var $input = $(this),
                                    input = this,
                                    value = input.inputmask._valueGet();
                                if (value !== getBuffer().join("")) { $input.trigger("setvalue") }
                            })
                        }
                        if (!npt.inputmask.__valueGet) {
                            if (opts.noValuePatching !== true) {
                                if (Object.getOwnPropertyDescriptor) {
                                    if (typeof Object.getPrototypeOf !== "function") { Object.getPrototypeOf = _typeof("test".__proto__) === "object" ? function(object) { return object.__proto__ } : function(object) { return object.constructor.prototype } }
                                    var valueProperty = Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(npt), "value") : undefined;
                                    if (valueProperty && valueProperty.get && valueProperty.set) {
                                        valueGet = valueProperty.get;
                                        valueSet = valueProperty.set;
                                        Object.defineProperty(npt, "value", { get: getter, set: setter, configurable: true })
                                    } else if (npt.tagName !== "INPUT") {
                                        valueGet = function valueGet() { return this.textContent };
                                        valueSet = function valueSet(value) { this.textContent = value };
                                        Object.defineProperty(npt, "value", { get: getter, set: setter, configurable: true })
                                    }
                                } else if (document.__lookupGetter__ && npt.__lookupGetter__("value")) {
                                    valueGet = npt.__lookupGetter__("value");
                                    valueSet = npt.__lookupSetter__("value");
                                    npt.__defineGetter__("value", getter);
                                    npt.__defineSetter__("value", setter)
                                }
                                npt.inputmask.__valueGet = valueGet;
                                npt.inputmask.__valueSet = valueSet
                            }
                            npt.inputmask._valueGet = function(overruleRTL) { return isRTL && overruleRTL !== true ? valueGet.call(this.el).split("").reverse().join("") : valueGet.call(this.el) };
                            npt.inputmask._valueSet = function(value, overruleRTL) { valueSet.call(this.el, value === null || value === undefined ? "" : overruleRTL !== true && isRTL ? value.split("").reverse().join("") : value) };
                            if (valueGet === undefined) {
                                valueGet = function valueGet() { return this.value };
                                valueSet = function valueSet(value) { this.value = value };
                                patchValhook(npt.type);
                                installNativeValueSetFallback(npt)
                            }
                        }
                    }
                    var elementType = input.getAttribute("type");
                    var isSupported = input.tagName === "INPUT" && $.inArray(elementType, opts.supportsInputType) !== -1 || input.isContentEditable || input.tagName === "TEXTAREA";
                    if (!isSupported) {
                        if (input.tagName === "INPUT") {
                            var el = document.createElement("input");
                            el.setAttribute("type", elementType);
                            isSupported = el.type === "text";
                            el = null
                        } else isSupported = "partial"
                    }
                    if (isSupported !== false) { patchValueProperty(input) } else input.inputmask = undefined;
                    return isSupported
                }
                EventRuler.off(elem);
                var isSupported = isElementTypeSupported(elem, opts);
                if (isSupported !== false) {
                    el = elem;
                    $el = $(el);
                    originalPlaceholder = el.placeholder;
                    maxLength = el !== undefined ? el.maxLength : undefined;
                    if (maxLength === -1) maxLength = undefined;
                    if (opts.colorMask === true) { initializeColorMask(el) }
                    if (mobile) {
                        if ("inputmode" in el) {
                            el.inputmode = opts.inputmode;
                            el.setAttribute("inputmode", opts.inputmode)
                        }
                        if (opts.disablePredictiveText === true) {
                            if ("autocorrect" in el) { el.autocorrect = false } else {
                                if (opts.colorMask !== true) { initializeColorMask(el) }
                                el.type = "password"
                            }
                        }
                    }
                    if (isSupported === true) {
                        el.setAttribute("im-insert", opts.insertMode);
                        EventRuler.on(el, "submit", EventHandlers.submitEvent);
                        EventRuler.on(el, "reset", EventHandlers.resetEvent);
                        EventRuler.on(el, "blur", EventHandlers.blurEvent);
                        EventRuler.on(el, "focus", EventHandlers.focusEvent);
                        if (opts.colorMask !== true) {
                            EventRuler.on(el, "click", EventHandlers.clickEvent);
                            EventRuler.on(el, "mouseleave", EventHandlers.mouseleaveEvent);
                            EventRuler.on(el, "mouseenter", EventHandlers.mouseenterEvent)
                        }
                        EventRuler.on(el, "paste", EventHandlers.pasteEvent);
                        EventRuler.on(el, "cut", EventHandlers.cutEvent);
                        EventRuler.on(el, "complete", opts.oncomplete);
                        EventRuler.on(el, "incomplete", opts.onincomplete);
                        EventRuler.on(el, "cleared", opts.oncleared);
                        if (!mobile && opts.inputEventOnly !== true) {
                            EventRuler.on(el, "keydown", EventHandlers.keydownEvent);
                            EventRuler.on(el, "keypress", EventHandlers.keypressEvent)
                        } else { el.removeAttribute("maxLength") }
                        EventRuler.on(el, "input", EventHandlers.inputFallBackEvent);
                        EventRuler.on(el, "beforeinput", EventHandlers.beforeInputEvent)
                    }
                    EventRuler.on(el, "setvalue", EventHandlers.setValueEvent);
                    undoValue = getBufferTemplate().join("");
                    if (el.inputmask._valueGet(true) !== "" || opts.clearMaskOnLostFocus === false || document.activeElement === el) {
                        var initialValue = $.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(inputmask, el.inputmask._valueGet(true), opts) || el.inputmask._valueGet(true) : el.inputmask._valueGet(true);
                        if (initialValue !== "") checkVal(el, true, false, initialValue.split(""));
                        var buffer = getBuffer().slice();
                        undoValue = buffer.join("");
                        if (isComplete(buffer) === false) { if (opts.clearIncomplete) { resetMaskSet() } }
                        if (opts.clearMaskOnLostFocus && document.activeElement !== el) { if (getLastValidPosition() === -1) { buffer = [] } else { clearOptionalTail(buffer) } }
                        if (opts.clearMaskOnLostFocus === false || opts.showMaskOnFocus && document.activeElement === el || el.inputmask._valueGet(true) !== "") writeBuffer(el, buffer);
                        if (document.activeElement === el) { caret(el, seekNext(getLastValidPosition())) }
                    }
                }
            }
            var valueBuffer;
            if (actionObj !== undefined) {
                switch (actionObj.action) {
                    case "isComplete":
                        el = actionObj.el;
                        return isComplete(getBuffer());
                    case "unmaskedvalue":
                        if (el === undefined || actionObj.value !== undefined) {
                            valueBuffer = actionObj.value;
                            valueBuffer = ($.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(inputmask, valueBuffer, opts) || valueBuffer : valueBuffer).split("");
                            checkVal.call(this, undefined, false, false, valueBuffer);
                            if ($.isFunction(opts.onBeforeWrite)) opts.onBeforeWrite.call(inputmask, undefined, getBuffer(), 0, opts)
                        }
                        return unmaskedvalue(el);
                    case "mask":
                        mask(el);
                        break;
                    case "format":
                        valueBuffer = ($.isFunction(opts.onBeforeMask) ? opts.onBeforeMask.call(inputmask, actionObj.value, opts) || actionObj.value : actionObj.value).split("");
                        checkVal.call(this, undefined, true, false, valueBuffer);
                        if (actionObj.metadata) { return { value: isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join(""), metadata: maskScope.call(this, { action: "getmetadata" }, maskset, opts) } }
                        return isRTL ? getBuffer().slice().reverse().join("") : getBuffer().join("");
                    case "isValid":
                        if (actionObj.value) {
                            valueBuffer = actionObj.value.split("");
                            checkVal.call(this, undefined, true, true, valueBuffer)
                        } else { actionObj.value = getBuffer().join("") }
                        var buffer = getBuffer();
                        var rl = determineLastRequiredPosition(),
                            lmib = buffer.length - 1;
                        for (; lmib > rl; lmib--) { if (isMask(lmib)) break }
                        buffer.splice(rl, lmib + 1 - rl);
                        return isComplete(buffer) && actionObj.value === getBuffer().join("");
                    case "getemptymask":
                        return getBufferTemplate().join("");
                    case "remove":
                        if (el && el.inputmask) {
                            $.data(el, "_inputmask_opts", null);
                            $el = $(el);
                            el.inputmask._valueSet(opts.autoUnmask ? unmaskedvalue(el) : el.inputmask._valueGet(true));
                            EventRuler.off(el);
                            if (el.inputmask.colorMask) {
                                colorMask = el.inputmask.colorMask;
                                colorMask.removeChild(el);
                                colorMask.parentNode.insertBefore(el, colorMask);
                                colorMask.parentNode.removeChild(colorMask)
                            }
                            var valueProperty;
                            if (Object.getOwnPropertyDescriptor && Object.getPrototypeOf) { valueProperty = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(el), "value"); if (valueProperty) { if (el.inputmask.__valueGet) { Object.defineProperty(el, "value", { get: el.inputmask.__valueGet, set: el.inputmask.__valueSet, configurable: true }) } } } else if (document.__lookupGetter__ && el.__lookupGetter__("value")) {
                                if (el.inputmask.__valueGet) {
                                    el.__defineGetter__("value", el.inputmask.__valueGet);
                                    el.__defineSetter__("value", el.inputmask.__valueSet)
                                }
                            }
                            el.inputmask = undefined
                        }
                        return el;
                        break;
                    case "getmetadata":
                        if ($.isArray(maskset.metadata)) {
                            var maskTarget = getMaskTemplate(true, 0, false).join("");
                            $.each(maskset.metadata, function(ndx, mtdt) { if (mtdt.mask === maskTarget) { maskTarget = mtdt; return false } });
                            return maskTarget
                        }
                        return maskset.metadata
                }
            }
        }
        return Inputmask
    })
}, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    (function(factory) { if (true) {!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(4)], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) } else {} })(function($) { return $ })
}, function(module, exports) { module.exports = jQuery }, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    if (true) !(__WEBPACK_AMD_DEFINE_RESULT__ = function() { return typeof window !== "undefined" ? window : new(eval("require('jsdom').JSDOM"))("").window }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    else {}
}, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    (function(factory) { if (true) {!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) } else {} })(function(Inputmask) {
        var $ = Inputmask.dependencyLib;
        var formatCode = { d: ["[1-9]|[12][0-9]|3[01]", Date.prototype.setDate, "day", Date.prototype.getDate], dd: ["0[1-9]|[12][0-9]|3[01]", Date.prototype.setDate, "day", function() { return pad(Date.prototype.getDate.call(this), 2) }], ddd: [""], dddd: [""], m: ["[1-9]|1[012]", Date.prototype.setMonth, "month", function() { return Date.prototype.getMonth.call(this) + 1 }], mm: ["0[1-9]|1[012]", Date.prototype.setMonth, "month", function() { return pad(Date.prototype.getMonth.call(this) + 1, 2) }], mmm: [""], mmmm: [""], yy: ["[0-9]{2}", Date.prototype.setFullYear, "year", function() { return pad(Date.prototype.getFullYear.call(this), 2) }], yyyy: ["[0-9]{4}", Date.prototype.setFullYear, "year", function() { return pad(Date.prototype.getFullYear.call(this), 4) }], h: ["[1-9]|1[0-2]", Date.prototype.setHours, "hours", Date.prototype.getHours], hh: ["0[1-9]|1[0-2]", Date.prototype.setHours, "hours", function() { return pad(Date.prototype.getHours.call(this), 2) }], hhh: ["[0-9]+", Date.prototype.setHours, "hours", Date.prototype.getHours], H: ["1?[0-9]|2[0-3]", Date.prototype.setHours, "hours", Date.prototype.getHours], HH: ["0[0-9]|1[0-9]|2[0-3]", Date.prototype.setHours, "hours", function() { return pad(Date.prototype.getHours.call(this), 2) }], HHH: ["[0-9]+", Date.prototype.setHours, "hours", Date.prototype.getHours], M: ["[1-5]?[0-9]", Date.prototype.setMinutes, "minutes", Date.prototype.getMinutes], MM: ["0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]", Date.prototype.setMinutes, "minutes", function() { return pad(Date.prototype.getMinutes.call(this), 2) }], ss: ["[0-5][0-9]", Date.prototype.setSeconds, "seconds", function() { return pad(Date.prototype.getSeconds.call(this), 2) }], l: ["[0-9]{3}", Date.prototype.setMilliseconds, "milliseconds", function() { return pad(Date.prototype.getMilliseconds.call(this), 3) }], L: ["[0-9]{2}", Date.prototype.setMilliseconds, "milliseconds", function() { return pad(Date.prototype.getMilliseconds.call(this), 2) }], t: ["[ap]"], tt: ["[ap]m"], T: ["[AP]"], TT: ["[AP]M"], Z: [""], o: [""], S: [""] },
            formatAlias = { isoDate: "yyyy-mm-dd", isoTime: "HH:MM:ss", isoDateTime: "yyyy-mm-dd'T'HH:MM:ss", isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'" };

        function getTokenizer(opts) {
            if (!opts.tokenizer) {
                var tokens = [];
                for (var ndx in formatCode) { if (tokens.indexOf(ndx[0]) === -1) tokens.push(ndx[0]) }
                opts.tokenizer = "(" + tokens.join("+|") + ")+?|.";
                opts.tokenizer = new RegExp(opts.tokenizer, "g")
            }
            return opts.tokenizer
        }

        function isValidDate(dateParts, currentResult) { return !isFinite(dateParts.rawday) || dateParts.day == "29" && !isFinite(dateParts.rawyear) || new Date(dateParts.date.getFullYear(), isFinite(dateParts.rawmonth) ? dateParts.month : dateParts.date.getMonth() + 1, 0).getDate() >= dateParts.day ? currentResult : false }

        function isDateInRange(dateParts, opts) {
            var result = true;
            if (opts.min) {
                if (dateParts["rawyear"]) {
                    var rawYear = dateParts["rawyear"].replace(/[^0-9]/g, ""),
                        minYear = opts.min.year.substr(0, rawYear.length);
                    result = minYear <= rawYear
                }
                if (dateParts["year"] === dateParts["rawyear"]) { if (opts.min.date.getTime() === opts.min.date.getTime()) { result = opts.min.date.getTime() <= dateParts.date.getTime() } }
            }
            if (result && opts.max && opts.max.date.getTime() === opts.max.date.getTime()) { result = opts.max.date.getTime() >= dateParts.date.getTime() }
            return result
        }

        function parse(format, dateObjValue, opts, raw) {
            var mask = "",
                match;
            while (match = getTokenizer(opts).exec(format)) {
                if (dateObjValue === undefined) {
                    if (formatCode[match[0]]) { mask += "(" + formatCode[match[0]][0] + ")" } else {
                        switch (match[0]) {
                            case "[":
                                mask += "(";
                                break;
                            case "]":
                                mask += ")?";
                                break;
                            default:
                                mask += Inputmask.escapeRegex(match[0])
                        }
                    }
                } else {
                    if (formatCode[match[0]]) {
                        if (raw !== true && formatCode[match[0]][3]) {
                            var getFn = formatCode[match[0]][3];
                            mask += getFn.call(dateObjValue.date)
                        } else if (formatCode[match[0]][2]) mask += dateObjValue["raw" + formatCode[match[0]][2]];
                        else mask += match[0]
                    } else mask += match[0]
                }
            }
            return mask
        }

        function pad(val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) { val = "0" + val }
            return val
        }

        function analyseMask(maskString, format, opts) {
            var dateObj = { date: new Date(1, 0, 1) },
                targetProp, mask = maskString,
                match, dateOperation, targetValidator;

            function extendProperty(value) {
                var correctedValue = value.replace(/[^0-9]/g, "0");
                if (correctedValue != value) {
                    var enteredPart = value.replace(/[^0-9]/g, ""),
                        min = (opts.min && opts.min[targetProp] || value).toString(),
                        max = (opts.max && opts.max[targetProp] || value).toString();
                    correctedValue = enteredPart + (enteredPart < min.slice(0, enteredPart.length) ? min.slice(enteredPart.length) : enteredPart > max.slice(0, enteredPart.length) ? max.slice(enteredPart.length) : correctedValue.toString().slice(enteredPart.length))
                }
                return correctedValue
            }

            function setValue(dateObj, value, opts) {
                dateObj[targetProp] = extendProperty(value);
                dateObj["raw" + targetProp] = value;
                if (dateOperation !== undefined) dateOperation.call(dateObj.date, targetProp == "month" ? parseInt(dateObj[targetProp]) - 1 : dateObj[targetProp])
            }
            if (typeof mask === "string") {
                while (match = getTokenizer(opts).exec(format)) {
                    var value = mask.slice(0, match[0].length);
                    if (formatCode.hasOwnProperty(match[0])) {
                        targetValidator = formatCode[match[0]][0];
                        targetProp = formatCode[match[0]][2];
                        dateOperation = formatCode[match[0]][1];
                        setValue(dateObj, value, opts)
                    }
                    mask = mask.slice(value.length)
                }
                return dateObj
            } else if (mask && (typeof mask === "undefined" ? "undefined" : _typeof(mask)) === "object" && mask.hasOwnProperty("date")) { return mask }
            return undefined
        }
        Inputmask.extendAliases({
            datetime: {
                mask: function mask(opts) {
                    formatCode.S = opts.i18n.ordinalSuffix.join("|");
                    opts.inputFormat = formatAlias[opts.inputFormat] || opts.inputFormat;
                    opts.displayFormat = formatAlias[opts.displayFormat] || opts.displayFormat || opts.inputFormat;
                    opts.outputFormat = formatAlias[opts.outputFormat] || opts.outputFormat || opts.inputFormat;
                    opts.placeholder = opts.placeholder !== "" ? opts.placeholder : opts.inputFormat.replace(/[\[\]]/, "");
                    opts.regex = parse(opts.inputFormat, undefined, opts);
                    return null
                },
                placeholder: "",
                inputFormat: "isoDateTime",
                displayFormat: undefined,
                outputFormat: undefined,
                min: null,
                max: null,
                i18n: { dayNames: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"], monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], ordinalSuffix: ["st", "nd", "rd", "th"] },
                postValidation: function postValidation(buffer, pos, currentResult, opts) {
                    opts.min = analyseMask(opts.min, opts.inputFormat, opts);
                    opts.max = analyseMask(opts.max, opts.inputFormat, opts);
                    var result = currentResult,
                        dateParts = analyseMask(buffer.join(""), opts.inputFormat, opts);
                    if (result && dateParts.date.getTime() === dateParts.date.getTime()) {
                        result = isValidDate(dateParts, result);
                        result = result && isDateInRange(dateParts, opts)
                    }
                    if (pos && result && currentResult.pos !== pos) { return { buffer: parse(opts.inputFormat, dateParts, opts), refreshFromBuffer: { start: pos, end: currentResult.pos } } }
                    return result
                },
                onKeyDown: function onKeyDown(e, buffer, caretPos, opts) {
                    var input = this;
                    if (e.ctrlKey && e.keyCode === Inputmask.keyCode.RIGHT) {
                        var today = new Date,
                            match, date = "";
                        while (match = getTokenizer(opts).exec(opts.inputFormat)) { if (match[0].charAt(0) === "d") { date += pad(today.getDate(), match[0].length) } else if (match[0].charAt(0) === "m") { date += pad(today.getMonth() + 1, match[0].length) } else if (match[0] === "yyyy") { date += today.getFullYear().toString() } else if (match[0].charAt(0) === "y") { date += pad(today.getYear(), match[0].length) } }
                        input.inputmask._valueSet(date);
                        $(input).trigger("setvalue")
                    }
                },
                onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) { return parse(opts.outputFormat, analyseMask(maskedValue, opts.inputFormat, opts), opts, true) },
                casing: function casing(elem, test, pos, validPositions) { if (test.nativeDef.indexOf("[ap]") == 0) return elem.toLowerCase(); if (test.nativeDef.indexOf("[AP]") == 0) return elem.toUpperCase(); return elem },
                insertMode: false,
                shiftPositions: false
            }
        });
        return Inputmask
    })
}, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    (function(factory) { if (true) {!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) } else {} })(function(Inputmask) {
        var $ = Inputmask.dependencyLib;

        function autoEscape(txt, opts) { var escapedTxt = ""; for (var i = 0; i < txt.length; i++) { if (Inputmask.prototype.definitions[txt.charAt(i)] || opts.definitions[txt.charAt(i)] || opts.optionalmarker.start === txt.charAt(i) || opts.optionalmarker.end === txt.charAt(i) || opts.quantifiermarker.start === txt.charAt(i) || opts.quantifiermarker.end === txt.charAt(i) || opts.groupmarker.start === txt.charAt(i) || opts.groupmarker.end === txt.charAt(i) || opts.alternatormarker === txt.charAt(i)) { escapedTxt += "\\" + txt.charAt(i) } else escapedTxt += txt.charAt(i) } return escapedTxt }

        function alignDigits(buffer, digits, opts) {
            if (digits > 0) {
                var radixPosition = $.inArray(opts.radixPoint, buffer);
                if (radixPosition === -1) {
                    buffer.push(opts.radixPoint);
                    radixPosition = buffer.length - 1
                }
                for (var i = 1; i <= digits; i++) { buffer[radixPosition + i] = buffer[radixPosition + i] || "0" }
            }
            return buffer
        }
        Inputmask.extendAliases({
            numeric: {
                mask: function mask(opts) {
                    if (opts.repeat !== 0 && isNaN(opts.integerDigits)) { opts.integerDigits = opts.repeat }
                    opts.repeat = 0;
                    if (opts.groupSeparator === opts.radixPoint && opts.digits && opts.digits !== "0") { if (opts.radixPoint === ".") { opts.groupSeparator = "," } else if (opts.radixPoint === ",") { opts.groupSeparator = "." } else opts.groupSeparator = "" }
                    if (opts.groupSeparator === " ") { opts.skipOptionalPartCharacter = undefined }
                    opts.autoGroup = opts.autoGroup && opts.groupSeparator !== "";
                    if (opts.autoGroup) {
                        if (typeof opts.groupSize == "string" && isFinite(opts.groupSize)) opts.groupSize = parseInt(opts.groupSize);
                        if (isFinite(opts.integerDigits)) {
                            var seps = Math.floor(opts.integerDigits / opts.groupSize);
                            var mod = opts.integerDigits % opts.groupSize;
                            opts.integerDigits = parseInt(opts.integerDigits) + (mod === 0 ? seps - 1 : seps);
                            if (opts.integerDigits < 1) { opts.integerDigits = "*" }
                        }
                    }
                    if (opts.placeholder.length > 1) { opts.placeholder = opts.placeholder.charAt(0) }
                    if (opts.positionCaretOnClick === "radixFocus" && opts.placeholder === "" && opts.integerOptional === false) { opts.positionCaretOnClick = "lvp" }
                    opts.definitions[";"] = opts.definitions["~"];
                    opts.definitions[";"].definitionSymbol = "~";
                    if (opts.numericInput === true) {
                        opts.positionCaretOnClick = opts.positionCaretOnClick === "radixFocus" ? "lvp" : opts.positionCaretOnClick;
                        opts.digitsOptional = false;
                        if (isNaN(opts.digits)) opts.digits = 2;
                        opts.decimalProtect = false
                    }
                    var mask = "[+]";
                    mask += autoEscape(opts.prefix, opts);
                    if (opts.integerOptional === true) { mask += "~{1," + opts.integerDigits + "}" } else mask += "~{" + opts.integerDigits + "}";
                    if (opts.digits !== undefined) { var radixDef = opts.decimalProtect ? ":" : opts.radixPoint; var dq = opts.digits.toString().split(","); if (isFinite(dq[0]) && dq[1] && isFinite(dq[1])) { mask += radixDef + ";{" + opts.digits + "}" } else if (isNaN(opts.digits) || parseInt(opts.digits) > 0) { if (opts.digitsOptional) { mask += "[" + radixDef + ";{1," + opts.digits + "}]" } else mask += radixDef + ";{" + opts.digits + "}" } }
                    mask += autoEscape(opts.suffix, opts);
                    mask += "[-]";
                    opts.greedy = false;
                    return mask
                },
                placeholder: "",
                greedy: false,
                digits: "*",
                digitsOptional: true,
                enforceDigitsOnBlur: false,
                radixPoint: ".",
                positionCaretOnClick: "radixFocus",
                groupSize: 3,
                groupSeparator: "",
                autoGroup: false,
                allowMinus: true,
                negationSymbol: { front: "-", back: "" },
                integerDigits: "+",
                integerOptional: true,
                prefix: "",
                suffix: "",
                rightAlign: true,
                decimalProtect: true,
                min: null,
                max: null,
                step: 1,
                insertMode: true,
                autoUnmask: false,
                unmaskAsNumber: false,
                inputType: "text",
                inputmode: "numeric",
                preValidation: function preValidation(buffer, pos, c, isSelection, opts, maskset) {
                    if (c === "-" || c === opts.negationSymbol.front) {
                        if (opts.allowMinus !== true) return false;
                        opts.isNegative = opts.isNegative === undefined ? true : !opts.isNegative;
                        if (buffer.join("") === "") return true;
                        return { caret: maskset.validPositions[pos] ? pos : undefined, dopost: true }
                    }
                    if (isSelection === false && c === opts.radixPoint && opts.digits !== undefined && (isNaN(opts.digits) || parseInt(opts.digits) > 0)) { var radixPos = $.inArray(opts.radixPoint, buffer); if (radixPos !== -1 && maskset.validPositions[radixPos] !== undefined) { if (opts.numericInput === true) { return pos === radixPos } return { caret: radixPos + 1 } } }
                    return true
                },
                postValidation: function postValidation(buffer, pos, currentResult, opts) {
                    function buildPostMask(buffer, opts) {
                        var postMask = "";
                        postMask += "(" + opts.groupSeparator + "*{" + opts.groupSize + "}){*}";
                        if (opts.radixPoint !== "") { var radixSplit = buffer.join("").split(opts.radixPoint); if (radixSplit[1]) { postMask += opts.radixPoint + "*{" + radixSplit[1].match(/^\d*\??\d*/)[0].length + "}" } }
                        return postMask
                    }
                    var suffix = opts.suffix.split(""),
                        prefix = opts.prefix.split("");
                    if (currentResult.pos === undefined && currentResult.caret !== undefined && currentResult.dopost !== true) return currentResult;
                    var caretPos = currentResult.caret !== undefined ? currentResult.caret : currentResult.pos;
                    var maskedValue = buffer.slice();
                    if (opts.numericInput) {
                        caretPos = maskedValue.length - caretPos - 1;
                        maskedValue = maskedValue.reverse()
                    }
                    var charAtPos = maskedValue[caretPos];
                    if (charAtPos === opts.groupSeparator) {
                        caretPos += 1;
                        charAtPos = maskedValue[caretPos]
                    }
                    if (caretPos === maskedValue.length - opts.suffix.length - 1 && charAtPos === opts.radixPoint) return currentResult;
                    if (charAtPos !== undefined) { if (charAtPos !== opts.radixPoint && charAtPos !== opts.negationSymbol.front && charAtPos !== opts.negationSymbol.back) { maskedValue[caretPos] = "?"; if (opts.prefix.length > 0 && caretPos >= (opts.isNegative === false ? 1 : 0) && caretPos < opts.prefix.length - 1 + (opts.isNegative === false ? 1 : 0)) { prefix[caretPos - (opts.isNegative === false ? 1 : 0)] = "?" } else if (opts.suffix.length > 0 && caretPos >= maskedValue.length - opts.suffix.length - (opts.isNegative === false ? 1 : 0)) { suffix[caretPos - (maskedValue.length - opts.suffix.length - (opts.isNegative === false ? 1 : 0))] = "?" } } }
                    prefix = prefix.join("");
                    suffix = suffix.join("");
                    var processValue = maskedValue.join("").replace(prefix, "");
                    processValue = processValue.replace(suffix, "");
                    processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), "");
                    processValue = processValue.replace(new RegExp("[-" + Inputmask.escapeRegex(opts.negationSymbol.front) + "]", "g"), "");
                    processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.negationSymbol.back) + "$"), "");
                    if (isNaN(opts.placeholder)) { processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.placeholder), "g"), "") }
                    if (processValue.length > 1 && processValue.indexOf(opts.radixPoint) !== 1) {
                        if (charAtPos === "0") { processValue = processValue.replace(/^\?/g, "") }
                        processValue = processValue.replace(/^0/g, "")
                    }
                    if (processValue.charAt(0) === opts.radixPoint && opts.radixPoint !== "" && opts.numericInput !== true) { processValue = "0" + processValue }
                    if (processValue !== "") {
                        processValue = processValue.split("");
                        if ((!opts.digitsOptional || opts.enforceDigitsOnBlur && currentResult.event === "blur") && isFinite(opts.digits)) {
                            var radixPosition = $.inArray(opts.radixPoint, processValue);
                            var rpb = $.inArray(opts.radixPoint, maskedValue);
                            if (radixPosition === -1) {
                                processValue.push(opts.radixPoint);
                                radixPosition = processValue.length - 1
                            }
                            for (var i = 1; i <= opts.digits; i++) { if ((!opts.digitsOptional || opts.enforceDigitsOnBlur && currentResult.event === "blur") && (processValue[radixPosition + i] === undefined || processValue[radixPosition + i] === opts.placeholder.charAt(0))) { processValue[radixPosition + i] = currentResult.placeholder || opts.placeholder.charAt(0) } else if (rpb !== -1 && maskedValue[rpb + i] !== undefined) { processValue[radixPosition + i] = processValue[radixPosition + i] || maskedValue[rpb + i] } }
                        }
                        if (opts.autoGroup === true && opts.groupSeparator !== "" && (charAtPos !== opts.radixPoint || currentResult.pos !== undefined || currentResult.dopost)) {
                            var addRadix = processValue[processValue.length - 1] === opts.radixPoint && currentResult.c === opts.radixPoint;
                            processValue = Inputmask(buildPostMask(processValue, opts), { numericInput: true, jitMasking: true, definitions: { "*": { validator: "[0-9?]", cardinality: 1 } } }).format(processValue.join(""));
                            if (addRadix) processValue += opts.radixPoint;
                            if (processValue.charAt(0) === opts.groupSeparator) { processValue.substr(1) }
                        } else processValue = processValue.join("")
                    }
                    if (opts.isNegative && currentResult.event === "blur") { opts.isNegative = processValue !== "0" }
                    processValue = prefix + processValue;
                    processValue += suffix;
                    if (opts.isNegative) {
                        processValue = opts.negationSymbol.front + processValue;
                        processValue += opts.negationSymbol.back
                    }
                    processValue = processValue.split("");
                    if (charAtPos !== undefined) { if (charAtPos !== opts.radixPoint && charAtPos !== opts.negationSymbol.front && charAtPos !== opts.negationSymbol.back) { caretPos = $.inArray("?", processValue); if (caretPos > -1) { processValue[caretPos] = charAtPos } else caretPos = currentResult.caret || 0 } else if (charAtPos === opts.radixPoint || charAtPos === opts.negationSymbol.front || charAtPos === opts.negationSymbol.back) { var newCaretPos = $.inArray(charAtPos, processValue); if (newCaretPos !== -1) caretPos = newCaretPos } }
                    if (opts.numericInput) {
                        caretPos = processValue.length - caretPos - 1;
                        processValue = processValue.reverse()
                    }
                    var rslt = { caret: (charAtPos === undefined || currentResult.pos !== undefined) && caretPos !== undefined ? caretPos + (opts.numericInput ? -1 : 1) : caretPos, buffer: processValue, refreshFromBuffer: currentResult.dopost || buffer.join("") !== processValue.join("") };
                    return rslt.refreshFromBuffer ? rslt : currentResult
                },
                onBeforeWrite: function onBeforeWrite(e, buffer, caretPos, opts) {
                    function parseMinMaxOptions(opts) {
                        if (opts.parseMinMaxOptions === undefined) {
                            if (opts.min !== null) {
                                opts.min = opts.min.toString().replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), "");
                                if (opts.radixPoint === ",") opts.min = opts.min.replace(opts.radixPoint, ".");
                                opts.min = isFinite(opts.min) ? parseFloat(opts.min) : NaN;
                                if (isNaN(opts.min)) opts.min = Number.MIN_VALUE
                            }
                            if (opts.max !== null) {
                                opts.max = opts.max.toString().replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), "");
                                if (opts.radixPoint === ",") opts.max = opts.max.replace(opts.radixPoint, ".");
                                opts.max = isFinite(opts.max) ? parseFloat(opts.max) : NaN;
                                if (isNaN(opts.max)) opts.max = Number.MAX_VALUE
                            }
                            opts.parseMinMaxOptions = "done"
                        }
                    }
                    if (e) {
                        switch (e.type) {
                            case "keydown":
                                return opts.postValidation(buffer, caretPos, { caret: caretPos, dopost: true }, opts);
                            case "blur":
                            case "checkval":
                                var unmasked;
                                parseMinMaxOptions(opts);
                                if (opts.min !== null || opts.max !== null) { unmasked = opts.onUnMask(buffer.join(""), undefined, $.extend({}, opts, { unmaskAsNumber: true })); if (opts.min !== null && unmasked < opts.min) { opts.isNegative = opts.min < 0; return opts.postValidation(opts.min.toString().replace(".", opts.radixPoint).split(""), caretPos, { caret: caretPos, dopost: true, placeholder: "0" }, opts) } else if (opts.max !== null && unmasked > opts.max) { opts.isNegative = opts.max < 0; return opts.postValidation(opts.max.toString().replace(".", opts.radixPoint).split(""), caretPos, { caret: caretPos, dopost: true, placeholder: "0" }, opts) } }
                                return opts.postValidation(buffer, caretPos, { caret: caretPos, placeholder: "0", event: "blur" }, opts);
                            case "_checkval":
                                return { caret: caretPos };
                            default:
                                break
                        }
                    }
                },
                regex: { integerPart: function integerPart(opts, emptyCheck) { return emptyCheck ? new RegExp("[" + Inputmask.escapeRegex(opts.negationSymbol.front) + "+]?") : new RegExp("[" + Inputmask.escapeRegex(opts.negationSymbol.front) + "+]?\\d+") }, integerNPart: function integerNPart(opts) { return new RegExp("[\\d" + Inputmask.escapeRegex(opts.groupSeparator) + Inputmask.escapeRegex(opts.placeholder.charAt(0)) + "]+") } },
                definitions: {
                    "~": {
                        validator: function validator(chrs, maskset, pos, strict, opts, isSelection) {
                            var isValid, l;
                            if (chrs === "k" || chrs === "m") {
                                isValid = { insert: [], c: 0 };
                                for (var i = 0, l = chrs === "k" ? 2 : 5; i < l; i++) { isValid.insert.push({ pos: pos + i, c: 0 }) }
                                isValid.pos = pos + l;
                                return isValid
                            }
                            isValid = strict ? new RegExp("[0-9" + Inputmask.escapeRegex(opts.groupSeparator) + "]").test(chrs) : new RegExp("[0-9]").test(chrs);
                            if (isValid === true) {
                                if (opts.numericInput !== true && maskset.validPositions[pos] !== undefined && maskset.validPositions[pos].match.def === "~" && !isSelection) {
                                    var processValue = maskset.buffer.join("");
                                    processValue = processValue.replace(new RegExp("[-" + Inputmask.escapeRegex(opts.negationSymbol.front) + "]", "g"), "");
                                    processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.negationSymbol.back) + "$"), "");
                                    var pvRadixSplit = processValue.split(opts.radixPoint);
                                    if (pvRadixSplit.length > 1) { pvRadixSplit[1] = pvRadixSplit[1].replace(/0/g, opts.placeholder.charAt(0)) }
                                    if (pvRadixSplit[0] === "0") { pvRadixSplit[0] = pvRadixSplit[0].replace(/0/g, opts.placeholder.charAt(0)) }
                                    processValue = pvRadixSplit[0] + opts.radixPoint + pvRadixSplit[1] || "";
                                    var bufferTemplate = maskset._buffer.join("");
                                    if (processValue === opts.radixPoint) { processValue = bufferTemplate }
                                    while (processValue.match(Inputmask.escapeRegex(bufferTemplate) + "$") === null) { bufferTemplate = bufferTemplate.slice(1) }
                                    processValue = processValue.replace(bufferTemplate, "");
                                    processValue = processValue.split("");
                                    if (processValue[pos] === undefined) { isValid = { pos: pos, remove: pos } } else { isValid = { pos: pos } }
                                }
                            } else if (!strict && chrs === opts.radixPoint && maskset.validPositions[pos - 1] === undefined) { isValid = { insert: { pos: pos, c: 0 }, pos: pos + 1 } }
                            return isValid
                        },
                        cardinality: 1
                    },
                    "+": { validator: function validator(chrs, maskset, pos, strict, opts) { return opts.allowMinus && (chrs === "-" || chrs === opts.negationSymbol.front) }, cardinality: 1, placeholder: "" },
                    "-": { validator: function validator(chrs, maskset, pos, strict, opts) { return opts.allowMinus && chrs === opts.negationSymbol.back }, cardinality: 1, placeholder: "" },
                    ":": { validator: function validator(chrs, maskset, pos, strict, opts) { var radix = "[" + Inputmask.escapeRegex(opts.radixPoint) + "]"; var isValid = new RegExp(radix).test(chrs); if (isValid && maskset.validPositions[pos] && maskset.validPositions[pos].match.placeholder === opts.radixPoint) { isValid = { caret: pos + 1 } } return isValid }, cardinality: 1, placeholder: function placeholder(opts) { return opts.radixPoint } }
                },
                onUnMask: function onUnMask(maskedValue, unmaskedValue, opts) {
                    if (unmaskedValue === "" && opts.nullable === true) { return unmaskedValue }
                    var processValue = maskedValue.replace(opts.prefix, "");
                    processValue = processValue.replace(opts.suffix, "");
                    processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), "");
                    if (opts.placeholder.charAt(0) !== "") { processValue = processValue.replace(new RegExp(opts.placeholder.charAt(0), "g"), "0") }
                    if (opts.unmaskAsNumber) {
                        if (opts.radixPoint !== "" && processValue.indexOf(opts.radixPoint) !== -1) processValue = processValue.replace(Inputmask.escapeRegex.call(this, opts.radixPoint), ".");
                        processValue = processValue.replace(new RegExp("^" + Inputmask.escapeRegex(opts.negationSymbol.front)), "-");
                        processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.negationSymbol.back) + "$"), "");
                        return Number(processValue)
                    }
                    return processValue
                },
                isComplete: function isComplete(buffer, opts) {
                    var maskedValue = (opts.numericInput ? buffer.slice().reverse() : buffer).join("");
                    maskedValue = maskedValue.replace(new RegExp("^" + Inputmask.escapeRegex(opts.negationSymbol.front)), "-");
                    maskedValue = maskedValue.replace(new RegExp(Inputmask.escapeRegex(opts.negationSymbol.back) + "$"), "");
                    maskedValue = maskedValue.replace(opts.prefix, "");
                    maskedValue = maskedValue.replace(opts.suffix, "");
                    maskedValue = maskedValue.replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator) + "([0-9]{3})", "g"), "$1");
                    if (opts.radixPoint === ",") maskedValue = maskedValue.replace(Inputmask.escapeRegex(opts.radixPoint), ".");
                    return isFinite(maskedValue)
                },
                onBeforeMask: function onBeforeMask(initialValue, opts) {
                    opts.isNegative = undefined;
                    var radixPoint = opts.radixPoint || ",";
                    if ((typeof initialValue == "number" || opts.inputType === "number") && radixPoint !== "") { initialValue = initialValue.toString().replace(".", radixPoint) }
                    var valueParts = initialValue.split(radixPoint),
                        integerPart = valueParts[0].replace(/[^\-0-9]/g, ""),
                        decimalPart = valueParts.length > 1 ? valueParts[1].replace(/[^0-9]/g, "") : "";
                    initialValue = integerPart + (decimalPart !== "" ? radixPoint + decimalPart : decimalPart);
                    var digits = 0;
                    if (radixPoint !== "") {
                        digits = decimalPart.length;
                        if (decimalPart !== "") {
                            var digitsFactor = Math.pow(10, digits || 1);
                            if (isFinite(opts.digits)) {
                                digits = parseInt(opts.digits);
                                digitsFactor = Math.pow(10, digits)
                            }
                            initialValue = initialValue.replace(Inputmask.escapeRegex(radixPoint), ".");
                            if (isFinite(initialValue)) initialValue = Math.round(parseFloat(initialValue) * digitsFactor) / digitsFactor;
                            initialValue = initialValue.toString().replace(".", radixPoint)
                        }
                    }
                    if (opts.digits === 0 && initialValue.indexOf(Inputmask.escapeRegex(radixPoint)) !== -1) { initialValue = initialValue.substring(0, initialValue.indexOf(Inputmask.escapeRegex(radixPoint))) }
                    return alignDigits(initialValue.toString().split(""), digits, opts).join("")
                },
                onKeyDown: function onKeyDown(e, buffer, caretPos, opts) {
                    var $input = $(this);
                    if (e.ctrlKey) {
                        switch (e.keyCode) {
                            case Inputmask.keyCode.UP:
                                $input.val(parseFloat(this.inputmask.unmaskedvalue()) + parseInt(opts.step));
                                $input.trigger("setvalue");
                                break;
                            case Inputmask.keyCode.DOWN:
                                $input.val(parseFloat(this.inputmask.unmaskedvalue()) - parseInt(opts.step));
                                $input.trigger("setvalue");
                                break
                        }
                    }
                }
            },
            currency: { prefix: "$ ", groupSeparator: ",", alias: "numeric", placeholder: "0", autoGroup: true, digits: 2, digitsOptional: false, clearMaskOnLostFocus: false },
            decimal: { alias: "numeric" },
            integer: { alias: "numeric", digits: 0, radixPoint: "" },
            percentage: { alias: "numeric", digits: 2, digitsOptional: true, radixPoint: ".", placeholder: "0", autoGroup: false, min: 0, max: 100, suffix: " %", allowMinus: false }
        });
        return Inputmask
    })
}, function(module, exports, __webpack_require__) {
    "use strict";
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function(obj) { return typeof obj } : function(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj };
    (function(factory) { if (true) {!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(4), __webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) } else {} })(function($, Inputmask) {
        if ($.fn.inputmask === undefined) {
            $.fn.inputmask = function(fn, options) {
                var nptmask, input = this[0];
                if (options === undefined) options = {};
                if (typeof fn === "string") {
                    switch (fn) {
                        case "unmaskedvalue":
                            return input && input.inputmask ? input.inputmask.unmaskedvalue() : $(input).val();
                        case "remove":
                            return this.each(function() { if (this.inputmask) this.inputmask.remove() });
                        case "getemptymask":
                            return input && input.inputmask ? input.inputmask.getemptymask() : "";
                        case "hasMaskedValue":
                            return input && input.inputmask ? input.inputmask.hasMaskedValue() : false;
                        case "isComplete":
                            return input && input.inputmask ? input.inputmask.isComplete() : true;
                        case "getmetadata":
                            return input && input.inputmask ? input.inputmask.getmetadata() : undefined;
                        case "setvalue":
                            Inputmask.setValue(input, options);
                            break;
                        case "option":
                            if (typeof options === "string") { if (input && input.inputmask !== undefined) { return input.inputmask.option(options) } } else { return this.each(function() { if (this.inputmask !== undefined) { return this.inputmask.option(options) } }) }
                            break;
                        default:
                            options.alias = fn;
                            nptmask = new Inputmask(options);
                            return this.each(function() { nptmask.mask(this) })
                    }
                } else if (Array.isArray(fn)) {
                    options.alias = fn;
                    nptmask = new Inputmask(options);
                    return this.each(function() { nptmask.mask(this) })
                } else if ((typeof fn === "undefined" ? "undefined" : _typeof(fn)) == "object") { nptmask = new Inputmask(fn); if (fn.mask === undefined && fn.alias === undefined) { return this.each(function() { if (this.inputmask !== undefined) { return this.inputmask.option(fn) } else nptmask.mask(this) }) } else { return this.each(function() { nptmask.mask(this) }) } } else if (fn === undefined) {
                    return this.each(function() {
                        nptmask = new Inputmask(options);
                        nptmask.mask(this)
                    })
                }
            }
        }
        return $.fn.inputmask
    })
}]);
/*! mailcheck v1.1.2 @licence MIT */
var Mailcheck = {
    domainThreshold: 2,
    secondLevelThreshold: 2,
    topLevelThreshold: 2,
    defaultDomains: ["msn.com", "bellsouth.net", "telus.net", "comcast.net", "optusnet.com.au", "earthlink.net", "qq.com", "sky.com", "icloud.com", "mac.com", "sympatico.ca", "googlemail.com", "att.net", "xtra.co.nz", "web.de", "cox.net", "gmail.com", "ymail.com", "aim.com", "rogers.com", "verizon.net", "rocketmail.com", "google.com", "optonline.net", "sbcglobal.net", "aol.com", "me.com", "btinternet.com", "charter.net", "shaw.ca"],
    defaultSecondLevelDomains: ["yahoo", "hotmail", "mail", "live", "outlook", "gmx"],
    defaultTopLevelDomains: ["com", "com.au", "com.tw", "ca", "co.nz", "co.uk", "de", "fr", "it", "ru", "net", "org", "edu", "gov", "jp", "nl", "kr", "se", "eu", "ie", "co.il", "us", "at", "be", "dk", "hk", "es", "gr", "ch", "no", "cz", "in", "net", "net.au", "info", "biz", "mil", "co.jp", "sg", "hu", "uk"],
    run: function(a) {
        a.domains = a.domains || Mailcheck.defaultDomains, a.secondLevelDomains = a.secondLevelDomains || Mailcheck.defaultSecondLevelDomains, a.topLevelDomains = a.topLevelDomains || Mailcheck.defaultTopLevelDomains, a.distanceFunction = a.distanceFunction || Mailcheck.sift4Distance;
        var b = function(a) { return a },
            c = a.suggested || b,
            d = a.empty || b,
            e = Mailcheck.suggest(Mailcheck.encodeEmail(a.email), a.domains, a.secondLevelDomains, a.topLevelDomains, a.distanceFunction);
        return e ? c(e) : d()
    },
    suggest: function(a, b, c, d, e) {
        a = a.toLowerCase();
        var f = this.splitEmail(a);
        if (c && d && -1 !== c.indexOf(f.secondLevelDomain) && -1 !== d.indexOf(f.topLevelDomain)) return !1;
        var g = this.findClosestDomain(f.domain, b, e, this.domainThreshold);
        if (g) return g == f.domain ? !1 : { address: f.address, domain: g, full: f.address + "@" + g };
        var h = this.findClosestDomain(f.secondLevelDomain, c, e, this.secondLevelThreshold),
            i = this.findClosestDomain(f.topLevelDomain, d, e, this.topLevelThreshold);
        if (f.domain) { g = f.domain; var j = !1; if (h && h != f.secondLevelDomain && (g = g.replace(f.secondLevelDomain, h), j = !0), i && i != f.topLevelDomain && "" !== f.secondLevelDomain && (g = g.replace(new RegExp(f.topLevelDomain + "$"), i), j = !0), j) return { address: f.address, domain: g, full: f.address + "@" + g } }
        return !1
    },
    findClosestDomain: function(a, b, c, d) {
        d = d || this.topLevelThreshold;
        var e, f = 1 / 0,
            g = null;
        if (!a || !b) return !1;
        c || (c = this.sift4Distance);
        for (var h = 0; h < b.length; h++) {
            if (a === b[h]) return a;
            e = c(a, b[h]), f > e && (f = e, g = b[h])
        }
        return d >= f && null !== g ? g : !1
    },
    sift4Distance: function(a, b, c) {
        if (void 0 === c && (c = 5), !a || !a.length) return b ? b.length : 0;
        if (!b || !b.length) return a.length;
        for (var d = a.length, e = b.length, f = 0, g = 0, h = 0, i = 0, j = 0, k = []; d > f && e > g;) {
            if (a.charAt(f) == b.charAt(g)) {
                i++;
                for (var l = !1, m = 0; m < k.length;) {
                    var n = k[m];
                    if (f <= n.c1 || g <= n.c2) { l = Math.abs(g - f) >= Math.abs(n.c2 - n.c1), l ? j++ : n.trans || (n.trans = !0, j++); break }
                    f > n.c2 && g > n.c1 ? k.splice(m, 1) : m++
                }
                k.push({ c1: f, c2: g, trans: l })
            } else { h += i, i = 0, f != g && (f = g = Math.min(f, g)); for (var o = 0; c > o && (d > f + o || e > g + o); o++) { if (d > f + o && a.charAt(f + o) == b.charAt(g)) { f += o - 1, g--; break } if (e > g + o && a.charAt(f) == b.charAt(g + o)) { f--, g += o - 1; break } } }
            f++, g++, (f >= d || g >= e) && (h += i, i = 0, f = g = Math.min(f, g))
        }
        return h += i, Math.round(Math.max(d, e) - h + j)
    },
    splitEmail: function(a) {
        a = null !== a ? a.replace(/^\s*/, "").replace(/\s*$/, "") : null;
        var b = a.split("@");
        if (b.length < 2) return !1;
        for (var c = 0; c < b.length; c++)
            if ("" === b[c]) return !1;
        var d = b.pop(),
            e = d.split("."),
            f = "",
            g = "";
        if (0 === e.length) return !1;
        if (1 == e.length) g = e[0];
        else {
            f = e[0];
            for (var h = 1; h < e.length; h++) g += e[h] + ".";
            g = g.substring(0, g.length - 1)
        }
        return { topLevelDomain: g, secondLevelDomain: f, domain: d, address: b.join("@") }
    },
    encodeEmail: function(a) { var b = encodeURI(a); return b = b.replace("%20", " ").replace("%25", "%").replace("%5E", "^").replace("%60", "`").replace("%7B", "{").replace("%7C", "|").replace("%7D", "}") }
};
"undefined" != typeof module && module.exports && (module.exports = Mailcheck), "function" == typeof define && define.amd && define("mailcheck", [], function() { return Mailcheck }), "undefined" != typeof window && window.jQuery && ! function(a) {
    a.fn.mailcheck = function(a) {
        var b = this;
        if (a.suggested) {
            var c = a.suggested;
            a.suggested = function(a) { c(b, a) }
        }
        if (a.empty) {
            var d = a.empty;
            a.empty = function() { d.call(null, b) }
        }
        a.email = this.val(), Mailcheck.run(a)
    }
}(jQuery);;
(function($) {
    'use strict';
    var WPForms = {
        init: function() {
            $(document).ready(WPForms.ready);
            $(window).on('load', WPForms.load);
            WPForms.bindUIActions();
            WPForms.bindOptinMonster();
        },
        ready: function() {
            WPForms.clearUrlQuery();
            WPForms.setUserIndentifier();
            WPForms.loadValidation();
            WPForms.loadDatePicker();
            WPForms.loadTimePicker();
            WPForms.loadInputMask();
            WPForms.loadSmartPhoneField();
            WPForms.loadPayments();
            $('.wpforms-randomize').each(function() {
                var $list = $(this),
                    $listItems = $list.children();
                while ($listItems.length) { $list.append($listItems.splice(Math.floor(Math.random() * $listItems.length), 1)[0]); }
            });
            $(document).trigger('wpformsReady');
        },
        load: function() {},
        clearUrlQuery: function() {
            var loc = window.location,
                query = loc.search;
            if (query.indexOf('wpforms_form_id=') !== -1) {
                query = query.replace(/([&?]wpforms_form_id=[0-9]*$|wpforms_form_id=[0-9]*&|[?&]wpforms_form_id=[0-9]*(?=#))/, '');
                history.replaceState({}, null, loc.origin + loc.pathname + query);
            }
        },
        loadValidation: function() {
            if (typeof $.fn.validate !== 'undefined') {
                $('.wpforms-input-temp-name').each(function(index, el) {
                    var random = Math.floor(Math.random() * 9999) + 1;
                    $(this).attr('name', 'wpf-temp-' + random);
                });
                $('.wpforms-validate input[type=url]').change(function() {
                    var url = $(this).val();
                    if (!url) { return false; }
                    if (url.substr(0, 7) !== 'http://' && url.substr(0, 8) !== 'https://') { $(this).val('http://' + url); }
                });
                $.validator.messages.required = wpforms_settings.val_required;
                $.validator.messages.url = wpforms_settings.val_url;
                $.validator.messages.email = wpforms_settings.val_email;
                $.validator.messages.number = wpforms_settings.val_number;
                if (typeof $.fn.payment !== 'undefined') { $.validator.addMethod('creditcard', function(value, element) { var valid = $.payment.validateCardNumber(value); return this.optional(element) || valid; }, wpforms_settings.val_creditcard); }
                $.validator.addMethod('extension', function(value, element, param) { param = 'string' === typeof param ? param.replace(/,/g, '|') : 'png|jpe?g|gif'; return this.optional(element) || value.match(new RegExp('\\.(' + param + ')$', 'i')); }, wpforms_settings.val_fileextension);
                $.validator.addMethod('maxsize', function(value, element, param) {
                    var maxSize = param,
                        optionalValue = this.optional(element),
                        i, len, file;
                    if (optionalValue) { return optionalValue; }
                    if (element.files && element.files.length) {
                        i = 0;
                        len = element.files.length;
                        for (; i < len; i++) { file = element.files[i]; if (file.size > maxSize) { return false; } }
                    }
                    return true;
                }, wpforms_settings.val_filesize);
                $.validator.methods.email = function(value, element) { return this.optional(element) || /^[a-z0-9.!#$%&'*+\/=?^_`{|}~-]+@((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}$/i.test(value); };
                $.validator.addMethod('confirm', function(value, element, param) { return $.validator.methods.equalTo.call(this, value, element, param); }, wpforms_settings.val_confirm);
                $.validator.addMethod('required-payment', function(value, element) { return WPForms.amountSanitize(value) > 0; }, wpforms_settings.val_requiredpayment);
                $.validator.addMethod('time12h', function(value, element) { return this.optional(element) || /^((0?[1-9]|1[012])(:[0-5]\d){1,2}(\ ?[AP]M))$/i.test(value); }, wpforms_settings.val_time12h);
                $.validator.addMethod('time24h', function(value, element) { return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(\ ?[AP]M)?$/i.test(value); }, wpforms_settings.val_time24h);
                $.validator.addMethod('check-limit', function(value, element) {
                    var $ul = $(element).closest('ul'),
                        $checked = $ul.find('input[type="checkbox"]:checked'),
                        choiceLimit = parseInt($ul.attr('data-choice-limit') || 0, 10);
                    if (0 === choiceLimit) { return true; }
                    return $checked.length <= choiceLimit;
                }, function(params, element) { var choiceLimit = parseInt($(element).closest('ul').attr('data-choice-limit') || 0, 10); return wpforms_settings.val_checklimit.replace('{#}', choiceLimit); });
                if (typeof $.fn.intlTelInput !== 'undefined') { $.validator.addMethod('smart-phone-field', function(value, element) { return this.optional(element) || $(element).intlTelInput('isValidNumber'); }, wpforms_settings.val_smart_phone); }
                $('.wpforms-validate').each(function() {
                    var form = $(this),
                        formID = form.data('formid'),
                        properties;
                    if (typeof window['wpforms_' + formID] !== 'undefined' && window['wpforms_' + formID].hasOwnProperty('validate')) { properties = window['wpforms_' + formID].validate; } else if (typeof wpforms_validate !== 'undefined') { properties = wpforms_validate; } else {
                        properties = {
                            errorClass: 'wpforms-error',
                            validClass: 'wpforms-valid',
                            errorPlacement: function(error, element) {
                                if ('radio' === element.attr('type') || 'checkbox' === element.attr('type')) {
                                    if (element.hasClass('wpforms-likert-scale-option')) { if (element.closest('table').hasClass('single-row')) { element.closest('table').after(error); } else { element.closest('tr').find('th').append(error); } } else if (element.hasClass('wpforms-net-promoter-score-option')) { element.closest('table').after(error); } else {
                                        element.closest('.wpforms-field-checkbox').find('label.wpforms-error').remove();
                                        element.parent().parent().parent().append(error);
                                    }
                                } else if (element.is('select') && element.attr('class').match(/date-month|date-day|date-year/)) { if (0 === element.parent().find('label.wpforms-error:visible').length) { element.parent().find('select:last').after(error); } } else if (element.hasClass('wpforms-smart-phone-field')) { element.parent().after(error); } else { error.insertAfter(element); }
                            },
                            highlight: function(element, errorClass, validClass) {
                                var $element = $(element),
                                    $field = $element.closest('.wpforms-field'),
                                    inputName = $element.attr('name');
                                if ('radio' === $element.attr('type') || 'checkbox' === $element.attr('type')) { $field.find('input[name=\'' + inputName + '\']').addClass(errorClass).removeClass(validClass); } else { $element.addClass(errorClass).removeClass(validClass); }
                                $field.addClass('wpforms-has-error');
                            },
                            unhighlight: function(element, errorClass, validClass) {
                                var $element = $(element),
                                    $field = $element.closest('.wpforms-field'),
                                    inputName = $element.attr('name');
                                if ('radio' === $element.attr('type') || 'checkbox' === $element.attr('type')) { $field.find('input[name=\'' + inputName + '\']').addClass(validClass).removeClass(errorClass); } else { $element.addClass(validClass).removeClass(errorClass); }
                                $field.removeClass('wpforms-has-error');
                            },
                            submitHandler: function(form) {
                                var $form = $(form),
                                    $submit = $form.find('.wpforms-submit'),
                                    altText = $submit.data('alt-text');
                                if (WPForms.empty($submit.get(0).recaptchaID) && $submit.get(0).recaptchaID !== 0) {
                                    if (altText) { $submit.text(altText).prop('disabled', true); }
                                    $('.wpforms-input-temp-name').removeAttr('name');
                                    form.submit();
                                } else { grecaptcha.execute($submit.get(0).recaptchaID); }
                            },
                            onkeyup: function(element, event) {
                                var excludedKeys = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];
                                if ($(element).hasClass('wpforms-novalidate-onkeyup')) { return; }
                                if (9 === event.which && '' === this.elementValue(element) || $.inArray(event.keyCode, excludedKeys) !== -1) { return; } else if (element.name in this.submitted || element.name in this.invalid) { this.element(element); }
                            },
                            onfocusout: function(element) {
                                var validate = false;
                                if ($(element).hasClass('wpforms-novalidate-onkeyup') && !element.value) { validate = true; }
                                if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) { validate = true; }
                                if (validate) { this.element(element); }
                            },
                            onclick: function(element) {
                                var validate = false;
                                if ('checkbox' === (element || {}).type) {
                                    $(element).closest('.wpforms-field-checkbox').find('label.wpforms-error').remove();
                                    validate = true;
                                }
                                if (validate) { this.element(element); }
                            },
                        };
                    }
                    form.validate(properties);
                });
            }
        },
        loadDatePicker: function() {
            if (typeof $.fn.flatpickr !== 'undefined') {
                $('.wpforms-datepicker').each(function() {
                    var element = $(this),
                        form = element.closest('.wpforms-form'),
                        formID = form.data('formid'),
                        fieldID = element.closest('.wpforms-field').data('field-id'),
                        properties;
                    if (typeof window['wpforms_' + formID + '_' + fieldID] !== 'undefined' && window['wpforms_' + formID + '_' + fieldID].hasOwnProperty('datepicker')) { properties = window['wpforms_' + formID + '_' + fieldID].datepicker; } else if (typeof window['wpforms_' + formID] !== 'undefined' && window['wpforms_' + formID].hasOwnProperty('datepicker')) { properties = window['wpforms_' + formID].datepicker; } else if (typeof wpforms_datepicker !== 'undefined') { properties = wpforms_datepicker; } else { properties = { disableMobile: true, }; }
                    if (!properties.hasOwnProperty('locale') && typeof wpforms_settings !== 'undefined' && wpforms_settings.hasOwnProperty('locale')) { properties.locale = wpforms_settings.locale; }
                    element.flatpickr(properties);
                });
            }
        },
        loadTimePicker: function() {
            if (typeof $.fn.timepicker !== 'undefined') {
                $('.wpforms-timepicker').each(function() {
                    var element = $(this),
                        form = element.closest('.wpforms-form'),
                        formID = form.data('formid'),
                        fieldID = element.closest('.wpforms-field').data('field-id'),
                        properties;
                    if (typeof window['wpforms_' + formID + '_' + fieldID] !== 'undefined' && window['wpforms_' + formID + '_' + fieldID].hasOwnProperty('timepicker')) { properties = window['wpforms_' + formID + '_' + fieldID].timepicker; } else if (typeof window['wpforms_' + formID] !== 'undefined' && window['wpforms_' + formID].hasOwnProperty('timepicker')) { properties = window['wpforms_' + formID].timepicker; } else if (typeof wpforms_timepicker !== 'undefined') { properties = wpforms_timepicker; } else { properties = { scrollDefault: 'now', forceRoundTime: true, }; }
                    element.timepicker(properties);
                });
            }
        },
        loadInputMask: function() { if (typeof $.fn.inputmask !== 'undefined') { $('.wpforms-masked-input').inputmask(); } },
        loadSmartPhoneField: function() {
            if (typeof $.fn.intlTelInput === 'undefined') { return; }
            var inputOptions = {};
            if (!wpforms_settings.gdpr) { inputOptions.geoIpLookup = WPForms.currentIpToCountry; }
            if (wpforms_settings.gdpr) {
                var lang = this.getFirstBrowserLanguage(),
                    countryCode = lang.indexOf('-') > -1 ? lang.split('-').pop() : '';
            }
            if (countryCode) {
                var countryData = window.intlTelInputGlobals.getCountryData();
                countryData = countryData.filter(function(country) { return country.iso2 === countryCode.toLowerCase(); });
                countryCode = countryData.length ? countryCode : '';
            }
            inputOptions.initialCountry = wpforms_settings.gdpr && countryCode ? countryCode : 'auto';
            $('.wpforms-smart-phone-field').each(function(i, el) {
                var $el = $(el);
                inputOptions.hiddenInput = $el.closest('.wpforms-field-phone').data('field-id');
                inputOptions.utilsScript = wpforms_settings.wpforms_plugin_url + 'pro/assets/js/vendor/jquery.intl-tel-input-utils.js';
                $el.intlTelInput(inputOptions);
                $el.removeAttr('name');
                $el.blur(function() { if ($el.intlTelInput('isValidNumber')) { $el.siblings('input[type="hidden"]').val($el.intlTelInput('getNumber')); } });
            });
        },
        loadPayments: function() {
            $('.wpforms-payment-total').each(function(index, el) { WPForms.amountTotal(this); });
            if (typeof $.fn.payment !== 'undefined') {
                $('.wpforms-field-credit-card-cardnumber').payment('formatCardNumber');
                $('.wpforms-field-credit-card-cardcvc').payment('formatCardCVC');
            }
        },
        bindUIActions: function() {
            $(document).on('click', '.wpforms-page-button', function(event) {
                event.preventDefault();
                WPForms.pagebreakNav($(this));
            });
            $(document).on('change input', '.wpforms-payment-price', function() { WPForms.amountTotal(this, true); });
            $(document).on('input', '.wpforms-payment-user-input', function() {
                var $this = $(this),
                    amount = $this.val();
                $this.val(amount.replace(/[^0-9.,]/g, ''));
            });
            $(document).on('focusout', '.wpforms-payment-user-input', function() {
                var $this = $(this),
                    amount = $this.val(),
                    sanitized = WPForms.amountSanitize(amount),
                    formatted = WPForms.amountFormat(sanitized);
                $this.val(formatted);
            });
            $(document).ready(function() {
                $('.wpforms-field-radio .wpforms-image-choices-item input:checked').change();
                $('.wpforms-field-payment-multiple .wpforms-image-choices-item input:checked').change();
                $('.wpforms-field-checkbox .wpforms-image-choices-item input').change();
                $('.wpforms-field-payment-checkbox .wpforms-image-choices-item input').change();
            });
            $('.wpforms-field-rating-item').hover(function() {
                $(this).parent().find('.wpforms-field-rating-item').removeClass('selected hover');
                $(this).prevAll().andSelf().addClass('hover');
            }, function() {
                $(this).parent().find('.wpforms-field-rating-item').removeClass('selected hover');
                $(this).parent().find('input:checked').parent().prevAll().andSelf().addClass('selected');
            });
            $(document).on('change', '.wpforms-field-rating-item input', function() {
                var $this = $(this),
                    $wrap = $this.closest('.wpforms-field-rating-items'),
                    $items = $wrap.find('.wpforms-field-rating-item');
                $items.removeClass('hover selected');
                $this.parent().prevAll().andSelf().addClass('selected');
            });
            $(document).ready(function() { $('.wpforms-field-rating-item input:checked').change(); });
            $(document).on('change', '.wpforms-field-checkbox input, .wpforms-field-radio input, .wpforms-field-payment-multiple input, .wpforms-field-payment-checkbox input, .wpforms-field-gdpr-checkbox input', function(event) {
                var $this = $(this),
                    $field = $this.closest('.wpforms-field');
                if ($field.hasClass('wpforms-conditional-hide')) { event.preventDefault(); return false; }
                switch ($this.attr('type')) {
                    case 'radio':
                        $this.closest('ul').find('li').removeClass('wpforms-selected').find('input[type=radio]').removeProp('checked');
                        $this.prop('checked', true).closest('li').addClass('wpforms-selected');
                        break;
                    case 'checkbox':
                        if ($this.prop('checked')) {
                            $this.closest('li').addClass('wpforms-selected');
                            $this.prop('checked', true);
                        } else {
                            $this.closest('li').removeClass('wpforms-selected');
                            $this.removeProp('checked');
                        }
                        break;
                }
            });
            $(document).on('change', '.wpforms-field-file-upload input', function() {
                var $this = $(this),
                    $uploads = $this.closest('form.wpforms-form').find('.wpforms-field-file-upload input'),
                    totalSize = 0,
                    postMaxSize = Number(wpforms_settings.post_max_size),
                    errorMsg = '<div class="wpforms-error-container-post_max_size">' + wpforms_settings.val_post_max_size + '</div>',
                    errorCntTpl = '<div class="wpforms-error-container">{errorMsg}</span></div>',
                    $submitCnt = $this.closest('form.wpforms-form').find('.wpforms-submit-container'),
                    $submitBtn = $submitCnt.find('button.wpforms-submit'),
                    $errorCnt = $submitCnt.prev();
                $uploads.each(function() {
                    var $upload = $(this),
                        i = 0,
                        len = $upload[0].files.length;
                    for (; i < len; i++) { totalSize += $upload[0].files[i].size; }
                });
                if (totalSize > postMaxSize) {
                    totalSize = Number((totalSize / 1048576).toFixed(3));
                    postMaxSize = Number((postMaxSize / 1048576).toFixed(3));
                    errorMsg = errorMsg.replace(/{totalSize}/, totalSize).replace(/{maxSize}/, postMaxSize);
                    if ($errorCnt.hasClass('wpforms-error-container')) {
                        $errorCnt.find('.wpforms-error-container-post_max_size').remove();
                        $errorCnt.append(errorMsg);
                    } else { $submitCnt.before(errorCntTpl.replace(/{errorMsg}/, errorMsg)); }
                    $submitBtn.prop('disabled', true);
                } else {
                    $errorCnt.find('.wpforms-error-container-post_max_size').remove();
                    $submitBtn.prop('disabled', false);
                }
            });
            $(document).on('blur', '.wpforms-field-email input', function() {
                var $t = $(this),
                    id = $t.attr('id');
                $t.mailcheck({
                    suggested: function(el, suggestion) {
                        $('#' + id + '_suggestion').remove();
                        var sugg = '<a href="#" class="mailcheck-suggestion" data-id="' + id + '" title="' + wpforms_settings.val_email_suggestion_title + '">' + suggestion.full + '</a>';
                        sugg = wpforms_settings.val_email_suggestion.replace('{suggestion}', sugg);
                        $(el).after('<label class="wpforms-error mailcheck-error" id="' + id + '_suggestion">' + sugg + '</label>');
                    },
                    empty: function() { $('#' + id + '_suggestion').remove(); },
                });
            });
            $(document).on('click', '.wpforms-field-email .mailcheck-suggestion', function(e) {
                var $t = $(this),
                    id = $t.attr('data-id');
                e.preventDefault();
                $('#' + id).val($t.text());
                $t.parent().remove();
            });
        },
        pagebreakNav: function(el) {
            var $this = $(el),
                valid = true,
                action = $this.data('action'),
                page = $this.data('page'),
                page2 = page,
                next = page + 1,
                prev = page - 1,
                formID = $this.data('formid'),
                $form = $this.closest('.wpforms-form'),
                $page = $form.find('.wpforms-page-' + page),
                $submit = $form.find('.wpforms-submit-container'),
                $indicator = $form.find('.wpforms-page-indicator'),
                $reCAPTCHA = $form.find('.wpforms-recaptcha-container'),
                pageScroll = false;
            if (false === window.wpforms_pageScroll) { pageScroll = false; } else if (!WPForms.empty(window.wpform_pageScroll)) { pageScroll = window.wpform_pageScroll; } else { pageScroll = 75; }
            if ('next' === action) {
                if (typeof $.fn.validate !== 'undefined') { $page.find('input.wpforms-field-required, select.wpforms-field-required, textarea.wpforms-field-required, .wpforms-field-required input, .wpforms-field-file-upload input').each(function(index, el) { if (!$(el).valid()) { valid = false; } }); var $topError = $page.find('.wpforms-error').first(); if ($topError.length) { $('html, body').animate({ scrollTop: $topError.offset().top - 75, }, 750, function() { $topError.focus(); }); } }
                if (valid) {
                    page2 = next;
                    $page.hide();
                    var $nextPage = $form.find('.wpforms-page-' + next);
                    $nextPage.show();
                    if ($nextPage.hasClass('last')) {
                        $reCAPTCHA.show();
                        $submit.show();
                    }
                    if (pageScroll) { $('html, body').animate({ scrollTop: $form.offset().top - pageScroll, }, 1000); }
                    $this.trigger('wpformsPageChange', [page2, $form]);
                }
            } else if ('prev' === action) {
                page2 = prev;
                $page.hide();
                $form.find('.wpforms-page-' + prev).show();
                $reCAPTCHA.hide();
                $submit.hide();
                if (pageScroll) { $('html, body').animate({ scrollTop: $form.offset().top - pageScroll, }, 1000); }
                $this.trigger('wpformsPageChange', [page2, $form]);
            }
            if ($indicator) {
                var theme = $indicator.data('indicator'),
                    color = $indicator.data('indicator-color');
                if ('connector' === theme || 'circles' === theme) {
                    $indicator.find('.wpforms-page-indicator-page').removeClass('active');
                    $indicator.find('.wpforms-page-indicator-page-' + page2).addClass('active');
                    $indicator.find('.wpforms-page-indicator-page-number').removeAttr('style');
                    $indicator.find('.active .wpforms-page-indicator-page-number').css('background-color', color);
                    if ('connector' === theme) {
                        $indicator.find('.wpforms-page-indicator-page-triangle').removeAttr('style');
                        $indicator.find('.active .wpforms-page-indicator-page-triangle').css('border-top-color', color);
                    }
                } else if ('progress' === theme) {
                    var $pageTitle = $indicator.find('.wpforms-page-indicator-page-title'),
                        $pageSep = $indicator.find('.wpforms-page-indicator-page-title-sep'),
                        totalPages = $form.find('.wpforms-page').length,
                        width = (page2 / totalPages) * 100;
                    $indicator.find('.wpforms-page-indicator-page-progress').css('width', width + '%');
                    $indicator.find('.wpforms-page-indicator-steps-current').text(page2);
                    if ($pageTitle.data('page-' + page2 + '-title')) {
                        $pageTitle.css('display', 'inline').text($pageTitle.data('page-' + page2 + '-title'));
                        $pageSep.css('display', 'inline');
                    } else {
                        $pageTitle.css('display', 'none');
                        $pageSep.css('display', 'none');
                    }
                }
            }
        },
        bindOptinMonster: function() {
            document.addEventListener('om.Campaign.load', function(event) {
                WPForms.ready();
                WPForms.optinMonsterRecaptchaReset(event.detail.Campaign.data.id);
            });
            $(document).on('OptinMonsterOnShow', function(event, data, object) {
                WPForms.ready();
                WPForms.optinMonsterRecaptchaReset(data.optin);
            });
        },
        optinMonsterRecaptchaReset: function(optinId) {
            var $form = $('#om-' + optinId).find('.wpforms-form'),
                $recaptchaContainer = $form.find('.wpforms-recaptcha-container'),
                $recaptcha = $form.find('.g-recaptcha'),
                recaptchaSiteKey = $recaptcha.attr('data-sitekey'),
                recaptchaID = 'recaptcha-' + Date.now();
            if ($form.length && $recaptcha.length) {
                $recaptcha.remove();
                $recaptchaContainer.prepend('<div class="g-recaptcha" id="' + recaptchaID + '" data-sitekey="' + recaptchaSiteKey + '"></div>');
                grecaptcha.render(recaptchaID, { sitekey: recaptchaSiteKey, callback: function() { wpformsRecaptchaCallback($('#' + recaptchaID)); }, });
            }
        },
        amountTotal: function(el, validate) {
            var validate = validate || false,
                $form = $(el).closest('.wpforms-form'),
                total = 0,
                totalFormatted = 0,
                totalFormattedSymbol = 0,
                currency = WPForms.getCurrency();
            $('.wpforms-payment-price', $form).each(function(index, el) {
                var amount = 0,
                    $this = $(this);
                if ('text' === $this.attr('type') || 'hidden' === $this.attr('type')) { amount = $this.val(); } else if (('radio' === $this.attr('type') || 'checkbox' === $this.attr('type')) && $this.is(':checked')) { amount = $this.data('amount'); } else if ($this.is('select') && $this.find('option:selected').length > 0) { amount = $this.find('option:selected').data('amount'); }
                if (!WPForms.empty(amount)) {
                    amount = WPForms.amountSanitize(amount);
                    total = Number(total) + Number(amount);
                }
            });
            totalFormatted = WPForms.amountFormat(total);
            if ('left' === currency.symbol_pos) { totalFormattedSymbol = currency.symbol + ' ' + totalFormatted; } else { totalFormattedSymbol = totalFormatted + ' ' + currency.symbol; }
            $form.find('.wpforms-payment-total').each(function(index, el) { if ('hidden' === $(this).attr('type') || 'text' === $(this).attr('type')) { $(this).val(totalFormattedSymbol); if ('text' === $(this).attr('type') && validate && $form.data('validator')) { $(this).valid(); } } else { $(this).text(totalFormattedSymbol); } });
        },
        amountSanitize: function(amount) {
            var currency = WPForms.getCurrency();
            amount = amount.toString().replace(/[^0-9.,]/g, '');
            if (',' === currency.decimal_sep && (amount.indexOf(currency.decimal_sep) !== -1)) {
                if ('.' === currency.thousands_sep && amount.indexOf(currency.thousands_sep) !== -1) { amount = amount.replace(currency.thousands_sep, ''); } else if ('' === currency.thousands_sep && amount.indexOf('.') !== -1) { amount = amount.replace('.', ''); }
                amount = amount.replace(currency.decimal_sep, '.');
            } else if (',' === currency.thousands_sep && (amount.indexOf(currency.thousands_sep) !== -1)) { amount = amount.replace(currency.thousands_sep, ''); }
            return WPForms.numberFormat(amount, 2, '.', '');
        },
        amountFormat: function(amount) {
            var currency = WPForms.getCurrency();
            amount = String(amount);
            if (',' === currency.decimal_sep && (amount.indexOf(currency.decimal_sep) !== -1)) {
                var sepFound = amount.indexOf(currency.decimal_sep),
                    whole = amount.substr(0, sepFound),
                    part = amount.substr(sepFound + 1, amount.strlen - 1);
                amount = whole + '.' + part;
            }
            if (',' === currency.thousands_sep && (amount.indexOf(currency.thousands_sep) !== -1)) { amount = amount.replace(',', ''); }
            if (WPForms.empty(amount)) { amount = 0; }
            return WPForms.numberFormat(amount, 2, currency.decimal_sep, currency.thousands_sep);
        },
        getCurrency: function() {
            var currency = { code: 'USD', thousands_sep: ',', decimal_sep: '.', symbol: '$', symbol_pos: 'left', };
            if (typeof wpforms_settings.currency_code !== 'undefined') { currency.code = wpforms_settings.currency_code; }
            if (typeof wpforms_settings.currency_thousands !== 'undefined') { currency.thousands_sep = wpforms_settings.currency_thousands; }
            if (typeof wpforms_settings.currency_decimal !== 'undefined') { currency.decimal_sep = wpforms_settings.currency_decimal; }
            if (typeof wpforms_settings.currency_symbol !== 'undefined') { currency.symbol = wpforms_settings.currency_symbol; }
            if (typeof wpforms_settings.currency_symbol_pos !== 'undefined') { currency.symbol_pos = wpforms_settings.currency_symbol_pos; }
            return currency;
        },
        numberFormat: function(number, decimals, decimalSep, thousandsSep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number;
            var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
            var sep = ('undefined' === typeof thousandsSep) ? ',' : thousandsSep;
            var dec = ('undefined' === typeof decimalSep) ? '.' : decimalSep;
            var s;
            var toFixedFix = function(n, prec) { var k = Math.pow(10, prec); return '' + (Math.round(n * k) / k).toFixed(prec); };
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) { s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep); }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        },
        empty: function(mixedVar) {
            var undef;
            var key;
            var i;
            var len;
            var emptyValues = [undef, null, false, 0, '', '0'];
            for (i = 0, len = emptyValues.length; i < len; i++) { if (mixedVar === emptyValues[i]) { return true; } }
            if ('object' === typeof mixedVar) {
                for (key in mixedVar) { if (mixedVar.hasOwnProperty(key)) { return false; } }
                return true;
            }
            return false;
        },
        setUserIndentifier: function() {
            if (((!window.hasRequiredConsent && typeof wpforms_settings !== 'undefined' && wpforms_settings.uuid_cookie) || (window.hasRequiredConsent && window.hasRequiredConsent())) && !WPForms.getCookie('_wpfuuid')) {
                var s = new Array(36),
                    hexDigits = '0123456789abcdef',
                    uuid;
                for (var i = 0; i < 36; i++) { s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1); }
                s[14] = '4';
                s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
                s[8] = s[13] = s[18] = s[23] = '-';
                uuid = s.join('');
                WPForms.createCookie('_wpfuuid', uuid, 3999);
            }
        },
        createCookie: function(name, value, days) {
            var expires = '';
            if (days) {
                if ('-1' === days) { expires = ''; } else {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = '; expires=' + date.toGMTString();
                }
            } else { expires = '; expires=Thu, 01 Jan 1970 00:00:01 GMT'; }
            document.cookie = name + '=' + value + expires + '; path=/';
        },
        getCookie: function(name) {
            var nameEQ = name + '=',
                ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (' ' === c.charAt(0)) { c = c.substring(1, c.length); }
                if (0 == c.indexOf(nameEQ)) { return c.substring(nameEQ.length, c.length); }
            }
            return null;
        },
        removeCookie: function(name) { WPForms.createCookie(name, '', -1); },
        getFirstBrowserLanguage: function() {
            var nav = window.navigator,
                browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'],
                i, language;
            if (Array.isArray(nav.languages)) { for (i = 0; i < nav.languages.length; i++) { language = nav.languages[i]; if (language && language.length) { return language; } } }
            for (i = 0; i < browserLanguagePropertyKeys.length; i++) { language = nav[browserLanguagePropertyKeys[i]]; if (language && language.length) { return language; } }
            return '';
        },
        currentIpToCountry: function(callback) {
            $.get('https://ipapi.co/jsonp', function() {}, 'jsonp').always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : '';
                if (!countryCode) {
                    var lang = WPForms.getFirstBrowserLanguage();
                    countryCode = lang.indexOf('-') > -1 ? lang.split('-').pop() : '';
                }
                callback(countryCode);
            });
        },
    };
    WPForms.init();
    window.wpforms = WPForms;
}(jQuery));