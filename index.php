<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
}
require 'providers/Database.php';

$controller = 'IndexController';

if (!isset($_REQUEST['controller'])) {
    require "controllers/" . $controller . ".php";

    $controller = ucwords($controller);
    $controller = new $controller;
    $controller->index();
} else {
    $controller = ucwords($_REQUEST['controller'] . 'Controller');
    //Condicional Ternario   condición       Si es Verdad         Si es Falso
    $method = isset($_REQUEST['method']) ? $_REQUEST['method'] : 'index';

    require "controllers/" . $controller . ".php";
    $controller = new $controller;

    //Función para llamar al controlados y ejecutar el metodo.
    call_user_func(array($controller, $method));
}