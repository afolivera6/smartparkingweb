<?php

/**
 * 
 */
class Empleado {

    private $pdo;

    public function __construct() {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll() {
        try {
            $strSql = "SELECT e.*, s.status, r.name FROM empleado e INNER JOIN statuses S ON S.id = e.status_id INNER JOIN roles r ON r.id = e.rol_id ORDER BY e.id_Empleado";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newEmpleado($data) {
        try {
            $this->pdo->insert('empleado', $data);
            return true;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getEmpleadoById($id_Empleado) {
        try {
            $strSql = "SELECT * FROM empleado WHERE id_Empleado = :id_Empleado";
            $arraydata = ['id_Empleado' => $id_Empleado];
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql, $arraydata);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editEmpleado($data) {
        try {
            $strWhere = 'id_Empleado = ' . $data['id_Empleado'];
            $this->pdo->update('empleado', $data, $strWhere);
            return true;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function deleteEmpleado($data) {
        try {
            var_dump($data);
            $strWhere = 'id = ' . $data['id'];
            $this->pdo->delete('empleado', $strWhere);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

}

?>