<?php

/**
 * Modelo de la Tabla users
 */
class Parqueadero
{

    private $id_Parqueadero;
    private $estado_Parqueadero;
    private $id_TipoParqueadero;
    private $imagenParqueadero;
    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT p.*, tp.*,se.FechaIngreso_ServicioParqueadero FROM parqueadero p LEFT JOIN servicio_parqueadero se on se.id_Parqueadero = p.id_Parqueadero INNER JOIN tipo_parqueadero tp ON p.id_tipoParqueadero = tp.id_tipoParqueadero ORDER BY p.id_Parqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll2()
    {
        try {
            $strSql = "SELECT * FROM tarifa";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll3()
    {
        try {
            $strSql = "SELECT * FROM Tipo_Servicio";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll4()
    {
        try {
            $strSql = "SELECT sp.*, t.*, ts.* FROM Servicio_parqueadero sp INNER JOIN tarifa t ON sp.id_Tarifa = t.id_tarifa INNER JOIN tipo_servicio ts ON ts.id_tipo_Servicio = sp.id_tipo_Servicio ORDER BY sp.id_ServicioParqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll5()
    {
        try {
            $strSql = "SELECT p.*, tp.* FROM parqueadero p INNER JOIN tipo_parqueadero tp ON p.id_tipoParqueadero = tp.id_tipoParqueadero ORDER BY p.id_Parqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getDateStatus($id_Parqueadero)
    {
        try {
            $strSql = "SELECT sp.FechaFinal_ServicioParqueadero FROM servicio_parqueadero sp WHERE id_Parqueadero IN (:id_Parqueadero)";
            $arrayData = ['id_Parqueadero' => $id_Parqueadero];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newParqueadero($data)
    {
        try {
            $this->pdo->insert('parqueadero', $data);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getParqueaderoById($id_Parqueadero)
    {
        try {
            $strSql = "SELECT * FROM parqueadero WHERE id_Parqueadero = :id_Parqueadero";
            $arrayData = ['id_Parqueadero' => $id_Parqueadero];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getServicioById($id_ServicioParqueadero)
    {
        try {
            $strSql = "SELECT * FROM servicio_parqueadero WHERE id_ServicioParqueadero = :id_ServicioParqueadero";
            $arrayData = ['id_ServicioParqueadero' => $id_ServicioParqueadero];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editParqueadero($data)
    {
        try {
            $strWhere = 'id_Parqueadero = ' . $data['id_Parqueadero'];
            $this->pdo->update('parqueadero', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteParqueadero($data)
    {
        try {
            $strWhere = ' id_Parqueadero = ' . $data['id_Parqueadero'];
            $this->pdo->delete('parqueadero', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
