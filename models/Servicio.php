<?php

/**
 * Modelo de la Tabla Vehiculo
 */
class Servicio
{
    private $id_ServicioParqueadero;
    private $FechaIngreso_ServicioParqueadero;
    private $placa_vehiculo;
    private $nombre_vehiculo;
    private $novedad_vehiculo;
    private $nombrePersonaVehiculo;
    private $cedulaPersonaVehiculo;
    private $telefonoPersonaVehiculo;
    private $FechaFinal_ServicioParqueadero;
    private $id_Parqueadero;
    private $id_Tarifa;
    private $id_tipo_Servicio;
    private $id_Empleado;
    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT sr.id_ServicioParqueadero,FechaIngreso_ServicioParqueadero,FechaFinal_ServicioParqueadero,placa_vehiculo,nombre_vehiculo,id_Parqueadero, e.nombre, t.nombre_tarifa FROM servicio_parqueadero sr INNER JOIN empleado e ON sr.id_ServicioParqueadero = e.id_Empleado INNER JOIN tarifa t ON t.id_tarifa ORDER BY sr.id_ServicioParqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getTarifaAll()
    {
        try {
            $strSql = "SELECT * FROM tarifa";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newServicioParqueadero($data)
    {
        try {
            // dd($data);

            $servicio = [
                'FechaIngreso_ServicioParqueadero' => $data['FechaIngreso_ServicioParqueadero'],
                'placa_vehiculo' => $data['placa_vehiculo'],
                'nombre_vehiculo' => $data['nombre_vehiculo'],
                'novedad_vehiculo' => $data['novedad_vehiculo'],
                'nombrePersonaVehiculo' => $data['nombrePersonaVehiculo'],
                'cedulaPersonaVehiculo' => $data['cedulaPersonaVehiculo'],
                'id_Tarifa' => $data['id_Tarifa'],
                'id_tipo_Servicio' => $data['id_tipo_Servicio'],
                'id_Parqueadero' => $data['id_Parqueadero'],
                'id_Empleado' => $data['id_Empleado'],
                'estado_servicio' => 1
            ];
            $this->pdo->insert('servicio_parqueadero', $servicio);
            // $Query2 = "UPDATE parqueadero  SET status_id = 1 WHERE id_Parqueadero = '$data[10]';";
            return true;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getVehiculoById($id_vehiculo)
    {
        try {
            $strSql = "SELECT * FROM servicio_parqueadero WHERE id_vehiculo = $id_vehiculo";
            $arrayData = ['id_vehiculo' => $id_vehiculo];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getAllById($id,$servicio)
    {
        try {
            $idservicio=$servicio->ultimo;
            $strSql = "SELECT * FROM servicio_parqueadero WHERE Id_Parqueadero = $id AND id_ServicioParqueadero=$idservicio";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getlastId($id)
    {
        try {
            $strSql = "SELECT MAX(id_ServicioParqueadero) as ultimo  FROM servicio_parqueadero s WHERE s.id_Parqueadero = $id";
            $query = $this->pdo->select($strSql);
            return $query[0];
            
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editServicio($data)
    {
        try {
            $strWhere = ' id_ServicioParqueadero = ' . $data['id_ServicioParqueadero'];
            $this->pdo->update('servicio_parqueadero', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function editServicioStatus($id)
    {
        try {
            $id_Parqueadero=$id['Id_Parqueadero'];
            var_dump($id_Parqueadero);
            $data['status_id']=1;
            $strWhere = ' id_Parqueadero = '.$id['id_Parqueadero'];
            $this->pdo->update('parqueadero', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function updateStatus($id)
    {
        try {
            $data['status_id']=2;
            $strWhere = ' id_Parqueadero = ' . $id;
            $this->pdo->update('parqueadero', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    
    public function deleteVehiculo($data)
    {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->delete('vehiculo', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
