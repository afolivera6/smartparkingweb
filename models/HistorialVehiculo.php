<?php

/**
 * Modelo de la Tabla Vehiculo
 */
class HistorialVehiculo {

    private $FechaIngreso_ServicioParqueadero;
    private $placa_vehiculo;
    private $nombre_vehiculo;
    private $estado_vehiculo;
    private $nombrePersonaVehiculo;
    private $cedulaPersonaVehiculo;
    private $telefonoPersonaVehiculo;
    private $FechaFinal_ServicioParqueadero;
    private $id_vehiculo;
    private $id_Parqueadero;
    private $id_Tarifa;
    private $id_ContratoAlquiler; 
    private $id_Empleado;  
    private $pdo;

    public function __construct() {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll() {
        try {
            $strSql = "SELECT * FROM servicio_parqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newVehiculo($data) {
        try {
            $this->pdo->insert('vehiculo', $data);
            return true;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getVehiculoById($id_vehiculo) {
        try {
            $strSql = "SELECT * FROM vehiculo WHERE id_vehiculo = :id_vehiculo";
            $arrayData = ['id_vehiculo' => $id_vehiculo];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editVehiculo($data) {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->update('vehiculo', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteVehiculo($data) {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->delete('vehiculo', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

}
