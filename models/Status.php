<?php
	 
	/**
	 * Modelo de la Tabla Movies
	 */
	class Status
	{
		private $id;
		private $status;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT s. *, tystatus.name as nametysta FROM statuses s
				INNER JOIN type_status tystatus ON tystatus.id = s.type_status_id
				ORDER BY s.id";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function newStatus($data)
		{
			try {
				$this->pdo->insert('statuses', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getStatusById($id)
		{
			try {
				$strSql = "SELECT * FROM statuses WHERE id = :id";
				$arrayData = ['id' => $id];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editStatus($data)
		{
			try {
				$strWhere = 'id = '. $data['id'];
				$this->pdo->update('statuses', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteStatus($data)
		{
			try {
				$strWhere = 'id = '. $data['id'];
				$this->pdo->delete('statuses', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
	} 