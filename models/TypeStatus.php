<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class TypeStatus
	{
		private $id;
		private $name;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM type_status";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		} 

		public function newTypeStatus($data)
		{
			try {
				$this->pdo->insert('type_status', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}	

		public function getTypeStatusById($id)
		{
			try {
				$strSql = "SELECT * FROM type_status WHERE id = :id";
				$arrayData = ['id' => $id];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editTypeStatus($data)
		{
			try {
				$strWhere = 'id = '. $data['id'];
				$this->pdo->update('type_status', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		// public function deleteUser($data)
		// {
		// 	try {
		// 		$strWhere = 'id = '. $data['id'];
		// 		$this->pdo->delete('users', $strWhere);
		// 	} catch(PDOException $e) {
		// 		die($e->getMessage());
		// 	}	
		// }
	}