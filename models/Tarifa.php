<?php

/**
 * 
 */
class Tarifa
{
	
	private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM tarifa";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function newTarifa($data)
		{
			try {
				$this->pdo->insert('tarifa', $data);
				return true;				
			} catch(PDOException $e) {
				return $e->getMessage();
			}	
		}

		public function getTarifaById($id_tarifa)
		{
			try {
				$strSql = "SELECT * FROM tarifa WHERE id_tarifa = :id_tarifa";
				$arraydata  = ['id_tarifa'=>$id_tarifa];   
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql,$arraydata);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function editTarifa($data)
		{
			try {
				$strWhere = 'id_tarifa = '.$data['id_tarifa'];
				$this->pdo->update('tarifa', $data,$strWhere);
				return true;				
			} catch(PDOException $e) {
				return $e->getMessage();
			}	
		}

		public function deleteTarifa($data) {
        try {
            $strWhere = ' id_tarifa = ' . $data['id_tarifa'];
            $this->pdo->delete('tarifa', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}		

?>