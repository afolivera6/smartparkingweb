<?php

/**
 * 
 */
class Cliente {

    private $pdo;

    public function __construct() {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll() {
        try {
            $strSql = "SELECT * FROM cliente";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newCliente($data) {
        try {
            $this->pdo->insert('cliente', $data);
            return true;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getClienteById($id_cliente) {
        try {
            $strSql = "SELECT * FROM cliente WHERE id_cliente = :id_cliente";
            $arraydata = ['id_cliente' => $id_cliente];
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql, $arraydata);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editCliente($data) {
        try {
            $strWhere = 'id_cliente = ' . $data['id_cliente'];
            $this->pdo->update('cliente', $data, $strWhere);
            return true;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function deleteCliente($data) {
        try {
            var_dump($data);
            $strWhere = 'id_cliente = ' . $data['id_cliente'];
            $this->pdo->delete('cliente', $strWhere);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

}

?>