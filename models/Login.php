<?php

/**
 * Modelo Login
 */
class Login {

    private $pdo;

    public function __construct() {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function validateUser($data) {
        try {
            $strSql = "SELECT u.*, s.status as status, r.name as rol FROM users u 
				INNER JOIN statuses s ON s.id = u.status_id
				INNER JOIN roles r ON r.id = u.rol_id
				WHERE u.email= '{$data['email']}' AND u.password = '{$data['password']}'";

            $query = $this->pdo->select($strSql);

            if (isset($query[0]->id)) {
                if ($query[0]->status_id == 1) {

                    $_SESSION['user'] = $query[0];
                    return true;
                } else {
                    return 'Error al iniciar sesion, su usuario esta inactivo';
                }
            } else {
                return 'Error al iniciar sesion, verique sus credenciales';
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

}
