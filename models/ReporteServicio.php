<?php

/**
 * Modelo de la Tabla Reporte
 */
class ReporteServicio {

    private $FechaIngreso_ServicioParqueadero;
    private $placa_vehiculo;
    private $nombre_vehiculo;
    private $estado_vehiculo;
    private $nombrePersonaVehiculo;
    private $cedulaPersonaVehiculo;
    private $telefonoPersonaVehiculo;
    private $FechaFinal_ServicioParqueadero;
    private $id_vehiculo;
    private $id_Parqueadero;
    private $id_Tarifa;
    private $id_ContratoAlquiler; 
    private $id_Empleado;  
    private $pdo;

    public function __construct() {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll() {
        try {
            $strSql = "SELECT sr.*, e.nombre, t.nombre_tarifa FROM servicio_parqueadero sr INNER JOIN empleado e ON e.id_Empleado = sr.id_Empleado INNER JOIN tarifa t ON t.id_tarifa = sr.id_Tarifa ORDER BY sr.id_ServicioParqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
/**
    public function getTarifaAll() {
        try {
            $strSql = "SELECT * FROM tarifa";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newVehiculo($data) {
        try {
            $this->pdo->insert('servicio_parqueadero', $data);
            return true;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getVehiculoById($id_vehiculo) {
        try {
            $strSql = "SELECT * FROM servicio_parqueadero WHERE id_vehiculo = :id_vehiculo";
            $arrayData = ['id_vehiculo' => $id_vehiculo];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editVehiculo($data) {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->update('vehiculo', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteVehiculo($data) {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->delete('vehiculo', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    */
}
