<?php

/**
 * Modelo de la Tabla Reporte
 */
class ReporteAlquiler {

    private $FechaIngreso_ServicioParqueadero;
    private $placa_vehiculo;
    private $nombre_vehiculo;
    private $estado_vehiculo;
    private $nombrePersonaVehiculo;
    private $cedulaPersonaVehiculo;
    private $telefonoPersonaVehiculo;
    private $FechaFinal_ServicioParqueadero;
    private $id_vehiculo;
    private $id_Parqueadero;
    private $id_Tarifa;
    private $id_ContratoAlquiler; 
    private $id_Empleado;  
    private $pdo;

    public function __construct() {
        try {
            $this->pdo = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll() {
        try {
            $strSql = "SELECT sp.id_ServicioParqueadero,FechaIngreso_ServicioParqueadero,FechaFinal_ServicioParqueadero,placa_vehiculo,nombre_vehiculo, t.nombre_tarifa, ts.nombre_tipo_servicio FROM servicio_parqueadero sp INNER JOIN tarifa t ON t.id_tarifa = sp.id_Tarifa INNER JOIN tipo_servicio ts ON ts.id_tipo_Servicio = sp.id_tipo_Servicio ORDER BY sp.id_ServicioParqueadero";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
/**
    public function getTarifaAll() {
        try {
            $strSql = "SELECT * FROM tarifa";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newVehiculo($data) {
        try {
            $this->pdo->insert('servicio_parqueadero', $data);
            return true;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getVehiculoById($id_vehiculo) {
        try {
            $strSql = "SELECT * FROM servicio_parqueadero WHERE id_vehiculo = :id_vehiculo";
            $arrayData = ['id_vehiculo' => $id_vehiculo];
            $query = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function editVehiculo($data) {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->update('vehiculo', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteVehiculo($data) {
        try {
            $strWhere = ' id_vehiculo = ' . $data['id_vehiculo'];
            $this->pdo->delete('vehiculo', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    */
}
